<?php
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();


$rubrika = $_GET['rubrika'];
$page = $_GET['page'];
$mode = $_GET['mode'];
$usedids = unserialize($_SESSION['usedIdsPosts']);
$useelastic = 0;
if (!is_null($_SESSION['useElasticBlock'])){
    $useelastic = $_SESSION['useElasticBlock'];
}

//if ($mode == 'getsimpleblogs'){
//    $getsimpleblogs = 1;
//}
//else{
//    $getsimpleblogs = 0;
//}

$items = $blogClass->getPosts($rubrika, $page, $usedids, $useelastic, true, $getsimpleblogs);
//var_dump($items);
if (!is_null($items)){
    $Widget->smarty->assign('items', $items['posts']);
    $Widget->smarty->assign('rubrika', $rubrika);
    $Widget->smarty->assign('page', $items['page']);
    $Widget->smarty->assign('getsimpleblogs', $items['getsimpleblogs']);
    $Widget->smarty->assign('usedids', $items['usedIdsPosts']);

    $result = $Widget->smarty->fetch('ajaxBlogs.tpl');
//    header("Content-type: application/json; charset=UTF-8");
    header("Cache-Control: must-revalidate");
    header("Pragma: no-cache");
    header("Expires: -1");

    print $items;
}
else{
    print "";
}

?>