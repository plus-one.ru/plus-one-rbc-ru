<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$page = $_GET['pagenum'];
$authorId = $_GET['writer_id'];
$usedids = unserialize($_SESSION['usedIdsPosts']);
$useelastic = 1;////$_SESSION['useElasticBlock'];

$result = $blogClass->getAuthorsPosts($authorId, $page, $usedBlock, $useElasticBlock);

header("Content-type: application/jsonp; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

print json_encode($result);
?>