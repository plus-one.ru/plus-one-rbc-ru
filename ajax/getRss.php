<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

define('DATE_FORMAT_RFC822','r');

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$feed = $blogClass->getRssFeed();

// Дата последней сборки фида
$lastBuildDate = date(DATE_FORMAT_RFC822);

$strRss = <<<END
<?xml version="1.0" encoding="utf-8"?>
    <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>РБК+1</title>
        <description>Раздел проекта «+1» о социальной и экологической ответственности на РБК</description>
        <language>ru-ru</language>
        <link><![CDATA[http://plus-one.rbc.ru]]></link>
        <copyright>РБК+1</copyright>
        <image><url><![CDATA[http://plus-one.rbc.ru/design/rbk_v2/images/logo.png]]></url>
            <title>РБК+1</title>
            <link><![CDATA[http://plus-one.rbc.ru]]></link>
        </image>
        <atom:link href="http://plus-one.rbc.ru/rss/feed" rel="self" type="application/rss+xml"></atom:link>

END;


    foreach($feed AS $post){
        // Убираем из тайтла html теги и лишние пробелы
        $title = strip_tags(trim($post->header_rss));
        // С аноносом можно не проводить такие
        // манипуляции, т.к. мы вставим его в блок CDATA
        $description = htmlspecialchars(
            strip_tags(
                trim($post->header)
            )
        );

        $url = $post->url;
        $tagUrl = $post->tag_url;
        $pubDate = date(DATE_FORMAT_RFC822, strtotime($post->post_created));
        if ($post->image_rss){
            $imageFileSize = filesize($_SERVER['DOCUMENT_ROOT'] . "/files/blogposts/" . $post->image_rss);
        }
        else{
            $imageFileSize = 0;
        }


        $strRss .= <<<END
        <item>
            <title>$title</title>
            <link>http://plus-one.rbc.ru/blog/$tagUrl/$url</link>
            <guid>http://plus-one.rbc.ru/blog/$tagUrl/$url</guid>
            <pubDate>$pubDate</pubDate>
            <description>$description</description>
            <enclosure url="http://plus-one.rbc.ru/files/blogposts/$post->image_rss" length="$imageFileSize" type="image/jpeg"></enclosure>
            <category>$post->tag</category>
        </item>

END;

    }


$strRss .= <<<END
</channel>
</rss>
END;

header("Content-type: text/xml; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

echo $strRss;
?>