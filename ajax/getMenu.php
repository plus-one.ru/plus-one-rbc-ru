<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$rubrikaUrl = $_POST['rubrikaUrl'];
$typeUrl = $_POST['typeUrl'];

$headerMenu = $blogClass->getHeaderMenu($rubrikaUrl, $typeUrl);

$Widget->smarty->assign('headerMenu', $headerMenu['menu']);
$Widget->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
$Widget->smarty->assign('typeUrl', $headerMenu['currentType']);

$result = $Widget->smarty->fetch('include/menu.tpl');

header("Content-type: text/html; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

print $result;
?>