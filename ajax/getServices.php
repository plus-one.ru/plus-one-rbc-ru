<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();
//
//
//$block = str_replace("-", "/", $_GET['block']);
//$start = $_GET['start'];
//$limit = explode("/", $block);
//
//$page = $_GET['pagenum'];
//$rubrika = $_GET['rubrika'];
//$type = $_GET['type'];
//$usedBlock = unserialize($_GET['used_block']);
//
//
//$result = $blogClass->getPosts($rubrika, $page, $usedBlock);

$pageName = $_GET['page'];
$type = $_GET['type'];

$metaData = $blogClass->getMetaData($pageName, $type);

// дата для шапки и день недели
$date = date('d.m.Y',time());
$day = date('w',time());
switch ($day){
    case 0: $d='воскресенье'; break;
    case 1: $d='понедельник'; break;
    case 2: $d='вторник'; break;
    case 3: $d='среда'; break;
    case 4: $d='четверг'; break;
    case 5: $d='пятница'; break;
    case 6: $d='суббота'; break;
}

$ndate = explode(".", $date);
switch ($ndate[1]){
    case 1: $m='января'; break;
    case 2: $m='февраля'; break;
    case 3: $m='марта'; break;
    case 4: $m='апреля'; break;
    case 5: $m='мая'; break;
    case 6: $m='июня'; break;
    case 7: $m='июля'; break;
    case 8: $m='августа'; break;
    case 9: $m='сентября'; break;
    case 10: $m='октября'; break;
    case 11: $m='ноября'; break;
    case 12: $m='декабря'; break;
}


$dateToMainPage = $ndate[0] . " " . $m . ", " . $d;

$headerMenu = $blogClass->getHeaderMenu();
$conference = $blogClass->getConferenceLink();


//echo "<pre>";
//print_r($headerMenu);
//echo "</pre>";
//die();

$result = array(
    'dateToMainPage' => $dateToMainPage,
    'metaData' => $metaData,
    'headerMenu' => $headerMenu,
    'conferenceUrl' => $conference->external_url,
);

header("Content-type: application/jsonp; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

print json_encode($result);
?>