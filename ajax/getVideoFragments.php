<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$videoId = $_POST['videoId'];

$result = $blogClass->getVideoFragments($videoId);
//var_dump($result);die();
$Widget->smarty->assign('videoFragmentsMainVideo', $result->video);
$Widget->smarty->assign('videoFragments', $result->videoFragments);

$result = $Widget->smarty->fetch('include/videoFragments.tpl');

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);
?>