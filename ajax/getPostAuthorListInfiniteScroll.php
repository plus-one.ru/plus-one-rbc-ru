<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

if (intval($_GET['page']) == 1 || is_null($_GET['page'])){
    unset($_SESSION['usedIdsPosts']);
    unset($_SESSION['useElasticBlock']);
}

$writerId = $_GET['writer_id'];
$page = $_GET['page'];
$usedids = unserialize($_SESSION['usedIdsPosts']);
$useelastic = 0;
if (!is_null($_SESSION['useElasticBlock'])){
    $useelastic = $_SESSION['useElasticBlock'];
}

$items = $blogClass->getAuthorsPosts($writerId, $page, $usedids, $useelastic);

$page = intval($page) + 1;

$Widget->smarty->assign('items', $items['posts']);
$Widget->smarty->assign('writerId', $writerId);
$Widget->smarty->assign('page', $page);

$result = $Widget->smarty->fetch('ajaxBlogs.tpl');

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

print $result;
?>