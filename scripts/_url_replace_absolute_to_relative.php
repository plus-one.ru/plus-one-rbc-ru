<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

ini_set('memory_limit', '512M');
require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
}
$db->query('SET NAMES utf8;');

$tablename = 'blogposts';
$localeFile = 'replaceUrlAbsoluteToRelative.csv';

$db->query("CREATE TABLE IF NOT EXISTS blogposts_logs ( id INT(11) AUTO_INCREMENT PRIMARY KEY, post_id INT, field VARCHAR(255), old_value LONGBLOB , new_value LONGBLOB ) CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci");
$db->query("SELECT id, url, tags, body, header, lead FROM blogposts");
$postList = $db->results();
$localeFilePath = __DIR__ .DIRECTORY_SEPARATOR. $localeFile;

function replace_path($fieldList, $postId, $tablename, $postUrl, Database $db,   $HOST_REPLACE)
{
    $fL = str_replace($HOST_REPLACE, '', $fieldList);
    $fileWrite = array();
    $fieldsReplace = array();
    $changedText = false;
    foreach ($fL as $field => $value) {
        if ($fL[$field] !== $fieldList[$field]) {
            $changedText = true;
            $fieldsReplace[] = sql_placeholder("$field = ?", $value);
            $fileWrite[] = array($postId, $field, $fieldList[$field], $fL[$field]);
        }
    }

    if ($changedText) {
        $fieldsReplace = join(', ', $fieldsReplace);
        $db->query("UPDATE {$tablename} SET {$fieldsReplace}  WHERE id = {$postId};");
        foreach ($fileWrite as $f => $row) {
            $db->query(sql_placeholder("INSERT INTO blogposts_logs (post_id, field, old_value, new_value) VALUE(?,?,?,?)", $row[0], $row[1], $row[2], $row[3]));
        }
    }
    return $changedText;
}

$full = count($postList);
$countChanged = 0;
$round = 1;
$countPerRound = round($full/100);
$oneRoundPercent = 1;
$HOST_REPLACE = $config->protocol.$config->host;
foreach ($postList as $i => $post) {
    $fieldList = [
        'body' => $post->body,
        'header' => $post->header,
        'lead' => $post->lead,
    ];
    $tags = [
        '',
        'economy/',
        'ecology/',
        'society/'
    ];
    $postUrl = $config->protocol.$config->host. '/' . $tags[$post->tags] . $post->url;
    if (replace_path($fieldList, $post->id, $tablename, $postUrl, $db,  $HOST_REPLACE)) {
        $countChanged++;
        $i++;
        if ($i == $countPerRound * $round) {
            echo "\r" . $oneRoundPercent * $round . '%';
            $round++;
        }
    }
}
echo PHP_EOL;
