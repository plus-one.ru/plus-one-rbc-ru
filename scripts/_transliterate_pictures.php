<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '512M');
setlocale(LC_ALL, 'ru_RU.utf8');
require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');


$configDB = new Config();
$db = new Database($configDB->dbname,$configDB->dbhost, $configDB->dbuser,$configDB->dbpass);
$db->query('SET NAMES utf8');


if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
}

function replace_path($fieldList, $postId, $newPath, $oldPath, Database $db)
{
    $fL = str_replace($oldPath, $newPath, $fieldList);
    $fileWrite = array();
    $fieldsReplace = array();
    $changedText = false;
    foreach ($fL as $field => $value) {
        if ($fL[$field] !== $fieldList[$field]) {
            $changedText = true;
            $fieldsReplace[] = sql_placeholder("$field = ?", $value);
            $fileWrite[] = array($postId, $field, $fieldList[$field], $fL[$field]);
        }
    }

    if ($changedText) {
        $fieldsReplace = join(', ', $fieldsReplace);
        $db->query("UPDATE blogposts SET {$fieldsReplace}  WHERE id = {$postId};");
        foreach ($fileWrite as $f => $row) {
            $db->query(sql_placeholder("INSERT INTO blogposts_logs (post_id, field, old_value, new_value) VALUE(?,?,?,?)", $row[0], $row[1], $row[2], $row[3]));
        }
    }
    return $changedText;
}

function __replaceInPostsDB($oldPath, $newPath, Database $db) {
    $db->query("SELECT id, url, tags, body, header, lead FROM blogposts");
    $postList = $db->results();
    $round = 1;
    foreach ($postList as $i => $post) {
        $fieldList = [
            'body' => $post->body,
            'header' => $post->header,
            'lead' => $post->lead,
        ];
        replace_path($fieldList, $post->id, $newPath, $oldPath, $db);
    }
    echo $round;
    echo PHP_EOL;
}

chdir('../adminv2/filemanager/');

$config = include 'config/config.php';
$_SESSION['RF']["verify"] = "RESPONSIVEfilemanager";
include 'include/utils.php';
$class_ext = '';
$folders = ['image/', 'images/'];
$subdir = isset($argv[1]) ? $argv[1] : 'image/';
if (!in_array($subdir, $folders)) {
    die("Параметры не допустимы");
}

$rfm_subfolder = "";
$src = '';
$ftp = false;
$files = scandir($config['current_path'] . $rfm_subfolder . $subdir);
$n_files = count($files);

$available_languages = include 'lang/languages.php';

list($preferred_language) = array_values(array_filter(array(
    isset($_GET['lang']) ? $_GET['lang'] : null,
    isset($_SESSION['RF']['language']) ? $_SESSION['RF']['language'] : null,
    $config['default_language']
)));

if (array_key_exists($preferred_language, $available_languages)) {
    $_SESSION['RF']['language'] = $preferred_language;
} else {
    $_SESSION['RF']['language'] = $config['default_language'];
}

//filter
$filter = "";

$cur_dir = $config['upload_dir'] . $rfm_subfolder . $subdir;
$cur_dir_thumb = $config['thumbs_upload_dir'] . $rfm_subfolder . $subdir;
$thumbs_path = $config['thumbs_base_path'] . $rfm_subfolder . $subdir;
$parent
    = $rfm_subfolder . $subdir;

//php sorting
$sorted = array();
//$current_folder=array();
//$prev_folder=array();
$current_files_number = 0;
$current_folders_number = 0;

foreach ($files as $k => $file) {
    if ($ftp) {
        $date = strtotime($file['day'] . " " . $file['month'] . " " . date('Y') . " " . $file['time']);
        $size = $file['size'];
        if ($file['type'] == 'file') {
            $current_files_number++;
            $file_ext = substr(strrchr($file['name'], '.'), 1);
            $is_dir = false;
        } else {
            $current_folders_number++;
            $file_ext = trans('Type_dir');
            $is_dir = true;
        }
        $sorted[$k] = array(
            'is_dir' => $is_dir,
            'file' => $file['name'],
            'file_lcase' => strtolower($file['name']),
            'date' => $date,
            'size' => $size,
            'permissions' => $file['permissions'],
            'extension' => fix_strtolower($file_ext)
        );
    } else {


        if ($file != "." && $file != "..") {
            if (is_dir($config['current_path'] . $rfm_subfolder . $subdir . $file)) {
                $date = filemtime($config['current_path'] . $rfm_subfolder . $subdir . $file);
                $current_folders_number++;
                if ($config['show_folder_size']) {
                    list($size, $nfiles, $nfolders) = folder_info($config['current_path'] . $rfm_subfolder . $subdir . $file, false);
                } else {
                    $size = 0;
                }
                $file_ext = trans('Type_dir');
                $sorted[$k] = array(
                    'is_dir' => true,
                    'file' => $file,
                    'file_lcase' => strtolower($file),
                    'date' => $date,
                    'size' => $size,
                    'permissions' => '',
                    'extension' => fix_strtolower($file_ext)
                );

                if ($config['show_folder_size']) {
                    $sorted[$k]['nfiles'] = $nfiles;
                    $sorted[$k]['nfolders'] = $nfolders;
                }
            } else {
                $current_files_number++;
                $file_path = $config['current_path'] . $rfm_subfolder . $subdir . $file;
                $date = filemtime($file_path);
                $size = filesize($file_path);
                $file_ext = substr(strrchr($file, '.'), 1);
                $sorted[$k] = array(
                    'is_dir' => false,
                    'file' => $file,
                    'file_lcase' => strtolower($file),
                    'date' => $date,
                    'size' => $size,
                    'permissions' => '',
                    'extension' => strtolower($file_ext)
                );
            }
        }
    }
}


if ($subdir != "") {
    $sorted = array_merge(array(array('file' => '..')), $sorted);
}

$files = $sorted;

foreach ($files as $file_array) {
    $file = $file_array['file'];
    if ($file == '.' || (substr($file, 0, 1) == '.' && isset($file_array['extension']) && $file_array['extension'] == fix_strtolower(trans('Type_dir')))
        || (isset($file_array['extension']) && $file_array['extension'] != fix_strtolower(trans('Type_dir')))
        || ($file == '..' && $subdir == '')
        || in_array($file, $config['hidden_folders'])
        || ($filter != '' && $n_files > $config['file_number_limit_js'] && $file != ".." && stripos($file, $filter) === false)
    ) {
        continue;
    }

    //add in thumbs folder if not exist
    if ($file != '..') {
        if (!$ftp && !file_exists($thumbs_path . $file)) {
            create_folder(false, $thumbs_path . $file, $ftp, $config);
        }
    }

    $class_ext = 3;
    if ($file == '..' && trim($subdir) != '') {
        $src = explode("/", $subdir);
        unset($src[count($src) - 2]);
        $src = implode("/", $src);
        if ($src == '') $src = "/";
    } elseif ($file != '..') {
        $src = $subdir . $file . "/";
    }

}
$files_prevent_duplicate = array();
foreach ($files as $nu => $file_array) {
    $file = $file_array['file'];
    if ($file == '.' || $file == '..' || $file_array['extension'] == fix_strtolower(trans('Type_dir'))
        || !check_extension($file_array['extension'], $config)
        || ($filter != '' && $n_files > $config['file_number_limit_js'] && stripos($file, $filter) === false))
        continue;
    foreach ($config['hidden_files'] as $hidden_file) {
        if (fnmatch($hidden_file, $file, FNM_PATHNAME)) {
            continue 2;
        }
    }
    $filename = substr($file, 0, '-' . (strlen($file_array['extension']) + 1));
    if (strlen($file_array['extension']) === 0) {
        $filename = $file;
    }
    $file_path = $config['current_path'] . $rfm_subfolder . $subdir . $file;
    $oldPath = $config['upload_dir'] . $rfm_subfolder . $subdir . $file;
    $newPath = $oldPath;
    //check if file have illegal caracter
    if ($file != fix_filename($file, $config)) {
        $file1 = fix_filename($file, $config);
        $file_path1 = ($config['current_path'] . $rfm_subfolder . $subdir . $file1);
        if (file_exists($file_path1)) {
            $i = 1;
            $info = pathinfo($file1);
            while (file_exists($config['current_path'] . $rfm_subfolder . $subdir . $info['filename'] . ".[" . $i . "]." . $info['extension'])) {
                $i++;
            }
            $file1 = $info['filename'] . ".[" . $i . "]." . $info['extension'];
        }
        $file_path1 = ($config['current_path'] . $rfm_subfolder . $subdir . $file1);
        $newPath = ($config['upload_dir'] . $rfm_subfolder . $subdir . $file1);

        $filename = substr($file1, 0, '-' . (strlen($file_array['extension']) + 1));
        if (strlen($file_array['extension']) === 0) {
            $filename = $file1;
        }
        rename_file($file_path, fix_filename($filename, $config), $ftp, $config);
        $file = $file1;
        $file_array['extension'] = fix_filename($file_array['extension'], $config);
        $file_path = $file_path1;
    }

    if ($newPath !== $oldPath) {
        __replaceInPostsDB($oldPath, $newPath, $db);
    }

    $is_img = false;
    $is_video = false;
    $is_audio = false;
    $show_original = false;
    $show_original_mini = false;
    $mini_src = "";
    $src_thumb = "";
    if (in_array($file_array['extension'], $config['ext_img'])) {
        $src = $file_path;
        $is_img = true;

        $img_width = $img_height = "";

        $creation_thumb_path = $mini_src = $src_thumb = $thumbs_path . $file;

        if (!file_exists($src_thumb)) {
            if (!create_img($file_path, $creation_thumb_path, 122, 91, 'crop', $config)) {
                $src_thumb = $mini_src = "";
            }
        }
        //check if is smaller than thumb
        list($img_width, $img_height, $img_type, $attr) = @getimagesize($file_path);
        if ($img_width < 122 && $img_height < 91) {
            $src_thumb = $file_path;
            $show_original = true;
        }

        if ($img_width < 45 && $img_height < 38) {
            $mini_src = $config['current_path'] . $rfm_subfolder . $subdir . $file;
            $show_original_mini = true;
        }
    }
    $is_icon_thumb = false;
    $is_icon_thumb_mini = false;
    $no_thumb = false;
    if ($src_thumb == "") {
        $no_thumb = true;
        if (file_exists('img/' . $config['icon_theme'] . '/' . $file_array['extension'] . ".jpg")) {
            $src_thumb = 'img/' . $config['icon_theme'] . '/' . $file_array['extension'] . ".jpg";
        } else {
            $src_thumb = "img/" . $config['icon_theme'] . "/default.jpg";
        }
        $is_icon_thumb = true;
    }
    if ($mini_src == "") {
        $is_icon_thumb_mini = false;
    }

    $class_ext = 0;
    if (in_array($file_array['extension'], $config['ext_video'])) {
        $class_ext = 4;
        $is_video = true;
    } elseif (in_array($file_array['extension'], $config['ext_img'])) {
        $class_ext = 2;
    } elseif (in_array($file_array['extension'], $config['ext_music'])) {
        $class_ext = 5;
        $is_audio = true;
    } elseif (in_array($file_array['extension'], $config['ext_misc'])) {
        $class_ext = 3;
    } else {
        $class_ext = 1;
    }
}



