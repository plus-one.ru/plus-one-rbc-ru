<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '512M');
require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');

$config = new Config();
$db = new Database($config->dbname,$config->dbhost,
    $config->dbuser,$config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
}
$db->query('SET NAMES utf8');

$tablename = 'blogposts';
$localeFile = '/thumbnail_added.csv';

$db->query("SELECT id, image_rss, image_1_1, type_post FROM blogposts WHERE thumbnail IS NULL");
$postList = $db->results();
$localeFilePath = __DIR__ . $localeFile;
$fp = fopen($localeFilePath, 'w');
fputcsv($fp, array('post id', 'result'));

function resize_image($file, $widthFinal, $heightFinal) {
    $imgInfo = getimagesize($file);
    $widthOrigin = $imgInfo[0];
    $heightOrigin = $imgInfo[1];
    switch ($imgInfo['mime']) {
        case 'image/png':
            $src = imagecreatefrompng($file);
            break;
        case 'image/gif':
            $src = imagecreatefromgif($file);
            break;
        default:
            $src = imagecreatefromjpeg($file);
    }
    $dst = imagecreatetruecolor($widthFinal, $heightFinal);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $widthFinal, $heightFinal, $widthOrigin, $heightOrigin);
    return $dst;
}

function getThumbnail($sourceImgPath)
{
    $thumbnailMaxSideLength = 80;
    list($widthOrigin, $heightOrigin) = getimagesize($sourceImgPath);
    if ($widthOrigin >= $heightOrigin) {
        $k = $widthOrigin / $heightOrigin;
        $width = $thumbnailMaxSideLength;
        $height = $thumbnailMaxSideLength / $k;
    } else {
        $k = $heightOrigin / $widthOrigin;
        $height = $thumbnailMaxSideLength;
        $width = $thumbnailMaxSideLength / $k;
    }
    $imgSrc = resize_image($sourceImgPath, $width, $height);
    return $imgSrc;
}

function addThumbnail($postId, $sourceImg, $tablename, Database $db, $fp)
{
    if (!empty($sourceImg)) {
        $uploaddir = __DIR__.'/../files/blogposts/';
        if (!file_exists($uploaddir . $sourceImg)) {
            $result = 'rss изображения нет на диске';
        } else {
            $thumbnail = $postId . '-thumbnail.jpg';
            $thumbnailSrc = getThumbnail($uploaddir . $sourceImg);
            if (!imagejpeg($thumbnailSrc, $uploaddir . $thumbnail)) {
                $result = 'Ошибка при сохранении файла превью';
            } else {
                $db->query("UPDATE {$tablename} SET thumbnail='$thumbnail' WHERE id={$postId}");
                $result = 'thumbnail создан';
            }
        }
        fputcsv($fp, array($postId, $result));
    }
}

$full = count($postList);
$round = 1;
$countPerRound = round($full/100);
$oneRoundPercent = 1;
foreach ($postList as $i => $post) {
    if ($post->type_post == 8) {
        addThumbnail($post->id, $post->image_1_1, $tablename, $db, $fp);
    } else {
        addThumbnail($post->id, $post->image_rss, $tablename, $db, $fp);
    }
    $i ++;
    if ($i == $countPerRound * $round) {
        echo "\r" . $oneRoundPercent * $round . '%';
        $round++;
    }
}
echo PHP_EOL;
fclose($fp);
