FROM php:5.6-apache

RUN echo "display_errors = On" >> /usr/local/etc/php/conf.d/errors.ini \
  && echo "error_reporting = E_ERROR | E_PARSE" >> /usr/local/etc/php/conf.d/errors.ini \
  && echo "date.timezone = Europe/Moscow" >> /usr/local/etc/php/conf.d/timezone.ini \
  && echo "post_max_size = 128M" >> /usr/local/etc/php/conf.d/upload.ini \
  && echo "upload_max_filesize = 128M" >> /usr/local/etc/php/conf.d/upload.ini \
  && echo "session.gc_maxlifetime = 604800" >> /usr/local/etc/php/conf.d/session.ini

RUN apt-get update -y \
  && apt-get install -y \
    less vim zip unzip \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libxpm-dev \
    libfreetype6-dev \
    zlib1g-dev \
    --no-install-recommends \
  && apt-get clean autoclean \
  && apt-get autoremove -y \
  && rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN a2enmod rewrite headers \
  && docker-php-source extract \
  && docker-php-ext-configure gd \
    --with-gd \
    --with-jpeg-dir \
    --with-png-dir \
    --with-zlib-dir \
    --with-xpm-dir \
    --with-freetype-dir \
    --enable-gd-native-ttf \
  && docker-php-ext-install gd \
  && docker-php-ext-install mysqli \
  && docker-php-source delete

RUN chown -R www-data:www-data /var/www/html
