<?php

/**
 * Simpla CMS
 *
 * @copyright 	2009 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Этот класс является базовым для всех классов.
 * В нем происходит подключение к бд, и другие важные действия
 *
 */

require_once('Config.class.php');
require_once('Database.class.php');
require_once('placeholder.php');
require_once('detect_browser.php');
require_once('Smarty/libs/Smarty.class.php');


class Widget
{
    var $parent; // Родительский контейнер
	var $params = array(); // get-параметры
    var $title = null; // meta title
    var $description = null; // meta description
    var $descriptionDefault = "Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить."; // meta-description по умолчанию
    var $keywords = null; // metakeywords
    var $body = null; // содержимое блока

    var $db; // база данных
	var $smarty; // смарти
	var $config; // конфиг (из конфиг-класса)
    var $settings; // параметры сайта (из таблицы settings)
    var $user; // пользователь, если залогинен
    var $currency; // текущая валюта
    var $gd_loaded = false; // подключена ли графическая библиотека
    var $mobile_user = false; // подключена ли графическая библиотека

    var $ogImageDefault = 'design/rbk_v3/img/og-rbc.png'; // Картинка по умолчаниб
	var $ogImage = null;
	var $ogImage_1_1 = null;
	var $ogImage_1_1_c = null;
	var $ogImage_1_2 = null;
	var $ogImage_1_3 = null;
	var $ogImage_galeries = null;
	var $ogSiteName = null;
	var $ogTitle = null;
	var $ogUrl = null;
	var $withUrlParams = false;

	/**
	 *
	 * Конструктор
	 *
	 */
    function Widget(&$parent = null)
    {
    	// если текущий блок входит в некий другой блок
        if (is_object($parent))
        {
        	// стырить у того все параметры
        	$this->parent=$parent;
            $this->db=&$parent->db;
            $this->smarty=&$parent->smarty;
            $this->config=&$parent->config;
            $this->params=&$parent->params;
            $this->settings=&$parent->settings;
            $this->user=&$parent->user;
            $this->root_dir=&$parent->root_dir;
            $this->root_url=&$parent->root_url;
            $this->currency=&$parent->currency;
            $this->main_currency=&$parent->main_currency;
            $this->currencies=&$parent->currencies;
            $this->gd_loaded=&$parent->gd_loaded;
			$this->ogImage=&$parent->ogImage;

			$this->ogImage_1_1 = &$parent->ogImage_1_1;
			$this->ogImage_1_1_c = &$parent->ogImage_1_1_c;
			$this->ogImage_1_2 = &$parent->ogImage_1_2;
			$this->ogImage_1_3 = &$parent->ogImage_1_3;
			$this->withUrlParams = &$parent->withUrlParams;

			$this->ogSiteName=&$parent->ogSiteName;
			$this->ogUrl=&$parent->ogUrl;
			$this->ogTitle=&$parent->ogTitle;
        }
        else
        {

			// Читаем конфиг
			$this->config = new Config();

			// Если установлены magic_quotes, убираем лишние слеши
			if(get_magic_quotes_gpc())
			{
				$_POST = $this->stripslashes_recursive($_POST);
				$_GET = $this->stripslashes_recursive($_GET);
			}

			// Подключаемся к базе данных
			$this->db=new Database($this->config->dbname,$this->config->dbhost,
			$this->config->dbuser,$this->config->dbpass);

			if(!$this->db->connect())
			{
//				print "Не могу подключиться к базе данных. Проверьте настройки подключения";
				exit();
			}
			$this->db->query('SET NAMES utf8');

			// Выбираем из базы настройки, которые задаются в разделе Настройки в панели управления
			$query = 'SELECT name, value FROM settings';
			$this->db->query($query);
			$sts = $this->db->results();
			foreach($sts as $s)
			{
				$name = $s->name;
				$this->settings->$name = $s->value;
			}

            if (@$this->config->timezone) {
                date_default_timezone_set( $this->config->timezone);
            }

			// Настраиваем смарти
			$this->smarty = new Smarty();
			$this->smarty->compile_check = true;
			$this->smarty->caching = false;
			$this->smarty->cache_lifetime = 0;
			$this->smarty->debugging = false;
			$this->smarty->security = true;
			$this->smarty->secure_dir[] = 'adminv2/templates';
			$this->smarty->compile_dir = 'compiled';

			$this->smarty->template_dir = $this->smarty->secure_dir[] = 'design/'.$this->settings->theme.'/html';

			$this->smarty->config_dir = 'configs';
			$this->smarty->cache_dir = 'cache';

			$this->smarty->assign('settings', $this->settings);

			// Проверка подключения графической библиотеки
			if(extension_loaded('gd'))
				$this->gd_loaded = true;
			$this->smarty->assign('gd_loaded', $this->gd_loaded);

			// Определяем корневую директорию сайта
			$this->root_dir =  str_replace(basename($_SERVER["PHP_SELF"]), '', $_SERVER["PHP_SELF"]);
			$this->smarty->assign('root_dir', $this->root_dir);

			// Корневой url сайта
			$dir = trim(dirname($_SERVER['SCRIPT_NAME']));
			$dir = str_replace("\\", '/', $dir);
			$this->root_url = $this->config->protocol . $this->config->host;

			if ($this->config->env=='dev'){
                $this->root_url .= ":" . $this->config->port;
			}

			if($dir!='/')
			    $this->root_url = $this->root_url.$dir;
			$this->smarty->assign('config_env', $this->config->env);
			$this->smarty->assign('root_url', $this->root_url);
			$this->smarty->assign('fullRootUrl', 'http://' . $this->root_url);
            $this->smarty->assign('_cache_version', @$this->config->cache_version ? $this->config->cache_version : 'v=1');
		}
    }


    function setSubscribeCheck($email)
    {
        $result = false;

        $hash = md5($email . $this->salt . time());

        $query = sql_placeholder("INSERT IGNORE INTO subscribe SET email = ?, check_hash = ?", $email, $hash);

        if ($this->db->query($query)){
            $result = $hash;
        }

        return $result;
    }

    function sendConfirmSubscribeEmail($email, $hash)
    {
        $url = "http://" . $this->root_url . "/confirm-subscribe/" . $hash;

        $subject = "Подписка на новости: " . $this->settings->site_name;
        $mess = file_get_contents('email.html');
        $message = str_ireplace('{%link%}', $url, $mess);

        return $this->email($email, $subject, $message);
    }

	/**
	 *
	 * Отображение блока
	 *
	 */
    function fetch()
    {
    	return $this->body="";
    }

	/**
	 *
	 * Рекурсивная уборка магических слешей
	 *
	 */
    function stripslashes_recursive($var)
    {
    	$res = null;
    	if(is_array($var))
    	  foreach($var as $k=>$v)
    	    $res[stripcslashes($k)] = $this->stripslashes_recursive($v);
    	  else
    	    $res = stripcslashes($var);
    	return $res;
    }


    function param($name)
    {
    	if(!empty($name))
      	{
      		if(isset($this->params[$name]))
	  		  return $this->params[$name];
	  		elseif(isset($_GET[$name]))
	  		  return $_GET[$name];
    	}
	    return null;
    }

	/**
	 *
	 * Подмена параметра get
	 *
	 */

    function add_param($name)
    {
    	if(!empty($name) && isset($_GET[$name]))
    	{
			$this->params[$name] = $_GET[$name];
	        return true;
    	}
	    return false;
    }

    function url_filter($val)
    {
    	$val =  preg_replace('/[^A-zА-я0-9_\-\.\%\s]/ui', '', $val);
	    return $val;
    }

    function url_filtered_param($name)
    {
	    return $this->url_filter($this->param($name));
    }

    function form_get($extra_params)
    {
    	$copy=$this->params;
      	foreach($extra_params as $key=>$value)
      	{
	    	if(!is_null($value))
    	  	{
          		$copy[$key]=$value;
	        }
    	}

	    $get='';
    	foreach($copy as $key=>$value)
		{
        	if(strval($value)!="")
	        {
    		    if(empty($get))
            	  $get .= '?';
	        	else
    	          $get .= '&';
    	        $get .= urlencode($key).'='.urlencode($value);
        	}
	    }
      	return $get;
    }

    function sendReportErrorText($text, $userMessage, $url)
    {
        if (!empty($text) && !empty($url)){
            $subject = "Найдена ошибка на сайте " . $this->settings->site_name;
            $message = "Найдена ошибка на странице <b>{$url}</b>.<br/>Пользователь посчитал ошибкой фразу: <b>{$text}</b>.";
            if (!empty($userMessage)){
                $message .= "<br/><br/>Пользователь так же оставил комментарий к найденной ошибке:<br/>";
                $message .= "<i>" . nl2br($userMessage) . "</i>";
            }

            return $this->email('plus-one.rbc.ru@plus-one.ru', $subject, $message);
        }
        else{
            return false;
        }
    }

    function email($to, $subject, $message, $additional_headers='')
    {
        $site_name = "=?utf-8?B?".base64_encode($this->settings->site_name)."?=";

        $from = "$site_name <noreply@plus-one.ru>";

        $headers = "MIME-Version: 1.0\n" ;
        $headers .= "Content-type: text/html; charset=utf-8; \r\n";
        $headers .= "From: $from \r\n";

        $subject = "=?utf-8?B?".base64_encode($subject)."?=";

        mail($to, $subject, $message, $headers);
    }
}

?>
