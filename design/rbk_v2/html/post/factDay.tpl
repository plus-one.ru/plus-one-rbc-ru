<div class="clear-zone">

    {include file="banners/300x600.tpl"}

    <div class="text-block">
        <strong class="h1">{$blog->name}</strong>
        <span>{$blog->lead}</span>
        <div class="meta">
            {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" >
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
            {/if}
            <a class="{$blog->tags->url}" href="/blogs/{$blog->tags->url}">
                {$blog->tags->name}
            </a>
            <time datetime="{$blog->postDateStr}">{$blog->postDateStr}</time>

            <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
        </div>

        <div class="item {if $blog->tags->color=="f9d606"}yellow{/if}{if $blog->tags->color=="18dc46"}green{/if}{if $blog->tags->color=="00bae9"}blue{/if} analytics fact fact--2">
            <a href="#" class="category-link">Факт недели</a>
            <div class="info" style="width: 100%;">
                {if $blog->amount_to_displaying|count_characters > 6 && $blog->amount_to_displaying|count_characters <= 13}
                    <strong class="digit" style="font-size: 80px; line-height: 90px">
                        {$blog->amount_to_displaying}
                    </strong>
                {elseif $blog->amount_to_displaying|count_characters > 14}
                    <strong class="digit" style="font-size: 60px; line-height: 65px">
                        {$blog->amount_to_displaying}
                    </strong>
                {else}
                    <strong class="digit">
                        {$blog->amount_to_displaying}
                    </strong>
                {/if}
                <p><span class="mln">{$blog->signature_to_sum}</p>
                {$blog->text_body}
            </div>
        </div>
    </div>
</div>
<div class="text-block">
    {$blog->body}
</div>

<div class="text-block">
    <ul class="tags static">
        {if $blog->postTags}
            {foreach item=postTag from=$blog->postTags name=postTag}
                <li>
                    <a href="/posttags/{$blog->tags->url}/{$postTag->url}">
                        {$postTag->name}
                    </a>
                </li>
            {/foreach}
        {/if}
    </ul>
</div>
{*<div class="clear-zone">*}

    {*{include file="banners/300x600.tpl"}*}

    {*<div class="text-block">*}
        {*<strong class="h1">{$blog->name}</strong>*}
        {*<span>{$blog->lead}</span>*}
        {*<div class="meta">*}
            {*<a class="{$blog->tags->url}" href="/blog/{$blog->tags->url}">*}
                {*{$blog->tags->name}*}
            {*</a>*}
        {*</div>*}
        {*<div class="item dark" style="margin-left: 0; float: none; height: 365px; width: 559px; background-color: #{$blog->tags->color}">*}
            {*<span class="category-link" style="color: #000;margin-top: 20px;padding-left: 24px; text-transform: none;font-weight: bold;">*}
                {*Факт дня*}
            {*</span>*}
            {*<div class="info" style="max-width: 559px;" >*}
                {*<strong class="h2" style="font-size: 70px; margin: 0 0 15px; line-height: 70px;">*}
                    {*{$blog->amount_to_displaying}*}
                {*</strong>*}

                {*<span class="d" style="color: #000; height: 110px; overflow: hidden;">*}
                    {*<span class="mln" style="font-size: 20px; color: #fff; padding-bottom: 5px; display: block;">*}
                        {*{$blog->signature_to_sum}*}
                    {*</span>*}
                    {*<span ng-bind-html="textDangerousSnippet(post.text_body)">*}
                        {*{$blog->text_body}*}
                    {*</span>*}
                {*</span>*}
            {*</div>*}
            {*<ul class="tags">*}
                {*{if $blog->partner_url}*}
                    {*<li>*}
                        {*<a href="{$blog->partner_url}">*}
                            {*<span style="color: {$blog->partner_color};font-size: 26px; background: transparent; position: relative; top: 0px; left: -7px;">*}
                                {*•*}
                            {*</span>*}
                            {*<span style="font-size: 11px; background: transparent; position: relative; top: -20px; left: 10px; color: #fff;">*}
                                {*{$blog->partner_name}*}
                            {*</span>*}
                        {*</a>*}
                    {*</li>*}
                {*{/if}*}
            {*</ul>*}
        {*</div>*}
    {*</div>*}
{*</div>*}
{*<div class="text-block">*}
    {*{$blog->body}*}
{*</div>*}

{*<div class="text-block">*}
    {*<ul class="tags static">*}
        {*{if $blog->partner_url}*}
            {*<li>*}
                {*<a href="{$blog->partner_url}">*}
                    {*<span style="color: {$blog->partner_color};font-size: 26px; background: transparent; position: relative; top: 0px; left: -7px;">*}
                        {*•*}
                    {*</span>*}
                    {*<span style="font-size: 11px; background: transparent; position: relative; top: -20px; left: 10px; color: #fff;">*}
                        {*{$blog->partner_name}*}
                    {*</span>*}
                {*</a>*}
            {*</li>*}
        {*{/if}*}

        {*{if $blog->postTagMainPage->name}*}
            {*<li ng-if="postObject.postTagMainPage.name">*}
                {*<a href="posttags/{$blog->postTagMainPage->blogtag_url}/{$blog->postTagMainPage->url}">*}
                    {*{$blog->postTagMainPage->name}*}
                {*</a>*}
            {*</li>*}
        {*{/if}*}

        {*{if $blog->postTags}*}
            {*{foreach item=postTag from=$blog->postTags name=postTag}*}
                {*<li>*}
                    {*<a href="/posttags/{$blog->tags->url}/{$postTag->url}">*}
                        {*{$postTag->name}*}
                    {*</a>*}
                {*</li>*}
            {*{/foreach}*}
        {*{/if}*}
    {*</ul>*}
{*</div>*}