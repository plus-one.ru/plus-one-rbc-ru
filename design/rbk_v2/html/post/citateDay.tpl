{include file="banners/300x600.tpl"}
<div class="clear-zone">



    <div class="text-block">
        {if $blog->header_on_page != ''}
            <strong class="h1">{$blog->header_on_page}</strong>
        {else}
            <strong class="h1">{$blog->name}</strong>
        {/if}
        <span>{$blog->lead}</span>

        <div class="meta">
            {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" ng-if="post.partner_name">
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
            {/if}

            <a class="{$blog->tags->url}" href="/blogs/{$blog->tags->url}">
                {$blog->tags->name}
            </a>
            <time datetime="{$blog->postDateStr}">{$blog->postDateStr}</time>
            <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
        </div>

        <p>
        <blockquote class="item {if $blog->tags->color=="f9d606"}yellow{/if}{if $blog->tags->color=="18dc46"}green{/if}{if $blog->tags->color=="00bae9"}blue{/if} valign blockquote-box" style="margin-left: -19px;">
            <span class="cat-name">ЦИТАТА</span>
            {if $blog->citateText|count_characters > 120}
                <div class="info info--2 info--long">
                    <p>{$blog->citateText}</p>
                </div>
            {else}
                <div class="info">
                    <p>{$blog->citateText}</p>
                </div>
            {/if}
            {if $blog->citate_author_name}
                <cite class="blockquote-box_cite">
                    <span class="blockquote-box_cite_img">
                        <img src="/files/blogposts/{$blog->id}-cauthor.jpg" alt="image description" width="70" height="70" />
                    </span>
                    <span class="blockquote-text">
                        {$blog->citate_author_name}
                    </span>
                </cite>
            {/if}
        </blockquote>
        </p>
    </div>
</div>

<div class="text-block">
    {$blog->body}
</div>

<div class="text-block">
    <ul class="tags static">
        {if $blog->postTags}
            {foreach item=postTag from=$blog->postTags name=postTag}
                <li>
                    <a href="/posttags/{$blog->tags->url}/{$postTag->url}">
                        {$postTag->name}
                    </a>
                </li>
            {/foreach}
        {/if}
    </ul>
</div>


{*<div class="clear-zone">*}

    {*<div class="text-block">*}
        {*{if $blog->header_on_page != ''}*}
            {*<strong class="h1">{$blog->header_on_page}</strong>*}
        {*{else}*}
            {*<strong class="h1">{$blog->name}</strong>*}
        {*{/if}*}
        {*<span>{$blog->lead}</span>*}

        {*<div class="meta">*}
            {*<a class="{$blog->tags->url}" href="/blog/{$blog->tags->url}">*}
                {*{$blog->tags->name}*}
            {*</a>*}
        {*</div>*}

        {*<div class="item long"  style="margin: 0 0 30px 0; width: 100%; background-color: #{$blog->tags->color}">*}
            {*{include file="../include/iconTagFirstLevel.tpl"}*}

            {*<div class="visual right absolute">*}
                {*<div class="bg" style='background-position: 0 0; background-image: url("files/blogposts/{$blog->id}-1_1.jpg");'>*}
                {*</div>*}
            {*</div>*}
            {*<div class="v-align">*}
                {*<div class="align-holder">*}
                    {*{$blog->citateText}*}
                {*</div>*}
            {*</div>*}
            {*<ul class="tags">*}
                {*{if $blog->partner_url}*}
                    {*<li>*}
                        {*<a href="{$blog->partner_url}">*}
                    {*<span style="color: {$blog->partner_color};font-size: 26px; background: transparent; position: relative; top: 0px; left: -7px;">*}
                        {*•*}
                    {*</span>*}
                    {*<span style="font-size: 11px; background: transparent; position: relative; top: -20px; left: 10px; color: #fff;">*}
                        {*{$blog->partner_name}*}
                    {*</span>*}
                        {*</a>*}
                    {*</li>*}
                {*{/if}*}
            {*</ul>*}
            {*<span class="category-link" style="color: #000; text-transform: none;font-weight: bold; position: absolute; top: 15px; left: -15px; height: 20px;">*}
                {*Цитата дня*}
            {*</span>*}
        {*</div>*}
    {*</div>*}
{*</div>*}

{*{include file="banners/300x600.tpl"}*}

{*<div class="text-block">*}
    {*{$blog->body}*}
{*</div>*}

{*<div class="text-block">*}
    {*<ul class="tags static">*}
        {*{if $blog->partner_url}*}
            {*<li>*}
                {*<a href="{$blog->partner_url}">*}
                    {*<span style="color: {$blog->partner_color};font-size: 26px; background: transparent; position: relative; top: 0px; left: -7px;">*}
                        {*•*}
                    {*</span>*}
                    {*<span style="font-size: 11px; background: transparent; position: relative; top: -20px; left: 10px; color: #fff;">*}
                        {*{$blog->partner_name}*}
                    {*</span>*}
                {*</a>*}
            {*</li>*}
        {*{/if}*}

        {*{if $blog->postTagMainPage->name}*}
            {*<li ng-if="postObject.postTagMainPage.name">*}
                {*<a href="posttags/{$blog->postTagMainPage->blogtag_url}/{$blog->postTagMainPage->url}">*}
                    {*{$blog->postTagMainPage->name}*}
                {*</a>*}
            {*</li>*}
        {*{/if}*}

        {*{if $blog->postTags}*}
            {*{foreach item=postTag from=$blog->postTags name=postTag}*}
                {*<li>*}
                    {*<a href="/posttags/{$blog->tags->url}/{$postTag->url}">*}
                        {*{$postTag->name}*}
                    {*</a>*}
                {*</li>*}
            {*{/foreach}*}
        {*{/if}*}
    {*</ul>*}
{*</div>*}
