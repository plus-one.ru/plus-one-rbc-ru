    <div class="clear-zone">

        {include file="banners/300x600.tpl"}

        <div class="text-block">

            {if $blog->spec_project != 0}
            <div class="special">
                <a style="background-color: #{$blog->specProjectColor};" href="{$blog->specProjectUrl}" target="_blank">
                    {$blog->specProjectName}
                </a>
            </div>
            {/if}

            {if $blog->conference != 0}
                <div class="special">
                    <a style="background-color: #{$blog->confColor};" href="{$blog->confUrl}" target="_blank">
                        {$blog->confName}
                    </a>
                </div>
            {/if}

            {if $blog->header_on_page != ''}
                <strong class="h1">
                    {$blog->header_on_page}
                </strong>
            {else}
                <strong class="h1">
                    {$blog->name}
                </strong>
            {/if}

            <span>
                {$blog->lead}
            </span>

            <div class="meta">
                {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" ng-if="post.partner_name">
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
                {/if}
                <a class="{$blog->tags->url}" href="/{$blog->tags->url}">
                    {$blog->tags->name}
                </a>
                <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
            </div>

            {$blog->body}

        </div>
    </div>
    {*<div class="text-block">*}
        {**}
    {*</div>*}

    <div class="text-block">
        <ul class="tags static">
            {if $blog->partner_url}
                <li>
                    <a href="{$blog->partner_url}">
                        <span style="color: {$blog->partner_color};font-size: 26px; background: transparent; position: relative; top: 0px; left: -7px;">
                            •
                        </span>
                    <span style="font-size: 11px; background: transparent; position: relative; top: -20px; left: 10px; color: #fff;">
                        {$blog->partner_name}
                    </span>
                    </a>
                </li>
            {/if}

            {if $blog->postTagMainPage->name}
                <li ng-if="postObject.postTagMainPage.name">
                    <a href="posttags/{$blog->postTagMainPage->blogtag_url}/{$blog->postTagMainPage->url}">
                        {$blog->postTagMainPage->name}
                    </a>
                </li>
            {/if}

            {if $blog->postTags}
                {foreach item=postTag from=$blog->postTags name=postTag}
                    <li>
                        <a href="/posttags/{$blog->tags->url}/{$postTag->url}">
                            {$postTag->name}
                        </a>
                    </li>
                {/foreach}
            {/if}
        </ul>
    </div>
