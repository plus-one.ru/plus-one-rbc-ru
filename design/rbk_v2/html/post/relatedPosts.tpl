<div class="items">
    {foreach item=post from=$blog->relatedPosts name=post}
        {*обычная запись блога *}
        {if $post->type_post==1 || $post->type_post==2 || $post->type_post==3}

            <div class="item trio gray">
                {include file="include/iconTagFirstLevel.tpl"}
                <div class="visual">
                    <div class="bg">
                        <a href="/blog/{$post->tags->url}/{$post->url}">
                            <img src="/files/blogposts/{$post->id}-1_3.jpg" alt="image description"/>
                        </a>
                    </div>
                </div>
                <div class="info">
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        <strong class="h3">
                            {$post->name}
                        </strong>

                        <p class="d">
                            {$post->header}
                        </p>
                    </a>
                </div>
                {include file="include/tagsOnPostTileWoDate.tpl"}
            </div>
        {/if}

        {* цитата дня *}
        {if $post->type_post==9}
        <div class="item trio blue no-image" style="background-color: #{$post->tags->color}">
            {include file="include/iconTagFirstLevel.tpl"}
            <div class="info">
                <a href="/blog/{$post->tags->url}/{$post->url}">
                    <strong class="h3" ng-bind-html="textDangerousSnippet(postObject.name)">
                        {$post->name}
                    </strong>
                    <p class="d">
                        {$post->header}
                    </p>
                </a>
            </div>
            {include file="include/tagsOnPostTileWoDate.tpl"}
        </div>
        {/if}

        {* фото дня *}
        {if $post->type_post==7}
            <div class="item trio gray analytics">
                <a href="/blog/{$post->tags->url}/{$post->url}" class="category-link white">Фото дня</a>
                <div class="visual">
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        <div class="bg"
                             style="background-position: 50% 50%; background-image: url(/files/blogposts/{$post->id}-1_1.jpg);">
                        </div>
                    </a>
                </div>
                <div class="info" style="padding-top: 24px;">
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        <strong class="h3">
                            {$post->name}
                        </strong>
                        <p class="d">
                            {$post->header}
                        </p>
                    </a>
                </div>
                {include file="include/tagsOnPostTileWoDate.tpl"}
            </div>
        {/if}

        {* факт дня *}
        {if $post->type_post==8}
        <div class="item trio green analytics" style="background-color: #{$blog->tags->color}">
            <a href="/blog/{$post->tags->url}/{$post->url}" class="category-link">Факт дня</a>
            <div class="info" style="height: 460px;">
                <strong class="digit">
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        {$post->amount_to_displaying}
                    </a>
                </strong>
                    <span class="mln" style="padding-bottom: 5px; color: #fff; display: block">{$post->signature_to_sum}</span>
                    <span ng-bind-html="textDangerousSnippet(postObject.text_body)">
                        {$post->text_body}
                    </span>

            </div>
            {include file="include/tagsOnPostTileWoDate.tpl"}
        </div>
        {/if}
    {/foreach}
</div>