<main id="main">
    <ul class="cat-tags {$tagUrl}">
        {foreach item=tag from=$tags name=tag}
        <li>
            {if $tag->countPosts->count > 0}
            <a href="/posttags/{$tagUrl}/{$tag->url}">
                <div class="pic">
                    {if $tag->image}
                        <img src="/files/tags/{$tag->image}" alt="{$tag->name}" />
                    {/if}
                </div>
                <div class="frame">
                    <strong class="title">{$tag->name}</strong>
                    {if $tag->countPosts->count > 0}
                        <span>{$tag->countPosts->count} {$tag->countPosts->text}</span>
                    {/if}
                </div>
            </a>
            {/if}

            {if $tag->countPosts->count <= 0}
            <span>
                <div class="pic">
                    {if $tag->image}
                        <img src="/files/tags/{$tag->image}" alt="{$tag->name}" />
                    {/if}
                </div>
                <div class="frame">
                    <strong class="title">{$tag->name}</strong>
                    {if $tag->countPosts->count > 0}
                        <span>{$tag->countPosts->count} {$tag->countPosts->text}</span>
                    {/if}
                </div>
            </span>
            {/if}
        </li>
        {/foreach}
    </ul>
</main>