<main id="main">
    {if $blog->type_post == 1 || $blog->type_post == 2 || $blog->type_post == 3 || $blog->type_post == 11}
        {include file="post/normalPost.tpl"}
    {/if}

    {if $blog->type_post == 7}
        {include file="post/photoDay.tpl"}
    {/if}

    {if $blog->type_post == 8}
        {include file="post/factDay.tpl"}
    {/if}

    {if $blog->type_post == 9}
        {include file="post/citateDay.tpl"}
    {/if}

    {literal}
        <script type="text/javascript">(function (w, doc) {
                if (!w.__utlWdgt) {
                    w.__utlWdgt = true;
                    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })(window, document);
        </script>
    {/literal}

    <div style="position: relative; display: block">
        <div data-mobile-view="true" data-share-size="30" data-like-text-enable="false" data-background-alpha="0.0"
             data-pid="1690547" data-mode="share" data-background-color="#ffffff" data-share-shape="rectangle"
             data-share-counter-size="12" data-icon-color="#ffffff" data-mobile-sn-ids="fb.vk.tw.ok.wh.vb.tm."
             data-text-color="#ffffff" data-buttons-color="#ffffff" data-counter-background-color="#ffffff"
             data-share-counter-type="separate" data-orientation="horizontal" data-following-enable="false"
             data-sn-ids="fb.vk.tw.ok.tm." data-preview-mobile="false" data-selection-enable="false"
             data-exclude-show-more="true" data-share-style="10" data-counter-background-alpha="0.0" data-top-button="false"
             class="uptolike-buttons" style="padding: 0 17px;">
            <!-- /-->
        </div>

        <span href="#lightbox" onclick="return false" class="error-link lightbox">
            Нашли опечатку? Выделите ее и нажмите Ctrl/Cmd+Enter
        </span>
    </div>

    <div class="popup-holder">
        <div id="lightbox" class="lightbox">
            <div class="lightbox__title">Вы выделили ошибку &laquo;<mark id="selectedtext">морских</mark>&raquo;</div>
            <form action="#" class="lightbox__form">
                <textarea name="lightbox__form" id="lightbox__form" class="lightbox__form__input" cols="20" rows="5" placeholder="Ваш комментарий (опционально)"></textarea>
                <input type="button" class="lightbox__form__btn" value="Отправить редакции" onclick="sendError();" />
            </form>
        </div>
    </div>

    <div class="text-block"><!-- /--></div>

    {if $issubscribe != 1}
        {if $blog->tags->url == 'ecology'}
            <form action="#" class="newsletter-form newsletter-form--green">
                <div class="newsletter-form__holder">
                    <div class="newsletter-form__title">Подпишитесь на рассылку +1</div>
                    <div class="newsletter-form__input-holder">
                        <input class="newsletter-form__input" name="email" id="email" placeholder="EMAIL" type="email">
                        <input class="newsletter-form__btn" value=" Подписаться" type="button" onclick="subscribe()">
                    </div>
                </div>
                <div class="success-text-box" style="display: none;">
                    Мы отправили вам проверочное письмо, <br>пожалуйста перейдите по ссылке, <br>которую мы отправили
                </div>
            </form>
        {/if}


        {if $blog->tags->url == 'economy'}
            <form action="#" class="newsletter-form newsletter-form--blue">
                <div class="newsletter-form__holder">
                    <div class="newsletter-form__title">Подпишитесь на рассылку +1</div>
                    <div class="newsletter-form__input-holder">
                        <input class="newsletter-form__input" name="email" id="email" placeholder="EMAIL" type="email">
                        <input class="newsletter-form__btn" value=" Подписаться" type="button" onclick="subscribe()">
                    </div>
                </div>
                <div class="success-text-box" style="display: none;">
                    Мы отправили вам проверочное письмо, <br>пожалуйста перейдите по ссылке, <br>которую мы отправили
                </div>
            </form>
        {/if}

        {if $blog->tags->url == 'society'}
            <form action="#" class="newsletter-form">
                <div class="newsletter-form__holder">
                    <div class="newsletter-form__title">Подпишитесь на рассылку +1</div>
                    <div class="newsletter-form__input-holder">
                        <input class="newsletter-form__input" name="email" id="email" placeholder="EMAIL" type="email">
                        <input class="newsletter-form__btn" value=" Подписаться" type="button" onclick="subscribe()">
                    </div>
                </div>
                <div class="success-text-box" style="display: none;">
                    Мы отправили вам проверочное письмо, <br>пожалуйста перейдите по ссылке, <br>которую мы отправили
                </div>
            </form>
        {/if}
    {/if}

    {if $blog->relatedPosts}
        {include file="post/relatedPosts.tpl"}
    {/if}
    {if $blog->partnersPosts}
        {include file="post/partnersPosts.tpl"}
    {/if}
</main>