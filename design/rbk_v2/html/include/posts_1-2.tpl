<!-- 1/2 -->
{if $post->blocks == '1/2' && $post->anounce_new_author == 0 && ($post->type_post == 1 || $post->type_post == 3)}
<div class="item half dark" style="height: 365px;">
    <div class="bg" style="background-position: 0 0; background-image: url(/files/blogposts/{$post->id}-1_2.jpg);"></div>
    <div class="info">

        {include file="include/iconTagFirstLevel.tpl"}

        {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
        <a href="case-1.html">
            <strong class="h2" style="width:370px;">{$post->name}</strong>
            <p class="d" style="width:370px;">{$post->header}</p>
        </a>
        {else}
        <a href="/blog/{$post->tags->url}/{$post->url}">
            <strong class="h2" style="width:370px;">{$post->name}</strong>
            <p class="d" style="width:370px;">{$post->header}</p>
        </a>
        {/if}
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

<!-- 1/2 цитата /-->
{if $post->blocks == '1/2' && $post->type_post == 2 && $post->anounce_new_author == 0}
<div class="item half quote" style="height: 365px; background-color: #{$post->tags->color}">
    <div class="visual right absolute">
        <div class="bg" style="background-image: url('/files/blogposts/{$post->id}-1_1_c.jpg');"></div>
    </div>
    <div class="info">

        {include file="include/iconTagFirstLevel.tpl"}

        <p class="center">
            {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
                <a href="case-1.html">
                    {$post->citateText}
                </a>
            {else}
                <a href="/blog/{$post->tags->url}/{$post->url}">
                    {$post->citateText}
                </a>
            {/if}
        </p>
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

<!-- 1/2 анонс нового блога /-->
{if $post->blocks == '1/2' && $post->type_post == 4}
<div class="item half quote valign" style="background-color: #{$post->tags->color}; height: 365px;">
    <a class="cat-name" href="/author/{$post->writer_id}">
        Новый блог
    </a>
    <div class="info">
        <a href="/author/{$post->writer_id}" class="general-reference">

            <strong class="h2{if $post->lenWriter <= 20} upper{/if}" >
                {$post->writer}
            </strong>
            {include file="include/iconTagFirstLevelLeader.tpl"}
        </a>
    </div>
    {include file="include/tagsOnPostTileOnlyDate.tpl"}
</div>
{/if}

{*1/2 цитата дня*}
{if $post->blocks == '1/2' && $post->type_post == 9}
    <blockquote class="item half {if $post->tags->color=="f9d606"}yellow{/if}{if $post->tags->color=="18dc46"}green{/if}{if $post->tags->color=="00bae9"}blue{/if} valign blockquote-box">
        <a href="#" class="cat-name">
            {if $post->conference != 0 || $post->spec_project != 0}
                {include file="include/iconTagFirstLevel.tpl"}
            {else}
                <a href="/blog/{$post->tags->url}/{$post->url}" class="cat-name">
                    ЦИТАТА
                </a>
            {/if}
        </a>
        <div class="info">
            <p>
                <a href="/blog/{$post->tags->url}/{$post->url}">
                    {$post->citateText}
                </a>
            </p>
        </div>
        {*{if $post->citate_author_name}
            <cite class="blockquote-box_cite">
                <span class="blockquote-box_cite_img">
                    <img src="/files/blogposts/{$post->id}-cauthor.jpg" alt="image description" width="70" height="70" />
                </span>
                <span class="blockquote-text">
                    {$post->citate_author_name}
                </span>
            </cite>
        {/if}*}
    </blockquote>
{/if}