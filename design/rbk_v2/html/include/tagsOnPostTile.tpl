<ul class="tags">
    {*<li>*}
        {*<span class="date">*}
            {*{$post->postDateStr}*}
        {*</span>*}
    {*</li>*}
    {if $post->partner}
        <li>
            <a href="{$post->partner_url}" target="_blank">
                <span style="color: {$post->partner_color};font-size: 26px; background: transparent; position: relative; top: 0px; left: -7px;">&bull;</span>
                <span style="font-size: 11px; background: transparent; position: relative; top: -20px; left: 10px; color: #000;">{$post->partner}</span>
            </a>
        </li>
    {/if}
    {if $post->postTagMainPage->name}
        <li>
            <a href="/posttags/{$post->postTagMainPage->blogtag_url}/{$post->postTagMainPage->url}">
                {$post->postTagMainPage->name}
            </a>
        </li>
    {/if}
</ul>