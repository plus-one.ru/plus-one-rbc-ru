<div class="panel panel-default">
    <div class="panel-heading">Авторы</div>
    <div class="panel-body">
        {foreach item=writer from=$writers name=writer}
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle img-thumbnail" src="/files/writers/{$writer->image}" alt="{$writer->name}" />
                    </a>
                </div>
                <div class="media-body">
                    <p>
                        <a href="#">{$writer->name|escape}</a>
                        <br/>
                        <span class="text-muted">27 записей в блоге автора</span>
                    </p>
                </div>
            </div>
        {/foreach}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Рубрики</div>
    <div class="panel-body">
        {foreach item=blogtag from=$blogtags name=blogtag}
            <p>
                <a href="/blog/{$blogtag->url}">
                    {$blogtag->name|escape}
                </a>
                <span class="text-muted pull-right">36 записей</span>
            </p>
        {/foreach}

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Теги</div>
    <div class="panel-body">

    </div>
</div>