<div class="container">
    <strong class="head-logo"><a href="http://www.rbc.ru/">РБК</a></strong>
    <ul>
        <li><a href="http://tv.rbc.ru/?utm_source=topline">ТЕЛЕКАНАЛ</a></li>
        <li><a href="http://www.rbc.ru/newspaper/?utm_source=topline">ГАЗЕТА</a></li>
        <li><a href="http://www.rbc.ru/magazine/?utm_source=topline">ЖУРНАЛ</a></li>
    </ul>
    <ul>
        <li><a href="http://money.rbc.ru/?utm_source=topline">Деньги</a></li>
        <li><a href="http://quote.rbc.ru/?utm_source=topline">QUOTE</a></li>
        <li><a href="http://www.autonews.ru/?utm_source=topline">АВТО</a></li>
        <li><a href="http://sport.rbc.ru/?utm_source=topline">СПОРТ</a></li>
        <li><a href="http://realty.rbc.ru/?utm_source=topline">НЕДВИЖИМОСТЬ</a></li>
        <li><a href="http://style.rbc.ru/?utm_source=topline">СТИЛЬ</a></li>
        <li><a href="http://www.cnews.ru/?utm_source=topline">C&bull;NEWS</a></li>
        <li><a href="http://marketing.rbc.ru/?utm_source=topline">Исследования</a></li>
        <li><a href="http://bc.rbc.ru/?utm_source=topline">Конференции</a></li>
    </ul>
    <a href="#" class="search-link">search</a>
</div>