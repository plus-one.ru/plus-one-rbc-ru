<strong class="logo">
    <a href="./">+1</a>
</strong>
<a href="./" class="blog-link">
    Блоги
</a>

<nav class="category">
    <ul>
    {foreach item=blogtag from=$blogtags name=blogtag}
        <li>
            <a class="category-link active" href="#/blog/{$blogtag->url}">
                <span class="icon {$blogtag->url} icon-{$blogtag->url}"></span>
                {$blogtag->name|escape}
            </a>
        </li>
    {/foreach}
    </ul>
</nav>
{*<a href="#" class="search-link">Поиск</a>*}