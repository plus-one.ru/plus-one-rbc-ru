{* 1/1 *}
{if $post->blocks == '1/1' && $post->type_post == 1 && $post->anounce_new_author == 0}
<div class="item big dark">
    <div class="bg" style="background-position: 0 0; background-image: url('/files/blogposts/{$post->id}-1_1.jpg');"></div>
    <div class="v-align">
        <div class="align-holder">

            {include file="include/iconTagFirstLevel.tpl"}

            {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
            <a href="/case-1.html" target="_blank">
                <strong class="h1">{$post->name}</strong>
                <p>{$post->header}</p>
            </a>
            {else}
            <a href="/blog/{$post->tags->url}/{$post->url}">
                <strong class="h1">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
            {/if}
        </div>
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

{*1/1 видео *}
{if $post->blocks == '1/1' && $post->type_post == 6 && $post->anounce_new_author == 0}
    <div style="margin-top:0px; height: 392px;" class="item big_video dark video_block">
        <div class="left" style="width:698px; height:392px;">
            <input type="hidden" id="initialVideoUrl" data-videoid="{$post->id}" class="initialVideoUrl" value="{$post->videoCode}">
            <span id="mainvideoplayer_{$post->id}"></span>
        </div>
        <div class="right">
            {if $post->state == '3'}
                <div class="anons">
                    <p class="small">Анонс</p>
                    <h2 style="width: 420px;">{$post->name}</h2>
                    <span>{$post->header}</span>
                    <p class="time">{$post->postDateStr}</p>
                </div>
            {/if}

            {if $post->state == '2'}
                <div class="live">
                    <p class="small">Live<span></span></p>
                    <h2 style="width: 420px;">{$post->name}</h2>
                    <span>{$post->header}</span>
                </div>
            {/if}

            {if $post->state == '1'}
                <div id="videoList_{$post->id}" class="moments">
                    <h2 style="width: 420px;">{$post->name}</h2>

                    {foreach item=video from=$post->videos name=video}
                        <div class="item">
                            <div class="left_item">
                                <img alt="player" onclick="loadVideo('{$video->video_url}', true, {$post->id})" class="video_prev" src="/files/blogposts/{$video->image}">
                                <img alt="active" onclick="loadVideo('{$video->video_url}', true, {$post->id})" class="active_video" src="/design/rbk_v2/images/play.png">
                                <img alt="video_hov" onclick="loadVideo('{$video->video_url}', true, {$post->id})" class="video_hov" src="/design/rbk_v2/images/video_hov.png">
                            </div>
                            <div class="right_item">
                                <p onclick="loadVideo('{$video->video_url}', true, {$post->id})">{$video->name}</p>
                                {if $video->divisionTime}
                                    <a class="detail" onclick="showVideoFragment({$video->id}, {$post->id})" >
                                        <img alt="detail" src="/design/rbk_v2/images/detail.png">
                                    </a>
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                </div>
            {/if}

            <div class="all_detail" id="videoFragments_{$post->id}" style="display: none; width: 457px;"></div>
        </div>
    </div>
{/if}

{* 1/1 цитата *}
{if $post->blocks == '1/1' && $post->type_post == 2 && $post->anounce_new_author == 0}
<div class="item long dark" style="background-color: #{$post->tags->color}">

    {include file="include/iconTagFirstLevel.tpl"}

    <div class="visual right absolute">
        <div class="bg"
             style="background-position: 0 0; background-image: url('files/blogposts/{$post->id}-1_1_c.jpg');">
        </div>
    </div>
    <div class="v-align">
        <div class="align-holder">
            <p>
                {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
                    <a href="/case-1.html" target="_blank">
                        {$post->citateText}
                    </a>
                {else}
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        {$post->citateText}
                    </a>
                {/if}
            </p>
        </div>
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

{* 1/1 новая цитата (с блоком-анонсом в статье) *}
{if $post->blocks == '1/1' && $post->type_post == 9 && $post->anounce_new_author == 0}
<div class="item long dark" style="background-color: #{$post->tags->color}">

    {include file="include/iconTagFirstLevel.tpl"}

    <div class="visual right absolute">
        <div class="bg" style="background-position: 0 0; background-image: url(/files/blogposts/{$post->id}-1_1.jpg);"></div>
    </div>
    <div class="v-align">
        <div class="align-holder">
            <p>
                {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
                    <a href="/case-1.html" target="_blank">
                        {$post->citateText}
                    </a>
                {else}
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        {$post->citateText}
                    </a>
                {/if}
            </p>
        </div>
    </div>
    {include file="include/tagsOnPostTile.tpl"}
    <span class="category-link" style="color: #000; text-transform: none;font-weight: bold; position: absolute; top: 15px; left: -15px; height: 20px;">
        <a href="/blog/{$post->tags->url}/{$post->url}">
            Цитата дня
        </a>
    </span>
</div>
{/if}

{* 1/1 фото дня *}
{if $post->blocks == '1/1' && $post->type_post == 7 && $post->anounce_new_author == 0}

    <div class="item big big--2">
        <div class="bg">
            <img src="/files/blogposts/{$post->id}-1_1.jpg" width="1141" height="505" alt="image description" />
        </div>
        <div class="info">
            <div class="special">
                <span>
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        Фото недели
                    </a>
                </span>
            </div>
            <strong class="h2">
                <a href="/blog/{$post->tags->url}/{$post->url}">
                    {$post->name}
                </a>
            </strong>
        </div>
        {include file="include/tagsOnPostTileOnlyDate.tpl"}
    </div>
{/if}

{* 1/1 анонс спецпроекта *}
{if $post->blocks == '1/1' && $post->type_post == 10 && $post->anounce_new_author == 0}
<div class="item big dark">
    <div class="bg" style="background-position: 0 0; background-image: url('/files/spec_project/{$post->specProjectImage}');"></div>
    <div class="v-align">
        <div class="align-holder">

            {include file="include/iconTagFirstLevel.tpl"}

            <a style="background-color: #{$post->specProjectColor};" href="{$post->specProjectUrl}"  target="_blank">
                <strong class="h1">{$post->specProjectName}</strong>
                <p class="d">{$post->specProjectHeader}</p>
            </a>
        </div>
    </div>
    {include file="include/tagsOnPostTileOnlyDate.tpl"}
</div>
{/if}