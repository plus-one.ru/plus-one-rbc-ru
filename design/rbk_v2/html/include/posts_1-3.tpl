<!-- 1/3 -->
{if $post->blocks == '1/3' && $post->type_post == 1 && $post->anounce_new_author == 0}
<div class="item trio gray">

    {include file="include/iconTagFirstLevel.tpl"}

    <div class="visual">
        <div class="bg">
            <a href="/blog/{$post->tags->url}/{$post->url}">
                <img src="/files/blogposts/{$post->id}-1_3.jpg" alt="image description"/>
            </a>
        </div>
    </div>
    <div class="info">
        {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
            <a href="case-1.html">
                <strong class="h2">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {else}
            <a href="/blog/{$post->tags->url}/{$post->url}">
                <strong class="h2">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {/if}
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

<!-- 1/3 цитата -->
{if $post->blocks == '1/3' && $post->type_post == 2  && $post->anounce_new_author == 0}
<div class="item trio blue no-image" style="background-color: #{$post->tags->color}">

    {include file="include/iconTagFirstLevel.tpl"}

    <div class="info">
        {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
            <a href="case-1.html">
                <strong class="h2">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {else}
            <a href="/blog/{$post->tags->url}/{$post->url}">
                <strong class="h2">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {/if}
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

<!-- 1/3 анонс нового автора (блога) (цитата) -->
{if $post->blocks == '1/3' && $post->type_post == 2 && $post->anounce_new_author == 1}
<div class="item trio gray no-image">

    {include file="include/iconTagFirstLevel.tpl"}

    <div class="info">
        {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
            <a href="case-1.html">
                <strong class="h2">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {else}
            <a href="/blog/{$post->tags->url}/{$post->url}">
                <strong class="h2">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {/if}
    </div>
    {include file="include/tagsOnPostTile.tpl"}
</div>
{/if}

<!-- 1/3 данные /-->
{if $post->blocks == '1/3' && $post->type_post == 5  && $post->anounce_new_author == 0}
<div class="item trio green analytics" style="background-color: #{$post->tags->color}">
    <a class="category-link" href="#" onclick="return false;">Аналитика</a>
    <div class="info">
        <strong class="digit">
            {$post->amount_to_displaying}
        </strong>

        <span class="mln">{$post->signature_to_sum}</span>
        <span>{$post->text_body}</span>

        <div class="graph">
            {foreach item=analiticData from=$post->analitycData name=analiticData}
            <span style="height: {$analiticData->percent}%;">
                <em>{$analiticData->years}</em>
            </span>
            {/foreach}
        </div>
    </div>
    {include file="include/tagsOnPostTileWoDateWoPartner.tpl"}
</div>
{/if}

{* 1/3 факт дня *}
{if $post->blocks == '1/3' && $post->type_post == 8  && $post->anounce_new_author == 0}
    <div class="item trio {if $post->tags->color=="f9d606"}yellow{/if}{if $post->tags->color=="18dc46"}green{/if}{if $post->tags->color=="00bae9"}blue{/if} analytics fact">
        <a class="category-link" href="/blog/{$post->tags->url}/{$post->url}">
            Факт недели
        </a>
        <div class="info">
            {if $post->amount_to_displaying|count_characters >= 6}
                <strong class="digit" style="font-size: 38px; line-height: 50px">
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        {$post->amount_to_displaying}
                    </a>
                </strong>
            {else}
                <strong class="digit">
                    <a href="/blog/{$post->tags->url}/{$post->url}">
                        {$post->amount_to_displaying}
                    </a>
                </strong>
            {/if}


            <span class="mln">
            <a href="/blog/{$post->tags->url}/{$post->url}">
                {$post->signature_to_sum}
            </a>
        </span>
        <span>
            <a href="/blog/{$post->tags->url}/{$post->url}">
                {$post->text_body}
            </a>
        </span>
        </div>
        {include file="include/tagsOnPostTileOnlyTagsTwoLevel.tpl"}
    </div>
{/if}


{* 1/3 DIY *}
{if $post->blocks == '1/3' && $post->type_post == 11  && $post->anounce_new_author == 0}
    <div class="item trio black-diy">
        <a href="/blog/{$post->tags->url}/{$post->url}" class="category-link">
            КАРТОЧКА DIY
        </a>
        <div class="info">
            <div class="digit">
                <a href="/blog/{$post->tags->url}/{$post->url}">
                    {$post->name}
                </a>
            </div>
        </div>
        <img class="item-img-bg" src="files/blogposts/{$post->id}-1_3.jpg" alt="image description" width="531" height="396" />
    </div>
{/if}