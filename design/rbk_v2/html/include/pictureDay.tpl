<span class="title">Картина дня</span>
{foreach item=item from=$pictureDay key=idx name=item}
    {if $idx == 0 || $idx == 1}
        <div class="col">
            <strong class="h3">
                <a href="/blog/{$item->tags->url}/{$item->url}">{$item->name}</a>
            </strong>
            <p>{$item->header}</p>
        </div>
    {/if}
{/foreach}

<div class="col">
{foreach item=item from=$pictureDay key=idx name=item}
    {if $idx > 1}
        <div class="short">
            <strong class="h4">
                <a href="/blog/{$item->tags->url}/{$item->url}">{$item->name}</a>
            </strong>
        </div>
    {/if}
{/foreach}
</div>


    {*{else}*}
        {*<div class="col">*}
            {*<div class="short">*}
                {*<strong class="h4">*}
                    {*<a ng-href="/blog/{$item->tags->url}/{$item->url}">{$item->name}</a>*}
                {*</strong>*}
            {*</div>*}
        {*</div>*}
    {*{/if}*}


    {*{$idx}==*}
{*<span ng-if="pictureDay" ng-repeat='pdItem in pictureDay'>*}
    {*<div class="col" ng-repeat='pd in pdItem|limitTo:2'>*}
        {*<strong class="h3">*}
            {*<a href="/blog/{{pd.tags.url}}/{{pd.url}}" ng-bind-html="textDangerousSnippet(pd.name)" internal-link="/blog/{{pd.tags.url}}/{{pd.url}}"></a>*}
        {*</strong>*}
        {*<p ng-bind-html="textDangerousSnippet(pd.header)"></p>*}
    {*</div>*}
    {*<div class="col">*}
        {*<div class="short" ng-repeat='pd in pdItem|limitTo: (2 - pdItem.length)'>*}
            {*<strong class="h4">*}
                {*<a ng-href="/blog/{{pd.tags.url}}/{{pd.url}}" ng-bind-html="textDangerousSnippet(pd.name)"></a></a>*}
            {*</strong>*}
        {*</div>*}
    {*</div>*}
{*</span>*}
{*{/foreach}*}