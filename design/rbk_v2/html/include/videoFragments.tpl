<div class="fix_block">
    <div class="fix_left">
        <a onclick="goBack({$videoFragmentsMainVideo->parent_video_post})">
            <img src="design/rbk_v2/images/prev.png"  alt="prev"/>
            <p>Назад</p>
        </a>
    </div>
    <div class="fix_right">
        <img src="design/rbk_v2/images/detail.png" alt="detail" />
    </div>
</div>
<div class="video_item">
    <div class="left_item">
        <img src="/files/blogposts/{$videoFragmentsMainVideo->image}" class="video_prev" alt="video_prev" />
        <img src="/design/rbk_v2/images/play.png" class="active_video" alt="active_video" />
    </div>
    <div class="right_item">
        <p>{$videoFragmentsMainVideo->name}</p>
    </div>
</div>
{foreach item=fragment from=$videoFragments name=fragment}
    <div class="item">
        <p onclick="loadVideoFragment('{$videoFragmentsMainVideo->video_url}', {$fragment->minute}, {$fragment->secunde}, {$videoFragmentsMainVideo->parent_video_post})">
            {$fragment->name}
        </p>
    </div>
{/foreach}