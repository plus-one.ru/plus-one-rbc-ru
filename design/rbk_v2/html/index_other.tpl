<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width">

    <title>{$title|escape}</title>
    <meta name="description" content="{$description|escape}" />
    <meta name="keywords" content="{$keywords|escape}" />
    <meta name="title" content="{$title|escape}" />
    <base href="/">

    <meta property="og:title" content="{$title|escape}" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{$description|escape}" />
    <meta property="og:image" content="http://{$root_url}/{$ogImage}" />

    <meta property="og:image" content="http://{$root_url}/{$ogImage_1_1}" />
    <meta property="og:image" content="http://{$root_url}/{$ogImage_1_1_c}" />
    <meta property="og:image" content="http://{$root_url}/{$ogImage_1_2}" />
    <meta property="og:image" content="http://{$root_url}/{$ogImage_1_3}" />

    <meta property="og:site_name" content="{$ogSiteName|escape}" />
    <meta property="og:url" content="http://{$root_url}/{$ogUrl}" />


    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{$title|escape}" />
    <meta name="twitter:description" content="{$description|escape}" />
    <meta name="twitter:url" content="http://{$root_url}/{$ogUrl}" />
    <meta name="twitter:image" content="http://{$root_url}/{$ogImage}" />
    <meta name="twitter:image:alt" content="{$title|escape}" />
    <meta name="twitter:site" content="{$ogSiteName|escape}" />

    <link rel="image_src" href="http://{$root_url}/{$ogImage}">

    <link rel="apple-touch-icon" sizes="57x57" href="/design/plusone/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/design/plusone/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/design/plusone/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/design/plusone/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/design/plusone/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/design/plusone/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/design/plusone/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/design/plusone/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/design/plusone/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/design/plusone/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/design/plusone/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/design/plusone/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/design/plusone/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/design/plusone/images/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/design/plusone/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="/design/plusone/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/design/plusone/js/jquery.plugins.js"></script>
    <script type="text/javascript" src="/design/plusone/js/jquery.jscroll.js?{$smarty.now}"></script>
    <script type="text/javascript" src="/design/plusone/js/jquery.mainScroll.js?{$smarty.now}"></script>
    <script type="text/javascript" src="/design/plusone/js/jquery.main.js?{$smarty.now}"></script>

    {* ADFox *}
    <script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>

    <link media="all" rel="stylesheet" href="/design/plusone/css/header.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone/css/main.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone/css/fotorama.css?{$smarty.now}">

    {literal}
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-NS6CBZH');</script>
        <!-- End Google Tag Manager -->
    {/literal}

    {include file="include/counterFb.tpl"}

</head>
<body>

    {include file="include/counters.tpl"}

    <div id="wrapper">

        {include file="include/topHeading.tpl"}

        <div class="container">

            {include file="banners/980x90.tpl"}

            <nav class="category-details">
                {include file="include/menu.tpl"}
            </nav>
        </div>
        <div class="container">

            {include file="banners/1140x290.tpl"}

            <main id="main">
                <div class="posts">
                    <div div class="items" style="width: calc(100% + 24px);">
                        {$content}
                    </div>
                </div>
            </main>
        </div>
    </div>

    <input type="hidden" id="rubrikaUrl" value="{$rubrikaUrl}">
    <input type="hidden" id="typeUrl" value="{$typeUrl}">

    {include file="include/retargetingСode.tpl"}

</body>
</html>