<main id="main">
    <div class="posts">

        <div div class="items" style="width: calc(100% + 24px);">
            {foreach item=item from=$items name=item}
                {foreach item=p from=$item name=p}
                    {foreach item=post from=$p name=post}
                        {if $post->blocks == '1/1'}
                            {include file="include/posts_1-1.tpl"}
                        {/if}
                        {if $post->blocks == '1/2'}
                            {include file="include/posts_1-2.tpl"}
                        {/if}
                        {if $post->blocks == '1/3'}
                            {include file="include/posts_1-3.tpl"}
                        {/if}
                    {/foreach}
                {/foreach}
            {/foreach}
            <div style="display: none">

                {if $tagUrl}
                    {if $page}
                        <a href="api/getposttags/{$rubrika}/{$tagUrl}/{$page}">next page</a>
                    {else}
                        <a href="api/posttags/{$rubrika}/{$tagUrl}/0">next page</a>
                    {/if}
                {elseif $writerId}
                    {if $page}
                        <a href="api/getpostauthor/{$writerId}/{$page}">next page</a>
                    {else}
                        <a href="api/getpostauthor/{$writerId}/0">next page</a>
                    {/if}
                {else}
                    {if $page}
                        <a href="blogs/{$rubrika}/{$page}">next page</a>
                        {*<a href="api/getpost/{$rubrika}/{$page}">next page</a>*}
                    {else}
                        <a href="blogs/main/1">next page</a>
                        {*<a href="api/getpost/main/0">next page</a>*}
                    {/if}
                {/if}

            </div>
        </div>
    </div>
</main>


