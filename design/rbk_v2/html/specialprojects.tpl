
<h2 style="margin: 25px 0;">Спецпроекты</h2>

    <div class="posts">
        <div class="items">
        {foreach item=sp from=$specialProjects name=sp}
        <div class="item big dark">
            <div class="bg" style="background-position: 0 0; background-image: url('/files/spec_project/{$sp->id}.jpg');"></div>
            <div class="v-align">
                <div class="align-holder">
                    <a href="{$sp->external_url}" target="_blank">
                        <strong class="h1">{$sp->name}</strong>
                        <p class="d">{$sp->header}</p>
                    </a>
                </div>
            </div>
        </div>
        {/foreach}
        </div>
    </div>
