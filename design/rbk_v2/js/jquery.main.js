jQuery(document).ready(function($) {
	initFixed();
	//initBgImage();
	initNavigation();
    setTimeout(initVideoPlayer, 1000);

	var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
	if (mac) $('body').addClass('mac');
});

$(window).load(function() {
	if($('.fotorama').size()) initGallery();
});

function initFixed() {
	$(window).scroll(function(event) {
		if($(window).scrollTop() >= 50) {
			$('#header').addClass('fixed');
			$('body').removeClass('navigation');
		} else $('#header').removeClass('fixed');
	});
}

function initNavigation() {
	$(document).on('click', '.icon-menu', function(event) {
		event.preventDefault();
		$('body').toggleClass('navigation');
	});
	$(document).on('click touchstart', function(event) {
		if ($(event.target).closest('.icon-menu').length || $(event.target).closest('#nav').length) return;
		$('body').removeClass('navigation');
		event.stopPropagation();
	});
}

function initGallery() {
	$('.fotorama').each(function(index, el) {
		var _this = $(this),
			_parent = _this.closest('.fotorama-holder');
		var fotorama = $(this).data('fotorama');
		_this.on('fotorama:showend ', function(e, fotorama) {
			$('.numbering', _parent).html((fotorama.activeIndex + 1) + '/' + fotorama.size);
			var _cotent = $('.fotorama__active .fotorama__html div p', _parent).html();
			$('.info p', _parent).html(_cotent);
		}).fotorama({
			nav: 'thumbs',
			width: 792,
			height: 450,
			fit: 'cover',
			loop: true,
			margin: 0,
			arrows: true,
			arrows: false,
			thumbwidth: 72,
			thumbheight: 55,
			thumbmargin: 3,
			allowfullscreen: true
		});
		$('.numbering', _parent).html((fotorama.activeIndex + 1) + '/' + fotorama.size);
		var _cotent = $('.fotorama__active .fotorama__html div p', _parent).html();
		$('.info p', _parent).html(_cotent);
	});

	$('.fotorama__wrap').addClass('fotorama__wrap--no-controls');

	$('.fotorama-holder .next').click(function(event) {
		event.preventDefault();
		var fotorama = $(this).closest('.fotorama-holder').find('.fotorama').data('fotorama');
		fotorama.show('>');
	});
	$('.fotorama-holder .prev').click(function(event) {
		event.preventDefault();
		var fotorama = $(this).closest('.fotorama-holder').find('.fotorama').data('fotorama');
		fotorama.show('<');
	});
}

function initBgImage() {
	$('.bg').each(function(index, el) {
		var _src = $('> img', this).attr('src');
		$('> img', this).hide();
		$(this).css('background-image', 'url(' + _src + ')');
	});
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {
	37: 1,
	38: 1,
	39: 1,
	40: 1
};

function preventDefault(e) {
	e = e || window.event;
	if (e.preventDefault)
		e.preventDefault();
	e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
	if (keys[e.keyCode]) {
		preventDefault(e);
		return false;
	}
}

function disableScroll() {
	if (window.addEventListener) // older FF
		window.addEventListener('DOMMouseScroll', preventDefault, false);
	window.onwheel = preventDefault; // modern standard
	window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
	window.ontouchmove = preventDefault; // mobile
	document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
	if (window.removeEventListener)
		window.removeEventListener('DOMMouseScroll', preventDefault, false);
	window.onmousewheel = document.onmousewheel = null;
	window.onwheel = null;
	window.ontouchmove = null;
	document.onkeydown = null;
}


function initVideoPlayer(){

	var videoCode = "";
	var videoBoxId = 0;

	$('.initialVideoUrl').each(function(){
		console.log($(this).val() + $(this).data('videoid'));

		videoCode = $(this).val();
		videoBoxId = $(this).data('videoid');
		loadVideo(videoCode, false, videoBoxId);
	})
}

function loadVideo(videoCode, autoPlay, videoBoxId) {
	console.log(videoBoxId);

	$("#mainvideoplayer_" + videoBoxId).tubeplayer("destroy");

	$("#mainvideoplayer_" + videoBoxId).tubeplayer({
		initialVideo: videoCode,
		width: 698,
		height: 392,
		autoPlay: autoPlay,
		annotations: false,
		color: "white",
	});
}

function showVideoFragment(videoId, videoBoxId){

	$.ajax({
		url: "/ajax/getVideoFragments.php",
		data: {videoId:videoId},
		type: "POST",
		success: function(data)
		{
			$("#videoFragments_" + videoBoxId).html(data);
			$("#videoList_" + videoBoxId).hide();
			$("#videoFragments_" + videoBoxId).show();
		}
	});
	return false;
}

function loadVideoFragment(videoCode, min, sec, videoBoxId){
	var minToSec = min * 60;
	var start = (minToSec * 1) + (sec * 1);

	$("#mainvideoplayer_" + videoBoxId).tubeplayer("play", {id: videoCode, time: start});
}

function goBack(videoBoxId){
	$("#videoFragments_" + videoBoxId).html('');
	$("#videoFragments_" + videoBoxId).hide();
	$("#videoList_" + videoBoxId).show();
}


jQuery(function(){
    jQuery(document).keydown(function (e) {

        if ((e.ctrlKey && e.keyCode == 13) || (e.metaKey && e.keyCode == 13)) {
            e.preventDefault();
            var text = "";
            if (window.getSelection) {
                text = window.getSelection().toString();
            } else if (document.selection && document.selection.type != "Control") {
                text = document.selection.createRange().text;
            }


            $("#selectedtext").html(text);

            if (text != "" && text != " "){
                $.fancybox.open({
                    src  : '#lightbox',
                    type : 'inline',
                    opts : {
                        afterShow : function( instance, current ) {
                            console.info( 'done!' );
                        }
                    }
                });
            }
        }
    });
});

function sendError()
{
    var text = $("#selectedtext").html();
    var message = $("#lightbox__form").val();

    if(text!==''){
        jQuery.ajax({
            url: '/ajax/findingErrorText.php',
            type: 'post',
            data: {'text': text, 'message': message, 'url': window.location.href}
        });

        $.fancybox.close();
    }
}

var show_molly_widget=function(){
    document.write('<div class="report-error">Нашли&nbsp;опечатку? Выделите&nbsp;ее&nbsp;и&nbsp;нажмите <span>Ctrl/Cmd&nbsp;+&nbsp;Enter</span></div>');
}

function subscribe()
{
    var email = $("#email").val();

    if(email != '') {
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if(pattern.test(email)){
            jQuery.ajax({
                url: '/ajax/subscribe.php',
                type: 'post',
                data: {'email': email}
            });

            $(".newsletter-form__holder").hide();
            $(".success-text-box").show();


        } else {
            $(this).css({'border' : '1px solid #ff0000'});
            $('#valid').text('Не верно');
        }
    } else {
        $(this).css({'border' : '1px solid #ff0000'});
        $('#valid').text('Поле email не должно быть пустым');
    }

    return false;
}