jQuery(document).ready(function($) {
    initScroll();
});

function initScroll(){
    $('.scroll').jscroll({
        loadingHtml: "<div class='loader'></div>",
        padding: 1900,
    });
}