$(document).ready(function($) {
	initBgImage();
	var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
	if (mac) {
		$('body').addClass('mac');
	}

    $('#main').css({
        'height': window.innerHeight - 120,
    });


    // определяем куда поудет запрос на вывод статей
    //var urls = document.location.href.split('/');
    //
    //var type = urls[urls.length - 2];
    //var link = urls[urls.length - 1];
    //
    //if (urls.length <= 4){
    //    var rubrika = 'main';
    //    type = 'blog';
    //}
    //else{
    //    var rubrika = link;
    //}
    //var pagenum = 0;
    //
    //var used_block = $(".used_blocks:last").html();
    //
    //$.ajax({
    //    url: "ajax/getPosts.php",
    //    data: {pagenum:pagenum, rubrika:rubrika, user_block:used_block, type:type},
    //    type: "POST",
    //    dataType: 'json',
    //    success: function(data)
    //    {
    //        $("#ajax-data-posts").html(data);
    //        initBgImage();
    //        iniHeight();
    //    }
    //});
});


//$(window).scroll(function(){
//    if ($(window).scrollTop() == $(document).height() - $(window).height()){
//        if($(".pagenum:last").val() < $(".total-page").val()) {
//
//            // определяем куда поудет запрос на вывод статей
//            var urls = document.location.href.split('/');
//
//            var type = urls[urls.length - 2];
//            var link = urls[urls.length - 1];
//
//            if (urls.length <= 4){
//                var rubrika = 'main';
//                type = 'blog';
//            }
//            else{
//                var rubrika = link;
//            }
//
//            var pagenum = parseInt($(".pagenum:last").val());
//            var used_block = $(".used_blocks:last").html();
//
//            $.ajax({
//                url: "ajax/getPosts.php",
//                data: {pagenum:pagenum, rubrika:rubrika, used_block:used_block},
//                type: "POST",
//                dataType: 'json',
//                success: function(data)
//                {
//                    $(".loader").hide();
//                    $("#ajax-data-posts").append(data);
//                    initBgImage();
//                    //iniHeight();
//                },
//                beforeSend: function(){
//                    $(".loader").show();
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                    $(".loader").hide();
//                    alert(xhr.status);
//                    alert(thrownError);
//                }
//
//            });
//        }
//    }
//});


$(window).load(function() {
	iniHeight();
});

function iniHeight() {
	$('.item').matchHeight();
}

function initBgImage() {
	$('.bg').each(function(index, el) {
		var _src = $('> img', this).attr('src');
		if (typeof _src !== 'undefined'){
            $('> img', this).hide();
            $(this).css('background-image', 'url(' + _src + ')');
        }
	});
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {
	37: 1,
	38: 1,
	39: 1,
	40: 1
};

function preventDefault(e) {
	e = e || window.event;
	if (e.preventDefault)
		e.preventDefault();
	e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
	if (keys[e.keyCode]) {
		preventDefault(e);
		return false;
	}
}

function disableScroll() {
	if (window.addEventListener) // older FF
		window.addEventListener('DOMMouseScroll', preventDefault, false);
	window.onwheel = preventDefault; // modern standard
	window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
	window.ontouchmove = preventDefault; // mobile
	document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
	if (window.removeEventListener)
		window.removeEventListener('DOMMouseScroll', preventDefault, false);
	window.onmousewheel = document.onmousewheel = null;
	window.onwheel = null;
	window.ontouchmove = null;
	document.onkeydown = null;
}

function getresult(url) {
    $.ajax({
        url: url,
        type: "GET",
        data:  {rowcount:$("#rowcount").val()},
        beforeSend: function(){
            $('#loader-icon').show();
        },
        complete: function(){
            $('#loader-icon').hide();
        },
        success: function(data){
            $("#faq-result").append(data);
        },
        error: function(){}
    });
}

$(function(){
    setTimeout(function(){
        ga("send", "event", "page", "spent_on_page_15_sec");
    }, 15*1000);
});