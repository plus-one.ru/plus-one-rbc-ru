


{*<div class="plusoneAboutLogo" style="background: url('/design/rbk_v3/img/plusOneLogo_black.svg') no-repeat 0 0"></div>*}


<h1 class="rbc_tape">
    {$about->header}
</h1>

<div class="rbcParagraphW about">
    {$about->body}
</div>

{*<h1 class="rbc_tape">*}
    {*О проекте*}
{*</h1>*}

{*<div class="rbcParagraphW about">*}
    {*<p>+1&nbsp;— коммуникационный проект, рассказывающий о&nbsp;лидерских практиках в&nbsp;области социальной и&nbsp;экологической ответственности. Мы&nbsp;сотрудничаем с&nbsp;крупнейшими медиаплощадками России ТАСС, РБК, «Ведомости» и&nbsp;Forbes, на&nbsp;которых мы&nbsp;представлены как <a href="http://tass.ru/plus-one" target="_blank" rel="noopener noreferrer" title="ТАСС +1">tass.ru/plus-one</a>, <a href="http://plus-one.rbc.ru/" target="_blank" rel="noopener noreferrer" title="РБК +1">plus-one.rbc.ru</a>, <a href="https://plus-one.vedomosti.ru/" target="_blank" rel="noopener noreferrer" title="Ведомости +1">plus-one.vedomosti.ru</a> и <a href="http://www.forbes.ru/native/plus-one" target="_blank" rel="noopener noreferrer" title="Forbes +1">forbes.ru/native/plus-one</a>.&nbsp;+1&nbsp;объединяет бизнес, некоммерческие организации, социальных предпринимателей и&nbsp;городские сообщества с&nbsp;целью повысить информационную прозрачность и&nbsp;увеличить привлекательность рынка ответственных инвестиций.</p>*}
    {*<p>Платформа +1&nbsp;— агрегатор рынка устойчивого развития. Мы&nbsp;создаем площадку для прямой коммуникации и&nbsp;обмена ресурсами между бизнесом, НКО, государством и&nbsp;обществом. В&nbsp;формате блогов участники проекта рассказывают о&nbsp;лучших практиках экологических и&nbsp;социальных преобразований. Мы&nbsp;помогаем формировать лояльность всех заинтересованных сторон посредством разделения общих ценностей и&nbsp;вовлечения в&nbsp;совместную деятельность.</p>*}
    {*<p>Миссия Проекта +1&nbsp;— содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы&nbsp;рассказываем о&nbsp;них массовой аудитории и&nbsp;формируем культуру ответственного производства, потребления и&nbsp;инвестирования.</p>*}
    {*<p>Партнеры: Unilever, Citibank, PepsiCo, QIWI, Amway, «Вымпелком», «Норильский никель», ГК&nbsp;«Сегежа», «Оптиком», WWF, «Вера», «Подари жизнь», «Игры будущего».</p>*}
    {*<p></p>*}
    {*<p>Контакты:</p>*}
    {*<p>Мероприятия +1<span>&nbsp;</span><span>—&nbsp;</span>Вероника Васильева<br><a href="mailto:v.vasilyeva@plus-one.ru" title="Вероника Васильева">v.vasilyeva@plus-one.ru</a></p>*}
    {*<p>Редакция +1 <span>—&nbsp;</span>Евгений Арсюхин<br><a href="mailto:e.arsyuhin@plus-one.ru" title="Евгений Арсюхин">e.arsyuhin@plus-one.ru</a></p>*}
    {*<p>Клиентский отдел +1&nbsp;<span>—&nbsp;</span>Наталия Павлова<br><a href="mailto:n.pavlova@plus-one.ru" title="Наталия Павлова">n.pavlova@plus-one.ru</a></p>*}
    {*<p></p>*}
{*</div>*}

                            
           