
<div class="error_404_plusOneWrapper">
    <div class="error_plusOne_logo">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 161.65 91.02">
            <defs>
                <clippath id="clip-path">
                    <path class="cls-1" d="M132.18,62.83h-24V55.28h7.65v-15c0-1.36,0-2.77,0-2.77h-.1a6.76,6.76,0,0,1-1.41,1.91l-2.18,2L107.16,36l9.72-9.12h7.59V55.28h7.7Zm-105-22H40v-14h8.53v14H61.39v8.16H48.53V62.87H40V48.94H27.14ZM88.43,77.85h62.46a1.81,1.81,0,0,0,1.81-1.8V13.61a1.81,1.81,0,0,0-1.81-1.8H88.43a1.81,1.81,0,0,0-1.8,1.8V76.05A1.81,1.81,0,0,0,88.43,77.85ZM44.27,9h0A35.32,35.32,0,0,0,9,44.27v2.48a35.32,35.32,0,0,0,70.63,0h0V44.27A35.31,35.31,0,0,0,44.27,9Z"/>
                </clippath>
            </defs>
            <g id="Layer_2" data-name="Layer 2">
                <g id="background">
                    <g class="cls-2">
                        <rect class="logoBg" width="161.65" height="91.02"/>
                    </g>
                </g>
            </g>
        </svg>
    </div>
    <p class="error_plusOne_numb">404</p>
    <img src="/design/rbk_v3/img/404.png" alt="ошибка" class="error_404_bg">

</div>