<h1 class="plusOneHeader">Лидеры</h1>

{foreach item=authorObject from=$authors name=authorObject}
<div class="plusOneListPosW">
    <div class="plusOneList {$rubrikaUrl}">
        <a href="/author/{$authorObject->id}">
            <p class="plusOneList_title">
                {$authorObject->name}
            </p>
        </a>
        <div class="plusOneListCount">
            <p class="plusOneListCount_numb">{$authorObject->countPost}</p>
            <p class="plusOneListCount_descr">{$authorObject->countPostText}</p>
        </div>
    </div>
</div>
{/foreach}