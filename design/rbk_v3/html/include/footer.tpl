<footer class="rbcFooter">
    <div class="container">
        <div class="rbcFooterTop">
            <a href="./">
                <div class="plusOneLogo_footer">

                </div>
            </a>
        </div>
        <div class="rbcFooterBtm">
            <ul class="rbcFnav">
                <li>
                    <a href="#" class="rbcFnavLink_header">Редакция</a>
                </li>
                <li>
                    <a href="/about/" class="rbcFnavLink">О проекте</a>
                </li>
            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Площадки</p>
                </li>
                <li>
                    <a href="https://plus-one.ru/" target="_blank" class="rbcFnavLink rbcFnavLink--borderWhite">+1</a>
                </li>
                <!-- <li>
                    <a href="https://tass.ru/plus-one" target="_blank" class="rbcFnavLink rbcFnavLink--borderYlw">+1 ТАСС</a>
                </li> -->
                <li>
                    <a href="https://plus-one.forbes.ru" target="_blank" class="rbcFnavLink rbcFnavLink--borderBlue">Forbes+1</a >
                </li>
                <li>
                    <a href="https://plus-one.vedomosti.ru/" target="_blank" class="rbcFnavLink rbcFnavLink--borderWhite">Ведомости+1</a >
                </li>
                <li>
                    <a href="https://award.plus-one.ru" target="_blank" class="rbcFnavLink rbcFnavLink--borderOrange">+1Премия</a >
                </li>

            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Рубрикатор</p>
                </li>
                <li>
                    <a href="/ecology/" class="rbcFnavLink">Экология</a>
                </li>
                <li>
                    <a href="/economy/" class="rbcFnavLink">Экономика</a>
                </li>
                <li>
                    <a href="/society/" class="rbcFnavLink">Общество</a>
                </li>
            </ul>

            <ul class="rbcFnav">
                <li>
                    <a href="#" class="rbcFnavLink_header">Другие проекты</a>
                </li>
                <li>
                    <a href="https://полезныйгород.рф" target="_blank" class="rbcFnavLink">+1Город</a>
                </li>
                <li>
                    <a href="https://platform.plus-one.ru" target="_blank" class="rbcFnavLink">+1Platform</a>
                </li>
                <li>
                    <a href="https://people.plus-one.ru" target="_blank" class="rbcFnavLink">+1Люди</a>
                </li>
            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Соцсети</p>
                </li>
{*                <li>*}
{*                    <a href="https://www.facebook.com/ProjectPlus1Official/" target="_blank" class="rbcFnavLink">Facebook</a>*}
{*                </li>*}
                <li>
                    <a href="https://vk.com/project_plus_one" target="_blank" class="rbcFnavLink">ВКонтакте</a>
                </li>
                <li>
                    <a href="https://twitter.com/project_plusone" target="_blank" class="rbcFnavLink">Twitter</a>
                </li>
{*                <li>*}
{*                    <a href="https://www.instagram.com/project_plus_one" target="_blank" class="rbcFnavLink">Instagram</a>*}
{*                </li>*}
                <li>
                    <a href="https://www.youtube.com/c/Plusoneru" target="_blank" class="rbcFnavLink">Youtube</a>
                </li>
                <li>
                    <a href="https://t.me/project_plus_one" target="_blank" class="rbcFnavLink">Telegram</a>
                </li>
            </ul>
        </div>

        <div class="rbcFooterExtra">
            <div>
                <span class="rbcFnavLink--pale">Реклама ООО</span> <a href="https://plus-one.ru/?erid=4CQwVszH9pSXLYxATjK" class="rbcFnavLink rbcFnavLink--pale" title="ООО «Один за всех»" target="_blank"><span class="rbcFnavLink--underline">«Один за всех»</span></a>
            </div>

            <div class="rbcFooterCopy">
                <a href="#" class="rbcFnavLink_date">© 2016-{$smarty.now|date_format:"%Y"}</a>
            </div>
        </div>
    </div>
</footer>

