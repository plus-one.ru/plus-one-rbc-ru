<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<base href="/">

<title>{$title|escape}</title>
{include file="include/blog_meta_tags.tpl"}
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/fonts.css'}"/>
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/main.css'}"  media="all"/>
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/footer.css'}" />
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/header.css'}" />
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/header_media.css'}" />
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/footer_media.css'}" />
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/media.css'}" />
<link rel="stylesheet" href="{assets url='/design/rbk_v3/css/fotorama.css'}" />
<link rel="stylesheet" href="{assets url='/design/rbk_v3/libs/scrollbar/scrollbar.css'}" media="all"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
<link rel="canonical" href="{$smarty.server.REQUEST_SCHEME}://{$smarty.server.HTTP_HOST}{$smarty.server.REDIRECT_URL}{$previewMode}" id="canonicalLink">

{include file="_gmt_head.tpl"}
