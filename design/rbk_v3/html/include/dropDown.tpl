<!-- Шторка -->
<div class="plusOneDropDown">
    <div class="container">
        {*<div class="plusOneDropDown">*}
        <div class="plusOneHambCloseW">
            <span class="plusOneHambClose"></span>
        </div>
        <ul class="headerNav">
            <li class="headerNav_item">
                <a href="/about/" class="headerNav_link">О проекте</a>
            </li>
            <li class="headerNav_item">
                <a href="https://полезныйгород.рф/" target="_blank" class="headerNav_link">+1Город </a>
            </li>
            <li class="headerNav_item">
                <a href="https://platform.plus-one.ru" target="_blank" class="headerNav_link">+1Платформа</a>
            </li>
            <li class="headerNav_item">
                <a href="https://people.plus-one.ru" target="_blank" class="headerNav_link">+1Люди</a>
            </li>
        </ul>
        <ul class="headerNav ">
            <li class="headerNav_item">
                <a href="https://plus-one.ru/" target="_blank" class="headerNav_link headerNav_link--white">+1 </a >
            </li>
            <!-- <li class="headerNav_item">
                <a href="https://tass.ru/plus-one" target="_blank" class="headerNav_link headerNav_link--yellow">+1 ТАСС</a >
            </li> -->
            <li class="headerNav_item">
                <a href="https://plus-one.forbes.ru" target="_blank" class="headerNav_link headerNav_link--blue">Forbes+1</a >
            </li>
            <li class="headerNav_item">
                <a href="https://plus-one.vedomosti.ru/" target="_blank" class="headerNav_link headerNav_link--white">Ведомости+1</a >
            </li>
            <li class="headerNav_item">
                <a href="https://award.plus-one.ru" target="_blank" class="headerNav_link headerNav_link--orange">+1Премия</a >
            </li>
        </ul>
        <ul class="headerNav">
            <li class="headerNav_item">
                <a href="https://vk.com/project_plus_one" target="_blank" class="headerNav_link">ВКонтакте</a>
            </li>
            <li class="headerNav_item">
                <a href="https://twitter.com/project_plusone" target="_blank" class="headerNav_link">Twitter</a>
            </li>
            <li class="headerNav_item">
                <a href="https://www.youtube.com/c/Plusoneru" target="_blank" class="headerNav_link">Youtube</a>
            </li>
            <li class="headerNav_item">
                <a href="https://t.me/project_plus_one" target="_blank" class="headerNav_link">Telegram</a>
            </li>
        </ul>
        {*</div>*}
    </div>
</div>
