<section class="quote1_1BlWrap {$post->frontClass} {$post->fontClass} scaleElem">
<a href="{$post->postUrl}" class="wrap-all"></a>
    <div class="quote1_1H">
        <a href="{$post->tagUrl}">
            <div class="quote1_1Week">
                <div class="quote1_1Week_logo">
                    {*<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 40 40" class="logoBl bgRed">*}
                        {*<g data-name="logo">*}
                            {*<g>*}
                                {*<path class="logoBl_bg" d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z" />*}
                                {*<path class="logoBl_elem" d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z" />*}
                            {*</g>*}
                        {*</g>*}
                    {*</svg>*}

                    {if $post->tagCode == 'ecology'}
                        {*ecology*}
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" class="logoBl bgRed">
                            <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                <g id="background">
                                    <path class="logoBl_bg" d="M30,15A15,15,0,1,1,15,0,15,15,0,0,1,30,15Z"/>
                                    <path class="logoBl_elem" d="M20.19,10.37c-1.79,0-4,1.93-5.16,3.12-1.19-1.19-3.38-3.12-5.17-3.12a4.63,4.63,0,1,0,0,9.25c1.79,0,4-1.93,5.17-3.12,1.18,1.19,3.37,3.12,5.16,3.12a4.63,4.63,0,1,0,0-9.25ZM9.86,17.56a2.57,2.57,0,0,1,0-5.13c1.53,0,2.54,1.35,3.75,2.57C12.4,16.21,11.32,17.56,9.86,17.56Zm10.33,0c-1.43,0-2.54-1.35-3.75-2.56,1.21-1.22,2.5-2.57,3.75-2.57a2.57,2.57,0,1,1,0,5.13Z"/>
                                </g>
                            </g>
                        </svg>
                    {/if}

                    {if $post->tagCode == 'society'}
                        {*community*}
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" class="logoBl bgRed">
                            <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                <g id="background">
                                    <path class="logoBl_bg" d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"></path>
                                    <path class="logoBl_elem" d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z"></path>

                                </g>
                            </g>
                        </svg>
                    {/if}

                    {if $post->tagCode == 'economy'}
                        {*community*}
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" class="logoBl bgRed">
                            <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                <g id="background">
                                    <circle class="cls-1" cx="15" cy="15" r="15"></circle>
                                    <polygon points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                    <circle cx="15" cy="15" r="15"></circle>
                                    <polygon class="logoBl_elem" points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                </g>
                            </g>
                        </svg>
                    {/if}
                </div>
                <div class="quote1_1Week_title">Цитата</div>
            </div>
        </a>
        {if $post->is_partner_material == 1}
            <div class="partner-material eco_taccBlock">
                <span class="partner-text eco_tacc">Партнерский материал</span>
                <span class="partner-text_mobile eco_tacc">Партнерский</span>
            </div>
        {else}
            <a href="" class="quote1_1H_today">{$post->dateStr}</a>
        {/if}
    </div>
    <div href="{$post->postUrl}" class="mainQuote1_1_wrap">
    <a href="{$post->postUrl}" class="wrap-all"></a>
        <blockquote class="mainQuote1_1">
            {$post->name}
        </blockquote>
        <div class="qoute1_1Descr">
            {$post->header}
        </div>
    </div>
</section>