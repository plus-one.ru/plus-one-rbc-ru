<script type="text/javascript">
    {literal}
    (function () {
        document.addEventListener("DOMContentLoaded", function (event) {
            oldCitateCorrector();
        });

        function oldCitateCorrector(container) {
            container = container ? container : document;
            // Фикс цитат когда в них есть ссылка
            function wrap(container) { // Оборачиваем содержимое цитаты в span
                let cite = container.querySelectorAll("blockquote .holder cite");
                for (let i = 0; i < cite.length; i++) {
                    if (cite[i].querySelector('.pic')) { continue; }

                    let nodeText = cite[i].innerHTML;
                    cite[i].innerHTML = `<span class="citeText-wrap">${nodeText}</span>`;
                }
            }

            wrap(container);


            function appendPicToCitate(container) {
                let blockQuote = container.querySelectorAll('.rbcDescr blockquote');
                for (let i = 0; i < blockQuote.length; i++) {
                    let pic = blockQuote[i].querySelector('.pic');
                    let cite = blockQuote[i].querySelector(".holder cite");
                    let authorText = blockQuote[i].querySelector(".holder cite .citeText-wrap");
                    let spanPic = document.createElement('span');
                    if (pic) {
                        let picImg = pic.querySelector('img');
                        picImg.setAttribute('alt', authorText.innerText);
                        picImg.removeAttribute('title');
                        spanPic.innerHTML = pic.innerHTML;
                        blockQuote[i].classList.add('citate-block');
                        spanPic.classList.add('pic');
                        cite.prepend(spanPic);
                        pic.remove();
                    }
                }
            }
            appendPicToCitate(container);
        }

        window._oldCitateCorrector = oldCitateCorrector;
    })();
    {/literal}
</script>