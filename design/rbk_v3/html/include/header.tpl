<div class="container">
    <nav class="rbcH">
        <div class="rbcNavSide">
            <div class="rbcNavSideLogoW">
                <a class="rbcNavSide_rbcHlogo" href="https://www.rbc.ru/" target="_blank"></a>
            </div>
            <div class="rbcNavElements">
                <ul class="rbcNavSide_rbcNavElWrapper rbc-js-list">
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="http://tv.rbc.ru/?utm_source=topline" target="_blank">Телеканал</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/newspaper/?utm_source=topline" target="_blank">Газета</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/magazine/?utm_source=topline" target="_blank">Журнал</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://plus.rbc.ru/?utm_source=topline" target="_blank">РБК+</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://pro.rbc.ru/?utm_source=topline" target="_blank">Pro</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://quote.rbc.ru/?utm_source=topline" target="_blank">Quote</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.autonews.ru/?utm_source=topline" target="_blank">Авто</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://sportrbc.ru/?utm_source=topline" target="_blank">Спорт</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://realty.rbc.ru/?utm_source=topline" target="_blank">Недвижимость</a>
                    </li> 
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="http://style.rbc.ru/?utm_source=topline" target="_blank">Стиль</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/crypto/?utm_source=topline" target="_blank">Крипто</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://marketing.rbc.ru/?utm_source=topline" target="_blank">Исследования</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/offers/main?utm_source=topline" target="_blank">Продажа бизнеса</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/franchises/main?utm_source=topline" target="_blank">Франшизы</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl " href="http://bc.rbc.ru/?utm_source=topline" target="_blank">Конференции</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards/?utm_source=topline" target="_blank">Премия РБК 2018</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards_spb/?utm_source=topline" target="_blank">Премия РБК СПб 2018</a>
                    </li>
                </ul>
                <div class="rbcNav_more_wrap">
                    <div class="rbcNav_more">
                        {*<p class="rbc_item">
                            <a class="rbcNavSide_rbcNavEl" href="http://health.rbc.ru/" target="_blank">Здоровье</a>
                        </p>*}
                        <p class="rbc_item">
                            <a class="rbcNavSide_rbcNavEl border_green" href="https://www.rbc.ru/trends/?utm_source=toppne" target="_blank">Зеленая экономика</a>
                        </p>
                    </div>
                    <div class="dots_dropdown">
                        <p class="dots_dropdown_btn">...</p>
                        <div class="dropdown_list">
                            <div class="dropdown_list_inner">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
             <div class="rbcNavSide_dropDown">
                <ul class="rbcNavSide_rbcNavElWrapper dropDown">
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="http://tv.rbc.ru/?utm_source=topline" target="_blank">Телеканал</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/newspaper/?utm_source=topline" target="_blank">Газета</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/magazine/?utm_source=topline" target="_blank">Журнал</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://plus.rbc.ru/?utm_source=topline" target="_blank">РБК+</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://pro.rbc.ru/?utm_source=topline" target="_blank">Pro</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://quote.rbc.ru/?utm_source=topline" target="_blank">Quote</a>
                    </li>
                      <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.autonews.ru/?utm_source=topline" target="_blank">Авто</a>
                    </li>
                      <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://sportrbc.ru/?utm_source=topline" target="_blank">Спорт</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://realty.rbc.ru/?utm_source=topline" target="_blank">Недвижимость</a>
                    </li>
                     <li>
                        <a class="rbcNavSide_rbcNavEl" href="http://style.rbc.ru/?utm_source=topline" target="_blank">Стиль</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/crypto/?utm_source=topline" target="_blank">Крипто</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://marketing.rbc.ru/?utm_source=topline" target="_blank">Исследования</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/offers/main?utm_source=topline" target="_blank">Продажа бизнеса</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/franchises/main?utm_source=topline" target="_blank">Франшизы</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://bc.rbc.ru/?utm_source=topline" target="_blank">Конференция</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards/?utm_source=topline" target="_blank">Премия РБК 2018</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards_spb/?utm_source=topline" target="_blank">Премия РБК СПБ 2018</a>
                    </li>
                    <li>
                        <a class="rbcNavSide_rbcNavEl border_grn" href="https://www.rbc.ru/trends/?utm_source=topline" target="_blank">Зеленая экономика</a>
                    </li>
                   
                    {*<li>
                        <a class="rbcNavSide_rbcNavEl border_grn" href="http://health.rbc.ru/" target="_blank">Здоровье</a>
                    </li>*}
                </ul>
            </div>

            <div class="rbcTopHHamb">
                <span class="navHamb"></span>
            </div>
        </div>
        <ul class="rbcNavSide">
          {* <li>
                <a class="rbcNavSide_rbcHEnter" href="https://auth.rbc.ru/login" target="_blank">Вход</a>
            </li>*}
            <li>
                <a class="rbcNavSide_rbcHSearch" href="https://www.rbc.ru/search/" target="_blank"></a>
            </li>
            <li class="rbcNavSide_humb_wrap">
                <p class="rbcNavSide_humb">
                <span class="humb_line"></span>
                </p>
            </li>
        </ul>
    </nav>
</div>