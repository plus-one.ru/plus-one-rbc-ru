    {if $customColors}
    {literal}
        <style>
            .insert-wrap {
                border-bottom-color: {/literal}{$blog->border_color}{literal}!important;
                border-top-color: {/literal}{$blog->border_color}{literal}!important;
            }

            .rbcDescr p a{
                border-bottom-color: {/literal}{$blog->border_color}{literal}!important;
            }

            .fotorama__active .fotorama__loaded--img::after {
                border: 4px solid;
                border-color: {/literal}{$blog->border_color}{literal}!important;
            }

            .customColor-{/literal}{$blog->id}{literal} .pattern{
                border-color: {/literal}{$blog->border_color}{literal}!important;
                color:{/literal}{$blog->font_color}{literal}!important;
            }

            -{/literal}{$blog->id}{literal} .circle_article{
                fill:{/literal}{$blog->font_color}{literal}!important;
            }
            .customColor-{/literal}{$blog->id}{literal} .polygon_article{
                fill:{/literal}{$blog->font_color}{literal}!important;
            }

            .customColor-{/literal}{$blog->id}{literal} .topic-item,
            .customColor-{/literal}{$blog->id}{literal} .pattern span,
            .customColor-{/literal}{$blog->id}{literal} strong{
                color:{/literal}{$blog->font_color}{literal}!important;
            }

            .customColor-{/literal}{$blog->id}{literal} p,
            .customColor-{/literal}{$blog->id}{literal} h1,
            .customColor-{/literal}{$blog->id}{literal} h2,
            .customColor-{/literal}{$blog->id}{literal} p a,
            .customColor-{/literal}{$blog->id}{literal} .rbc_tape,
            .customColor-{/literal}{$blog->id}{literal} .articleAuthor_h,
            .customColor-{/literal}{$blog->id}{literal} cite,
            .customColor-{/literal}{$blog->id}{literal} .topic-item span a,
            .customColor-{/literal}{$blog->id}{literal} p cite,
            .customColor-{/literal}{$blog->id}{literal} .cls-1,
            .customColor-{/literal}{$blog->id}{literal} .rbcTopic,
            .customColor-{/literal}{$blog->id}{literal} .numbering,
            .customColor-{/literal}{$blog->id}{literal} .dialog-block,
            .customColor-{/literal}{$blog->id}{literal} .rbcDescr h2,
            .customColor-{/literal}{$blog->id}{literal} .rbcDescr blockquote .holder cite,
            .customColor-{/literal}{$blog->id}{literal} .fotorama-holder .info p span,
            .customColor-{/literal}{$blog->id}{literal} figure figcaption span.name,
            .customColor-{/literal}{$blog->id}{literal} .rbcDescr figcaption,
            .customColor-{/literal}{$blog->id}{literal} .info-side > p a.bulletImg {
                color:{/literal}{$blog->font_color}{literal}!important;
            }

            .customColor-{/literal}{$blog->id}{literal} .info-side  {
                background: {/literal}{$blog->bodyColor}{literal}!important;
            }
            .customColor-{/literal}{$blog->id}{literal} .topic-wrap a, {
            .customColor-{/literal}{$blog->id}{literal} .topic-wrap a:hover, {
                border-bottom: none!important;
            }
            .customColor-{/literal}{$blog->id}{literal} a {
                color:{/literal}{$blog->font_color}{literal}!important;
                border-bottom: 2px solid {/literal}{$blog->border_color}{literal}!important;
            }
            .customColor-{/literal}{$blog->id}{literal} a:hover {
                color:{/literal}{$blog->font_color}{literal}!important;
                border-bottom-color: {/literal}{$blog->border_color}{literal}!important;
            }

            .mainWrapper--material .articleAuthor::before,
            .mainWrapper--special .articleAuthor::before {
                background-color: {/literal}{$blog->font_color}{literal}!important;
            }
        </style>
    {/literal}
    {/if}
