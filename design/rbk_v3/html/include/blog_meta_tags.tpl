<!-- Cache control with meta tags -->
<meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<meta name="title" content="{$blog->meta->title|escape}" />
<meta name="description" content="{if ($blog->meta->description)}{$blog->meta->description|escape}{else}{$descriptionDefault|escape}{/if}" />
<meta name="keywords" content="{$blog->meta->keywords|escape}" />

<meta property="og:title" content="{$ogTitle|escape}" />
<meta property="og:type" content="article" />
<meta property="og:description" content="{if ($blog->meta->description)}{$blog->meta->description|escape}{else}{$descriptionDefault|escape}{/if}" />
<meta property="og:image" content="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}" />
<meta property="og:site_name" content="{$ogSiteName|escape}" />
<meta property="og:url" content="{$root_url}/{$ogUrl}" />


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{$ogTitle|escape}" />
<meta name="twitter:description" content="{if ($blog->meta->description)}{$blog->meta->description|escape}{else}{$descriptionDefault|escape}{/if}" />
<meta name="twitter:url" content="{$root_url}/{$ogUrl}" />
<meta name="twitter:image" content="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}" />
<meta name="twitter:image:alt" content="{$blog->ogTitle|escape}" />
<meta name="twitter:site" content="{$ogSiteName|escape}" />

<link rel="image_src" href="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}">

<link rel="apple-touch-icon" sizes="57x57" href="/design/rbk_v3/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/design/rbk_v3/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/design/rbk_v3/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/design/rbk_v3/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/design/rbk_v3/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/design/rbk_v3/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/design/rbk_v3/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/design/rbk_v3/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/design/rbk_v3/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/design/rbk_v3/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/design/rbk_v3/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/design/rbk_v3/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/design/rbk_v3/img/favicon/favicon-16x16.png">
<link rel="manifest" href="/design/rbk_v3/img/favicon/manifest.json">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/design/rbk_v3/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
