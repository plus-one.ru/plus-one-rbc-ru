<div class="blocked-wrap">
  <div class="blocked-content">
    <h2 class="blocked-title">Упс...</h2>
    <div class="blocked-description">
      <p class="blocked-text"> Материал, который был опубликован по этой ссылке, перестал соответствовать требованиям
        российского законодательства, поэтому нам пришлось его удалить. </p>
      <p class="blocked-text"> Приносим извинения за доставленные неудобства </p>
    </div>
  </div>
</div>
