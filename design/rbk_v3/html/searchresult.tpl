<div class="search-items-block">
    {if $searchItems}
        {foreach item=si from=$searchItems name=si}
            <div class="search-item">
                {if $si->image_rss}
                    <a href="/blog/{$si->tags->url}/{$si->url}" class="search-item_img">
                        <img src="files/blogposts/{$si->image_rss}" alt="" width="270" height="180" />
                    </a>
                {/if}
                <div class="search-item_text">
                    <div class="search-item_title">
                        <a href="/blog/{$si->tags->url}/{$si->url}">
                            {$si->name}
                        </a>
                    </div>
                    <p>
                        {$si->lead}
                    </p>
                </div>
            </div>
        {/foreach}
    {else}
        <div class="search-item" style="border: none;">
            <div class="search-item_text">
                <div class="search-item_title">
                    Поиск не дал результатов.
                </div>
            </div>
        </div>
    {/if}
</div>