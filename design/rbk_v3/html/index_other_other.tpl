<!DOCTYPE html>
<html prefix= "og: http://ogp.me/ns#">
<head>
    {include file="include/head_meta.tpl"}
</head>
<body>

{include file="_gmt.tpl"}
    {*{include file="include/counters.tpl"}*}

    <div id="wrapper">

        {*{include file="include/topHeading.tpl"}*}

        <div class="container">

            {*{include file="banners/980x90.tpl"}*}

            {*<nav class="category-details">*}
                {*{include file="include/menu.tpl"}*}
            {*</nav>*}
        </div>
        <div class="container">

            {include file="banners/1140x290.tpl"}

            <main id="main">
                <div class="posts">
                    <div div class="items" style="width: calc(100% + 24px);">
                        {$content}
                    </div>
                </div>
            </main>
        </div>
    </div>

    <input type="hidden" id="rubrikaUrl" value="{$rubrikaUrl}">
    <input type="hidden" id="typeUrl" value="{$typeUrl}">

    {include file="include/retargetingСode.tpl"}

</body>
</html>
