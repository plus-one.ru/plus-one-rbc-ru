{include file="banners/300x600.tpl"}
<div class="clear-zone">



    <div class="text-block">
        {if $blog->header_on_page != ''}
            <strong class="h1">{$blog->header_on_page}</strong>
        {else}
            <strong class="h1">{$blog->name}</strong>
        {/if}
        <span>{$blog->lead}</span>

        <div class="meta">
            {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" ng-if="post.partner_name">
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
            {/if}

            <a class="{$blog->tags->url}" href="/blogs/{$blog->tags->url}">
                {$blog->tags->name}
            </a>
            <time datetime="{$blog->postDateStr}">{$blog->postDateStr}</time>

            <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
            <a href="{$urlToFavorite}" title="{$titleToFavorite}" class="btn-add-to-favorite" onclick="AddToBookmark(this);" rel="sidebar">Добавить в закладки</a>

        </div>


        <blockquote class="item {if $blog->tags->color=="f9d606"}yellow{/if}{if $blog->tags->color=="18dc46"}green{/if}{if $blog->tags->color=="00bae9"}blue{/if} valign blockquote-box">
            <span class="cat-name">ЦИТАТА</span>
            {if $blog->citateText|count_characters > 120}
                <div class="info info--2 info--long">
                    <p>{$blog->citateText}</p>
                </div>
            {else}
                <div class="info">
                    <p>{$blog->citateText}</p>
                </div>
            {/if}
            {*{if $blog->citate_author_name}
                <cite class="blockquote-box_cite">
                    <span class="blockquote-box_cite_img">
                        <img src="/files/blogposts/{$blog->id}-cauthor.jpg" alt="image description" width="70" height="70" />
                    </span>
                    <span class="blockquote-text">
                        {$blog->citate_author_name}
                    </span>
                </cite>
            {/if}*}
        </blockquote>

    </div>
</div>

<div class="text-block">
    {$blog->body}
</div>

<div class="text-block">
    <ul class="tags static">
        <li>
            <a href="/author/{$blog->writer_id}">
                {$blog->writer}
            </a>
        </li>
        {if $blog->postTags}
            {foreach item=postTag from=$blog->postTags name=postTag}
                <li>
                    <a href="/posttags/{$blog->tags->url}/{$postTag->url}">
                        {$postTag->name}
                    </a>
                </li>
            {/foreach}
        {/if}
    </ul>
</div>
