{if $blog->partnersPosts}
    <strong class="h2">Партнерские материалы</strong>
    <div class="items">
        {foreach item=partnersPost from=$blog->partnersPosts name=partnersPost}
        <div class="item trio gray" style="min-height: 564px;" ng-repeat='partnersPost in post.partnersPosts'>
            <a href="/blog/{$partnersPost->tag_url}" class="category-link white">
                <span class="icon {$partnersPost->tag_url}">
                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                        <title>{$partnersPost->tag_url}</title>
                        <circle cx="14.68" cy="14.68" r="14.68"/>
                        {if $partnersPost->tag_url == 'ecology'}
                            <path d="M19.71,10.19c-1.73,0-3.85,1.87-5,3-1.15-1.16-3.27-3-5-3a4.48,4.48,0,1,0,0,9c1.73,0,3.85-1.87,5-3,1.15,1.16,3.27,3,5,3a4.48,4.48,0,1,0,0-9Zm-10,7a2.48,2.48,0,1,1,0-5c1.48,0,2.46,1.31,3.63,2.48C12.16,15.85,11.12,17.16,9.7,17.16Zm10,0c-1.38,0-2.46-1.31-3.63-2.48,1.17-1.17,2.42-2.48,3.63-2.48a2.48,2.48,0,1,1,0,5Z"/>
                        {/if}
                        {if $partnersPost->tag_url == 'economy'}
                            <polygon points="24.18 11.22 22.75 9.79 15.55 16.99 10.38 11.82 5.37 16.84 6.8 18.26 10.38 14.68 15.55 19.85 24.18 11.22"/>
                        {/if}
                        {if $partnersPost->tag_url == 'society'}
                            <path d="M19.37,11.68a4.67,4.67,0,1,0-9.33,0c0,1.82,2,3.87,3.19,5L9,20.93l1.41,1.41L14.7,18,19,22.29l1.41-1.41-4.22-4.22C17.41,15.55,19.37,13.51,19.37,11.68Zm-4.61,3.56-.06-.06-.06.06C13.42,14.14,12,12.56,12,11.68a2.67,2.67,0,1,1,5.33,0C17.37,12.56,16,14.14,14.76,15.24Z"/>
                        {/if}
                    </svg>
                </span>
                {$partnersPost->tag_name}
            </a>


            <div class="visual">
                <div class="bg">
                    <a href="{$partnersPost->link}" target="_blank">
                        <img src="/files/blogposts/{$partnersPost->image}" alt="image description" />
                    </a>
                </div>
            </div>
            <div class="info">
                <a href="{$partnersPost->link}" target="_blank">
                    <strong class="h3">
                        {$partnersPost->header}
                    </strong>

                    <p class="d">
                        {$partnersPost->subheader}
                    </p>
                </a>
            </div>
            {if $partnersPost->partner_name}
            <ul class="tags">
                <li>
                    <a href="{$partnersPost->link}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1">
                        <div style="color: {$partnersPost->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                            &bull;
                            <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$partnersPost->partner_name}</span>
                        </div>
                    </a>
                </li>
            </ul>
            {/if}
        </div>
        {/foreach}
    </div>
{/if}