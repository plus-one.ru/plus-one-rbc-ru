<div id="{$blog->id}" data-url="{$blog->fullUrl}" class="post-item"  {if $bodyColor}style="background: {$bodyColor}"{/if} data-id="{$blog->id}">
{include file="include/_custom_colors.tpl"}
{if $blog->platformPost == 1}<p class="platform_h">МАТЕРИАЛ УЧАСТНИКА ПЛАТФОРМЫ +1</p>{/if}

<div class="secondContainer customColor-{$blog->id}">
    <div class="banner-over-topic">
        {include file="banners/banners.tpl"}
    </div>
    <div class="topic-wrap">
        <div class="topic-header">
            {if $bodyColor || $blog->spec_project}
                <div class="topic-item">СПЕЦПРОЕКТ {$blog->specProjectName}</div>
            {/if}
            <div class="topic-item">{$blog->dateStr}</div>
{*            <div class="topic-item">{$totalReadTime} {$totalReadTimeCaption} на чтение</div>*}
        </div>
        <div class="topic-tags-wrap">
            {if $blog->is_partner_material == 1}
                <div class="partner-material_inside eco_taccBlock">
                    <span class="partner-text_inside eco_tacc">Партнерский материал</span>
                </div>
            {/if}
            <a href="/{$blog->tags->url}">
                <div class="topic-tags__item topic-tags__item--{$blog->tags->url}">{$blog->tags->name}</div>
            </a>
            {foreach  item=postTag from=$blog->postTags name=postTag}
                <div class="topic-tags__item">{$postTag->name}</div>
            {/foreach}
        </div>
    </div>

    {if $blog->typePost == 'factday' || $blog->typePost == 'citate'}
        <h1 class="rbc_tape">{if $blog->header_on_page}{$blog->header_on_page}{else}{$blog->name}{/if}</h1>
    {elseif $blog->typePost == 'imageday' || $blog->typePost == 'diypost' || $blog->typePost == 'post' }
        <h1 class="rbc_tape">{if $blog->header_on_page}{$blog->header_on_page}{else}{$blog->name}{/if}</h1>
    {else}
        <h1 class="rbc_tape">{$blog->name}</h1>
    {/if}

    <span class="rbcDescr rbcDescr--lead">{$blog->lead}</span>

    <span class="rbcDescr">
        {$blog->body}
    </span>
    <div class="share-block">
        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,evernote,viber,whatsapp,skype,telegram"></div>
    </div>
</div>

{if $blog->relatedPosts | count }
<div class="container" style="margin-top: 100px; padding-bottom: 46px;">
    <div class="grid_flex">
        {foreach item=post from=$blog->relatedPosts name=post}
            {if $post->typePost == 'post'}
                {include file="include/blocks/post_1_3.tpl"}
            {elseif $post->typePost == 'factday'}
                {include file="include/blocks/factWeek_1_3.tpl"}
            {elseif $post->typePost == 'citate'}
                {include file="include/blocks/citate_1_3.tpl"}
            {elseif $post->typePost == 'diypost'}
                {include file="include/blocks/diy_1_3.tpl"}
            {else}
                {include file="include/blocks/post_1_3.tpl"}
            {/if}
        {/foreach}
    </div>
</div>
{/if}
</div>