{*<div class="clear-zone">*}

    <div class="text-block">

        <figure class="text-block_figure">
            <a href="#" class="text-block_figure_link">мем недели</a>
            <img src="/files/blogposts/{$blog->image_rss}" width="1014" height="676" alt="image description">
            {*<figcaption>Автор: Родион Тынстрем / ТАСС</figcaption>*}
        </figure>

        {include file="banners/300x600.tpl"}

        <strong class="h1">{$blog->name}</strong>
        <span>{$blog->lead}</span>
        <div class="meta">
            {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" ng-if="post.partner_name">
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
            {/if}
            <a class="{$blog->tags->url}" href="/blogs/{$blog->tags->url}">
                {$blog->tags->name}
            </a>
            <time datetime="{$blog->postDateStr}">{$blog->postDateStr}</time>

            <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
            <a href="{$urlToFavorite}" title="{$titleToFavorite}" class="btn-add-to-favorite" onclick="AddToBookmark(this);" rel="sidebar">Добавить в закладки</a>
        </div>
    </div>
    {*<div class="item big black" style="background-color: #{$blog->tags->color}" >*}
        {*<div class="visual left absolute">*}
            {*<div class="bg">*}
                {*<img src="/files/blogposts/{$blog->id}-1_1.jpg" width="1141" height="505" alt="image description" />*}
            {*</div>*}

            {*<ul class="tags">*}
                {*<li>*}
                    {*<a href="/author/{$blog->writer_id}">*}
                        {*{$blog->writer}*}
                    {*</a>*}
                {*</li>*}
            {*</ul>*}
        {*</div>*}
        {*<div class="info" style="width: 420px;">*}
            {*<strong class="type" style="color: #000;">Фото дня</strong>*}
            {*<h2 style="font-size: 21pt;line-height: 26.8pt; ">*}
                {*<span style="color: #000;">{$blog->name}</span>*}
            {*</h2>*}
            {*<span style="color: #000;">{$blog->header}</span>*}
        {*</div>*}
    {*</div>*}
{*</div>*}
<div class="text-block">
    {$blog->body}
</div>

<div class="text-block">
    <ul class="tags static">
        <li>
            <a href="/author/{$blog->writer_id}">
                {$blog->writer}
            </a>
        </li>
        {if $blog->postTags}
        {foreach item=postTag from=$blog->postTags name=postTag}
            <li>
                <a href="/posttags/{$blog->tags->url}/{$postTag->url}">
                    {$postTag->name}
                </a>
            </li>
        {/foreach}
        {/if}
    </ul>
</div>