<!DOCTYPE html>
<html lang="ru" prefix= "og: http://ogp.me/ns#">
<head>
    {include file="include/head_meta.tpl"}
</head>

<body>
{include file="_gmt.tpl"}

<div class="mainWrapper">
    <!-- Шторка -->
    {include file="include/dropDown.tpl"}
    <!-- РБК хедер -->
    <header class="rbcHWrap">
        {include file="include/header.tpl"}
    </header>

    <div class="bottomHeaderRbc">
        {include file="include/subheader.tpl"}
    </div>

    <div class="secondContainer">
        {$content}
    </div>

    {include file="include/footer.tpl"}

    <div class="bgforCloseEl"></div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{assets url='/design/rbk_v3/libs/masonry/masonry.pkgd.min.js'}"></script>
<script src="{assets url='/design/rbk_v3/js/common.js'}"></script>
<script src="{assets url='/design/rbk_v3/js/scrollString.js'}"></script>
<script src="{assets url='/design/rbk_v3/libs/scrollbar/jquery.scrollbar.min.js'}"></script>

{literal}
<script>
          jQuery('.scrollbar-macosx').scrollbar();
    (function($){
        $(window).on("load",function(){
            console.log($(".scrollbar-macosx"))
        });
    })(jQuery);
</script>
{/literal}


</body>
</html>
