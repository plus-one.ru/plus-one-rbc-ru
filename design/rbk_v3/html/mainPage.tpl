{if $videoList}
    <div class="container">
        <div class="customPlayer_wrapper">
            <div class="wrapper_bg">
                <div class="customPlayer_bg--desctop" style="background:url(/files/blogposts/{$videoImageDesktop}) no-repeat 0 0;background-size: 100% 100%;"></div>
                <div class="customPlayer_bg--mobile" style="background:url(/files/blogposts/{$videoImageMobile}) no-repeat 0 0; background-size: 100% 100%;"></div>
            </div>
            <span class="closeCustomPlayer"></span>
            <div class="customPlayer">
                <div class="customPlayer_screen">
                    <div class="iframe">
                        {$firstVideo}
                    </div>
                </div>
                <aside class="customPlayer_side scrollbar-macosx" data-mcs-theme="dark">
                    <ul class="customPlayer_descrWrap" >
                        {foreach item=video from=$videoList name=video}
                            <li class="customPlayer_descr" data-video-url='{$video->video_url}'>
                                <p class="customPlayer_title" >{$video->name}</p>
                                {if $typeVideo == 'live'}
                                    <p class="customPlayer_title--logo">Live</p>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                </aside>
            </div>
        </div>
    </div>
{/if}

{foreach item=row from=$posts name=row}
    {foreach item=post from=$row key=k name=post}
        {if $post->typeRow == 'banner'}
            {*для десктопа*}
            <div class="container">
                <div class="plusOneBanner desktopBanner">
                    <div id="adfox_155714933378043637"></div>
                    {literal}
                        <script>
                            window.Ya.adfoxCode.create({
                                ownerId: 260854,
                                containerId: "adfox_155714933378043637",
                                params: {
                                    pp: 'g',
                                    ps: 'codu',
                                    p2: 'ghgp'
                                }
                            });
                        </script>
                    {/literal}
                </div>
            </div>
            {*для мобайла*}
            <div class="container">
                <div class="plusOneBanner mobileBanner">
                    <div id="adfox_15571495137937239" class="zone top"></div>

                    {literal}
                        <script>
                            window.Ya.adfoxCode.create({
                                ownerId: 260854,
                                containerId: "adfox_15571495137937239",
                                params: {
                                    pp: 'g',
                                    ps: 'codu',
                                    p2: 'ghgm'
                                }
                            });
                        </script>
                    {/literal}
                </div>
            </div>
        {/if}


        {if $post->typeRow == '1_1'}
            <div class="container">
                <div class="blockWrap scaleElem">
                    {if $post->typePost == 'post'}
                        {include file="include/blocks/post_1_1.tpl"}
                    {/if}
                    {if $post->typePost == 'factday'}
                        {include file="include/blocks/factWeek_1_1.tpl"}
                    {/if}
                    {if $post->typePost == 'citate'}
                        {include file="include/blocks/citate_1_1.tpl"}
                    {/if}
                    {if $post->typePost == 'diypost' || $post->typePost == 'imageday'}
                        {include file="include/blocks/diy_1_1.tpl"}
                    {/if}
                </div>
            </div>
        {/if}

        {if $post->typeRow == '1_2'}
            {if $smarty.foreach.post.first}
                <div class="container">
                <div class="blockWrap">
                <div class="blTypeString">
                <div class="blTypeStringHalfWrap">
            {/if}

            {if $post->typePost == 'post'}
                {include file="include/blocks/post_1_2.tpl"}
            {/if}
            {if $post->typePost == 'factday'}
                {include file="include/blocks/factWeek_1_2.tpl"}
            {/if}
            {if $post->typePost == 'citate'}
                {include file="include/blocks/citate_1_2.tpl"}
            {/if}
            {if $post->typePost == 'diypost' || $post->typePost == 'imageday'}
                {include file="include/blocks/diy_1_2.tpl"}
            {/if}

            {if $smarty.foreach.post.last}
                </div>
                </div>
                </div>
                </div>
            {/if}
        {/if}

        {if $post->typeRow == '1_3'}
            {if $smarty.foreach.post.first}
                <div class="grid grid_flex">
            {/if}

            {*format: {$post->format}, font: {$post->font}*}
            {if $post->typePost == 'post'}
                {include file="include/blocks/post_1_3.tpl"}
            {elseif $post->typePost == 'factday'}
                {include file="include/blocks/factWeek_1_3.tpl"}
            {elseif $post->typePost == 'citate'}
                {include file="include/blocks/citate_1_3.tpl"}
            {elseif $post->typePost == 'diypost' || $post->typePost == 'imageday'}
                {include file="include/blocks/diy_1_3.tpl"}
            {else}
                {include file="include/blocks/post_1_3.tpl"}
            {/if}

            {if $smarty.foreach.post.last}
                </div>
            {/if}
        {/if}

        {if $post->typeRow == 'ticker'}
            <div class="parallaxTxtW">
                <div class="container parallaxTxtBorder"></div>
                <div class="parallaxTxt" style="right: -1409.78px;">
                    <div class="parallaxTxt_flex">
                        <a href="{$post->postUrl}" class="parallaxTxt_link">
                            {$post->name}
                        </a>
                    </div>
                </div>
            </div>
        {/if}
    {/foreach}
{/foreach}
