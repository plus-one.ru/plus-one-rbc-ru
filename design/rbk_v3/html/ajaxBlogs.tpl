{if $page == 1}
<div class="container">
    {if $writerInfo}
        {if $writerInfo->private == 1}
            <h1 class="plusOneHeader">Колонка {$writerInfo->name_genitive}</h1>
        {elseif $writerInfo->bloger == 1}
            <h1 class="plusOneHeader">Блог {$writerInfo->name_genitive}</h1>
        {else}
            <h1 class="plusOneHeader">{$writerInfo->name}</h1>
        {/if}
    {else}
        <h1>&nbsp;</h1>
    {/if}
</div>
{elseif $queryString}
    <div class="container">
        <h1 class="searchResultHeader" style="display: flex; flex-flow: column wrap; align-content: space-between;flex-direction: row; color: black;">
            <span style="flex: 4;">Поиск: "{$queryString}"</span>
            <span style="flex: 1; text-align: right;">найдено: {$countAll->count_all}</span>
        </h1>
    </div>
{/if}

    <div class="grid grid_tape scroll" id="gridtails">
        {foreach item=post from=$items name=post}
            {if $post->typePost == 'post'}
                {include file="include/blocks/post_1_3.tpl"}
            {elseif $post->typePost == 'factday'}
                {include file="include/blocks/factWeek_1_3.tpl"}
            {elseif $post->typePost == 'citate'}
                {include file="include/blocks/citate_1_3.tpl"}
            {elseif $post->typePost == 'diypost' || $post->typePost == 'imageday'}
                {include file="include/blocks/diy_1_3.tpl"}
            {else}
                {include file="include/blocks/post_1_3.tpl"}
            {/if}
        {/foreach}
    </div>


{if $items}
    <p class="pagination" style="display: none;">
        {if $tagUrl}
            <a class="pagination__next" href="/api/getposttags/{$rubrika}/{$tagUrl}/{$page}">Next page</a>
        {elseif $getsimpleblogs == 1}
            <a class="pagination__next" href="/map/{$page}">Next page</a>
        {elseif $writerId}
            <a class="pagination__next" href="/api/getpostauthor/{$writerId}/{$page}">Next page</a>
        {else}
            <a class="pagination__next" href="/blogs/{$rubrika}/{$page}">Next page</a>
        {/if}
    </p>
{/if}