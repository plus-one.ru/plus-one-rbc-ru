<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />

    <base href="/" />

    <title>Страница не найдена</title>
    <meta
            name="description"
            content="Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить."
    />
    <meta name="keywords" content="" />
    <meta name="title" content="РБК +1" />
    <meta property="og:title" content="РБК +1" />
    <meta property="og:type" content="article" />
    <meta
            property="og:description"
            content="Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить."
    />
    <meta
            property="og:image"
            content="http://plus-one.rbc.ru//design/rbk_v3/img/og-pbc.png"
    />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="http://plus-one.rbc.ru/" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="РБК +1" />
    <meta
            name="twitter:description"
            content="Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить."
    />
    <meta name="twitter:url" content="http://plus-one.rbc.ru/" />
    <meta name="twitter:image" content="http://plus-one.rbc.ru/" />
    <meta
            name="twitter:image:alt"
            content="+1 — Проект об устойчивом развитии"
    />
    <meta name="twitter:site" content="" />

    <link rel="image_src" href="http://plus-one.rbc.ru/" />

    <link
            rel="apple-touch-icon"
            sizes="57x57"
            href="/design/rbk_v3/img/favicon/apple-icon-57x57.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="60x60"
            href="/design/rbk_v3/img/favicon/apple-icon-60x60.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="72x72"
            href="/design/rbk_v3/img/favicon/apple-icon-72x72.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="/design/rbk_v3/img/favicon/apple-icon-76x76.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="114x114"
            href="/design/rbk_v3/img/favicon/apple-icon-114x114.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="120x120"
            href="/design/rbk_v3/img/favicon/apple-icon-120x120.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="144x144"
            href="/design/rbk_v3/img/favicon/apple-icon-144x144.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="152x152"
            href="/design/rbk_v3/img/favicon/apple-icon-152x152.png"
    />
    <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/design/rbk_v3/img/favicon/apple-icon-180x180.png"
    />
    <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href="/design/rbk_v3/img/favicon/android-icon-192x192.png"
    />
    <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/design/rbk_v3/img/favicon/favicon-32x32.png"
    />
    <link
            rel="icon"
            type="image/png"
            sizes="96x96"
            href="/design/rbk_v3/img/favicon/favicon-96x96.png"
    />
    <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/design/rbk_v3/img/favicon/favicon-16x16.png"
    />
    <link rel="manifest" href="/design/rbk_v3/img/favicon/manifest.json" />

    <link
            rel="shortcut icon"
            href="/design/rbk_v3/img/favicon/favicon.ico"
            type="image/x-icon"
    />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1"
    />

    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/fonts.css'}" />
    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/main.css'}" media="all"/>
    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/footer.css'}" />
    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/header.css'}" />
    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/header_media.css'}" />
    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/footer_media.css'}" />
    <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/media.css'}" />
</head>

<body>
<div class="mainWrapper mainPage">
    <header class="rbcHWrap">
        <div class="container">
            <nav class="rbcH">
                <div class="rbcNavSide">
                    <div class="rbcNavSideLogoW">
                        <a class="rbcNavSide_rbcHlogo" href="https://www.rbc.ru/" target="_blank"></a>
                    </div>
                    <div class="rbcNavElements">
                        <ul class="rbcNavSide_rbcNavElWrapper rbc-js-list">
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="http://tv.rbc.ru/?utm_source=topline" target="_blank">Телеканал</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/newspaper/?utm_source=topline" target="_blank">Газета</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/magazine/?utm_source=topline" target="_blank">Журнал</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://plus.rbc.ru/?utm_source=topline" target="_blank">РБК+</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://pro.rbc.ru/?utm_source=topline" target="_blank">Pro</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://quote.rbc.ru/?utm_source=topline" target="_blank">Quote</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://www.autonews.ru/?utm_source=topline" target="_blank">Авто</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://sportrbc.ru/?utm_source=topline" target="_blank">Спорт</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://realty.rbc.ru/?utm_source=topline" target="_blank">Недвижимость</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="http://style.rbc.ru/?utm_source=topline" target="_blank">Стиль</a>
                            </li>
                            <li data-hide="false">
                                <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/crypto/?utm_source=topline" target="_blank">Крипто</a>
                            </li>
                            <li data-hide="true">
                                <a class="rbcNavSide_rbcNavEl " href="https://marketing.rbc.ru/?utm_source=topline" target="_blank">Исследования</a>
                            </li>
                            <li data-hide="true">
                                <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/offers/main?utm_source=topline" target="_blank">Продажа бизнеса</a>
                            </li>
                            <li data-hide="true">
                                <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/franchises/main?utm_source=topline" target="_blank">Франшизы</a>
                            </li>
                            <li data-hide="true">
                                <a class="rbcNavSide_rbcNavEl " href="http://bc.rbc.ru/?utm_source=topline" target="_blank">Конференции</a>
                            </li>
                            <li data-hide="true">
                                <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards/?utm_source=topline" target="_blank">Премия РБК 2018</a>
                            </li>
                            <li data-hide="true">
                                <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards_spb/?utm_source=topline" target="_blank">Премия РБК СПб 2018</a>
                            </li>
                        </ul>
                        <div class="rbcNav_more_wrap">
                            <div class="rbcNav_more">
                                <p class="rbc_item">
                                    <a class="rbcNavSide_rbcNavEl border_green" href="https://www.rbc.ru/trends/?utm_source=toppne" target="_blank">Зеленая экономика</a>
                                </p>
                            </div>
                            <div class="dots_dropdown">
                                <p class="dots_dropdown_btn">...</p>
                                <div class="dropdown_list">
                                    <div class="dropdown_list_inner">
                                        <li data-hide="true">
                                            <a class="rbcNavSide_rbcNavEl " href="https://marketing.rbc.ru/?utm_source=topline" target="_blank">Исследования</a>
                                        </li><li data-hide="true">
                                            <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/offers/main?utm_source=topline" target="_blank">Продажа бизнеса</a>
                                        </li><li data-hide="true">
                                            <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/franchises/main?utm_source=topline" target="_blank">Франшизы</a>
                                        </li><li data-hide="true">
                                            <a class="rbcNavSide_rbcNavEl " href="http://bc.rbc.ru/?utm_source=topline" target="_blank">Конференции</a>
                                        </li><li data-hide="true">
                                            <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards/?utm_source=topline" target="_blank">Премия РБК 2018</a>
                                        </li><li data-hide="true">
                                            <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards_spb/?utm_source=topline" target="_blank">Премия РБК СПб 2018</a>
                                        </li></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rbcNavSide_dropDown">
                        <ul class="rbcNavSide_rbcNavElWrapper dropDown">
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="http://tv.rbc.ru/?utm_source=topline" target="_blank">Телеканал</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/newspaper/?utm_source=topline" target="_blank">Газета</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/magazine/?utm_source=topline" target="_blank">Журнал</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://plus.rbc.ru/?utm_source=topline" target="_blank">РБК+</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://pro.rbc.ru/?utm_source=topline" target="_blank">Pro</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://quote.rbc.ru/?utm_source=topline" target="_blank">Quote</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://www.autonews.ru/?utm_source=topline" target="_blank">Авто</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://sportrbc.ru/?utm_source=topline" target="_blank">Спорт</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://realty.rbc.ru/?utm_source=topline" target="_blank">Недвижимость</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="http://style.rbc.ru/?utm_source=topline" target="_blank">Стиль</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl" href="https://www.rbc.ru/crypto/?utm_source=topline" target="_blank">Крипто</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl " href="https://marketing.rbc.ru/?utm_source=topline" target="_blank">Исследования</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/offers/main?utm_source=topline" target="_blank">Продажа бизнеса</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl " href="http://biztorg.ru/franchises/main?utm_source=topline" target="_blank">Франшизы</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl " href="https://bc.rbc.ru/?utm_source=topline" target="_blank">Конференция</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards/?utm_source=topline" target="_blank">Премия РБК 2018</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl " href="https://www.rbc.ru/awards_spb/?utm_source=topline" target="_blank">Премия РБК СПБ 2018</a>
                            </li>
                            <li>
                                <a class="rbcNavSide_rbcNavEl border_grn" href="https://www.rbc.ru/trends/?utm_source=topline" target="_blank">Зеленая экономика</a>
                            </li>

                        </ul>
                    </div>

                    <div class="rbcTopHHamb">
                        <span class="navHamb"></span>
                    </div>
                </div>
                <ul class="rbcNavSide">
                    <li>
                        <a class="rbcNavSide_rbcHSearch" href="https://www.rbc.ru/search/" target="_blank"></a>
                    </li>
                    <li class="rbcNavSide_humb_wrap">
                        <p class="rbcNavSide_humb">
                            <span class="humb_line"></span>
                        </p>
                    </li>
                </ul>
            </nav>
        </div>    </header>
    <div class="bottomHeaderRbc">
        <!-- Шторка -->
        <div class="plusOneDropDown">
            <div class="container">
                <div class="plusOneHambCloseW">
                    <span class="plusOneHambClose"></span>
                </div>
                <ul class="headerNav">
                    <li>
                        <a href="/about/" class="headerNav_link">О проекте</a>
                    </li>
                    <li>
                        <a href="https://conf.plus-one.ru/" target="_blank" class="headerNav_link">Конференции</a>
                    </li>
                    <li>
                        <a href="https://полезныйгород.рф/" target="_blank" class="headerNav_link">+1 Город</a>
                    </li>
                    <li>
                        <a href="https://platform.plus-one.ru" class="headerNav_link">Платформа</a>
                    </li>
                </ul>
                <ul class="headerNav ">
                    <li>
                        <a href="http://plus-one.rbc.ru/" target="_blank" class="headerNav_link headerNav_link--green">+1 РБК</a>
                    </li>
                    <li>
                        <a href="https://tass.ru/plus-one" target="_blank" class="headerNav_link headerNav_link--yellow">+1 ТАСС</a>
                    </li>
                    <li>
                        <a href="http://www.forbes.ru/native/plus-one" target="_blank" class="headerNav_link headerNav_link--blue">+1 Forbes</a>
                    </li>
                    <li>
                        <a href="https://plus-one.vedomosti.ru/" target="_blank" class="headerNav_link headerNav_link--white">+1 Ведомости</a>
                    </li>
                    <li>
                        <a href="https://www.bfm.ru/special/visioners" target="_blank" class="headerNav_link headerNav_link--orange">+1 BFM</a>
                    </li>
                </ul>
                <ul class="headerNav">
                    </li>
                    <li>
                        <a href="https://vk.com/project_plus_one" target="_blank" class="headerNav_link">Вконтакте</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/project_plusone" target="_blank" class="headerNav_link">Twitter</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container">
            <div class="btmHTopics">
                <div class="rbcLeftHTopic">
                    <a href="./">
                        <div class="rbcPlusOneLogo"></div>
                    </a>
                    <div class="rbcTopicW">
                        <a href="/ecology">
                            <p class="rbcTopic_logo">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <g id="Layer_2" data-name="Layer 2">
                                        <g id="background">
                                            <path fill="#2adc46" d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"></path>
                                            <path d="M26.91,13.82c-2.38,0-5.29,2.58-6.88,4.17-1.58-1.59-4.49-4.17-6.89-4.17a6.17,6.17,0,0,0,0,12.34c2.4,0,5.31-2.57,6.89-4.17,1.59,1.6,4.5,4.17,6.88,4.17a6.17,6.17,0,1,0,0-12.34ZM13.14,23.41a3.42,3.42,0,0,1,0-6.83c2,0,3.4,1.8,5,3.42C16.54,21.61,15.09,23.41,13.14,23.41Zm13.77,0c-1.89,0-3.38-1.8-5-3.41,1.61-1.62,3.33-3.42,5-3.42a3.42,3.42,0,1,1,0,6.83Z"></path>
                                        </g>
                                    </g>
                                </svg>
                            </p>
                        </a>
                        <a href="/ecology">
                            <p class="rbcTopic_title">Экология</p>
                        </a>
                    </div>
                    <div class="rbcTopicW">
                        <a href="/society">
                            <p class="rbcTopic_logo">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" class="logoBl">
                                    <g data-name="logo">
                                        <g>
                                            <path fill="#fdbe0f" d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"></path>
                                            <path fill="#000" d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z"></path>
                                        </g>
                                    </g>
                                </svg>
                            </p>
                        </a>
                        <a href="/society">
                            <p class="rbcTopic_title">Общество</p>
                        </a>
                    </div>
                    <div class="rbcTopicW">
                        <a href="/economy">
                            <p class="rbcTopic_logo">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                                    <g data-name="Layer 2">
                                        <g fill="#00bae9">
                                            <circle class="cls-1" cx="15" cy="15" r="15"></circle>
                                            <polygon points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                            <circle cx="15" cy="15" r="15"></circle>
                                            <polygon fill="#000" points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                        </g>
                                    </g>
                                </svg>
                            </p>
                        </a>
                        <a href="/economy">
                            <p class="rbcTopic_title">Экономика</p>
                        </a>
                    </div>
                </div>
                <div class="rbcCenterHTopic">
                    <p class="rbcTopic_title">Партнерский проект</p>
                </div>
                <div class="rbcRightHTopic">
                    <div class="rbcTopicSearch">
                        <form action="/search/" method="get" class="search-form">
                            <div class="search-form_input-holder">
                                <div class="inputPlusOneSearch autocomplete-block">
                                    <input type="text" class="autocomplete" name="query" placeholder="Поиск" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="rbcTopicHamb">
                        <span class="navHamb"></span></div>
                </div>
            </div>
        </div>
    </div>

    <div class="error_404_plusOneWrapper">
        <a href="./">
            <div class="error_plusOne_logo">
                <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 161.65 91.02">
                        <defs>
                            <clippath id="clip-path">
                                <path class="cls-1" d="M132.18,62.83h-24V55.28h7.65v-15c0-1.36,0-2.77,0-2.77h-.1a6.76,6.76,0,0,1-1.41,1.91l-2.18,2L107.16,36l9.72-9.12h7.59V55.28h7.7Zm-105-22H40v-14h8.53v14H61.39v8.16H48.53V62.87H40V48.94H27.14ZM88.43,77.85h62.46a1.81,1.81,0,0,0,1.81-1.8V13.61a1.81,1.81,0,0,0-1.81-1.8H88.43a1.81,1.81,0,0,0-1.8,1.8V76.05A1.81,1.81,0,0,0,88.43,77.85ZM44.27,9h0A35.32,35.32,0,0,0,9,44.27v2.48a35.32,35.32,0,0,0,70.63,0h0V44.27A35.31,35.31,0,0,0,44.27,9Z"/>
                            </clippath>
                        </defs>
                        <title>Проект +1</title>
                        <g id="Layer_2" data-name="Layer 2">
                            <g id="background">
                                <g class="cls-2">
                                    <rect class="logoBg" width="161.65" height="91.02"/>
                                </g>
                            </g>
                        </g>
                    </svg> -->
            </div>
        </a>
        <p class="error_plusOne_numb">404</p>
        <img
                src="/design/rbk_v3/img/err.png"
                alt="ошибка"
                class="error_404_bg"
        />
    </div>

    <footer class="rbcFooter">
        <div class="container">
            <div class="rbcFooterTop">
                <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 161.65 91.02"
                >
                    <defs>
                        <style>{literal}
                            .cls-1 {
                                fill: none;
                                clip-rule: evenodd;
                            }
                            .cls-2 {
                                clip-path: url("#clip-path");
                            }
                            .cls-3 {
                                fill: #4a4a4a;
                            {/literal}
                        </style>
                        <clipPath id="clip-path">
                            <path
                                    class="cls-1"
                                    d="M132.18,62.83h-24V55.28h7.65v-15c0-1.36,0-2.77,0-2.77h-.1a6.76,6.76,0,0,1-1.41,1.91l-2.18,2L107.16,36l9.72-9.12h7.59V55.28h7.7Zm-105-22H40v-14h8.53v14H61.39v8.16H48.53V62.87H40V48.94H27.14ZM88.43,77.85h62.46a1.81,1.81,0,0,0,1.81-1.8V13.61a1.81,1.81,0,0,0-1.81-1.8H88.43a1.81,1.81,0,0,0-1.8,1.8V76.05A1.81,1.81,0,0,0,88.43,77.85ZM44.27,9h0A35.32,35.32,0,0,0,9,44.27v2.48a35.32,35.32,0,0,0,70.63,0h0V44.27A35.31,35.31,0,0,0,44.27,9Z"
                            ></path>
                        </clipPath>
                    </defs>
                    <title>Проект +1</title>
                    <g id="Layer_2" data-name="Layer 2">
                        <g id="background">
                            <g class="cls-2">
                                <rect class="cls-3" width="161.65" height="91.02"></rect>
                            </g>
                        </g>
                    </g>
                </svg>
                <!-- <p class="rbcFooterLogo"></p> -->
            </div>
            <div class="rbcFooterBtm">
                <ul class="rbcFnav">
                    <li>
                        <a href="#" class="rbcFnavLink_header">Редакция</a>
                    </li>
                    <li>
                        <a href="/about/" class="rbcFnavLink">О проекте</a>
                    </li>
                    <li>
                        <a href="#" class="rbcFnavLink_date">© 2019</a>
                    </li>
                </ul>

                <ul class="rbcFnav">
                    <li>
                        <p class="rbcFnavLink_header">Площадки</p>
                    </li>
                    <li>
                        <a
                                href="http://plus-one.rbc.ru/"
                                target="_blank"
                                class="rbcFnavLink rbcFnavLink--borderGrn"
                        >+1 РБК</a
                        >
                    </li>
                    <li>
                        <a
                                href="https://tass.ru/plus-one"
                                target="_blank"
                                class="rbcFnavLink rbcFnavLink--borderYlw"
                        >+1 ТАСС</a
                        >
                    </li>
                    <li>
                        <a
                                href="http://www.forbes.ru/native/plus-one"
                                target="_blank"
                                class="rbcFnavLink rbcFnavLink--borderBlue"
                        >+1 Forbes</a
                        >
                    </li>
                    <li>
                        <a
                                href="https://plus-one.vedomosti.ru/"
                                target="_blank"
                                class="rbcFnavLink rbcFnavLink--borderWhite"
                        >+1 Ведомости</a
                        >
                    </li>
                    <li>
                        <a
                                href="https://www.bfm.ru/special/visioners"
                                target="_blank"
                                class="rbcFnavLink rbcFnavLink--borderOrange"
                        >+1 BFM</a
                        >
                    </li>
                </ul>

                <ul class="rbcFnav">
                    <li>
                        <p class="rbcFnavLink_header">Рубрикатор</p>
                    </li>
                    <li>
                        <a href="/ecology/" class="rbcFnavLink">Экология</a>
                    </li>
                    <li>
                        <a href="/economy/" class="rbcFnavLink">Экономка</a>
                    </li>
                    <li>
                        <a href="/society/" class="rbcFnavLink">Общество</a>
                    </li>
                </ul>

                <ul class="rbcFnav">
                    <li>
                        <a href="#" class="rbcFnavLink_header">Другие проекты</a>
                    </li>
                    <li>
                        <a href="https://ivsezaodnogo.ru" class="rbcFnavLink"
                        >И все за одного</a
                        >
                    </li>
                    <li>
                        <a href="https://полезныйгород.рф" class="rbcFnavLink"
                        >+1 Город</a
                        >
                    </li>
                    <li>
                        <a href="https://award.plus-one.ru" class="rbcFnavLink"
                        >+1 Award</a
                        >
                    </li>
                    <li>
                        <a href="https://conf.plus-one.ru" class="rbcFnavLink"
                        >+1 Conf</a
                        >
                    </li>
                    <li>
                        <a href=" https://platform.plus-one.ru" class="rbcFnavLink"
                        >+1 Platform</a
                        >
                    </li>
                </ul>

                <ul class="rbcFnav">
                    <li>
                        <p class="rbcFnavLink_header">Соцсети</p>
                    </li>
                    <li>
                        <a href="https://vk.com/project_plus_one" target="_blank" class="rbcFnavLink">ВКонтакте</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/project_plusone" target="_blank" class="rbcFnavLink">Twitter</a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/c/Plusoneru" target="_blank" class="rbcFnavLink">Youtube</a>
                    </li>
                    <li>
                        <a href="https://t.me/project_plus_one" target="_blank" class="rbcFnavLink">Telegram</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>

    <div class="bgforCloseEl"></div>
</div>
<script src="/design/rbk_v3/libs/JQuery/jquery-3.1.1.min.js"></script>
<script src="/design/rbk_v3/libs/masonry/masonry.pkgd.min.js"></script>
<script src="/design/rbk_v3/js/common.js"></script>
</body>
</html>
