<!DOCTYPE html>
<html prefix= "og: http://ogp.me/ns#">
<head>
    {include file="include/head_meta.tpl"}
</head>
<body>
{include file="_gmt.tpl"}

    <div id="wrapper" style="padding-top: 125px;">
        {include file="include/topHeading.tpl"}
        <div class="container">
            {include file="banners/980x90.tpl"}
        </div>
        <div class="container">
            <div class="row-top">
                <iframe id="map" name="map" style="border: none; height:100%; width:100%;" src="https://map.plus-one.ru"></iframe>
            </div>
            <div class="row-bottom">

                {include file="banners/1140x290.tpl"}

                <div class="scroll">
                    {$content}
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="rubrikaUrl" value="{$rubrikaUrl}">
    <input type="hidden" id="typeUrl" value="{$typeUrl}">
    <input type="hidden" id="writerId" value="{$writerId}">

    {include file="include/retargetingСode.tpl"}

</body>
</html>
