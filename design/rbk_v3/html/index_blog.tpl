<!DOCTYPE html>
<html lang="ru" prefix= "og: http://ogp.me/ns#">
<head>
    {include file="include/blog_head_meta.tpl"}

    {* ADFox *}
    <script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{assets url='/design/rbk_v3/libs/masonry/masonry.pkgd.min.js'}"></script>
    <script src="{assets url='/design/rbk_v3/js/jquery.plugins.js'}"></script>

    {if $blockedRkn}
        <link rel="stylesheet" href="{assets url='/design/rbk_v3/css/blocked-content.css'}">
    {/if}
</head>

<body>
{include file="_gmt.tpl"}

{include file="include/_fixOldCitate.tpl"}

{if $bodyColor}
    <div class="mainWrapper mainWrapper--special" style="background: {$bodyColor}">
{else}
    <div class="mainWrapper materialPage">
{/if}
        <!-- Шторка -->
        {include file="include/dropDown.tpl"}
        <!-- РБК хедер -->
        <header class="rbcHWrap">
            {include file="include/header.tpl"}
        </header>

        <div class="bottomHeaderRbc">
            {include file="include/subheader.tpl"}
        </div>

        <main class="rbcMain">
            {$content}
        </main>

        <div id="articles-scroll-wrap"></div>
    {include file="include/footer.tpl"}
    <div class="bgforCloseEl"></div>
</div>
<script src="{assets url='/design/rbk_v3/js/common.js'}"></script>
</body>

</html>
