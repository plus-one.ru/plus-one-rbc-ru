
<h1 class="plusOneHeader">Спецпроекты +1</h1>

<div class="container specialProject">
    {foreach item=sp from=$specialProjects name=sp}
        <section class="newsBl newsBl_img scaleElem">
            <div class="newsBl_bg" style="background: url('/files/spec_project/{$sp->id}_1-1.jpg') no-repeat 50%; background-size: cover;"></div>
            <div class="newsBl_bg1_3" style="background: url('/files/spec_project/{$sp->id}_1-3.jpg') no-repeat 50%; background-size: cover;"></div>
            <a href="{$sp->external_url}" class="wrap-all" target="_blank"></a>
            <div class="newsBl_top">
                <a href="{$sp->external_url}" class="newsH" target="_blank">
                    <p class="newsH_title">СПЕЦПРОЕКТ</p>
                </a>
            </div>
            <a href="{$sp->external_url}" class="newsMain" target="_blank">
                <div class="newsMain_title">{$sp->name}</div>
                <div class="newsMain_subtitle">{$sp->header}</div>
            </a>
            <a href="" class="news_today">{$sp->dateStr}</a>
        </section>

    {/foreach}
</div>