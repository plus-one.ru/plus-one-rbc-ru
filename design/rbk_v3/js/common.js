$(document).ready(function () {
  //Добавление канонического урла <link rel="canonical" href={window.location.href} />

  // шапка рбк
  DynamicsHeader();
  $(".dots_dropdown").hover(
    function (e) {
      $(".dots_dropdown").addClass("active");
    },
    function (e) {
      if (e.target !== this) $(".dots_dropdown").removeClass("active");
    }
  );
  // шапка рбк конец


  // шилды 1_2 и 1_3. подстройка ширины рамки
  function SetShieldsWidth() {
    const shields = $('.eco_tacc');
    shields.each((i, shield) => {
      if (shield.closest('.blType1_2_bgImgStringTop--wrap') || shield.closest('.blType1_2StringTop')) {
        if (document.documentElement.offsetWidth < 1024) {
          
          const textWidth = shield.offsetWidth;
          if(shield.classList.contains('partner-text') || shield.classList.contains('partner-text_mobile')) {
            return
          } else {
            shield.parentElement.style.maxWidth = `170px`;
            shield.parentElement.style.width = `${textWidth + 17}px`;
          }
        } else {
          // if (shield.closest('.blType1_2_bgImgStringTop--wrap')) shield.parentElement.style.width = `310px`;

          const textWidth = shield.offsetWidth;

          if(shield.classList.contains('partner-text') || shield.classList.contains('partner-text_mobile')) {
            shield.parentElement.style.width = `auto`;
          } else {
            shield.parentElement.style.width = `${textWidth + 1}px`;
          }
        }
      } else if (shield.closest('.blType1_3StringTop') || shield.closest('.blType1_3StringTop--wrap')) {
        if (shield.closest('.eco_taccBlock')) shield.parentElement.style.width = `170px`;

        const textWidth = shield.offsetWidth;

        if(shield.classList.contains('partner-text') || shield.classList.contains('partner-text_mobile')) {
          shield.parentElement.style.width = `auto`;
        } else {
          textWidth < 60 ? shield.parentElement.style.width = `auto` : shield.parentElement.style.width = `${textWidth + 20}px`;
        }
          
      }
    });
  }
  SetShieldsWidth();
  setTimeout(() => { SetShieldsWidth() }, 500);

  // Для верхнего гамб
  $(".rbcNavSide_humb_wrap").click(function () {
    $(".rbcNavSide_humb_wrap").toggleClass("active");
    if ($(".rbcNavSide_humb_wrap").hasClass("active")) {
      $(".rbcNavSide_dropDown").addClass("show");
      return;
    }
    $(".rbcNavSide_dropDown").removeClass("show");
  });

  $(".customPlayer_wrapper").toggleClass("active");

  // Для кастомного плеера
  $(".customPlayer_wrapper").click(function (e) {
    if (!$(".customPlayer_wrapper").hasClass("active"))
      $(".customPlayer_wrapper").toggleClass("active");
  });
  // при клике на эл-ты списка меню.
  $(".customPlayer_descr").each(function (index, el) {
    $(el).click(function () {
      $(".customPlayer_screen--img").css("display", "none");
      $(".customPlayer_screen > div.iframe").css("display", "block");
      const url = $(this).attr("data-video-url");
      $(".customPlayer_screen > div.iframe").html(url);
      // для кастомного видео
      calculateWHCustomPlayer(
        $(".customPlayer_screen iframe"),
        customPlayerRatio
      );
    });
  });
  // для кастомного видео/ Первая инициализация
  calculateWHCustomPlayer($(".customPlayer_screen iframe"), customPlayerRatio);
  // при закрытии плеера
  $(".closeCustomPlayer").click(function (e) {
    e.stopPropagation();
    // Срос вопроизведения видео в iframe
    $(".customPlayer_screen > div.iframe > iframe").attr(
      "src",
      $(".customPlayer_screen > div.iframe > iframe").attr("src")
    );
    $(".customPlayer_screen--img").css("display", "block");
    $(".customPlayer_wrapper").removeClass("active");
  });
  // конец

  //Для инпута поиска

  $(".rbcTopicSearch").click(function (e) {
    e.stopPropagation();

    $(".inputPlusOneSearch").toggleClass("show");
    $(".rbcTopicSearch").toggleClass("show");

    $(".rbcPlusOneLogo").toggleClass("active");
    setTimeout(function () {
      $(".inputPlusOneSearch").focus();
    }, 100);
  });
  $(".inputPlusOneSearch").click(function (e) {
    if ($(".inputPlusOneSearch").hasClass("show")) {
      return false;
    }
  });
  function showDropDown() {
    $(".plusOneDropDown").addClass("show");
    $(".bgforCloseEl").addClass("show");
  }

  function closeDropDown() {
    const plusOneDropDown = $(".plusOneDropDown");
    const bgforCloseEl = $(".bgforCloseEl");
    if (plusOneDropDown.hasClass("show") || bgforCloseEl.hasClass("show")) {
      plusOneDropDown.removeClass("show");
      bgforCloseEl.removeClass("show");
    }
    return;
  }

  $(".rbcTopicHamb").click(showDropDown);
  $(".plusOneHambCloseW").click(closeDropDown);
  $(window).scroll(closeDropDown);
  $(".mainWrapper").click(function (e) {
    if (
      !$(e.target).hasClass("plusOneDropDown") &&
      !$(e.target).hasClass("container") &&
      !$(e.target).hasClass("rbcTopicHamb") &&
      !$(e.target).hasClass("headerNav") &&
      !$(e.target).hasClass("headerNav_link") &&
      !$(e.target).hasClass("headerNav_item") &&
      !$(e.target).hasClass("navHamb")
    ) {
      closeDropDown();
    }
  });
  $(window).click(function (e) {
    if (e.target === $(".inputPlusOneSearch")[0]) return;

    $(".inputPlusOneSearch").removeClass("show");
    $(".btmHTopics").removeClass("active");
    $(".inputPlusOneSearch").val("");
    $(".rbcPlusOneLogo").removeClass("active");
    $(".rbcTopicSearch").removeClass("show");
  });

  // $('.rbcTopicSearch').click(function(e){
  //   if($(".inputPlusOneSearch").hasClass("show")){
  //     $(".inputPlusOneSearch").removeClass('show');
  //   }
  // })

  // initOpenClose();

  initGallery("fotorama");
  initAutocomplete();

  getMenu();

  // Функция для уменьшения шрифта
  // если текст не вписывается в блок
  function myScale(block, parent, scrollPage) {
    var step = 0.5;
    var minfs = 10;
    var sch = block.offsetHeight;
    var hp = parent.offsetHeight;
    var scw = block.offsetWidth;
    var wp = parent.offsetWidth;

    var parentMaxHeight = parseFloat($(parent).css("max-height"));
    var blockHeight = parseFloat($(block).css("height"));
    var parentWidth = parseFloat($(parent).css("width"));
    var blockWidth = parseFloat($(block).css("width"));

    // если в блок не влезает текст
    // по высоте или ширине
    // то уменьшить шрифт
    // 60 - left, right padding (30)

    if (
      scrollPage &&
      (parentMaxHeight - 120 <= blockHeight || parentWidth - 40 <= blockWidth)
    ) {
      var fontsizeSubtitle = parseFloat($(block).css("font-size")) - step;
      var lineHeightSubtitle = parseFloat($(block).css("line-height")) - step;
      if (fontsizeSubtitle >= minfs) {
        $(block).css({
          "font-size": fontsizeSubtitle + "px",
          "line-height": lineHeightSubtitle + "px",
        });
        setTimeout(() => {
          window.myScale(block, parent, scrollPage);
        }, 0);
      }
    }

    if (sch > hp) {
      var fontsizeSubtitle = parseFloat($(block).css("font-size")) - step;
      var lineHeightSubtitle = parseFloat($(block).css("line-height")) - step;
      if (fontsizeSubtitle >= minfs) {
        $(block).css({
          "font-size": fontsizeSubtitle + "px",
          "line-height": lineHeightSubtitle + "px",
        });
        setTimeout(() => {
          window.myScale(block, parent);
        }, 0);
      }
    }
  }

  document.fonts.ready.then(function () {
    let scrollPage = false;
    if ($(".scroll").find(".grid").length) scrollPage = true;
    if (scrollPage) {
      // уменьшение шрифта у блоков факт недели
      for (var i = 0; i < $(".fact1_3BlWrap  .mainFact1_3").length; i++) {
        window.myScale(
          $(".fact1_3BlWrap").eq(i).find(".mainFact1_3")[0],
          $(".fact1_3BlWrap").eq(i),
          true
        );
      }
    }
    if (document.documentElement.clientWidth > 1141 && !scrollPage) {
      // уменьшение шрифта у блоков факт недели
      for (var i = 0; i < $(".fact1_3BlWrap .mainFact1_3").length; i++) {
        window.myScale(
          $(".fact1_3BlWrap").eq(i).find(".mainFact1_3")[0],
          $(".fact1_3BlWrap").eq(i).find(".mainFact1_3_link")[0]
        );
      }

      // 1_2 факт недели
      for (var i = 0; i < $(".fact1_2BlWrap").length; i++) {
        window.myScale(
          $(".fact1_2BlWrap").eq(i).find(".mainFact1_2")[0],
          $(".fact1_2BlWrap").eq(i).find(".mainFact1_2_link")[0]
        );
      }
      // 1_1 факт недели
      for (var i = 0; i < $(".factBlWrap ").length; i++) {
        myScale(
          $(".factBlWrap").eq(i).find(".mainFact")[0],
          $(".factBlWrap").eq(i).find(".mainFact_link")[0]
        );
      }

      // 1_2 ЦИТАТА НЕДЕЛИ
      for (var i = 0; i < $(".quote1_2BlWrap").length; i++) {
        window.myScale(
          $(".quote1_2BlWrap").eq(i).find(".mainQuote1_2")[0],
          $(".quote1_2BlWrap").eq(i).find(".mainQuote1_2_wrap")[0]
        );
      }
      // 1_1 ЦИТАТА НЕДЕЛИ
      for (var i = 0; i < $(".quote1_1BlWrap  ").length; i++) {
        myScale(
          $(".quote1_1BlWrap ").eq(i).find(".mainQuote1_1")[0],
          $(".quote1_1BlWrap ").eq(i).find(".mainQuote1_1_wrap")[0]
        );
      }
    }
    // end document.fonts.ready
  });

  window.myScale = myScale;
  // end document,ready

  // Скрипты для Iframe'ов с видео
  const ratioVimeo = 360 / 640; //соотношение высоты к ширине для Vimeo
  // const ratioYoutube = 315 / 734; //соотношение высоты к ширине для Youtube
  const ratioYoutube = 315 / 560; //соотношение высоты к ширине для Youtube
  // TODO нужно понять на какой странице запускать
  // пока что сделано, если не главная
  if (!$(".mainPage")) widthAndHeightVideo(ratioVimeo, ratioYoutube);

  $(window).resize(function () {
    SetShieldsWidth();
    if ($(window).width() > 870 && $(".rbcNavSide_dropDown").hasClass("show"))
      $(".rbcNavSide_dropDown").removeClass("show");
    // для поиска
    // $(".inputPlusOneSearch").removeClass("show");
    // if (!$(".inputPlusOneSearch").hasClass('show')) $(".btmHTopics").removeClass("active");

    // для шапки РБК
    DynamicsHeader();

    // TODO нужно понять на какой странице запускать
    // пока что сделано, если не главная
    if (!$(".mainPage")) widthAndHeightVideo(ratioVimeo, ratioYoutube);

    // текстовый врез
    $(".info-side").each((i, el) => {
      const heightChild = $(".info-side")
        .eq(i)
        .find(".bulletImg")
        .css("height");

      setHeight($(".info-side").eq(i), heightChild);
    });
  });
  // Фикс цитат когда в них есть ссылка

  // function wrap(node){ // Оборачиваем содержимое цитаты в span
  //   var cite = document.querySelectorAll(node)
  //   for(var i = 0; i < cite.length; i++){
  //     var nodeText = cite[i].innerHTML;
  //     cite[i].innerHTML = `<span class="citeText-wrap">${nodeText}</>`
  //   }
  // }
  // wrap(".holder cite");

  // var blockQuote = document.querySelectorAll('.rbcDescr blockquote');

  // function appendPicToCitate (){
  //   for(let i = 0; i < blockQuote.length; i++){
  //     let pic = blockQuote[i].querySelector('.pic');
  //     let cite = blockQuote[i].querySelector(".holder cite");
  //     cite.prepend(pic);
  //     // blockQuote[i].remove(pic)
  //   }
  // }appendPicToCitate ()

  // БЕСКОНЕЧНЫЙ СКРОЛЛ СТАТЕЙ

  var validUrlsPattern = /^\/(economy|ecology|society|platform|news)\/.*/i;
  /* Переменная-флаг для отслеживания того, происходит ли в данный момент ajax-запрос. В самом начале даем ей значение false, т.е. запрос не в процессе выполнения */
  var inProgress = false;

  var locString = window.location.pathname;
  var localStringArray = locString.split("/");

  var timer = null;
  // Сюда падает разметка при запросе
  var dataBody;
  // Распаршеная разметка внутри которой ищем подходящий виджет
  var dataWidget;
  var widgetCounter = 1;

  function appendWidget() {
    var widgetPulse = dataWidget.querySelector(".pulse-widget");
    var widgetSmi = dataWidget.querySelector(".smi-widget");
    var widgetLenta = dataWidget.querySelector(".lenta-widget");
    var widgetMediametric = dataWidget.querySelector(".mediamtric-widget");

    if (widgetCounter == 0) {
      if (widgetPulse) widgetPulse.style.display = "block";
      if (widgetSmi) widgetSmi.style.display = "none";
      if (widgetLenta) widgetLenta.style.display = "none";
      if (widgetMediametric) widgetMediametric.style.display = "none";
    } else if (widgetCounter == 1) {
      if (widgetSmi) widgetSmi.style.display = "block";
      if (widgetSmi) widgetPulse.style.display = "none";
      if (widgetLenta) widgetLenta.style.display = "none";
      if (widgetMediametric) widgetMediametric.style.display = "none";
    } else if (widgetCounter == 2) {
      if (widgetLenta) widgetLenta.style.display = "block";
      if (widgetPulse) widgetPulse.style.display = "none";
      if (widgetSmi) widgetSmi.style.display = "none";
      if (widgetMediametric) widgetMediametric.style.display = "none";
    } else if (widgetCounter == 3) {
      if (widgetMediametric) widgetMediametric.style.display = "block";
      if (widgetPulse) widgetPulse.style.display = "none";
      if (widgetSmi) widgetSmi.style.display = "none";
      if (widgetLenta) widgetLenta.style.display = "none";
    }

    if (widgetCounter >= 4) {
      widgetCounter = 0;
    }
    widgetCounter++;
  }
  function appendData(data) {
    // Отрисовываем разметку
    var parser = document.createElement("div");
    parser.classList.add("parser-wrap");
    parser.innerHTML = data;
    dataWidget = parser;
    appendWidget(dataWidget);
    $("#articles-scroll-wrap").append(dataBody);
    //  И делаем следущий запрос чтобы успел прийти респонс
    dataBody = parser;
    inProgress = false;
  }

  function getFotoramaCollection(selector) {
    var fotoramaClass;
    var arrs = [];
    var classPrefix = 0;
    var fotoramaCollection = document.querySelectorAll(selector);
    fotoramaCollection = Array.from(fotoramaCollection);
    // = 'fotoramaId_' + classPrefix;
    fotoramaCollection.forEach(function (fotoramaElem) {
      fotoramaClass = "fotoramaId_" + classPrefix;

      arrs = [fotoramaClass];
      fotoramaElem.classList.add(arrs[0]);
      if (arrs.indexOf(fotoramaClass) != -1) {
        // console.log(fotoramaClass);
        classPrefix++;

        initGallery(fotoramaClass);
      }
    });
    console.log("getFotoramaCollection", fotoramaClass);
  }

  // Делаем запрос при загрузке
  $.ajax({
    xhrFields: { withCredentials: true },
    url: `api/getPost/${localStringArray[1]}`,
    dataType: "json",
    beforeSend: function () {
      inProgress = true;
    },
  }).done(function (data) {
    appendData(data.body);
    getFotoramaCollection(".fotorama");
  });

  let previousRoute = location.pathname;
  let triggerAnalytics = (pathname, title) => {
    // ym(40580670, "hit", pathname, title); // Метрика засчитывает переход при скроле и изменении урла;
    // ga("create", "UA-86584410-1", "auto"); // Гугл аналитика засчитывает переход при скроле и изменении урла
    // ga("send", "pageview", pathname);
  };

  $(window).scroll(function () {
    var posts = document.querySelectorAll(".post-item");
    clearTimeout(timer);
    for (let i = 0; i < posts.length; i++) {
      if (posts[i].getBoundingClientRect().bottom >= 0) {
        let options = { title: posts[i].querySelector(".rbc_tape").innerText };
        // Для сафари пришлось юзать таймаут
        timer = setTimeout(function () {
          if (previousRoute !== "/" + posts[i].dataset.url) {
            // перезаписываем url в адресной строке
            window.history.replaceState(
              posts[i].body,
              "plus-one.rbc.ru",
              posts[i].dataset.url
            );
            document.title = options.title;
            console.log('PUSH STATE', posts[i].dataset.url, previousRoute);
            triggerAnalytics(location.pathname, options.title);
            previousRoute = location.pathname;
          }
        }, 200);
        break;
      }
    }
    /* Если высота окна + высота прокрутки больше или равны высоте всего документа и ajax-запрос в настоящий момент не выполняется, то запускаем ajax-запрос */
    if (
      $(window).scrollTop() + $(window).height() >=
        $(document).height() - 425 &&
      !inProgress &&
      window.location.pathname.match(validUrlsPattern)
    ) {
      setTimeout(function () {
        if ($("#articles-scroll-wrap").length > 0) {
          $("footer.rbcFooter").hide();
        }
      });

      $.ajax({
        xhrFields: { withCredentials: true },
        /* адрес файла-обработчика запроса */
        url: `api/getPost/${localStringArray[1]}`,
        dataType: "json",
        /* что нужно сделать до отправки запрса */
        beforeSend: function () {
          /* меняем значение флага на true, т.е. запрос сейчас в процессе выполнения */
          inProgress = true;
        },
        /* что нужно сделать по факту выполнения запроса */
      }).done(function (data) {
        appendData(data.body);
        getFotoramaCollection(".fotorama");
        if (window._oldCitateCorrector && data.id) {
          window._oldCitateCorrector(document.getElementById(data.id));
        }
      });
    }
  });
  // -------------------------
});

// пропорции для кастомного плеера
const customPlayerRatio = 246 / 420;
// Для пропорционального сжатия кастомного плеера
function calculateWHCustomPlayer(player, ratio) {
  // для кастомного видео
  if (
    document.documentElement.clientWidth < 1140 &&
    document.documentElement.clientWidth > 609
  ) {
    $(".customPlayer_screen > .iframe > iframe").attr("height", 333);
    $(".customPlayer_screen").css({ height: "333px" });
  }

  if (
    document.documentElement.clientWidth <= 609 &&
    document.documentElement.clientWidth > 440
  ) {
    $(".customPlayer_screen > .iframe > iframe").attr("height", 242);
    $(".customPlayer_screen").css({ height: "238px" });
  }

  if (document.documentElement.clientWidth > 1140) {
    $(".customPlayer_screen > .iframe > iframe").attr("height", 480);
    $(".customPlayer_screen").css({ height: "480px" });
  }
  if (document.documentElement.clientWidth < 440) {
    const widthParent = $(player).width();
    const currentHeight = Math.floor(widthParent * ratio);
    const currentHeightParent = `${currentHeight - 6}px`;
    // Устанавливаем значения
    $(".customPlayer_screen").css({ height: currentHeightParent });
    $(".customPlayer_screen > .iframe > iframe").attr("height", currentHeight);
  }
}

// Для пропорционального сжатия Iframe video
function calculateWHIrame(iframe, ratio) {
  iframe.closest("p").style.width = "100%";
  iframe.closest("p").style.position = "relative";
  const widthParent = $(iframe).parent().width();
  let currentHeight = Math.floor(widthParent * ratio);
  // Устанавливаем значения
  iframe.closest("p").style.height = `${currentHeight}px`;
  $(iframe).attr("width", widthParent);
  $(iframe).attr("height", currentHeight);
  iframe.style.height = `${currentHeight}px`;
  iframe.style.position = "absolute";
}
// Для пропорционального сжатия Iframe video
function widthAndHeightVideo(ratioVimeo, ratioYoutube) {
  let allIrame = $("iframe");
  for (let i = 0; i < allIrame.length; i++) {
    if (allIrame[i].src.indexOf("player.vimeo") !== -1) {
      calculateWHIrame(allIrame[i], ratioVimeo);
    }
    if (allIrame[i].src.indexOf("www.youtube") !== -1) {
      calculateWHIrame(allIrame[i], ratioYoutube);
    }
  }
}
var _isMobile = window.innerWidth < 768 ? true : false;
window.addEventListener("resize", checkWidth);
function checkWidth(e) {
  if (_isMobile && window.innerWidth > 767) {
    initGallery("fotorama", { thumbwidth: 88, thumbheight: 70 });
    _isMobile = false;
  } else if (!_isMobile && window.innerWidth < 768) {
    initGallery("fotorama", { thumbwidth: 62, thumbheight: 48 });
    _isMobile = true;
  }
}
function initGallery(fotoramaClass, params) {
  let thumbWidth, thumbHeight;
  if (params) {
    thumbWidth = params.thumbwidth;
    thumbHeight = params.thumbheight;
  } else {
    thumbWidth = window.innerWidth < 768 ? 62 : 88;
    thumbHeight = window.innerWidth < 768 ? 48 : 70;
  }
  $(".fotorama").each(function (index, el) {
    var _this = $(this),
      _parent = _this.closest(".fotorama-holder");
    var fotorama = $(this).data("fotorama");
    _this
      .on("fotorama:showend ", function (e, fotorama) {
        $(".numbering", _parent).html(
          fotorama.activeIndex + 1 + " / " + fotorama.size + ".&nbsp;"
        );
        var _cotent = $(
          ".fotorama__active .fotorama__html div p",
          _parent
        ).html();
        $(".info p", _parent).html(_cotent);
      })
      .fotorama({
        clicktransition: "dissolve",
        nav: "thumbs",
        width: "100%",
        maxwidth: 996,
        maxheight: 525,
        fit: "scaledown",
        loop: true,
        swipe: true,
        click: true,
        arrows: true,
        margin: 7,
        keyboard: { space: true },
        allowfullscreen: true,
        thumbwidth: thumbWidth,
        thumbheight: thumbHeight,
        thumbmargin: 7,
        navwidth: Math.min(755, window.innerWidth),
        ratio: "800/600",
        thumbfit: "cover",
      });
    $(".numbering", _parent).html(
      fotorama.activeIndex + 1 + " / " + fotorama.size + ".&nbsp;"
    );
    var _cotent = $(".fotorama__active .fotorama__html div p", _parent).html();
    $(".info p", _parent).html(_cotent);
  });

  $(".fotorama__wrap").addClass("fotorama__wrap--no-controls");
}

function getMenu() {
  var rubrikaUrl = $("#rubrikaUrl").val();
  var typeUrl = $("#typeUrl").val();

  $.ajax({
    url: "/ajax/getMenu.php",
    data: {
      rubrikaUrl: rubrikaUrl,
      typeUrl: typeUrl,
    },
    type: "POST",
    //dataType: 'json',
    success: function (data) {
      $(".informer").html(data);
    },
  });
}

// open-close init
function initOpenClose() {
  $("#header").openClose({
    activeClass: "active",
    opener: ".search-opener",
    slider: ".open-close_slide",
    animSpeed: 400,
    hideOnClickOutside: true,
    effect: "slide",
  });

  $(".search-opener").click(function () {
    $(".autocomplete").focus();
  });
}
// Логика поведения хедера , лучше не трогать
let bottomHeaderRbc;
let isHeaderFixed = false;

function headerHeight(uiMenuHeight = $(".ui-menu").height() + 40) {
  const uiMenu = document.querySelector(".ui-menu");
  const showedUiMenu =
    getComputedStyle(uiMenu).display !== "none" ? true : false;
  const headerFixed = $(".header-fixed_active .bottomHeaderRbc");
  headerFixed.length ? (isHeaderFixed = true) : (isHeaderFixed = false);

  if (headerFixed.length && showedUiMenu) {
    headerFixed.height(uiMenuHeight);
  } else {
    headerFixed.innerHeight(60);
  }
  if (!headerFixed.length) {
    $(".bottomHeaderRbc").height(72);
  }
}
// --------------------------------------------

function initAutocomplete() {
  $("input.autocomplete")
    .autocomplete({
      open: function () {
        const uiMenuHeight = $(".ui-menu").height();
        headerHeight(uiMenuHeight + 40);
      },
      close: function () {
        headerHeight(60);
      },
      source: "api/autocomplette",
      appendTo: ".autocomplete-block",
      highlight: true,
      autoFocus: false,
      minLength: 2,
      select: function (event, ui) {
        if (ui.item.url != "") {
          document.location.href = ui.item.url;
        }
      },
    })
    .data("ui-autocomplete")._renderItem = function (ul, item) {
    return $("<li>")
      .append("<div>" + (item.label || "") + "</div>")
      .appendTo(ul);
  };
}

// функция для текстового вреза
// устанавливает высоту верную
function setHeight(element, height) {
  $(element).css({
    height: height,
  });
}

$(".info-side").each((i, el) => {
  const heightChild = $(".info-side").eq(i).find(".bulletImg").css("height");

  setHeight($(".info-side").eq(i), heightChild);
});

// Для Iframe video
function calculateWHIrame(iframe, ratio) {
  iframe.closest("p").style.width = "100%";
  iframe.closest("p").style.position = "relative";
  const widthParent = $(iframe).parent().width();
  let currentHeight = Math.floor(widthParent * ratio);
  // Устанавливаем значения
  iframe.closest("p").style.height = `${currentHeight}px`;
  $(iframe).attr("width", widthParent);
  $(iframe).attr("height", currentHeight);
  iframe.style.height = `${currentHeight}px`;
  iframe.style.position = "absolute";
}
// Для Iframe video
function widthAndHeightVideo(ratioVimeo, ratioYoutube) {
  let allIrame = $("iframe");
  for (let i = 0; i < allIrame.length; i++) {
    if (allIrame[i].src.indexOf("player.vimeo") !== -1) {
      calculateWHIrame(allIrame[i], ratioVimeo);
    }
    if (allIrame[i].src.indexOf("www.youtube") !== -1) {
      calculateWHIrame(allIrame[i], ratioYoutube);
    }
  }
}

function DynamicsHeader() {
  let fullwidthTitles = 0;
  let hidedTitles = [];
  $(".rbc-js-list > li").each(function (index, el) {
    fullwidthTitles += $(el).outerWidth();

    $(el).attr("data-hide", `false`);
    if ($(".rbc-js-list").outerWidth() < fullwidthTitles)
      $(el).attr("data-hide", `true`);
  });

  $(".rbc-js-list > li").each(function (index, elem) {
    if ($(elem).attr("data-hide") === "true") hidedTitles.push(elem);
    // if($(elem).attr("data-hide") === "false") visibleTitles += $(elem).outerWidth()
  });

  $(".dropdown_list_inner > li").each(function (index, el) {
    $(el).remove();
  });

  hidedTitles.map(function (cloneEl) {
    $(cloneEl).clone().appendTo(".dropdown_list_inner");
  });
}

function changeCanonical() {
  const link = document.getElementById("canonicalLink");
  // link.setAttribute("href", window.location.origin + window.location.pathname);
}

function cleanParams(arrParams) {
  for (let i = 0; i < arrParams.length; i++) {
    if (window.location.search.includes(arrParams[i])) {
      window.location = `${window.location.origin}${window.location.pathname}`;
    }
  }
}
const materialPage = document.querySelector(".materialPage");
const header = document.querySelector(".bottomHeaderRbc");

// Изминияем позицию top дропдауна поиска при возникновении события изменения хедера
//т.к. у фиксированного и статического хедеров разная высота
materialPage &&
  materialPage.addEventListener("changeHeader", function () {
    const inputPlusOneSearchHeight = document
      .querySelector(".inputPlusOneSearch")
      .getBoundingClientRect().height;
    const uiMenu = document.querySelector(".ui-menu");
    uiMenu.style.top = inputPlusOneSearchHeight + "px";
  });

materialPage &&
  materialPage.addEventListener("changeHeader", function () {
    bottomHeaderRbc = $(".bottomHeaderRbc").innerHeight();
    headerHeight();
  });

document.addEventListener("DOMContentLoaded", function () {
  materialPage && checkIsHeaderFixed();
  changeCanonical();
  // cleanParams(["?fbclid=", "?utm_"]);
});
// Создаём событие изменения хедера
const headerEvent = new Event("changeHeader");

function checkIsHeaderFixed() {
  if (window.scrollY > header.getBoundingClientRect().height) {
    if (materialPage.classList.contains("header-fixed_active")) {
      return;
    } else {
      materialPage.classList.add("header-fixed_active");
      materialPage.dispatchEvent(headerEvent); // Диспатчим событие изменения хедера, только когда нужно, а не при каждом скролле
    }
  } else {
    if (!materialPage.classList.contains("header-fixed_active")) {
      return;
    } else {
      materialPage.classList.remove("header-fixed_active");
      materialPage.dispatchEvent(headerEvent); // Диспатчим событие изменения хедера, только когда нужно, а не при каждом скролле
    }
  }
}

window.addEventListener("scroll", () => {
  materialPage && checkIsHeaderFixed();
});
