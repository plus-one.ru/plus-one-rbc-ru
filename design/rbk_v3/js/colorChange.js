$(document).ready(function () {

  const pageUrl = window.location.href;
  const quantity = $('.plusOneList').length;
  const greenGradient = [24, 214, 0, quantity];
  const yellowGradient = [250, 214, 0, quantity];
  const blueGradient = [0, 186, 233, quantity];
  const grayGradient = [143, 143, 143, quantity];

  // Функция для передачи цвета 
  function* colorChange(r, g, b, quantity) {

    const toStr = ({ r, g, b }) => `rgb(${r}, ${g}, ${b})`;

    const current = { r, g, b };
    const stepG = Math.round(20 / quantity);
    const stepB = Math.round(130 / quantity);

    while (true) {
      yield toStr(current);
      current.g += stepG;
      current.b += stepB;
    }
  }

  // функция принимает цвет и запускает colorChange
  function setColor(rgbColor) {
    let generateColor = colorChange(...rgbColor);
    // если активный и повторный клик , то не обрабатываем
    if ($(this).hasClass('active')) return;
    // если нет, то убираем активность со всех
    $('.statisticsBl ').each(function (i, el) {
      $(el).removeClass('active');
    })

    // и устанавливаем только для текущего
    $(this).addClass('active');
    $('.plusOneList_title').each(function (i, el) {
      let currentColor = generateColor.next().value;
      $(el).css('color', currentColor);
    });
  }

  // Смотрим на URL и в завиимости от урла устанавливаем цвет

  if (pageUrl.indexOf('/leaders/ecology') !== -1 || pageUrl.indexOf('/posttags/ecology') !== -1) {
    setColor(greenGradient);
    return
  }
  if (pageUrl.indexOf('/leaders/community') !== -1 || pageUrl.indexOf('/posttags/community') !== -1) {
    setColor(yellowGradient);
    return
  }
  if (pageUrl.indexOf('/leaders/economy') !== -1 || pageUrl.indexOf('/posttags/economy') !== -1) {
    setColor(blueGradient);
    return
  }
  if (pageUrl.indexOf('/authors/platform') !== -1) {
    setColor(grayGradient);
    return
  }

});
