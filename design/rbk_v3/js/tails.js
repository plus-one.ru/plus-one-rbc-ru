$(document).ready(function () {
    var $grid = $(".grid").masonry({
        // columnWidth: 360,
        itemSelector: ".grid-item",
        gutter: 20
    });

    var msnry = $grid.data('masonry');

    function reloadMsnry() {
        setTimeout(
            function () {
                $grid.masonry('reloadItems')
                $grid.masonry('layout')
            },
            1000);
    }

    function getPenPath() {
        reloadMsnry();
        let prefixAPI =  window.location.origin + '/api' ;
        let pageCounter = window.location.pathname.substr(-1) === '/' ? '' : '/';
        pageCounter += "" + (this.loadCount + 2) + "/";
        return prefixAPI + window.location.pathname +  pageCounter +  window.location.search;
    }

    $grid.infiniteScroll({
        path: getPenPath,
        append: '.grid-item',
        outlayer: msnry,
        status: '.page-load-status',
        history: false,
    });

    // при подгрузке новых блоков запускать функцию для уменьшения шрифта
    $grid.on("append.infiniteScroll", function (event, response, path, items) {
        document.fonts.ready.then(function () {
            let scrollPage = false;
            if ($(".scroll")) scrollPage = true;
            if (scrollPage && document.documentElement.clientWidth > 1141) {
                // уменьшение шрифта у блоков факт недели
                for (var i = 0; i < $(".fact1_3BlWrap  .mainFact1_3").length; i++) {
                    window.myScale(
                        $(".fact1_3BlWrap")
                            .eq(i)
                            .find(".mainFact1_3")[0],
                        $(".fact1_3BlWrap").eq(i),
                        true
                    );
                }
            }
        });
    });

    // первая инициализация
    document.fonts.ready.then(function () {
        let scrollPage = false;
        if ($(".scroll")) scrollPage = true;
        if ((scrollPage && document.documentElement.clientWidth > 1141)) {
            // уменьшение шрифта у блоков факт недели
            for (var i = 0; i < $(".fact1_3BlWrap  .mainFact1_3").length; i++) {
                window.myScale(
                    $(".fact1_3BlWrap")
                        .eq(i)
                        .find(".mainFact1_3")[0],
                    $(".fact1_3BlWrap").eq(i),
                    true
                );
            }
        }
    });

});