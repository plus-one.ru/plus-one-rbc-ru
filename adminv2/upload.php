<?php
chdir('../');

require_once('Widget.class.php');
$widget = new Widget();

$targetPath = getcwd() . '/files/products/';
if (!empty($_FILES)) {

    // есть ли такой товар
    $query = sql_placeholder('SELECT product_id FROM products WHERE product_id=?', intval($_POST['product_id']));
    $widget->db->query($query);
    $prd = $widget->db->result();

    if (!empty($prd)){
        $tempFile = $_FILES['file']['tmp_name'];
        $targetFile =  $targetPath. $_FILES['file']['name'];

        if (move_uploaded_file($tempFile,$targetFile)){
            // запишем в базу картинку товара
            $query = sql_placeholder('INSERT IGNORE INTO images SET filename=?, product_id=?', $_FILES['file']['name'], intval($_POST['product_id']));
            $widget->db->query($query);
        }

    }



}
