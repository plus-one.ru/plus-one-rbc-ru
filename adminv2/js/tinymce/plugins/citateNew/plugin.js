/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*global tinymce:true */

tinymce.PluginManager.add('citateNew', function(editor) {
	let win;
	let templateSelector = 'blockquote.citate-block';
	let lastSelectedTemplate = false;

	function showDialog(data) {
		function onSubmitForm(e) {
			// Setup new data excluding style properties
			/*eslint dot-notation: 0*/

			editor.undoManager.transact(function() {
				var block = editor.dom.create('blockquote', {'class': 'citate-block', 'contenteditable': false});
				var str = `<div class="holder">
									<p>${e.data.citateText}</p>							
									<cite class="cite">
										<span class="pic">
											<span>
												<img src="${e.data.src}" citatetext="${e.data.citateText}" caption="true" alt="${e.data.title}">
											</span>
										</span>
										<span class="citeText-wrap">
											${e.data.title}
										</span>
									</cite>
								</div>`;

				block.innerHTML = str;

				editor.insertContent(block.outerHTML);
			});
		}

		function onBeforeCall(e) {
			e.meta = win.toJSON();
		}

		// General settings shared between simple and advanced dialogs
		var generalFormItems = [
			{
				name: 'src',
				type: 'filepicker',
				filetype: 'image',
				label: 'Фото',
				autofocus: true,
				onchange: srcChange,
				onbeforecall: onBeforeCall,
				value: data.src
			},
			{name: 'citateText', type: 'textbox', multiline: true, label: 'Текст цитаты', value: data.citateText},
			{name: 'title', type: 'textbox', label: 'Автор', value: data.title}
		];

		// Simple default dialog
		win = editor.windowManager.open({
			minWidth: 780,
			title: 'Вставка цитаты',
			data: data,
			body: generalFormItems,
			onSubmit: onSubmitForm
		});

	}

	editor.addButton('citateNew', {
		image: '/adminv2/js/tinymce/skins/lightgray/img/citate.png',
		tooltip: 'Цитата',
		onclick: showDialog,
		stateSelector: 'img:not([data-mce-object],[data-mce-placeholder]),figure.image'
	});

	editor.addCommand('mceImage', showDialog);

	function removeTemplate() {
		return function () { lastSelectedTemplate.remove(); };
	}

	function editCitateNew() {
		let imgElm = lastSelectedTemplate.querySelector('.pic img');
		let text = lastSelectedTemplate.querySelector('.holder > p');
		let author = lastSelectedTemplate.querySelector('.citeText-wrap');
		let data = {
			src: imgElm.getAttribute('src'),
			citateText: text.innerText,
			title:  author.innerText
		};
		showDialog(data);
	}

	function isEditableTemplate(eventElement) {
		return editor.dom.is(eventElement, templateSelector);
	}

	tinymce.util.Tools.each({
		mceEditCitateNew: editCitateNew,
		mceDeleteCitateNew: removeTemplate()
	}, function (fn, cmd) {
		editor.addCommand(cmd, fn);
	});

	function addToolbars() {
		let toolbarItems = false;

		if (!toolbarItems) {
			toolbarItems = 'editCitateNew | deleteCitateNew';
		}

		editor.addContextToolbar(
			isEditableTemplate,
			toolbarItems
		);
	}

	addToolbars();
	function addButtons() {

		editor.addButton('deleteCitateNew', {
			title: 'Удалить',
			text: 'Удалить',
			cmd: 'mceDeleteCitateNew'
		});
		editor.addButton('editCitateNew', {
			title: 'Редактировать',
			text: 'Редактировать',
			cmd: 'mceEditCitateNew'
		});
	}

	addButtons();

	let imageListCtrl = {};
	function srcChange(e) {
		let srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

		if (imageListCtrl) {
			imageListCtrl.value = editor.convertURL(this.value(), 'src');
		}

		tinymce.each(meta, function(value, key) {
			win.find('#' + key).value(value);
		});

		if (!meta.width && !meta.height) {
			srcURL = editor.convertURL(this.value(), 'src');

			// Pattern test the src url and make sure we haven't already prepended the url
			prependURL = editor.settings.image_prepend_url;
			absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
			if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
				srcURL = prependURL + srcURL;
			}

			this.value(srcURL);
		}
	}
	function addEvents() {
		editor.on('NodeChange', function(e) {
			if(lastSelectedTemplate) {
				lastSelectedTemplate.classList.remove('selected--template');
			}
			lastSelectedTemplate = false;
			//Set up the lastSelectedTemplate
			let isEditable = isEditableTemplate(e.element);
			if (!isEditable) {
				e.parents.forEach(function (el, i) {
					if (lastSelectedTemplate) { return ;}
					if (editor.dom.is(el, templateSelector) ) {
						lastSelectedTemplate = el;
					}
				});
			} else {
				lastSelectedTemplate = e.element;
			}
			if (lastSelectedTemplate) {
				lastSelectedTemplate.classList.add('selected--template');
			}
		});
	}
	addEvents();
});
