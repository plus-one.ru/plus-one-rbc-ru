<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>{$title|escape}</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=Permissions">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Имя пользователя</label>
                                        <input name="name_user" type="text" value='{$Item->name|escape}'
                                               placeholder="Укажите имя пользователя" class="form-control" autocomplete="off" />
                                    </div>
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Логин пользователя</label>
                                        <input id="usrl" name="usrl" type="text" value='{$Item->login|escape}' placeholder="Укажите логин пользователя" class="form-control" autocomplete="off" />
                                    </div>
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Пароль пользователя</label>
                                        <input id="usrp" name="usrp" type="password" value='' placeholder="Укажите пароль пользователя" class="form-control" autocomplete="off" />
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1" {if $Item->enabled==1}checked{/if} /> Активен
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group">
                                        <label>Модули системы для доступа пользователя</label>
                                        {foreach item=module from=$modules name=module}
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="modules[{$module->module_id}]" {if $module->check==1}checked{/if} value="1" /> {$module->name|escape}
                                                </label>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{*{include file='tinymce_init.tpl'}*}

{*{if $Settings->meta_autofill}*}
    {*<!-- Autogenerating meta tags -->*}
{*{literal}*}
    {*<script>*}
        {*var item_form = document.form;*}
        {*var url_touched = true;*}

        {*// generating meta_title*}
        {*function generate_url(name) {*}
            {*url = name;*}
            {*return translit(url);*}
        {*}*}


        {*// sel all metatags*}
        {*function set_meta() {*}
            {*var name = item_form.name.value;*}

            {*// Url*}
            {*if (!url_touched)*}
                {*item_form.url.value = generate_url(name);*}

        {*}*}

        {*function translit(url) {*}
            {*url = url.replace(/[\s]+/gi, '_');*}
            {*return url.replace(/[^0-9a-zа-я_]+/gi, '');*}
        {*}*}

        {*function autometageneration_init() {*}

            {*var name = item_form.name.value;*}

            {*if (item_form.url.value == '' || item_form.url.value == generate_url(name))*}
                {*url_touched = false;*}
        {*}*}

        {*// Attach events*}
        {*function myattachevent(target, eventName, func) {*}
            {*if (target.addEventListener)*}
                {*target.addEventListener(eventName, func, false);*}
            {*else if (target.attachEvent)*}
                {*target.attachEvent("on" + eventName, func);*}
            {*else*}
                {*target["on" + eventName] = func;*}
        {*}*}

        {*if (window.attachEvent) {*}
            {*window.attachEvent("onload", function () {*}
                {*setTimeout("autometageneration_init();", 1000)*}
            {*});*}
        {*} else if (window.addEventListener) {*}
            {*window.addEventListener("DOMContentLoaded", autometageneration_init, false);*}
        {*} else {*}
            {*document.addEventListener("DOMContentLoaded", autometageneration_init, false);*}
        {*}*}


        {*myattachevent(item_form.url, 'change', function () {*}
            {*url_touched = true*}
        {*});*}
        {*myattachevent(item_form.name, 'keyup', set_meta);*}
        {*myattachevent(item_form.name, 'change', set_meta);*}


    {*</script>*}
{*{/literal}*}
    {*<!-- END Autogenerating meta tags -->*}
{*{/if}*}