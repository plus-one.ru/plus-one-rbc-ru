<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Покупатели</h1>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Error}
<div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    {$Error}
</div>
{/if}

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;

                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="index.php?section=Users&xls=1">
                            <i class="fa fa-file-excel-o"></i> Выгрузить в XLS
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {if $Items}
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 5%;"></th>
                                <th style="width: 10%;">Зарегистрирован</th>
                                <th style="width: 10%;">Последний визит</th>
                                <th style="width: 25%;">Имя / email</th>
                                <th style="width: 10%;">Заказов</th>
                                <th style="width: 5%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=item from=$Items name=item}
                                <tr>
                                    <td>
                                        <a href="users/enable/{$item->user_id}">
                                        {if $item->enabled}
                                            <i class="fa fa-toggle-on fa-1x"></i>
                                        {else}
                                            <i class="fa fa-toggle-off fa-1x"></i>
                                        {/if}
                                        </a>
                                    </td>
                                    <td>
                                        {$item->date_created|escape}<br/>
                                        {$item->time_created|escape}
                                    </td>
                                    <td>
                                        {$item->date_modified|escape}<br/>
                                        {$item->time_modified|escape}
                                    </td>
                                    <td>
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            {if $item->firstname || $item->lastname}
												{$item->firstname|escape} {$item->lastname|escape}
											{else}
												Пользователь не представился
											{/if}
                                        </p>
                                        <p>
                                            <a href="mailto:{$item->email|escape}">{$item->email|escape}
                                        </p>
                                    </td>
                                    <td>
                                        {$item->orders_num|escape}
                                    </td>
                                    <td>
                                        <a href="users/delete/{$item->user_id}" class="btn btn-danger btn-xs" type="button" onclick='if(!confirm("{$Lang->ARE_YOU_SURE_TO_DELETE}")) return false;'>
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                {else}
                    <p>Еще нет ни одной зарегистрированного покупателя.</p>
                {/if}
            </div>
        </div>
    </div>
</div>