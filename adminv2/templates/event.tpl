<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=Events&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->header}has-warning{/if}">
                                        <label>Название мероприятия</label>
                                        <input name="name" type="text" value='{$Item->header|escape}'
                                               placeholder="Укажите заголовок мероприятия" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Url мероприятия</label>
                                        <input name="url" type="text" value='{$Item->url|escape}'
                                               placeholder="Укажите Url мероприятия" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="well">
                                    <label>Анонс</label>

                                    <div class="form-group">
                                        {foreach item=tag from=$tags name=tag}
                                            {if $tag->url != 'main'}
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="tags" value="{$tag->id}" {if $tag->id == $Item->tag}checked{/if}> {$tag->name|escape}
                                                </label>
                                            {/if}
                                        {/foreach}

                                        <label class="checkbox-inline">
                                            <input type="radio" name="tags" value="0" {if $Item->tag == 0}checked{/if}> Ни одного
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->image}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Основное изображение</label>
                                                <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group">
                                        <label>Лид</label>
                                        <textarea rows="20" id="lead" name="lead" class="form-control smalleditor">{$Item->lead}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group">
                                        <label>Текст статьи</label>
                                        <textarea rows="20" id="body" name="body" class="form-control fulleditor">{$Item->body}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{include file='tinymce_init.tpl'}


{if $Settings->meta_autofill}
    <!-- Autogenerating meta tags -->
{literal}
    <script>
        var item_form = document.form;
        var url_touched = true;

        // generating meta_title
        function generate_url(name) {
            url = name;
            return translit(url);
        }


        // sel all metatags
        function set_meta() {
            var name = item_form.name.value;

            // Url
            if (!url_touched)
                item_form.url.value = generate_url(name);

        }

        function translit(url) {
            url = url.replace(/[\s]+/gi, '_');
            return url.replace(/[^0-9a-zа-я_]+/gi, '');
        }

        function autometageneration_init() {

            var name = item_form.name.value;

            if (item_form.url.value == '' || item_form.url.value == generate_url(name))
                url_touched = false;
        }

        // Attach events
        function myattachevent(target, eventName, func) {
            if (target.addEventListener)
                target.addEventListener(eventName, func, false);
            else if (target.attachEvent)
                target.attachEvent("on" + eventName, func);
            else
                target["on" + eventName] = func;
        }

        if (window.attachEvent) {
            window.attachEvent("onload", function () {
                setTimeout("autometageneration_init();", 1000)
            });
        } else if (window.addEventListener) {
            window.addEventListener("DOMContentLoaded", autometageneration_init, false);
        } else {
            document.addEventListener("DOMContentLoaded", autometageneration_init, false);
        }


        myattachevent(item_form.url, 'change', function () {
            url_touched = true
        });
        myattachevent(item_form.name, 'keyup', set_meta);
        myattachevent(item_form.name, 'change', set_meta);


    </script>
{/literal}
    <!-- END Autogenerating meta tags -->
{/if}