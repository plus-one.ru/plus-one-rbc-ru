<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Покупатель</h1>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button" href="index.php?section=Articles&menu={$Menu->menu_id}&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="well">
                                <div class="form-group {if !$Item->name}has-warning{/if}">
                                    <label>Название страницы</label>
                                    <input name="name" type="text" value='{$Item->name|escape}' placeholder="Название" class="form-control">
                                </div>
                                <div class="form-group {if !$Item->header}has-warning{/if}">
                                    <label>Заголовок страницы</label>
                                    <input name="header" type="text" value='{$Item->header|escape}' placeholder="Заголовок" class="form-control">
                                </div>
                                <div class="form-group input-group {if !$Item->url}has-warning{/if}">
                                    <span class="input-group-addon">http://{$root_url}/articles/</span>
                                    <input name="url" class="form-control" type="text" placeholder="URL" value='{$Item->url|escape}'>
                                </div>

                                <div class="form-group {if $Item->category_id==0 || !$Item->category_id}has-warning{/if}">
                                    <label>Раздел</label>
                                    <select class="form-control" name="category">
                                        <option value='0' selected>Выберите раздел</option>
                                        {foreach name=ac key=key item=ac from=$articlescategories}
                                            <option value='{$ac->id}' {if $Item->category_id EQ $ac->id}selected{/if}>{$ac->name|escape}</option>
                                        {/foreach}
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Раздел каталога товаров</label>
                                    <select class="form-control" name="category_products">
                                        <option value='0' selected>Выберите раздел каталога товаров</option>
                                        {foreach name=pc key=key item=pc from=$categories}
                                            <option value='{$pc->category_id}' {if $Item->category_products_id EQ $pc->category_id}selected{/if}>{$pc->name|escape}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="well">
                                <div class="form-group">
                                    <label>Meta Title</label>
                                    <input name="meta_title" type="text" value='{$Item->meta_title}' placeholder="Meta Title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Meta Keywords</label>
                                    <input name="meta_keywords" type="text" value='{$Item->meta_keywords}' placeholder="Meta Keywords" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Meta Description</label>
                                    <input name="meta_description" type="text" value='{$Item->meta_description}' placeholder="Meta Description" class="form-control">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="enabled" value="1" {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Изображения /-->
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="well">
                                <div class="row">
                                    <div class="col-lg-3">
                                        {if $Item->large_image}
                                            <img id="large_image" class="image_preview" src='../files/articles/{$Item->large_image}?r={math equation="rand(1,1000000)"}' alt="" style="width: 100%;" />
                                            <p>
                                                <a id="button_delete_largeimage" class="btn btn-outline btn-danger btn-xs" type="button" href="#">
                                                    <i class="fa fa-times-circle"></i> Удалить изображение
                                                </a>
                                            </p>
                                        {else}
                                            <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                        {/if}
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label>Основное изображение</label>
                                            <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                        </div>
                                        {if $UseGd}
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input {if (!$Item->small_image && !$smarty.post) || $smarty.post.auto_small == 1}checked{/if} name='auto_small' value='1' type="checkbox"/> создать маленькое автоматически
                                                    </label>
                                                </div>
                                            </div>
                                        {/if}
                                    </div>
                                </div>
                                <input type="hidden" value="0" id="delete_large_image" name="delete_large_image" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="well">
                                <div class="row">
                                    <div class="col-lg-3">
                                        {if $Item->small_image}
                                            <img id="small_image" class="image_preview" src='../files/articles/{$Item->small_image}?r={math equation="rand(1,1000000)"}' alt="" style="width: 100%;" />
                                            <p>
                                                <a id="button_delete_smallimage" class="btn btn-outline btn-danger btn-xs" type="button" href="#">
                                                    <i class="fa fa-times-circle"></i> Удалить изображение
                                                </a>
                                            </p>
                                        {else}
                                            <img id="small_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                        {/if}
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label>Маленькое изображение</label>
                                            <input name="small_image" type="file" style="width: 100%; overflow: hidden;"/>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="0" id="delete_small_image" name="delete_small_image" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well">
                                <div class="form-group">
                                    <label>Краткий текст статьи</label>
                                    <textarea id="annotation" name="annotation" rows="20" class="form-control">{$Item->annotation|escape}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well">
                                <div class="form-group">
                                    <label>Полный текст статьи</label>
                                    <textarea id="body" name="body" rows="20" class="form-control">{$Item->body|escape}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


{include file='tinymce_init.tpl'}

{if $Settings->meta_autofill}
    <!-- Autogenerating meta tags -->
{literal}
    <script>

        // Templates
        var meta_title_template = '%name';
        var meta_keywords_template = '%name';
        var meta_description_template = '%text';

        var item_form = document.form;

        var meta_title_touched = true;
        var meta_keywords_touched = true;
        var meta_description_touched = true;
        var url_touched = true;

        // generating meta_title
        function generate_title(template, name, text)
        {
            return template.replace('%name', name).replace('%text', text).replace(/^(,\s)+|\s+$/g,"");
        }

        // generating meta_keywords
        function generate_keywords(template, name, text)
        {
            return template.replace('%name', name).replace('%text', text).replace(/^(,\s)+|\s+$/g,"");
        }

        // generating meta_description
        function generate_description(template, name,  text)
        {
            return template.replace('%name', name).replace('%text', text).replace(/^\s+|\s+$/g,"");
        }

        // generating meta_title
        function generate_url(name)
        {
            url = name;
            return translit(url);
        }


        // sel all metatags
        function set_meta()
        {
            var name = item_form.header.value;

            var text = tinyMCE.get("body").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ");

            // Meta Title
            if(!meta_title_touched)
                item_form.meta_title.value = generate_title(meta_title_template, name, text);

            // Meta Keywords
            if(!meta_keywords_touched)
                item_form.meta_keywords.value = generate_keywords(meta_keywords_template, name, text);

            // Meta Description
            if(!meta_description_touched)
                item_form.meta_description.value = generate_description(meta_description_template, name, text);

            // Url
            if(!url_touched)
                item_form.url.value = generate_url(name);

        }

        function translit(url){
            url = url.replace(/[\s]+/gi, '_');
            return url.replace(/[^0-9a-zа-я_]+/gi, '');
        }

        function autometageneration_init()
        {
            tinyMCE.get("body").onChange.add(set_meta);
            tinyMCE.get("body").onKeyUp.add(set_meta);

            var name = item_form.header.value;

            var text = tinyMCE.get("body").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ");

            if(item_form.meta_title.value == '' || item_form.meta_title.value == generate_title(meta_title_template, name, text))
                meta_title_touched=false;
            if(item_form.meta_keywords.value == '' || item_form.meta_keywords.value == generate_keywords(meta_keywords_template, name, text))
                meta_keywords_touched=false;
            if(item_form.meta_description.value == '' || item_form.meta_description.value == generate_description(meta_description_template, name, text))
                meta_description_touched=false;
            if(item_form.url.value == '' || item_form.url.value == generate_url(name))
                url_touched=false;
        }

        // Attach events
        function myattachevent(target, eventName, func)
        {
            if ( target.addEventListener )
                target.addEventListener(eventName, func, false);
            else if ( target.attachEvent )
                target.attachEvent("on" + eventName, func);
            else
                target["on" + eventName] = func;
        }

        if (window.attachEvent) {
            window.attachEvent("onload", function(){setTimeout("autometageneration_init();", 1000)});
        } else if (window.addEventListener) {
            window.addEventListener("DOMContentLoaded", autometageneration_init, false);
        } else {
            document.addEventListener("DOMContentLoaded", autometageneration_init, false);
        }



        myattachevent(item_form.url, 'change', function(){url_touched = true});
        myattachevent(item_form.meta_title, 'change', function(){meta_title_touched = true});
        myattachevent(item_form.meta_keywords, 'change', function(){meta_keywords_touched = true});
        myattachevent(item_form.meta_description, 'change', function(){meta_description_touched = true});
        myattachevent(item_form.header, 'keyup',  set_meta);
        myattachevent(item_form.header, 'change', set_meta);


    </script>
{/literal}
    <!-- END Autogenerating meta tags -->
{/if}


<!-- Управление товарами /-->

<div id="inserts_all">
  <!-- Вкладки /-->
  <ul id="inserts">
    <li><a href="index.php?section=Users{if $smarty.get.group}&group={$smarty.get.group}{/if}{if $smarty.get.page}&page={$smarty.get.page}{/if}{if $smarty.get.keyword}&keyword={$smarty.get.keyword}{/if}" class="on">покупатели</a></li>
    <li><a href="index.php?section=Groups" class="off">группы</a></li>
  </ul>
  <!-- /Вкладки /-->
   
  <!-- Путь /-->
  <table id="in_right">
    <tr>
      <td>
        <p>
          <a href="./">Simpla</a> →
          <a href='index.php?section=Users'>Покупатели</a> →
          {if $User->user_id}{$User->name}{else}Новый покупатель{/if}
        </p>
      </td>
    </tr>
  </table>
  <!-- /Путь /-->
</div>

 
<!-- Content #Begin /-->
<div id="content">
  <div id="cont_border">
    <div id="cont">
     
      <div id="cont_top">
        <!-- Иконка раздела /--> 
	    <img src="./images/icon_content.jpg" alt="" class="line"/>
	    <!-- /Иконка раздела /-->
	    
	    <!-- Заголовок раздела /-->
        <h1 id="headline">{if $User->user_id}{$User->name}{else}Новый покупатель{/if}</h1>
        <!-- /Заголовок раздела /-->
        
        <!-- Помощь /-->
        <div id="help">
          <a href="usermanual.html?height=450&width=700&scrollto=users" title="Помощь" class="thickbox">Помощь</a>
        </div>
        <!-- /Помощь /-->
      </div>

      <div id="cont_center">

     
        <div class="clear">&nbsp;</div>	  
        {if $Error}
        <!-- Error #Begin /-->
        <div id="error_minh">
          <div id="error">
            <img src="./images/error.jpg" alt=""/><p>{$Error}</p>					
          </div>
        </div>
        <!-- Error #End /-->
        {/if}
          



        <!-- Форма товара #Begin /-->

				<FORM name=user METHOD=POST>
					<div id="over">		
					<div id="over_left">	
							<table>
								<tr>
									<td class="model">Имя, фамилия</td>
									<td class="m_t"><p><input name="name" type="text" class="input3" value='{$User->name|escape}'  {literal}pattern='.{1,}'{/literal} notice='{$Lang->ENTER_NAME}'/></p></td>
								</tr>
								<tr>
									<td class="model">Email</td>
									<td class="m_t"><p><input name="email" type="text" class="input3" value='{$User->email|escape}' /></p></td>
								</tr>
								<tr>
									<td class="model">Компания</td>
									<td class="m_t"><p><input name="company" type="text" class="input3" value='{$User->name_company|escape}' /></p></td>
								</tr>								
								<tr>
									<td class="model">Город</td>
									<td class="m_t"><p><input name="town" type="text" class="input3" value='{$User->city_company|escape}' /></p></td>
								</tr>
								<tr>
									<td class="model">Телефон</td>
									<td class="m_t"><p><input name="phone" type="text" class="input3" value='{$User->phone_company|escape}' /></p></td>
								</tr>
								<tr>
									<td class="model">Является партнером?</td>
									<td class="m_t">
										<p>{if $User->im_partner==1}Да{else}Нет{/if}</p>
										{if $User->group_id != "3"}
											<p style="color: red; font-size: 11px;">Эти данные указал пользователь. необходимо проверять достоверность, и только после проверки пользователя можно перенести в группу "Партнеры"</p>
										{/if}
									</td>
								</tr>
								<tr>
									<td class="model">Группа</td>
									<td class="m_t"><p>
									
										<select name=group_id class="select2">
                                          <OPTION VALUE='' {if !$User->group_id}SELECTED{/if}>Не определена</OPTION>
                                         {foreach name=group key=key item=group from=$Groups}
                                            {if $User->group_id == $group->group_id}
                                              <OPTION VALUE='{$group->group_id}' SELECTED>{$group->name|escape}</OPTION>
                                            {else}
                                              <OPTION VALUE='{$group->group_id}'>{$group->name|escape}</OPTION>
                                            {/if}
                                          {/foreach}
										</select>
										<nobr><input name=enabled type="checkbox" class="checkbox" {if $User->enabled}checked{/if} value='1'/><span class="akt">Активен</span></nobr> &nbsp; &nbsp;
										
									</p>
									</td>
								</tr>
							</table>


							<p><input type="submit" value="Сохранить" class="submit"/></p>
					</div>
					


				</div>
				<br/><br/>
			</div>
			<input name="sent_notice" type="hidden" class="input3" value='{$User->sent_notice}' />
			</form>
			
	 
    </div>
  </div>	    
</div>
<!-- Content #End /--> 
