<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <input type="hidden" value="{$Item->id}" name="item_id">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlogWriters&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Заголовок</label>
                                        <input name="header" type="text" value='{$Item->header|escape}'
                                               placeholder="Укажите заголовок" class="form-control">
                                    </div>
                                </div>
                            </div>
                          {*  <div class="col-lg-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->image_name}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image_name}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%; height: auto"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Основное изображение</label>
                                                <input name="image_name" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>
                            </div>*}
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group">
                                        {*<label>Текст статьи</label>*}
                                        <textarea rows="20" id="body" name="body" class="form-control fulleditor">{$Item->body}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{include file='tinymce_init.tpl'}
