<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

<div class="row">
<form name="blogpost" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="dropzone" enctype="multipart/form-data">
    <input type="hidden" name="item_id" id="posttag_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            {if $preview}
                                <script>
                                    window.open('{$root_url}{$articleUrl}?mode=preview', '_blank');
                                </script>
                            {/if}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">

                                    {include file='include/format_colors.tpl'}

                                    <div class="form-group">
                                        <label id="writers_label" {if !$Item->writers}class="text-danger"{/if}>
                                            Автор
                                        </label>
                                        <select class="form-control" id="writers_input" name="writers">
                                            <option value="0"> -- Выберите автора -- </option>
                                            {foreach item=writer from=$writers name=writer}
                                                <option value="{$writer->id}" {if $Item->writers==$writer->id}selected{/if}>{$writer->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи," class="form-control">
                                    </div>

                                    <div class="form-group">
                                        {include file="include/tagsPost.tpl"}
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_partner_material" value="1"
                                                       {if $Item->is_partner_material==1}checked{/if} /> Партнерский материал
                                            </label>
                                        </div>
                                    </div>
                                    {*<div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="useful_city" value="1" {if $Item->useful_city==1}checked{/if} /> Опубликовать в Полезном городе
                                            </label>
                                        </div>
                                    </div>*}

                                </div>
                            </div>

                            <div class="col-lg-6">
                                {include file="include/tagFirstLevel.tpl"}
                            </div>

                            <div class="row"><!-- /--></div>

                            <div class="col-lg-6">
                                <div class="well">
                                    <h3>Данные</h3>

                                    <label>Укажите единицу измерения</label>
                                    {foreach item=um from=$unitMeasures name=um}
                                        <label class="checkbox-inline">
                                            <input type="radio" name="unit_measure" value="{$um->id}" {if $um->id == $Item->unit_measure}checked{/if}> {$um->name|escape} ({$um->short_name|escape})
                                        </label>
                                    {/foreach}

                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Число для учета</label>
                                        <input name="amount_to_account" type="text" value='{$Item->amount_to_account|escape}' placeholder="Укажите число для учета" class="form-control">
                                    </div>



                                    <div class="form-group">
                                        <label>Год для учета</label>

                                        <select class="selectpicker form-control" data-live-search="true" name="year_to_ammount" title="-- Укажите год для учета --">

                                            <option value="2000" {if $Item->year_to_ammount == '2000'}selected{/if}>
                                                2000
                                            </option>
                                            <option value="2001" {if $Item->year_to_ammount == '2001'}selected{/if}>
                                                2001
                                            </option>
                                            <option value="2002" {if $Item->year_to_ammount == '2002'}selected{/if}>
                                                2002
                                            </option>
                                            <option value="2003" {if $Item->year_to_ammount == '2003'}selected{/if}>
                                                2003
                                            </option>
                                            <option value="2004" {if $Item->year_to_ammount == '2004'}selected{/if}>
                                                2004
                                            </option>
                                            <option value="2005" {if $Item->year_to_ammount == '2005'}selected{/if}>
                                                2005
                                            </option>
                                            <option value="2006" {if $Item->year_to_ammount == '2006'}selected{/if}>
                                                2006
                                            </option>
                                            <option value="2007" {if $Item->year_to_ammount == '2007'}selected{/if}>
                                                2007
                                            </option>
                                            <option value="2008" {if $Item->year_to_ammount == '2008'}selected{/if}>
                                                2008
                                            </option>
                                            <option value="2009" {if $Item->year_to_ammount == '2009'}selected{/if}>
                                                2009
                                            </option>
                                            <option value="2010" {if $Item->year_to_ammount == '2010'}selected{/if}>
                                                2010
                                            </option>
                                            <option value="2011" {if $Item->year_to_ammount == '2011'}selected{/if}>
                                                2011
                                            </option>
                                            <option value="2012" {if $Item->year_to_ammount == '2012'}selected{/if}>
                                                2012
                                            </option>
                                            <option value="2013" {if $Item->year_to_ammount == '2013'}selected{/if}>
                                                2013
                                            </option>
                                            <option value="2014" {if $Item->year_to_ammount == '2014'}selected{/if}>
                                                2014
                                            </option>
                                            <option value="2015" {if $Item->year_to_ammount == '2015'}selected{/if}>
                                                2015
                                            </option>
                                            <option value="2016" {if $Item->year_to_ammount == '2016'}selected{/if}>
                                                2016
                                            </option>
                                            <option value="2017" {if $Item->year_to_ammount == '2017' || $Item->year_to_ammount == ''}selected{/if}>
                                                2017
                                            </option>
                                            <option value="2018" {if $Item->year_to_ammount == '2018'}selected{/if}>
                                                2018
                                            </option>
                                            <option value="2019" {if $Item->year_to_ammount == '2019'}selected{/if}>
                                                2019
                                            </option>
                                            <option value="2020" {if $Item->year_to_ammount == '2020'}selected{/if}>
                                                2020
                                            </option>
                                            <option value="2021" {if $Item->year_to_ammount == '2021'}selected{/if}>
                                                2021
                                            </option>
                                            <option value="2022" {if $Item->year_to_ammount == '2022'}selected{/if}>
                                                2022
                                            </option>
                                            <option value="2023" {if $Item->year_to_ammount == '2023'}selected{/if}>
                                                2023
                                            </option>
                                            <option value="2024" {if $Item->year_to_ammount == '2024'}selected{/if}>
                                                2024
                                            </option>
                                            <option value="2025" {if $Item->year_to_ammount == '2025'}selected{/if}>
                                                2025
                                            </option>
                                        </select>
                                    </div>


                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Число для отображения</label>
                                        <input name="amount_to_displaying" type="text" value='{$Item->amount_to_displaying|escape}' placeholder="Укажите число для отображения" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Подпись к числу</label>
                                        <input name="signature_to_sum" type="text" value='{$Item->signature_to_sum|escape}' placeholder="миллионов рублей или рублей" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Текст</label>
                                        <textarea name="text_body" class="form-control smalleditor">{$Item->text_body}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well" id="dynamic_analitic">
                                    <h3>
                                        Динамика
                                        <div class="pull-right">
                                            <div class="btn-group">
                                                <button onclick="addDynamicAnaliticElements(); return false;" id="btnAddRow" class="btn btn-primary btn-xs" type="button">
                                                    Добавить строку
                                                </button>
                                            </div>
                                        </div>
                                    </h3>


                                    {if $Item->analitycData}
                                        {foreach item=adata from=$Item->analitycData name=adata}
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Год</label>
                                                    <select class="form-control" name="years[]">
                                                        <option {if $adata->years == 2017}selected{/if}>2017</option>
                                                        <option {if $adata->years == 2016}selected{/if}>2016</option>
                                                        <option {if $adata->years == 2015}selected{/if}>2015</option>
                                                        <option {if $adata->years == 2014}selected{/if}>2014</option>
                                                        <option {if $adata->years == 2013}selected{/if}>2013</option>
                                                        <option {if $adata->years == 2012}selected{/if}>2012</option>
                                                        <option {if $adata->years == 2011}selected{/if}>2011</option>
                                                        <option {if $adata->years == 2010}selected{/if}>2010</option>
                                                        <option {if $adata->years == 2009}selected{/if}>2009</option>
                                                        <option {if $adata->years == 2008}selected{/if}>2008</option>
                                                        <option {if $adata->years == 2007}selected{/if}>2007</option>
                                                        <option {if $adata->years == 2006}selected{/if}>2006</option>
                                                        <option {if $adata->years == 2005}selected{/if}>2005</option>
                                                        <option {if $adata->years == 2004}selected{/if}>2004</option>
                                                        <option {if $adata->years == 2003}selected{/if}>2003</option>
                                                        <option {if $adata->years == 2002}selected{/if}>2002</option>
                                                        <option {if $adata->years == 2001}selected{/if}>2001</option>
                                                        <option {if $adata->years == 2000}selected{/if}>2000</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Значение</label>
                                                    <input name="value_to_yeats[]" type="text" value='{$adata->value_to_yeats|escape}' placeholder="Укажите значение" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        {/foreach}
                                    {else}
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Год</label>
                                                    <select class="form-control" name="years[]">
                                                        <option>2017</option>
                                                        <option>2016</option>
                                                        <option>2015</option>
                                                        <option>2014</option>
                                                        <option>2013</option>
                                                        <option>2012</option>
                                                        <option>2011</option>
                                                        <option>2010</option>
                                                        <option>2009</option>
                                                        <option>2008</option>
                                                        <option>2007</option>
                                                        <option>2006</option>
                                                        <option>2005</option>
                                                        <option>2004</option>
                                                        <option>2003</option>
                                                        <option>2002</option>
                                                        <option>2001</option>
                                                        <option>2000</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Значение</label>
                                                    <input name="value_to_yeats[]" type="text" value='{$Item->value_to_yeats|escape}' placeholder="Укажите значение" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Год</label>
                                                    <select class="form-control" name="years[]">
                                                        <option>2017</option>
                                                        <option>2016</option>
                                                        <option>2015</option>
                                                        <option>2014</option>
                                                        <option>2013</option>
                                                        <option>2012</option>
                                                        <option>2011</option>
                                                        <option>2010</option>
                                                        <option>2009</option>
                                                        <option>2008</option>
                                                        <option>2007</option>
                                                        <option>2006</option>
                                                        <option>2005</option>
                                                        <option>2004</option>
                                                        <option>2003</option>
                                                        <option>2002</option>
                                                        <option>2001</option>
                                                        <option>2000</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Значение</label>
                                                    <input name="value_to_yeats[]" type="text" value='{$Item->value_to_yeats|escape}' placeholder="Укажите значение" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Год</label>
                                                    <select class="form-control" name="years[]">
                                                        <option>2017</option>
                                                        <option>2016</option>
                                                        <option>2015</option>
                                                        <option>2014</option>
                                                        <option>2013</option>
                                                        <option>2012</option>
                                                        <option>2011</option>
                                                        <option>2010</option>
                                                        <option>2009</option>
                                                        <option>2008</option>
                                                        <option>2007</option>
                                                        <option>2006</option>
                                                        <option>2005</option>
                                                        <option>2004</option>
                                                        <option>2003</option>
                                                        <option>2002</option>
                                                        <option>2001</option>
                                                        <option>2000</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Значение</label>
                                                    <input name="value_to_yeats[]" type="text" value='{$Item->value_to_yeats|escape}' placeholder="Укажите значение" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_post" value="5">
    <input type="hidden" name="type_material" value="0">
    <input type="hidden" name="type_visual_block" value="data">
    <input type="hidden" id="countrows" value="3" >
</form>
</div>

{include file='tinymce_init.tpl'}