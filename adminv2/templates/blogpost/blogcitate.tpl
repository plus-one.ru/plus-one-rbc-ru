<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

<div class="row">
<form name="blogpost" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="dropzone" enctype="multipart/form-data">
    <input type="hidden" name="item_id" id="posttag_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            {if $preview}
                                <script>
                                    window.open('{$root_url}{$articleUrl}?mode=preview', '_blank');
                                </script>
                            {/if}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                {include file='include/format_post.tpl'}
                                <div class="well">

                                    {include file='include/format_colors.tpl'}

                                    <div class="form-group">
                                        <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Текст цитаты</label>
                                        <textarea name="name" class="form-control smalleditor">{$Item->name}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label id="header_on_page_label">Заголовок для внутренней страницы</label>
                                        <textarea name="header_on_page" class="form-control smalleditor">{$Item->header_on_page}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label id="header_rss_label" {if !$Item->header_rss}class="text-danger"{/if}>Заголовок для социальных сетей (max 140 символов) </label>
                                        <input id="header_rss_input" name="header_rss" type="text" value='{$Item->header_rss|escape}' maxlength="140" placeholder="Укажите заголовок для RSS" class="form-control">
                                        {*<textarea name="header_rss" class="form-control smalleditor">{$Item->header_rss}</textarea>*}
                                    </div>

                                    <div class="form-group">
                                        <label id="writers_label" {if !$Item->writers}class="text-danger"{/if}>
                                            Автор
                                        </label>
                                        <select class="form-control" id="writers_input" name="writers">
                                            <option value="0"> -- Выберите автора -- </option>
                                            {foreach item=writer from=$writers name=writer}
                                                <option value="{$writer->id}" {if $Item->writers==$writer->id}selected{/if}>{$writer->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 form-group">
                                            <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                            <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи," class="form-control">
                                        </div>
                                    </div>

                                    {*<div class="row">
                                        <div class="col-lg-4">
                                            <label id="citate_author_name_label">Фото автора цитаты</label>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    {if $Item->citate_author_photo}
                                                        <img id="citate_author_photo" class="image_preview img-responsive"
                                                             src='{$images_uploaddir}{$Item->citate_author_photo}' alt=""
                                                             style="width: 40px; height: 40px;"/>
                                                    {else}
                                                        <img id="citate_author_photo" class="image_preview" style="width: 40px; height: 40px;" src='images/no_foto.gif' alt=""/>
                                                    {/if}
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Изображение</label>
                                                        <input name="citate_author_photo" type="file"
                                                               style="width: 100%; overflow: hidden;"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label id="citate_author_name_label">Подпись к фото автора цитаты</label>
                                                <input id="citate_author_name_input" name="citate_author_name" type="text" value='{$Item->citate_author_name|escape}' class="form-control">
                                            </div>
                                        </div>
                                    </div>*}

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

{*                                    <div class="form-group">*}
{*                                        <div class="checkbox">*}
{*                                            <label>*}
{*                                                <input type="checkbox" name="is_partner_material" value="1"*}
{*                                                       {if $Item->is_partner_material==1}checked{/if} /> Партнерский материал*}
{*                                            </label>*}
{*                                        </div>*}
{*                                    </div>*}
                                    {*<div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="useful_city" value="1" {if $Item->useful_city==1}checked{/if} /> Опубликовать в Полезном городе
                                            </label>
                                        </div>
                                    </div>*}

                                </div>
                            </div>

                            <div class="col-lg-6">

                                {include file="include/tagFirstLevel.tpl"}

                                <div class="well">
                                    <div class="form-group">
                                        {include file="include/tagsPost.tpl"}
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label>Спецпроект</label>
                                        <select class="form-control" name="spec_project">
                                            <option value="0"> -- Выберите спецпроект -- </option>
                                            {foreach item=specProject from=$specProjects name=specProject}
                                                <option value="{$specProject->id}" {if $Item->spec_project==$specProject->id}selected{/if}>{$specProject->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>


                                <div class="well">
                                    <div class="form-group">
                                        <label>Конференция</label>
                                        <select class="form-control" name="conference">
                                            <option value="0"> -- Выберите конференцию -- </option>
                                            {foreach item=conf from=$conferences name=conf}
                                                <option value="{$conf->id}" {if $Item->conference==$conf->id}selected{/if}>{$conf->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label>Тип материала</label>
                                        <select class="form-control" name="type_material">
                                            <option value="0"> -- Выберите тип материала -- </option>
                                            {foreach item=typeMaterial from=$typeMaterials name=typeMaterial}
                                                <option value="{$typeMaterial->id}" {if $Item->type_material==$typeMaterial->id}selected{/if}>{$typeMaterial->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label>Партнер записи</label>
                                        <select class="form-control" name="partner">
                                            <option value="0"> -- Выберите партнера -- </option>
                                            {foreach item=partner from=$partners name=partner}
                                                <option value="{$partner->id}" {if $Item->partner==$partner->id}selected{/if}>{$partner->name}</option>
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>URL оригинала статьи партнера</label>
                                        <input name="partner_url" type="text" value='{$Item->partner_url|escape}'
                                               placeholder="Укажите URL оригинала статьи партнера" class="form-control">
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label id="url_label" {if !$Item->url}class="text-danger"{/if}>
                                            URL записи
                                        </label>
                                        <input name="url" type="text" value='{$Item->url|escape}'
                                               placeholder="Укажите URL записи" class="form-control">
                                    </div>

                                    {include file="include/_meta_tags.tpl"}
                                </div>
                            </div>

                                <div class="col-lg-6">
                                    <div class="well col-lg-12" >
                                        <h4>Иллюстрация для соцсетей и RSS</h4>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {if $Item->image_1_1}
                                                    <img id="image_1_1" class="image_preview img-responsive"
                                                         src='{$images_uploaddir}{$Item->image_1_1}?{$Item->modified}' alt=""
                                                         style="width: 100%; height: 100%"/>
                                                {else}
                                                    <img id="image_1_1" class="image_preview" src='images/no_foto.gif' alt=""/>
                                                {/if}
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Изображение</label>
                                                    <input name="image_1_1" type="file"
                                                           style="width: 100%; overflow: hidden;"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="col-lg-12">
                                {*<div class="well">*}
                                    {*<div class="form-group">*}
                                        {*<label id="header_label">Текст подзаголовка</label>*}
                                        {*<textarea rows="20" name="header"  id="header" class="form-control fulleditor">{$Item->header}</textarea>*}
                                    {*</div>*}
                                {*</div>*}

                                <div class="well">
                                    <div class="form-group">
                                        <label id="header_label">Лид</label>
                                        <textarea rows="20" name="lead" id="lead" class="form-control fulleditor">{$Item->lead}</textarea>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label>Текст статьи</label>
                                        <textarea rows="20" name="body" id="body" class="form-control fulleditor">{$Item->body}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_post" value="8">
    <input type="hidden" name="type_material" value="0">
    <input type="hidden" name="type_visual_block" value="citate">
    <input type="hidden" id="countrows" value="3" >
</form>
</div>

{include file='tinymce_init.tpl'}
{include file='include/_set_meta.tpl'}