<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

<div class="row">
<form name="blogpost" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="dropzone" enctype="multipart/form-data">
    <input type="hidden" name="item_id" id="posttag_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            {if $preview}
                                <script>
                                    window.open('http://{$root_url}{$articleUrl}?mode=preview', '_blank');
                                </script>
                            {/if}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">

                                    <div class="form-group">
                                        <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Заголовок (max 100 символов)</label>
                                        <textarea name="name" class="form-control smalleditor">{$Item->name}</textarea>
                                    </div>

                                    {*<div class="form-group">*}
                                        {*<label id="header_label">Текст подзаголовка</label>*}
                                        {*<textarea rows="20" name="header"  id="header" class="form-control smalleditor">{$Item->header}</textarea>*}
                                    {*</div>*}

                                    <div style="display: none;">
                                        <div class="form-group">
                                            <label id="header_label">Лид</label>
                                            <textarea rows="20" name="lead" id="lead" class="form-control fulleditor">{$Item->lead}</textarea>
                                        </div>

                                        {include file="include/_meta_tags.tpl"}

                                        <div class="form-group">
                                            <label id="header_rss_label" {if !$Item->header_rss}class="text-danger"{/if}>Заголовок для социальных сетей (max 140 символов) </label>
                                            <input id="header_rss_input" name="header_rss" type="text" value='{$Item->header_rss|escape}' maxlength="140" placeholder="Укажите заголовок для RSS" class="form-control">
                                        </div>
                                    </div>

                                    {*<div class="form-group">*}
                                        {*<label id="writers_label" {if !$Item->video_on_main}class="text-danger"{/if}>*}
                                            {*URL видео для отображения на главной и в блоге*}
                                        {*</label>*}
                                        {*<input name="video_on_main" type="text" value='{$Item->video_on_main}' placeholder="Вставьте ссылку на видео" class="form-control">*}
                                    {*</div>*}

                                    {*<label>Тип видео</label>*}

                                    {*<div class="form-group">*}
                                        {*<label class="checkbox-inline">*}
                                            {*<input type="radio" name="type_video" value="youtube" {if $Item->type_video == 'youtube' || !$Item->type_video}checked{/if}> YouTube (по умолчанию)*}
                                        {*</label>*}
                                        {*<label class="checkbox-inline">*}
                                            {*<input type="radio" name="type_video" value="other" {if $Item->type_video == 'other'}checked{/if}> Другое (указывать полный урл к видео)*}
                                        {*</label>*}
                                    {*</div>*}

                                    {*<div class="form-group">*}
                                        {*<label id="url_label" {if !$Item->url}class="text-danger"{/if}>*}
                                            {*URL записи*}
                                        {*</label>*}
                                        {*<input name="url" type="text" value='{$Item->url|escape}'*}
                                               {*placeholder="Укажите URL записи" class="form-control">*}
                                    {*</div>*}

                                    <div class="form-group" style="display: none;">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи," class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_partner_material" value="1"
                                                       {if $Item->is_partner_material==1}checked{/if} /> Партнерский материал
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                {*<div class="well">*}
                                    {*<label {if !$Item->tags}class="text-danger"{/if}>Анонс</label>*}

                                    {*<div class="form-group">*}
                                        {*<label class="checkbox-inline">*}
                                            {*<input type="checkbox" name="show_main_page" value="1" {if $Item->show_main_page == 1}checked{/if}> Показывать на главной*}
                                        {*</label>*}

                                        {*{foreach item=tag from=$tags name=tag}*}
                                            {*{if $tag->url != 'main'}*}
                                                {*<label class="checkbox-inline">*}
                                                    {*<input type="radio" name="tags" value="{$tag->id}" {if $tag->id == $Item->tags}checked{/if}> {$tag->name|escape}*}
                                                {*</label>*}
                                            {*{/if}*}
                                        {*{/foreach}*}
                                    {*</div>*}
                                {*</div>*}

                                <div class="well">
                                    <label>Состояние видеопоста</label>

                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="radio" name="state" value="1" {if $Item->state == 1 || !$Item->state}checked{/if}> Записи
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="radio" name="state" value="2" {if $Item->state == 2}checked{/if}> Live
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="radio" name="state" value="3" {if $Item->state == 3}checked{/if}> Анонс
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row"><!-- /--></div>

                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-lg-12">
                                    <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                        <h4>Баннер для главной страницы (Desktop)</h4>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {if $Item->image_1_1_c}
                                                    <img id="image_1_1_c" class="image_preview" src='{$images_uploaddir}{$Item->image_1_1_c}' alt="" style="width: 100%; height: 100%"/>
                                                {else}
                                                    <img id="image_1_1_c" class="image_preview" src='images/no_foto.gif' alt=""/>
                                                {/if}
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Изображение</label>
                                                    <input name="image_1_1_c" type="file" style="width: 100%; overflow: hidden;"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                        <h4>Баннер для главной страницы (Mobile)</h4>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {if $Item->image_1_4}
                                                    <img id="image_1_4" class="image_preview" src='{$images_uploaddir}{$Item->image_1_4}' alt="" style="width: 100%;  height: 100%" />
                                                {else}
                                                    <img id="image_1_4" class="image_preview" src='images/no_foto.gif' alt=""/>
                                                {/if}
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Изображение</label>
                                                    <input name="image_1_4" type="file" style="width: 100%; overflow: hidden;"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row"><!-- /--></div>

                            <div class="col-lg-12">
                                <div class="well" >
                                    <h3>
                                        Список трансляций
                                        <div class="pull-right">
                                            <div class="btn-group">
                                                <button onclick="addDynamicVideo(); return false;" id="btnAddRow" class="btn btn-primary btn-xs" type="button">
                                                    Добавить трансляцию
                                                </button>
                                            </div>
                                        </div>
                                    </h3>

                                    <div id="dynamic_video">
                                        {if $Item->videos}
                                        {foreach item=video from=$Item->videos name=video}
                                        <div class="row{if $video->enabled == 0} disabled{/if}" style="border-bottom: 1px solid #C0C0C0; padding: 14px 0; margin: 7px 0" id="row_{$video->id}">
                                            <input type="hidden" name="videoId[]" value="{$video->id}" />
                                            <input type="hidden" name="type_video[{$video->id}]" value="live" />
                                            <div class="col-lg-12">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Код для вставки</label>
                                                        <input type="text" name="video_url[{$video->id}]" class="form-control{if $video->enabled == 0} disabled{/if}" value='{$video->video_url}' />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Название записи</label>
                                                        <input type="text" name="header_interview[{$video->id}]" class="form-control{if $video->enabled == 0} disabled{/if}" value="{$video->name}" />
                                                    </div>

                                                    <button onclick="addTimingDivisionVideo({$video->id}); return false;" class="btn btn-primary btn-xs" type="button">
                                                        Добавить разбивку по времени
                                                    </button>

                                                    <button onclick="enabledVideo({$video->id}); return false;" id="disableButton_{$video->id}" class="btn btn-warning btn-xs" type="button">{if $video->enabled == 0}Включить в показ{else}Выключить из показа{/if}</button>

                                                    <button onclick="deleteVideo({$video->id}); return false;" class="btn btn-danger btn-xs" type="button">
                                                        Удалить видео
                                                    </button>


                                                    <button class="btn btn-default btn-xs pull-right showTimingVideo {if !$video->divisionTime}hidden{/if}" type="button" data-toggle="collapse" id="showTimingDivision_{$video->id}" href="#timingDivision_{$video->id}" aria-expanded="false" aria-controls="collapseExample">
                                                        <i class="fa fa-bars"></i>
                                                    </button>


                                                    <div id="timingDivision_{$video->id}" class="collapse">
                                                    {*<div id="timingDivision_{$video->id}">*}
                                                        {if $video->divisionTime}
                                                        {foreach item=videoDivisionTime from=$video->divisionTime name=videoDivisionTime}
                                                        <div id="row_{$videoDivisionTime->id}" class="row{if $videoDivisionTime->enabled == 0} disabled{/if}" style="border-top: 1px solid #C0C0C0; margin: 7px 0">
                                                            <div class="col-lg-12" style="margin: 15px 0;">
                                                                <div class="form-group">
                                                                    <label>Временная метка</label>
                                                                    <label class="checkbox-inline">
                                                                        <input type="text" name="minute[{$video->id}][{$videoDivisionTime->id}]" class="form-control{if $videoDivisionTime->enabled == 0} disabled{/if}" style="width:40px; float: left;" value="{$videoDivisionTime->minute}" />
                                                                    </label>
                                                                    <label>
                                                                        <p>мин</p>
                                                                    </label>
                                                                    <label class="checkbox-inline">
                                                                        <input type="text" name="secunde[{$video->id}][{$videoDivisionTime->id}]" class="form-control{if $videoDivisionTime->enabled == 0} disabled{/if}" style="width:40px; float: left;" value="{$videoDivisionTime->secunde}" />
                                                                    </label>
                                                                    <label>
                                                                        <p>сек</p>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Подпись</label>
                                                                    <input type="text" name="description_time_division[{$video->id}][{$videoDivisionTime->id}]" class="form-control{if $videoDivisionTime->enabled == 0} disabled{/if}" value="{$videoDivisionTime->name}" />
                                                                </div>

                                                                <button onclick="enabledTimingPartVideo({$videoDivisionTime->id}); return false;" id="disableButton_{$videoDivisionTime->id}" class="btn btn-warning btn-xs" type="button">{if $videoDivisionTime->enabled == 0}Включить в показ{else}Выключить из показа{/if}</button>
                                                                <button onclick="deleteTimingPartVideo({$videoDivisionTime->id}, {$video->id}); return false;" class="btn btn-danger btn-xs" type="button">Удалить видео</button>

                                                            </div>
                                                        </div>
                                                        {/foreach}
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="well" >
                                    <h3>
                                        Список записей
                                        <div class="pull-right">
                                            <div class="btn-group">
                                                <button onclick="addDynamicVideoRecord(); return false;" id="btnAddRow" class="btn btn-primary btn-xs" type="button">
                                                    Добавить запись
                                                </button>
                                            </div>
                                        </div>
                                    </h3>

                                    <div id="dynamic_video_record">
                                        {if $Item->videosRecord}
                                            {foreach item=video from=$Item->videosRecord name=video}
                                                <div class="row{if $video->enabled == 0} disabled{/if}" style="border-bottom: 1px solid #C0C0C0; padding: 14px 0; margin: 7px 0" id="row_{$video->id}">
                                                    <input type="hidden" name="videoId[]" value="{$video->id}" />
                                                    <input type="hidden" name="type_video[{$video->id}]" value="record" />
                                                    <div class="col-lg-12">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Код для вставки</label>
                                                                <input type="text" name="video_url[{$video->id}]" class="form-control{if $video->enabled == 0} disabled{/if}" value='{$video->video_url}' />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Название записи</label>
                                                                <input type="text" name="header_interview[{$video->id}]" class="form-control{if $video->enabled == 0} disabled{/if}" value="{$video->name}" />
                                                            </div>

                                                            <button onclick="addTimingDivisionVideo({$video->id}); return false;" class="btn btn-primary btn-xs" type="button">
                                                                Добавить разбивку по времени
                                                            </button>

                                                            <button onclick="enabledVideo({$video->id}); return false;" id="disableButton_{$video->id}" class="btn btn-warning btn-xs" type="button">{if $video->enabled == 0}Включить в показ{else}Выключить из показа{/if}</button>

                                                            <button onclick="deleteVideo({$video->id}); return false;" class="btn btn-danger btn-xs" type="button">
                                                                Удалить видео
                                                            </button>


                                                            <button class="btn btn-default btn-xs pull-right showTimingVideo {if !$video->divisionTime}hidden{/if}" type="button" data-toggle="collapse" id="showTimingDivision_{$video->id}" href="#timingDivision_{$video->id}" aria-expanded="false" aria-controls="collapseExample">
                                                                <i class="fa fa-bars"></i>
                                                            </button>


                                                            <div id="timingDivision_{$video->id}" class="collapse">
                                                                {*<div id="timingDivision_{$video->id}">*}
                                                                {if $video->divisionTime}
                                                                    {foreach item=videoDivisionTime from=$video->divisionTime name=videoDivisionTime}
                                                                        <div id="row_{$videoDivisionTime->id}" class="row{if $videoDivisionTime->enabled == 0} disabled{/if}" style="border-top: 1px solid #C0C0C0; margin: 7px 0">
                                                                            <div class="col-lg-12" style="margin: 15px 0;">
                                                                                <div class="form-group">
                                                                                    <label>Временная метка</label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="text" name="minute[{$video->id}][{$videoDivisionTime->id}]" class="form-control{if $videoDivisionTime->enabled == 0} disabled{/if}" style="width:40px; float: left;" value="{$videoDivisionTime->minute}" />
                                                                                    </label>
                                                                                    <label>
                                                                                        <p>мин</p>
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="text" name="secunde[{$video->id}][{$videoDivisionTime->id}]" class="form-control{if $videoDivisionTime->enabled == 0} disabled{/if}" style="width:40px; float: left;" value="{$videoDivisionTime->secunde}" />
                                                                                    </label>
                                                                                    <label>
                                                                                        <p>сек</p>
                                                                                    </label>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Подпись</label>
                                                                                    <input type="text" name="description_time_division[{$video->id}][{$videoDivisionTime->id}]" class="form-control{if $videoDivisionTime->enabled == 0} disabled{/if}" value="{$videoDivisionTime->name}" />
                                                                                </div>

                                                                                <button onclick="enabledTimingPartVideo({$videoDivisionTime->id}); return false;" id="disableButton_{$videoDivisionTime->id}" class="btn btn-warning btn-xs" type="button">{if $videoDivisionTime->enabled == 0}Включить в показ{else}Выключить из показа{/if}</button>
                                                                                <button onclick="deleteTimingPartVideo({$videoDivisionTime->id}, {$video->id}); return false;" class="btn btn-danger btn-xs" type="button">Удалить видео</button>

                                                                            </div>
                                                                        </div>
                                                                    {/foreach}
                                                                {/if}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_post" value="11">
    <input type="hidden" name="type_material" value="0">
    <input type="hidden" name="type_visual_block" value="newvideo">
    <input type="hidden" id="countrows" value="3" >
</form>
</div>



{include file='tinymce_init.tpl'}
{include file="include/_set_meta.tpl"}
