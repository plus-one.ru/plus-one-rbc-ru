<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlogWriters&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">

                        {include file="include/seoContent.tpl"}

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название писателя</label>
                                        <input name="name" type="text" value='{$Item->name|escape}'
                                               placeholder="Укажите название писателя" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Название писателя в родительном падеже</label>
                                        <input name="name_genitive" type="text" value='{$Item->name_genitive|escape}'
                                               placeholder="Укажите название писателя в родительном падеже" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Краткое описание автора</label>
                                        <textarea rows="2" name="description" class="form-control">{$Item->description}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="useplatforma" value="1"
                                                       {if $Item->useplatforma==1}checked{/if} /> Участник "Платформы"
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="private" value="1"
                                                       {if $Item->private==1}checked{/if} /> Автор колонки
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="bloger" value="1"
                                                       {if $Item->bloger==1}checked{/if} /> Автор блога
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Лидер направления</label>
                                        <select class="selectpicker form-control" data-live-search="true" name="leader">
                                            <option value="0"
                                                    {if $Item->leader == $tagItem->id}selected{/if}> -- Выберите направление в котором автор - лидер -- </option>
                                            {foreach item=tag from=$blogTags name=tag}
                                                {if $tag->items}
                                                    <optgroup label="{$tag->name}">
                                                        {foreach item=tagItem from=$tag->items name=tagItem}
                                                            <option value="{$tagItem->id}"
                                                                    {if $Item->leader == $tagItem->id}selected{/if}>{$tagItem->name_lead}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->image}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Основное изображение</label>
                                                <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>


                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->stub_image}
                                                <img id="stub_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->stub_image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_stub_image"
                                                       class="btn btn-outline btn-danger btn-xs" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="stub_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Изображение заглушка для блока 1/3</label>
                                                <input name="stub_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_stub_image" name="delete_stub_image"/>

                                    <div class="form-group">
                                        <label>Ссылка с изображения-заглушки</label>
                                        <input name="stub_link" type="text" style="width: 100%;" value='{$Item->stub_link}'/>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group">
                                        {*<label>Текст статьи</label>*}
                                        <textarea rows="20" id="body" name="body" class="form-control fulleditor">{$Item->body}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{include file='tinymce_init.tpl'}
