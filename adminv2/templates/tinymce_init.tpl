<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.js?{$Config->admin_cache_version}"></script>

<script language="javascript">

    {literal}
    function OnInit (ed) {

        ed.serializer.addAttributeFilter('contenteditable', function (e) {
            e.forEach(function (tmplBlock) {
                tmplBlock.attr('contenteditable', null);
                let classTmpl = tmplBlock.attr('class');
                if (classTmpl) {
                    tmplBlock.attr('class', classTmpl.replace('selected--template', ''));
                }
            });
        });

        ed.dom.select('.info-side,blockquote.citate-block').forEach(function (templ) {
            templ.contentEditable = false;
        });
    }
    {/literal}

	tinymce.init({literal}{{/literal}
		selector: '.smalleditor',
        cache_suffix: '?v=4.1.8&cache=?{$Config->admin_cache_version}',
		height: 100,
		theme: 'modern',
        menubar: false,
            relative_urls: false,
            valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html: false,
            convert_urls: false,
            cleanup: false,
        {*forced_root_block : 'div',*}
        {*forced_root_block_attrs: {literal}{{/literal}*}
            {*'class': 'text-block',*}
        {*{literal}}{/literal},*}
		plugins: [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | bold italic | code',
		//toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
		image_advtab: false,
		templates: [
                {literal}{{/literal} title: 'Test template 1', content: 'Test 1' {literal}}{/literal},
                {literal}{{/literal} title: 'Test template 2', content: 'Test 2' {literal}}{/literal}
		],
		{*content_css: ["{$root_url}/design/{$Settings->theme}/css/main_to_adm.css"],*}
        file_browser_callback :
            function(field_name, url, type, win){literal}{{/literal}

                var filebrowser = "filebrowser.php";
                filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                tinymce.activeEditor.windowManager.open({literal}{{/literal}
                    title : "Выбор изображения",
                    width : 800,
                    height : 600,
                    url : filebrowser
                    {literal}}{/literal}, {literal}{{/literal}
                    window : win,
                    input : field_name
                    {literal}}{/literal});
                    return false;
                {literal}}{/literal}
{literal}
	}
{/literal});


            tinymce.init({literal}{{/literal}
                        selector: '.fulleditor',
                        cache_suffix: '?v=4.1.8&cache=?{$Config->admin_cache_version}',
                        height: 500,
                        theme: 'modern',
                        menubar: false,
                        relative_urls: false,
                        valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
                        cleanup_on_startup: false,
                        trim_span_elements: false,
                        verify_html: false,
                        convert_urls: true,
                        cleanup: false,
                        {*forced_root_block : 'div',*}
                        {*forced_root_block_attrs: {literal}{{/literal}*}
                        {*'class': 'text-block',*}
                        {*{literal}}{/literal},*}
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc bulletImage imageVert imageFullWidth imageGallery embedInsert signLine headerTwoLevel citateNew dialogBox textColoredImg fullscreen',
                            'responsivefilemanager'
                        ],
                        toolbar1: 'fullscreen | undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | image bulletImage imageVert imageFullWidth imageGallery | embedInsert | signLine | headerTwoLevel | citateNew | dialogBox | textColoredImg',
                        toolbar2: 'responsivefilemanager print preview media | forecolor backcolor emoticons | codesample | code',
                        image_advtab: false,
                        templates: [
                                {literal}{{/literal} title: 'Подпись автора', content: '<span class="author">Инервью: Имя Автора, «+1»</span>' {literal}}{/literal},
                        ],
                        content_css: [
                            "{$root_url}/design/{$Settings->theme}/css/fonts.css",
                            "{$root_url}/design/{$Settings->theme}/css/main_to_adm.css?v=7"
                        ],
                        importcss_append: true,
                    external_filemanager_path: "/adminv2/filemanager/",
                    filemanager_title: "Файловый менеджер" ,
                    filemanager_sort_by: "date",
                    filemanager_descending: 1,
                    external_plugins: {literal}{{/literal} "filemanager" : "/adminv2/filemanager/plugin.min.js"{literal}}{/literal},
                    init_instance_callback: 'OnInit'
            {literal} } {/literal}
            );
</script>