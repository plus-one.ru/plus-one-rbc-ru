{literal}
    <style>
        #dataTable_filter label{width:100%}
        #dataTable_filter label input {width:86%}
        th {font-weight: 500; color: #000}
    </style>
{/literal}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>Статьи</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>



<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" type="button">
                            Добавить
                            <span class="caret"></span>
                        </button>
                        <ul role="menu" class="dropdown-menu pull-right">
                            <li>
                                <a href="blogpost/add/post/{$Token}">Пост</a>
                            </li>
                            <li>
                                <a href="blogpost/add/data/{$Token}">Данные</a>
                            </li>
                            <li>
                                <a href="blogpost/add/newvideo/{$Token}">Новый видео пост</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {if $Items}
                    {*<div class="table-responsive">*}
                        <table id="dataTable" class="table order-column">
                            <thead>
                            <tr>
                                <th style="width:2%;"></th>
                                <th style="width:10%;">Изображение</th>
                                <th style="width:10%; cursor: pointer">Создана</th>
                                <th style="width:10%; cursor: pointer">Изменена</th>
                                <th style="width: 10%">Анонс</th>
                                <th style="width: 10%">Теги</th>
                                <th style="width: 10% ">Тип материала</th>
                                <th style="width: 10% ">Автор</th>
                                <th style="width: 10% ">Партнер</th>
                                <th style="width: 35%; cursor: pointer">Название</th>
                                <th style="width: 35%; cursor: pointer">Заголовок</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=item from=$Items name=item}
                                <tr>
                                    <td nowrap>
                                    {if $item->enabled}
                                        <a href="blogpost/enable/{$item->id}/{$Token}">
                                            <i class="fa fa-toggle-on fa-1x"></i>
                                        </a>
                                    {else}
                                        <a href="blogpost/enable/{$item->id}/{$Token}">
                                            <i class="fa fa-toggle-off fa-1x"></i>
                                        </a>
                                    {/if}
                                    </td>
                                    <td nowrap>
                                        {if $item->image_1_2}
                                            <img src="{$images_uploaddir}{$item->image_1_2}" style="width: 80px;"/>
                                        {else}
                                            <img src="images/no_foto.gif" style="width: 80px;"/>
                                        {/if}
                                    </td>
                                    <td >
                                        {$item->created|escape}
                                    </td>
                                    <td >
                                        {$item->modified|escape}
                                    </td>

                                    <td width="10%" >
                                        На главной:
                                        {if $item->show_main_page == 1}
                                            <strong class="text-success">Да</strong>
                                        {else}
                                            <strong class="text-warning">Нет</strong>
                                        {/if}
                                        <br/>
                                        {if $item->tag->name}
                                            <span class="text-success">{$item->tag->name}</span>
                                        {/if}
                                    </td>
                                    <td width="10%">
                                        {foreach item=tag from=$item->postTags name=tag}
                                            {$tag->name|escape}{if !$smarty.foreach.tag.last},<br/>{/if}
                                        {/foreach}
                                    </td>
                                    <td width="10%">
                                        {$item->typematerial->name|escape}
                                    </td>
                                    <td width="10%">{$item->writer->name|escape}</td>
                                    <td width="10%">
                                        {$item->partnerRecord->name|escape}
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="blogpost/edit/{$item->typePost}/{$item->id}/{$Token}">
                                                {if $item->type_post == 1 || $item->type_post == 2 || $item->type_post == 3}
                                                    {$item->name}
                                                {/if}
                                                {if $item->type_post == 5}
                                                    Данные {$item->writer->name|escape}
                                                {/if}
                                                {if $item->type_post == 6}
                                                     Видео {$item->name|escape}
                                                {/if}
                                            </a>
                                        </p>
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="blogpost/edit/{$item->typePost}/{$item->id}/{$Token}">
                                                <a href="blogpost/edit/{$item->typePost}/{$item->id}/{$Token}">
                                                    {if $item->type_post == 1 || $item->type_post == 2 || $item->type_post == 3}
                                                        {$item->header}
                                                    {/if}
                                                    {if $item->type_post == 5}
                                                        Данные {$item->writer->name|escape}
                                                    {/if}
                                                </a>
                                            </a>
                                        </p>
                                    </td>
                                    <td nowrap>
                                        <a href="blogpost/delete/{$item->id}/{$Token}" class="btn btn-danger btn-xs"
                                           type="button"
                                           onclick='if(!confirm("Удалить запись?")) return false;'>
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    {*</div>*}
                    <div class="row">
                        <div class="col-lg-12">
                            <p align="center">{$PagesNavigation}</p>
                        </div>
                    </div>
                {else}
                    <p>
                        Еще нет ни одной статьи. <a href="blogpost/add/{$Token}">Добавить статью?</a>
                    </p>
                {/if}
            </div>
        </div>
    </div>
</div>