{literal}
    <style>
        #dataTable_filter label{width:100%}
        #dataTable_filter label input {width:86%}
        th {font-weight: 500; color: #000}
    </style>
{/literal}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>Статьи</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" type="button">
                            Добавить
                            <span class="caret"></span>
                        </button>
                        <ul role="menu" class="dropdown-menu pull-right">
                            <li>
                                <a href="blogpost/add/post/{$Token}">Пост</a>
                            </li>
                            <li>
                                <a href="blogpost/add/data/{$Token}">Данные</a>
                            </li>
                            <li>
                                <a href="blogpost/add/newvideo/{$Token}">Новый видео пост</a>
                            </li>
                            {*<li>
                                <a href="blogpost/add/newblog/{$Token}">Новый блог</a>
                            </li>*}
                            <li>
                                <a href="blogpost/add/imageday/{$Token}">Мем недели</a>
                            </li>
                            <li>
                                <a href="blogpost/add/factday/{$Token}">Факт недели</a>
                            </li>
                            <li>
                                <a href="blogpost/add/citate/{$Token}">Цитата</a>
                            </li>
                            <li>
                                <a href="blogpost/add/specproject/{$Token}">Анонс спецпроекта</a>
                            </li>
                            <li>
                                <a href="blogpost/add/diypost/{$Token}">Карточка DIY</a>
                            </li>
                            <li>
                                <a href="blogpost/add/ticker/{$Token}">Бегущая строка</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-8">
                        <form method="get" action="index.php?section=BlogPosts" class="form-horizontal row" style="padding-top: 20px;">
                            <input type="hidden" class="form-control" value="BlogPosts" name="section">
                            <div class="col-xs-5" >
                                <input type="text" class="form-control pull-left"  placeholder="Поиск" value="{$search}" name="search"/>
                            </div>
                            <div class="col-xs-3" >
                                <select name="type_post" class="form-control">
                                    <option value="">-- Тип материала --</option>
                                    <option value="1" {if $type_post == 1}selected{/if}>Пост</option>
                                    <option value="5" {if $type_post == 5}selected{/if}>Данные</option>
                                    <option value="11" {if $type_post == 11}selected{/if}>Новый видео пост</option>
                                    <option value="6" {if $type_post == 6}selected{/if}>Мем недели</option>
                                    <option value="7" {if $type_post == 7}selected{/if}>Факт недели</option>
                                    <option value="8" {if $type_post == 8}selected{/if}>Цитата</option>
                                    <option value="9" {if $type_post == 9}selected{/if}>Анонс спецпроекта</option>
                                    <option value="10" {if $type_post == 10}selected{/if}>Карточка DIY</option>
                                    <option value="12" {if $type_post == 12}selected{/if}>Бегущая строка</option>
                                </select>

                            </div>
                            <div class="col-xs-3" >
                                <select name="author" class="form-control">
                                    <option value="">-- Автор --</option>
                                    <option value="empty" {if $filterWriters == 'empty'}selected{/if}>< Не указан ></option>
                                    {foreach item=writer from=$writers name=writer}
                                        <option value="{$writer->id}">{$writer->name}</option>
                                    {/foreach}
                                </select>

                            </div>
                            <div class="col-xs-1">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 text-right">
                        <ul class="pagination">

                            {if !$paginationStructure->startPageLink}
                                <li class="paginate_button previous disabled" tabindex="0">
                                    <a href="#" onclick="return false">В начало</a>
                                </li>
                            {else}
                                <li class="paginate_button previous" tabindex="0">
                                    <a href="{$paginationStructure->startPageLink}">В начало</a>
                                </li>
                            {/if}

                            {if $paginationStructure->page3left}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page3left}">{$page-3}</a>
                                </li>
                            {/if}
                            {if $paginationStructure->page2left}
                            <li class="paginate_button" tabindex="0">
                                <a href="{$paginationStructure->page2left}">{$page-2}</a>
                            </li>
                            {/if}
                            {if $paginationStructure->page1left}
                            <li class="paginate_button" tabindex="0">
                                <a href="{$paginationStructure->page1left}">{$page-1}</a>
                            </li>
                            {/if}

                            <li class="paginate_button active" tabindex="0">
                                <a href="#" onclick="return false">{$page}</a>
                            </li>

                            {if $paginationStructure->page1right}
                            <li class="paginate_button" tabindex="0">
                                <a href="{$paginationStructure->page1right}">{$page+1}</a>
                            </li>
                            {/if}
                            {if $paginationStructure->page2right}
                            <li class="paginate_button" tabindex="0">
                                <a href="{$paginationStructure->page2right}">{$page+2}</a>
                            </li>
                            {/if}
                            {if $paginationStructure->page3right}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page3right}">{$page+3}</a>
                                </li>
                            {/if}

                            {if !$paginationStructure->lastPageLink}
                                <li class="paginate_button next disabled" tabindex="0">
                                    <a href="#" onclick="return false">Последняя</a>
                                </li>
                            {else}
                                <li class="paginate_button next" tabindex="0">
                                    <a href="{$paginationStructure->lastPageLink}">Последняя</a>
                                </li>
                            {/if}
                        </ul>
                    </div>
                </div>

                <table id="dataTable" class="table order-column">
                    <thead>
                        <tr>
                            <th style="width: 2%;"></th>
                            <th>Изображение</th>
                            <th>Создана</th>
                            <th>Изменена</th>
                            <th>Анонс</th>
                            <th>Теги</th>
                            <th nowrap>Тип материала</th>
                            <th>Автор</th>
                            <th>Партнер</th>
                            <th>Название</th>
                            <th>Заголовок</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach item=item from=$Items name=item}
                        <tr class="{if $item->deferred}deferred{/if}">
                            <td>{$item->enabled}<br/>{$item->linkToPost}</td>
                            <td><div style="width:80px; height: 80px; display: table-cell; vertical-align: middle; text-align: center;">{$item->image}</div></td>
                            <td><small>{$item->date_created} {if $item->deferred}<i class="fa fa-clock-o" title="Отложенный"></i>{/if}</small></td>
                            <td><small>{$item->date_modified}</small></td>
                            <td nowrap>{$item->tagStr}</td>
                            <td>{$item->postTagsStr}</td>
                            <td>{$item->typematerial}</td>
                            <td class="{if !$item->writerEnabled}text-hidden{/if}">
                                {$item->writerStr}
                            </td>
                            <td>{$item->partnerRecordStr}</td>
                            <td><div class="break-all max-height-400">{$item->name}</div></td>
                            <td><div class="break-all max-height-400">
                                    {if $item->header}
                                        {$item->header}
                                    {elseif $item->text_body}
                                        {$item->text_body}
                                    {/if}
                                </div>
                            </td>
                            <td>{$item->deleteBtn}</td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
