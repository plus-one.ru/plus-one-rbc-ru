            <div class="row">
                <div class="col-lg-12">
                  <h2 class="page-header"><small>Статьи</small></h2>
                </div>
            </div>
            <div class="row">
              <!-- /-->
            </div>

            <div class="row">
              <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Меню
                    </div>
                    <div class="panel-body">
                      <ul class="nav">
                      {foreach item=category from=$categories name=category}
                        <li><a {if $current_category==$category->category_id}class="active"{/if} href="index.php?section=Articles&category={$category->id}">{$category->name|escape}</a></li>
                      {/foreach}
                      </ul>
                    </div>
                </div>
              </div>
              <div class="col-lg-10">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                      <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="index.php?section=Article&token={$Token}">
                          <i class="fa fa-check"></i> Добавить
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="panel-body">
                    {if $Items}
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Создана</th>
                            <th>Изменена</th>
                            <th>Изображение</th>
                            <th>Название</th>
                            <th>Заголовок</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        {foreach item=item from=$Items name=item}
                          <tr>
                            <td nowrap">
                              {if $item->enabled}
                                <a href="index.php{$item->enable_get}"><i class="fa fa-toggle-on fa-1x"></i></a>
                              {else}
                                <a href="index.php{$item->enable_get}"><i class="fa fa-toggle-off fa-1x"></i></a>                            
                              {/if}
                            </td>
                            <td nowrap>
                              {$item->date_created|escape}<br/>
                              {$item->time_created|escape}
                            </td>
                            <td nowrap>
                              {$item->date_modified|escape}<br/>
                              {$item->time_modified|escape}                              
                            </td>
                            <td nowrap>
                              {if $item->small_image}
                                <img src="{$images_uploaddir}{$item->small_image}" style="width: 80px;" />
                              {else}
                                <img src="images/no_foto.gif" style="width: 80px;"/>
                              {/if}
                            </td>
                            <td width="35%">
                              <p {if !$item->enabled}class="text-muted"{/if}>
                                <a href="index.php{$item->edit_get}">{$item->name|escape}</a>
                              </p>
                            </td>
                            <td width="35%">
                              <p {if !$item->enabled}class="text-muted"{/if}>
                                <a href="index.php{$item->edit_get}">{$item->header|escape}</a>
                              </p>
                            </td>
                            <td nowrap>
                              <a href="index.php{$item->edit_get}" class="btn btn-success btn-xs" type="button">
                                <i class="fa fa-pencil fa-1x"></i>
                              </a>
                              <a href="index.php{$item->delete_get}" class="btn btn-danger btn-xs" type="button" onclick='if(!confirm("{$Lang->ARE_YOU_SURE_TO_DELETE}")) return false;'>
                                <i class="fa fa-times-circle fa-1x"></i>
                              </a>
                            </td>
                          </tr>
                          {/foreach}
                        </tbody>
                      </table>
                      
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <p align="center">{$PagesNavigation}</p>
                      </div>
                    </div>


                    {else}
                      <p>Еще нет ни одной статьи. <a href="index.php?section=Article&token={$Token}">Добавить статью?</a></p>
                    {/if}
                  </div>
                </div>
              </div>
            </div>