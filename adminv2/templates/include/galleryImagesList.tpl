
<div class="fotorama-holder">
    <a href="#" class="prev"></a>
    <a href="#" class="next"></a>
    <div class="fotorama" 
        data-auto="true"
        data-maxwidth="996"
        data-maxheight="525"
        data-arrows="true"
        data-click="true"
        data-swipe="true"
        data-navwidth="755"
    >
        {foreach from=$imagesList item=image name=image}
        <div data-img="/files/photogallerys/{$image->filename}" data-thumb="/files/photogallerys/{$image->filename}">
            <p>
                {$image->image_name|escape}
                <span>{$image->image_author|escape}</span>
            </p>
        </div>
        {/foreach}
    </div>
    <div class="info">
        <span class="numbering">1/{$imagesList|@count}</span>
        <p>
            {$imagesList[0]->image_name|escape}
            <span>{$imagesList[0]->image_author|escape}</span>
        </p>
    </div>
</div>