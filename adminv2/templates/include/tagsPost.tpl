<label>Теги статьи</label>

<h5>Для главной страницы</h5>
<select class="selectpicker form-control" data-max-options="1" multiple data-live-search="true" name="post_tag_main" title="-- Укажите тег статьи для главной страницы --">
    {foreach item=tag from=$blogTags name=tag}
        {if $tag->items}
            <optgroup label="{$tag->name}">
                {foreach item=tagItem from=$tag->items name=tagItem}
                    <option value="{$tagItem->id}" {if $tagItem->id == $Item->post_tag}selected{/if}>{$tagItem->name}</option>
                {/foreach}
            </optgroup>
        {/if}
    {/foreach}
</select>

<h5>Для внутренней страницы</h5>
<select class="selectpicker form-control" data-max-options="2" multiple data-live-search="true" name="post_tags[]" title="-- Укажите тег статьи для внутренней страницы --">
    {foreach item=tag from=$tags name=tag}
        {if $tag->items}
            <optgroup label="{$tag->name}">
                {foreach item=tagItem from=$tag->items name=tagItem}
                    <option value="{$tagItem->id}" {if $tagItem->check == 1}selected{/if}>{$tagItem->name}</option>
                {/foreach}
            </optgroup>
        {/if}
    {/foreach}
</select>