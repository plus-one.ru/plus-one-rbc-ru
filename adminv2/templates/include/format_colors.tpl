<div class="form-group">
    <label id="body_color">Цвет фона страницы</label>
    <div id="__body_color" class="pull-right" style="width:25px; background: {$Item->body_color}">&nbsp;</div>
    <input type="text" id="POST__body_color" name="body_color" class="form-control" value="{$Item->body_color}"
           onkeyup="{literal} document.getElementById('__body_color').style.background = this.value;{/literal}"
           onchange="{literal} document.getElementById('__body_color').style.background = this.value;{/literal}"
    >
</div>

<div class="form-group">
    <label id="font_color">Цвет шрифта</label>
    <div id="__form_color" class="pull-right" style="width:25px; background: {$Item->font_color}">&nbsp;</div>
    <input type="text" id="POST__form_color" name="font_color" class="form-control" value="{$Item->font_color}"
           onkeyup="{literal} document.getElementById('__form_color').style.background = this.value;{/literal}"
           onchange="{literal} document.getElementById('__form_color').style.background = this.value;{/literal}"
    >
</div>

<div class="form-group">
    <label id="border_color">Цвет рамки и подчеркивания</label>
    <div id="__border_color" class="pull-right" style="width:25px; background: {$Item->border_color}">&nbsp;</div>
    <input type="text" id="POST__border_color" name="border_color" class="form-control" value="{$Item->border_color}"
           onkeyup="{literal} document.getElementById('__border_color').style.background = this.value;{/literal}"
           onchange="{literal} document.getElementById('__border_color').style.background = this.value;{/literal}"
    >
</div>