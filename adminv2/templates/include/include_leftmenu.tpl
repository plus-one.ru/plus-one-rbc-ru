<ul class="nav" id="side-menu">
    <li>
        <a href="./?r=s"><i class="fa fa-dashboard fa-fw"></i> Главная</a>
    </li>
    {foreach item=module from=$allowedModules name=module}
            <li>
                {if $module->url}
                <a href="{$module->url}">
                {else}
                <a href="index.php?section={$module->class|escape}s">
                {/if}
                    {if $module->icon != '0'}
                    <i class="fa {$module->icon|escape} fa-fw"></i> {$module->name|escape}
                    {else}
                    <i class="fa fa-sticky-note-o fa-fw"></i> {$module->name|escape}
                    {/if}
                </a>
            </li>
    {/foreach}
</ul>