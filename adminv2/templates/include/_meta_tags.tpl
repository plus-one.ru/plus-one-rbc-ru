<div class="form-group  {if $Errors->meta_title}has-error{/if}">
    <label for="meta_title" class="control-label">Метатег Title (название страницы). Max 100 символов.</label>
    <input id="meta_title" name="meta_title" type="text" value='{$Item->meta_title|escape}' class="form-control">
    <small class="js_counters text-muted pull-right"></small>
</div>

<div class="form-group {if $Errors->meta_description}has-error{/if}">
    <label for="meta_description" class="control-label">Метатег Description (описание страницы). Max 170 символов.
        Отображается в соц.сетях</label>
    <input id="meta_description" name="meta_description" type="text" value='{$Item->meta_description|escape}'
           class="form-control">
    <small class="js_counters text-muted pull-right"></small>
</div>

<div class="form-group {if $Errors->meta_keywords}has-error{/if}">
    <label for="meta_keywords" class="control-label">Метатег Keywords (ключевые слова статьи). Max 170 символов</label>
    <input id="meta_keywords" name="meta_keywords" type="text" value='{$Item->meta_keywords|escape}'
           class="form-control">
    <small class="js_counters text-muted pull-right"></small>
</div>