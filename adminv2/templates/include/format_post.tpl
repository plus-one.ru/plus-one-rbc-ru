<div class="well">
    <h4>Оформление</h4>
    <div class="form-group">
        <label id="format_label" {if !$Item->format}class="text-danger"{/if}>
            Цвет блока
        </label>
        <select class="form-control" id="format_input" name="format">
            {foreach item=format from=$presetFormat key=k name=format}
                {if $k != 'picture'}
                    <option value="{$k}" {if $Item->format==$k || ($k=='white'  && !isset($Item->format))}selected{/if}>{$k}</option>
                {/if}
            {/foreach}
        </select>
    </div>

    <div class="form-group">
        <label id="font_label" {if !$Item->font}class="text-danger"{/if}>
            Шрифт блока
        </label>
        <select class="form-control" id="font_input" name="font">
            {foreach item=font from=$presetFont key=k name=font}
                <option value="{$k}" {if $Item->font==$k || ($k == 'MullerBold' && !isset($Item->format))}selected{/if}>{$k}</option>
            {/foreach}
        </select>
    </div>
</div>