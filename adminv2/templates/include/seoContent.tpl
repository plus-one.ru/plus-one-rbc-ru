<div class="row">
    <div class="col-lg-12">
        <div class="well">
            <h4>SEO мета-теги</h4>

            <div class="form-group">
                <div class="form-group">
                    <label id="seo_title_label" {if !$Item->seo_title}class="text-danger"{/if}>Title</label>
                    <input id="seo_title_input" name="seo_title" type="text" value='{$Item->seo_title|escape}' class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label id="seo_description_label" {if !$Item->seo_description}class="text-danger"{/if}>Description</label>
                <textarea id="seo_description_input" name="seo_description" class="form-control">{$Item->seo_description}</textarea>
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label id="seo_keyword_label" {if !$Item->seo_keywords}class="text-danger"{/if}>Keywords (ключевые слова/фразы через запятую</label>
                    <input id="seo_keyword_input" name="seo_keywords" type="text" value='{$Item->seo_keywords|escape}' class="form-control">
                </div>
            </div>

        </div>
    </div>
</div>
