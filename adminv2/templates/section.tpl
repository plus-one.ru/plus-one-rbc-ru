            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{if $Section->id}{$Section->name}{else}Новая страница{/if}</h1>
                </div>
            </div>
            <div class="row">
				{*include file="include_mainheaderblock.tpl"*}
            </div>

			<form name="section" method="post">
	            <div class="row">
					<div class="col-lg-12">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {$Error}&nbsp;
								<div class="pull-right">
									<div class="btn-group">
										<button class="btn btn-outline btn-primary btn-xs" type="submit">
											<i class="fa fa-check"></i> Сохранить
										</button>
										<a class="btn btn-outline btn-warning btn-xs" type="button" href="index.php?section=Sections&menu={$Menu->menu_id}&token={$Token}">
											<i class="fa fa-ban"></i> Отменить
										</a>
									</div>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6">
										<div class="well">
											<div class="form-group {if !$Section->name}has-warning{/if}">
												<label>Название страницы</label>
												<input name="name" type="text" value='{$Section->name|escape}' placeholder="Название" class="form-control">
											</div>
											<div class="form-group {if !$Section->header}has-warning{/if}">
												<label>Заголовок страницы</label>
												<input name="header" type="text" value='{$Section->header|escape}' placeholder="Заголовок" class="form-control">
											</div>
											<div class="form-group input-group {if !$Section->url}has-warning{/if}">
												<span class="input-group-addon">http://{$root_url}/sections/</span>
												<input name="url" class="form-control" type="text" placeholder="URL" value='{$Section->url|escape}'>
											</div>
											
											<div class="form-group {if $Section->menu_id==0 || !$Section->menu_id}has-warning{/if}">
												<label>Меню</label>
												<select class="form-control" name="menu_id">
													<option value='0' selected>Выберите раздел меню для создаваемой страницы</option>
													{foreach name=menu key=key item=item from=$Menus}
													<option value='{$item->menu_id}' {if $Section->menu_id EQ $item->menu_id}selected{/if}>{$item->name|escape}</option>
													{/foreach}
												</select>
											</div>
											<div class="form-group {if $Section->module_id==0 || !$Section->module_id}has-warning{/if}">
												<label>Тип контента</label>
												<select class="form-control" name="module_id">
													<option value='0' selected>Выберите модуль, который будет отображать страницу</option>
													{foreach name=service_type key=key item=item from=$Modules}
													<option value='{$item->module_id}' {if $Section->module_id EQ $item->module_id}selected{/if}>{$item->name|escape}</option>
													{/foreach}
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="well">
											<div class="form-group">
												<label>Meta Title</label>
												<input name="meta_title" type="text" value='{$Section->meta_title}' placeholder="Meta Title" class="form-control">
											</div>
											<div class="form-group">
												<label>Meta Keywords</label>
												<input name="meta_keywords" type="text" value='{$Section->meta_keywords}' placeholder="Meta Keywords" class="form-control">
											</div>
											<div class="form-group">
												<label>Meta Description</label>
												<input name="meta_description" type="text" value='{$Section->meta_description}' placeholder="Meta Description" class="form-control">
											</div>
											<div class="form-group">
												<div class="checkbox">
													<label>
														<input type="checkbox" name="enabled" value="1" {if $Section->enabled==1}checked{/if} /> Отображать на сайте
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-12">
										<div class="well">
											<div class="form-group">
												<label>Текст страницы</label>
												<textarea id="body" name="body" rows="20" class="form-control">{$Section->body|escape}</textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
	            <input type="hidden" name="section_id" value="{if $Section->id}{$Section->id}{else}0{/if}" /> 
			</form>


{include file='tinymce_init.tpl'}

{if $Settings->meta_autofill}
<!-- Autogenerating meta tags -->
{literal}
<script>

// Templates
var meta_title_template = '%name';
var meta_keywords_template = '%name';
var meta_description_template = '%text';

var item_form = document.section;

var meta_title_touched = true;
var meta_keywords_touched = true;
var meta_description_touched = true;
var url_touched = true;
	
// generating meta_title
function generate_title(template, name, text)
{
	return template.replace('%name', name).replace('%text', text).replace(/^(,\s)+|\s+$/g,"");
}	

// generating meta_keywords
function generate_keywords(template, name, text)
{	
	return template.replace('%name', name).replace('%text', text).replace(/^(,\s)+|\s+$/g,"");
}	

// generating meta_description
function generate_description(template, name,  text)
{	
	return template.replace('%name', name).replace('%text', text).replace(/^\s+|\s+$/g,"");
}	

// generating meta_title
function generate_url(name)
{
	url = name;
	return translit(url);
}	


// sel all metatags
function set_meta()
{	
	var name = item_form.header.value;

	var text = tinyMCE.get("body").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ");

	// Meta Title
	if(!meta_title_touched)
		item_form.meta_title.value = generate_title(meta_title_template, name, text);		

	// Meta Keywords
	if(!meta_keywords_touched)
		item_form.meta_keywords.value = generate_keywords(meta_keywords_template, name, text);		

	// Meta Description
	if(!meta_description_touched)
		item_form.meta_description.value = generate_description(meta_description_template, name, text);		

	// Url
	if(!url_touched)
		item_form.url.value = generate_url(name);		

}

function translit(url){
	url = url.replace(/[\s]+/gi, '_');
	return url.replace(/[^0-9a-zа-я_]+/gi, '');
}

function autometageneration_init()
{ 
	tinyMCE.get("body").onChange.add(set_meta);
	tinyMCE.get("body").onKeyUp.add(set_meta);
	
	var name = item_form.header.value;

	var text = tinyMCE.get("body").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ");

	if(item_form.meta_title.value == '' || item_form.meta_title.value == generate_title(meta_title_template, name, text))
		meta_title_touched=false;
	if(item_form.meta_keywords.value == '' || item_form.meta_keywords.value == generate_keywords(meta_keywords_template, name, text))
		meta_keywords_touched=false;
	if(item_form.meta_description.value == '' || item_form.meta_description.value == generate_description(meta_description_template, name, text))
		meta_description_touched=false;
	if(item_form.url.value == '' || item_form.url.value == generate_url(name))
		url_touched=false;
}

// Attach events
function myattachevent(target, eventName, func)
{
    if ( target.addEventListener )
        target.addEventListener(eventName, func, false);
    else if ( target.attachEvent )
        target.attachEvent("on" + eventName, func);
    else
        target["on" + eventName] = func;
}

if (window.attachEvent) {
	window.attachEvent("onload", function(){setTimeout("autometageneration_init();", 1000)});
} else if (window.addEventListener) {
	window.addEventListener("DOMContentLoaded", autometageneration_init, false);
} else {
	document.addEventListener("DOMContentLoaded", autometageneration_init, false);
}



myattachevent(item_form.url, 'change', function(){url_touched = true});
myattachevent(item_form.meta_title, 'change', function(){meta_title_touched = true});
myattachevent(item_form.meta_keywords, 'change', function(){meta_keywords_touched = true});
myattachevent(item_form.meta_description, 'change', function(){meta_description_touched = true});
myattachevent(item_form.header, 'keyup',  set_meta);
myattachevent(item_form.header, 'change', set_meta);


</script>
{/literal}
<!-- END Autogenerating meta tags -->
{/if}