            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Разделы сайта</h1>
                </div>
            </div>
            <div class="row">
            <!-- /-->
            </div>

            <div class="row">
				<div class="col-lg-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Меню
                        </div>
                        <div class="panel-body">
                        	<ul class="nav">
                        	{foreach item=m from=$Menus name=m}
                        		<li><a href="index.php?section=Sections&menu={$m->menu_id}">{$m->name|escape}</a></li>
                        	{/foreach}
                        	</ul>
                        </div>
                    </div>
				</div>
                <div class="col-lg-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {$Menu->name}
							<div class="pull-right">
								<div class="btn-group">
									<a class="btn btn-primary btn-xs" type="button" href="index.php?section=Section&menu={$Menu->menu_id}&token={$Token}">
										<i class="fa fa-check"></i> Добавить
									</a>
								</div>
							</div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Название</th>
                                            <th>Заголовок</th>
                                            <th>Модуль</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody id="sortable_sections">
                                    	{foreach item=section from=$Sections name=section}
                                        <tr id="item-{$section->id}">
                                            <td nowrap style="cursor: move;">
                                            	<i class="fa fa-arrows fa-1x"></i>
                                            </td>
                                            <td nowrap">
                                            {if $section->enabled}
												<a href="index.php{$section->enable_get}"><i class="fa fa-toggle-on fa-1x"></i></a>
											{else}
												<a href="index.php{$section->enable_get}"><i class="fa fa-toggle-off fa-1x"></i></a>														
											{/if}
                                            </td>
                                            <td width="35%">
                                            	<p {if !$section->enabled}class="text-muted"{/if}>
                                            		<a href="index.php{$section->edit_get}">{$section->name|escape}</a>
                                            	</p>
                                            </td>
                                            <td width="35%">
                                            	<p {if !$section->enabled}class="text-muted"{/if}>
													<a href="index.php{$section->edit_get}">{$section->header|escape}</a>
												</p>
                                            </td>
                                            <td width="30%">
                                            	<p {if !$section->enabled}class="text-muted"{/if}>{$section->module_name|escape}</p>
                                            </td>
                                            <td nowrap>
												<a href="index.php{$section->delete_get}" class="btn btn-danger btn-xs" type="button" onclick='if(!confirm("{$Lang->ARE_YOU_SURE_TO_DELETE}")) return false;'>
													<i class="fa fa-times-circle fa-1x"></i>
												</a>
                                            </td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
