<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class BlogWriters
 */
class BlogWriters extends Widget
{
    private $tableName = 'blogwriters';
    private $section = 'writers';

    /**
     * @param $parent
     */
    function BlogWriters(&$parent = null)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

//            header("Location: index.php$get");
        }

        if ($_POST['seo_title'] || $_POST['seo_description'] || $_POST['seo_keywords']){

            $this->setSeoContent($_POST);

            $get = $this->form_get(array());

//            header("Location: index.php$get");
        }

    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Авторы';

        $items = $this->getWriters();

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'BlogWriter', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
        }

        $seoContent = $this->getSeoContent();

        $this->smarty->assign('Items', $items);
        $this->smarty->assign('seoContent', $seoContent);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('blogwriters.tpl');
    }

    function getWriters(){
        $query = sql_placeholder("SELECT w.*, t.name_lead AS `lead`, bt.color AS blog_tags_color FROM " . $this->tableName . " AS w
        LEFT JOIN post_tags AS t ON t.id = w.leader
        LEFT JOIN blogtags AS bt ON bt.id=t.parent
        ORDER BY w.name ASC");
//        echo $query;
        $this->db->query($query);
        $items = $this->db->results();
//var_dump($items);die();
        return $items;
    }

    function setSeoContent($post)
    {
        $query = sql_placeholder("SELECT * FROM seo_content WHERE section = ?", $this->section);
        $this->db->query($query);
        $seoContent = $this->db->result();

        if (!empty($seoContent->id)){
            $query = sql_placeholder("UPDATE seo_content SET 
              section = ?,
              seo_title =?,
              seo_description =?,
              seo_keywords = ?
              WHERE id = ?
              ", $this->section, $post['seo_title'], $post['seo_description'], $post['seo_keywords'], $seoContent->id);
        }
        else{
            $query = sql_placeholder("INSERT IGNORE INTO seo_content SET 
              section = ?,
              seo_title =?,
              seo_description =?,
              seo_keywords = ?
              ", $this->section, $post['seo_title'], $post['seo_description'], $post['seo_keywords']);
        }

        $this->db->query($query);
    }

    function getSeoContent()
    {
        $query = sql_placeholder("SELECT * FROM seo_content WHERE section = ?", $this->section);
        $this->db->query($query);
        $seoContent = $this->db->result();

        return $seoContent;
    }
}
