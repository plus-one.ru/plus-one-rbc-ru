<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('BlogWriters.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('UnitMeasures.admin.php');

/**
 * Class BlogPost
 */
class BlogPost extends Widget
{
    /** @var   */
    public $item;
    /** @var string  */
    private $tableName = 'blogposts';
    /** @var string  */
    public $uploaddir = '../files/blogposts/';
    /** @var string  */
    public $large_image_width = "900";
    /** @var string  */
    public $large_image_height = "600";

    public $sizeImage_1_1 = array('x' => 1141, 'y' => 505);
    public $sizeImage_1_2 = array('x' => 559, 'y' => 365);
    public $sizeImage_1_3 = array('x' => 363, 'y' => 293);
    public $sizeImage_1_4 = array('x' => 615, 'y' => 410);




    function BlogPost(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));


        if ($_POST){
            $this->check_token();

            $item_id = $_POST['item_id'];

            $this->item->name = $_POST['name'];
            $this->item->header = $_POST['header'];
            $this->item->lead = $_POST['lead'];
            $this->item->header_rss = $_POST['header_rss'];
            $this->item->url = $_POST['url'];
            $this->item->blocks = $_POST['blocks'];
            $this->item->blocks_blog = $_POST['blocks_blog'];
            $this->item->blocks_author = $_POST['blocks_author'];
            $this->item->body = $_POST['body'];
            $this->item->meta_title = $_POST['meta_title'];
            $this->item->meta_description = substr($_POST['meta_description'], 0, 300);
            $this->item->meta_keywords = $_POST['meta_keywords'];
            $this->item->type_material = $_POST['type_material'];
            $this->item->tags = $_POST['tags'];
            $this->item->video_on_main = $_POST['video_on_main'];

            $this->item->post_tag = 0;


            if (empty($this->item->url)){
                $this->item->url = $this->translit($this->item->name);
            }

            $this->item->year_to_ammount = 0;

            $this->item->unit_measure = 0;
            if(isset($_POST['unit_measure'])) {
                $this->item->unit_measure = $_POST['unit_measure'];
            }

            if(isset($_POST['state'])) {
                $this->item->state = $_POST['state'];
            }

            $this->item->writers = 5;
            if(isset($_POST['writers'])) {
                $this->item->writers = $_POST['writers'];
            }

            $amountToAccount = str_replace(" ", "", $_POST['amount_to_account']);
            $amountToAccount = str_replace(",", ".", $amountToAccount);

            $this->item->amount_to_account = $amountToAccount;
            $this->item->amount_to_displaying = $_POST['amount_to_displaying'];
            $this->item->signature_to_sum = $_POST['signature_to_sum'];
            $this->item->text_body = $_POST['text_body'];

            $this->item->enabled = 0;
            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }

            $this->item->anounce_new_author = 0;
            if(isset($_POST['anounce_new_author']) && $_POST['anounce_new_author']==1) {
                $this->item->anounce_new_author = 1;
            }

            $this->item->partner = 0;
            $this->item->partner_url  = '';
            if(isset($_POST['partner']) && $_POST['partner']!='') {
                $this->item->partner = $_POST['partner'];
                $this->item->partner_url  = $_POST['partner_url'];
            }

            $this->item->show_main_page = 0;
            if(isset($_POST['show_main_page']) && $_POST['show_main_page']==1) {
                $this->item->show_main_page = 1;
            }

            $this->item->picture_day = 0;
            if(isset($_POST['picture_day']) && $_POST['picture_day']==1) {
                $this->item->picture_day = 1;
            }

            $created = $_POST['created'];
            $createdToDb = date('Y-m-d H:i:s', strtotime($created));
            $this->item->created = $createdToDb;

            $this->item->type_post = $_POST['type_post'];

            // цвет блока указываем по тегу первого уровня, тега втрого уровня, в котором автор является лидером
            if ($this->item->type_post == 4) {
                $query = sql_placeholder("SELECT w.*, t.name_lead AS lead, bt.color AS blog_tags_color FROM blogwriters AS w
                            LEFT JOIN post_tags AS t ON t.id = w.leader
                            LEFT JOIN blogtags AS bt ON bt.id=t.parent
                            WHERE w.id = ?", $this->item->writers);
                $this->db->query($query);

                $itemWriter = $this->db->result();
                if ($itemWriter->blog_tags_color) {
                    $this->item->block_color = $itemWriter->blog_tags_color;
                }

                $this->item->blocks_blog = $_POST['blocks'];
            }
            elseif($this->item->type_post == 5){

                $this->item->post_tag = $_POST['post_tag'];
                // тип поста - данные
                $query = sql_placeholder("SELECT t.name_lead AS lead, bt.color AS blog_tags_color FROM post_tags AS t
                            LEFT JOIN blogtags AS bt ON bt.id=t.parent
                            WHERE t.id = ?", $this->item->post_tag);
                $this->db->query($query);
                $itemPostTag = $this->db->result();

                if ($itemPostTag->blog_tags_color) {
                    $this->item->block_color = $itemPostTag->blog_tags_color;
                }

                $this->item->blocks_blog = $_POST['blocks'];

                $this->item->year_to_ammount = $_POST['year_to_ammount'];

            }
            else{
                $this->item->block_color = $_POST['block_color'];
            }

            ## Не допустить одинаковые URL статей
            $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE url=? AND id!=?', $this->item->url, $item_id);
            $this->db->query($query);
            $res = $this->db->result();

            if(empty($this->item->name)){
                $this->error_msg = $this->lang->ENTER_TITLE;
            }
            elseif($res->count>0){
                $this->error_msg = 'Пост не сохранен. Запись с таким URL уже существует. Укажите другой.';
            }


            // проверка на обязательные поля
            switch ($this->item->type_post) {
                case 1:
                case 2:
                case 3:
                    // проверка на обычный пост
                    if (empty($this->item->name) || empty($this->item->header_rss) || $this->item->created == '1970-01-01 00:00:00' || $this->item->writers == 0 || empty($this->item->url)){
                        $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                    }
                    break;
                case 4:
                    // проверка анонса нового блога
                    if ($this->item->writers == 0 || $this->item->created == '1970-01-01 00:00:00'){
                        $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                    }
                    break;
                case 5:
                    // проверка на аналитику (данные)
                    if ($this->item->writers == 0 || $this->item->created == '1970-01-01 00:00:00'){
                        $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                    }
                    break;
                case 6:
                    // проверка на обычный пост
                    if (empty($this->item->name) || empty($this->item->video_on_main) || $this->item->created == '1970-01-01 00:00:00' || empty($this->item->url)){
                        $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                    }
                    break;
            }



            if (empty($this->error_msg)) {
                if(empty($item_id)) {
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }

                // партнерские ссылки-посты
                if (!empty($_POST['partnerlink'])){
                    $this->addRelatedPartnersPosts($item_id, $_POST['partnerlink']);
                }

                if (!empty($_POST['post_tag'])){
                    $this->setRelationsPostItemTags($_POST['post_tag'], $item_id);
                }
                elseif(!isset($_POST['post_tag'])){
                    // никаких тегов не выбрано, убираем все старые теги от этой статьи
                    $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $item_id);
                    $this->db->query($query);
                }

                // добавляем данные для аналитики
                if ($_POST['value_to_yeats']){
                    // записываем данные аналитики в бд
                    $this->setWritersData($item_id, $this->item->writers, $_POST);
                }

                // загрузка картинки
                $this->add_fotos($item_id);

                // переданы видео (список видео)
                if (isset($_POST['videoId'])){
                    $postVideoIds = $_POST['videoId'];
                    foreach ($postVideoIds AS $key=>$videoId){
                        // загрузка картинки к видео и заголовка для каждой записи
                        $res = $this->addVideoImages($videoId, $_POST['header_interview'][$videoId], $_POST['video_url'][$videoId]);

                        if (isset($_POST['description_time_division'][$videoId])){

                            foreach ($_POST['description_time_division'][$videoId] AS $fragmentId => $videoFragments){
                                $minute = $_POST['minute'][$videoId][$fragmentId];
                                $secunde = $_POST['secunde'][$videoId][$fragmentId];
                                $resSaveFragment = $this->saveVideoFragment($videoId, $fragmentId, $minute, $secunde, $videoFragments);
                            }
                        }
                    }
                }
                header("Location: /adminv2/blogpost/");
            }
        }
        elseif (!empty($item_id)){
            $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created FROM " . $this->tableName . " WHERE id=?", $item_id);
            $this->db->query($query);
            $this->item = $this->db->result();
        }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новая запись в блоге';
            $this->item->type_post = 1;
            $this->item->date_created = date('d.m.Y H') . ":00";
		}
		else{
			$this->title = 'Изменение записи: ' . $this->item->name;
		}

        // тип материала может приетет как гетом, так и постом
        $type_visual_block = $_REQUEST['type_visual_block'];

        $bw = new BlogWriters();
        $writers = $bw->getWriters();

        $bt = new BlogTags();
        $tags = $bt->getTags();

        $um = new UnitMeasures();
        $unitMeasures = $um->getItems();

        $blogTags = $tags;

        foreach ($blogTags AS $k=>$blogTag){

            $query = sql_placeholder("SELECT id, name FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);

            $blogTags[$k]->items = $this->db->results();
        }

        $query = sql_placeholder('SELECT id, name FROM partners');
        $this->db->query($query);
        $partners = $this->db->results();

        $query = sql_placeholder('SELECT id, name FROM typematerial');
        $this->db->query($query);
        $typeMaterials = $this->db->results();

        // партнерские посты
        $query = sql_placeholder('SELECT * FROM partners_posts_links WHERE post_id=? LIMIT 3',$this->item->id);
        $this->db->query($query);
        $partnersPosts = $this->db->results();

        $query = sql_placeholder('SELECT posttag_id FROM relations_postitem_tags WHERE post_id=?',$this->item->id);
        $this->db->query($query);
        $selectedTags = $this->db->results();



        foreach ($selectedTags AS $selectedTag){
            $selectTags[$selectedTag->posttag_id] = $selectedTag->posttag_id;
        }

        foreach ($tags AS $key=>$tag){
            foreach ($tag->items AS $k=>$v){
                if (in_array($v->id, (array)$selectTags)){
                    $tags[$key]->items[$k]->check = 1;
                }
                else{
                    $tags[$key]->items[$k]->check = 0;
                }
            }

        }

        $query = sql_placeholder("SELECT years, value_to_yeats FROM blogwriters_data WHERE post_id=? AND writer_id=? ORDER BY years DESC LIMIT 7",
            $this->item->id,
            $this->item->writers);
        $this->db->query($query);
        $this->item->analitycData = $this->db->results();

        $postsClass = new BlogPosts();
        $linkedPosts = $postsClass->getLinkedPost($this->item->id);
        $this->smarty->assign('linkedPosts', $linkedPosts);


        $query = sql_placeholder("SELECT * FROM videos WHERE parent_video_post=? ORDER BY id DESC",
            $this->item->id);
        $this->db->query($query);
        $videos = $this->db->results();

        foreach ($videos AS $key=>$video){
            $query = sql_placeholder("SELECT * FROM division_time_videos WHERE parent_video=? ORDER BY id DESC",
                $video->id);
            $this->db->query($query);
            $divisionTime = $this->db->results();
            $videos[$key]->divisionTime = $divisionTime;
        }

        $this->item->videos = $videos;

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('writers', $writers);
        $this->smarty->assign('tags', $tags);
        $this->smarty->assign('blogTags', $blogTags);
        $this->smarty->assign('typeMaterials', $typeMaterials);
        $this->smarty->assign('partners', $partners);
        $this->smarty->assign('partnersPosts', $partnersPosts);
        $this->smarty->assign('unitMeasures', $unitMeasures);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('root_url', $this->root_url);

        if ($type_visual_block == 'post'){
            $this->body = $this->smarty->fetch('blogpost/blogpost.tpl');
        }
        elseif ($type_visual_block == 'data'){
            $this->body = $this->smarty->fetch('blogpost/blogdata.tpl');
        }
        elseif ($type_visual_block == 'newblog'){
            $this->body = $this->smarty->fetch('blogpost/newblogpost.tpl');
        }
        elseif ($type_visual_block == 'newvideo'){
            $this->body = $this->smarty->fetch('blogpost/videopost.tpl');
        }
        else{
            $this->body = $this->smarty->fetch('blogpost/blogpost.tpl');
        }


	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);

        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * сохранение связей между постом и тегами-разделами
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
            foreach ($tags AS $tag){
                $query = sql_placeholder('INSERT INTO relations_post_tags SET tag_id=?, post_id=?', $tag, $itemId);
                $this->db->query($query);
            }
        }
        elseif (empty($tags) && $itemId!=0){
            // если к нам прилетел пустой массив с тегами первого уровня (рубриками)
            // это значит что пост должен быть удален отовсюду.
            // и отображаться только в блоге автора материала
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
        }
    }

    function setWritersData($postId = 0, $writerId = 0, $data = array()){
        if ($postId != 0 && $writerId != 0 && !empty($data)){
            if (isset($data['value_to_yeats'])){

                $query = sql_placeholder('DELETE FROM blogwriters_data WHERE post_id=? AND writer_id=?', $postId, $writerId);
                $this->db->query($query);

                foreach ($data['value_to_yeats'] AS $key=>$value){
                    if (!empty($value)){
                        $year = $data['years'][$key];

                        $query = sql_placeholder('INSERT INTO blogwriters_data SET post_id=?, writer_id=?, years=?, value_to_yeats=?',
                            $postId, $writerId, $year, $value);
                        $this->db->query($query);
                    }
                }
            }
            return false;
        }
        return false;
    }

    /**
     * сохранение связей между постом и тегами самой статьи (облаком тегов)
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsPostItemTags($tags=array(), $itemId=0){

        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $itemId);
            $this->db->query($query);

            if (is_array($tags)){
                foreach ($tags AS $posttagId){
                    $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $posttagId, $itemId);
                    $this->db->query($query);
                }
            }
            elseif($tags != 0){
                $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $tags, $itemId);
                $this->db->query($query);
            }
        }
    }

    /**
     * добавление партнерского поста
     *
     * @param int $postId
     * @param array $partnerPosts
     * @return null
     */
    function addRelatedPartnersPosts($postId = 0, $partnerPosts = array()){
       if ($postId != 0 && !empty($partnerPosts)){
            foreach ($partnerPosts AS $key=>$post){
                // проверим, нет ли такого партнерского материала, у этой статьи с такой же ссылкой
                $query = sql_placeholder('SELECT id FROM partners_posts_links WHERE post_id=? AND link=?',
                    $postId, $post['link']);
                $this->db->query($query);
                $availablePartnerPost = $this->db->result();

                if (!$availablePartnerPost) {
                    $post['post_id'] = $postId;
                    if(isset($_FILES)){
                        $post['image'] = '';
                    }

                    $query = sql_placeholder('INSERT INTO partners_posts_links SET ?%', $post);
                    $this->db->query($query);
                    $partnerPostId = $this->db->insert_id();

                    $this->addPartnerPostPhoto($partnerPostId, $key);
                }
                else{
                    $query = sql_placeholder('UPDATE partners_posts_links SET ?% WHERE id=?', $post, $availablePartnerPost->id);
                    $this->db->query($query);

                    $this->addPartnerPostPhoto($availablePartnerPost->id, $key);
                }

            }
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        $image_1_1 = $itemId . '-1_1.jpg';
        $image_1_1_c = $itemId . '-1_1_c.jpg';
        $image_1_2 = $itemId . '-1_2.jpg';
        $image_1_3 = $itemId . '-1_3.jpg';
        $image_1_4 = $itemId . '-1_4.jpg';
        $image_rss = $itemId . '-rss.jpg';

        if(isset($_FILES['image_1_1']) && !empty($_FILES['image_1_1']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->uploaddir.$image_1_1)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1='$image_1_1' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_1_c']) && !empty($_FILES['image_1_1_c']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1_c']['tmp_name'], $this->uploaddir.$image_1_1_c)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1 цитата';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1_c='$image_1_1_c' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_2']) && !empty($_FILES['image_1_2']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->uploaddir.$image_1_2)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_2='$image_1_2' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_3']) && !empty($_FILES['image_1_3']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->uploaddir.$image_1_3)){
                $this->error_msg = 'Ошибка при загрузке файла 1/3';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_3='$image_1_3' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_4']) && !empty($_FILES['image_1_4']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_4']['tmp_name'], $this->uploaddir.$image_1_4)){
                $this->error_msg = 'Ошибка при загрузке файла 1/4';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_4='$image_1_4' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_rss']) && !empty($_FILES['image_rss']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_rss']['tmp_name'], $this->uploaddir.$image_rss)){
                $this->error_msg = 'Ошибка при загрузке файла RSS';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_rss='$image_rss' WHERE id={$itemId}");
                $result = true;
            }
        }

        return $result;
    }

    /**
     * добавление фото к партнерскому посту
     *
     * @param $partnerPostId
     * @param $key
     * @return bool
     */
    function addPartnerPostPhoto($partnerPostId, $key){
        $result = false;

        $imageName = 'pi_' . $partnerPostId . '-1_3.jpg';

        if ($key == 1){
            if(isset($_FILES['partnerimage1']) && !empty($_FILES['partnerimage1']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage1']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 1';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 2){
            if(isset($_FILES['partnerimage2']) && !empty($_FILES['partnerimage2']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage2']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 2';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 3){
            if(isset($_FILES['partnerimage3']) && !empty($_FILES['partnerimage3']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage3']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 3';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }
        return $result;
    }

    function addVideoImages($videoId, $headerText, $urlVideo){
        $result = false;

        $imageName = 'videoImage_' . $videoId . '.jpg';

        if (isset($_FILES['imageVideo']) && !empty($_FILES['imageVideo']['tmp_name'][$videoId])) {


            if (!move_uploaded_file($_FILES['imageVideo']['tmp_name'][$videoId], $this->uploaddir . $imageName)) {
                $this->error_msg = 'Ошибка при загрузке файла 1';
            } else {
                if (!empty($urlVideo) && !empty($headerText)){
                    $query = "UPDATE videos SET image='{$imageName}', video_url='{$urlVideo}', name='{$headerText}' WHERE id={$videoId}";
                    $this->db->query($query);
                    $result = true;
                }
            }
        }
        else{
            if (!empty($urlVideo) && !empty($headerText)){
                $query = "UPDATE videos SET image='{$imageName}', video_url='{$urlVideo}', name='{$headerText}' WHERE id={$videoId}";
                $this->db->query($query);
                $result = true;
            }
        }

        return $result;
    }

    function saveVideoFragment($videoId, $fragmentId, $min, $sec, $name){

        $result = false;

        $query = "UPDATE division_time_videos SET parent_video='{$videoId}', minute='{$min}', secunde='{$sec}', name='{$name}' WHERE id={$fragmentId}";
        if ($this->db->query($query)){
            $result = true;
        }

        return $result;

    }

}