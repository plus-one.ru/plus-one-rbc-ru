<?PHP
require_once('Widget.admin.php');
require_once('Storefront.admin.php');
require_once('../placeholder.php');

/**
 * Class Article
 */
class Article extends Widget
{
    var $item;
    var $uploaddir = '../file/articles/'; # Папка для хранения картинок (default)
    var $large_image_width = "800";
    var $large_image_height = "600";
    var $small_image_width = "150";
    var $small_image_height = "113";

  
    function Article(&$parent){
        Widget::Widget($parent);
        # параметры из настроек сайта
        // папка для хранения картинок
		if (!empty($this->settings->article_path_to_uploaded_images)){
			$this->uploaddir = '../' . $this->settings->article_path_to_uploaded_images;
		}
        // ширина большой картинки
        if (!empty($this->settings->article_large_image_width)){
            $this->large_image_width = $this->settings->article_large_image_width;
        }
        // высота большой картинки
        if (!empty($this->settings->article_large_image_height)){
            $this->large_image_height = $this->settings->article_large_image_height;
        }
        // ширина маленькой картинки
        if (!empty($this->settings->article_small_image_width)){
            $this->small_image_width = $this->settings->article_small_image_width;
        }
        // высота маленькой картинки
        if (!empty($this->settings->article_small_image_height)){
            $this->small_image_height = $this->settings->article_small_image_height;
        }

        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));
        if(isset($_POST['name']) && isset($_POST['header']) && isset($_POST['url']) && intval($_POST['category'])!=0){
            $this->check_token();

            $this->item->name = $_POST['name'];
            $this->item->header = $_POST['header'];
            $this->item->meta_title = $_POST['meta_title'];
            $this->item->meta_keywords = $_POST['meta_keywords'];
            $this->item->meta_description = $_POST['meta_description'];
            $this->item->url = $_POST['url'];
            $this->item->annotation = $_POST['annotation'];
            $this->item->body = $_POST['body'];
            $this->item->category_id = $_POST['category'];
            $this->item->category_products_id = $_POST['category_products'];

            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }
            else{
                $this->item->enabled = 0;
            }
  		  
            ## Не допустить одинаковые URL статей
    	    $query = sql_placeholder('SELECT COUNT(*) AS count FROM articles WHERE url=? AND id!=?', $this->item->url, $item_id);
            $this->db->query($query);
            $res = $this->db->result();

  		    if(empty($this->item->header)){
			    $this->error_msg = $this->lang->ENTER_TITLE;
  		    }
  		    elseif($res->count>0){
			    $this->error_msg = 'Статья с таким URL уже существует. Выберите другой URL.';
  		    }
            else{
  			    if(empty($item_id)) {
                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
  	    		else{
                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $this->db->query("UPDATE articles SET url=article_id WHERE url=''");

                 // Если нужно, удаляем фотографии товара
                if(isset($_POST['delete_small_image']) && $_POST['delete_small_image']==1){
                    $this->delete_small_image($item_id);
                }

                if(isset($_POST['delete_large_image']) && $_POST['delete_large_image']==1){
                    $this->delete_large_image($item_id);
                }

                $this->add_fotos($item_id);

                $get = $this->form_get(array('section'=>'Articles'));
			
			    if(isset($_GET['from'])){
				    header("Location: ".$_GET['from']);
			    }
			    else{
				    header("Location: index.php$get");
			    }
  		    }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM articles WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = $this->lang->NEW_ARTICLE;
		}
		else{
			$this->title = $this->lang->EDIT_ARTICLE;
		}

		$query = "SELECT * FROM articlescategories";
		$this->db->query($query);
		$articlescategories = $this->db->results();

		$categories = Storefront::get_categories();

		$this->smarty->assign('Item', $this->item);
		$this->smarty->assign('articlescategories', $articlescategories);
        $this->smarty->assign('categories', $categories);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('images_human_uploaddir', "http://".$this->root_url.str_replace("..", "", $this->uploaddir));
        $this->smarty->assign('large_images_width', $this->large_image_width);
        $this->smarty->assign('large_images_height', $this->large_image_height);
        $this->smarty->assign('small_images_width', $this->small_image_width);
        $this->smarty->assign('small_images_height', $this->small_image_height);

		$this->body = $this->smarty->fetch('article.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO articles SET category_id=?, category_products_id=?, name=?, header=?, url=?, meta_title=?, meta_keywords=?, meta_description=?, annotation=?, body=?, enabled=?, created=now(), modified=now()',
            $this->item->category_id,
            $this->item->category_products_id,
            $this->item->name,
            $this->item->header,
            $this->item->url,
            $this->item->meta_title,
            $this->item->meta_keywords,
            $this->item->meta_description,
            $this->item->annotation,
            $this->item->body,
            $this->item->enabled
        );
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE articles SET order_num=article_id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE articles SET category_id=?, category_products_id=?, name=?, header=?, url=?, meta_title=?, meta_keywords=?, meta_description=?, annotation=?, body=?, enabled=?, modified=now() WHERE id=?',
            $this->item->category_id,
            $this->item->category_products_id,
            $this->item->name,
            $this->item->header,
            $this->item->url,
            $this->item->meta_title,
            $this->item->meta_keywords,
            $this->item->meta_description,
            $this->item->annotation,
            $this->item->body,
            $this->item->enabled,
            $item_id
        );
        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
	function add_fotos($article_id){
		$result = false;

		$largeuploadfile = $article_id."_large.jpg";
		$smalluploadfile = $article_id."_small.jpg";

		/// Загрузка большой картинки
		$large_image_uploaded = false;
		if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
			if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
				$this->error_msg = $this->lang->FILE_UPLOAD_ERROR;
			}
			else{
		 		$large_image_uploaded = true;
		 	}
		}

		if($large_image_uploaded){
            $upload_folder = $this->uploaddir.$largeuploadfile;
			$this->im_image_resize($upload_folder, $upload_folder, $this->large_image_width, $this->large_image_height);
			@chmod($this->uploaddir.$largeuploadfile, 0644);
			$this->db->query("UPDATE articles SET large_image='$largeuploadfile' WHERE id=$article_id");
			$result = true;
		}

		/// Загрузка маленькой картинки
		$small_image_uploaded = false;
		if(isset($_FILES['small_image']) && !empty($_FILES['small_image']['tmp_name']) && intval($_FILES['error'])!=0){
			if (!move_uploaded_file($_FILES['small_image']['tmp_name'], $this->uploaddir.$smalluploadfile)){
				$this->error_msg = $this->lang->FILE_UPLOAD_ERROR;
			}
			else{
				$small_image_uploaded = true;
			}
		}

		if($small_image_uploaded){
            $upload_folder = $this->uploaddir.$smalluploadfile;
            $this->im_image_resize($upload_folder, $upload_folder, $this->small_image_width, $this->small_image_height);
			@chmod($this->uploaddir.$smalluploadfile, 0644);
			$this->db->query("UPDATE articles SET small_image='$smalluploadfile' WHERE id=$article_id");
			$result = true;
		}

		if (is_file($this->uploaddir.$largeuploadfile) && $_POST['auto_small'] == '1'){
            $upload_folder = $this->uploaddir.$largeuploadfile;
            $upload_folder_sm = $this->uploaddir.$smalluploadfile;
            $this->im_image_resize($upload_folder, $upload_folder_sm, $this->small_image_width, $this->small_image_height);
			@chmod($this->uploaddir.$smalluploadfile, 0644);
			$this->db->query("UPDATE articles SET small_image='$smalluploadfile' WHERE id=$article_id");
			$result = true;
		}

		return $result;
	}


	// Удаляем маленькую картинку
	function delete_small_image($article_id)
	{
	    $query = "SELECT small_image FROM articles WHERE id='$article_id'";
	    $this->db->query($query);
	    $article = $this->db->result();
	    if(!empty($article->small_image))
	    {
	      $file = $this->uploaddir.$article->small_image;
	      // удаляем файл
	      unlink($file);
	      // И удаляем из таблицы новостей картинку
	      $this->db->query("UPDATE articles SET small_image='' WHERE id=$article_id");
	    }
	}

	// Удаляем большую картинку новости
	function delete_large_image($article_id) 
	{
	    $query = "SELECT large_image FROM articles WHERE id = '$article_id'";
	    $this->db->query($query);
	    $article = $this->db->result();

	    if(!empty($article->large_image))
	    {    
	      $file = $this->uploaddir.$article->large_image;
	      // удаляем файл
	      unlink($file);
	      // И удаляем из таблицы новостей картинку
	      $this->db->query("UPDATE articles SET large_image='' WHERE id=$article_id");
	    }
	}
}