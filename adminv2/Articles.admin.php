<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('PagesNavigation.admin.php');


/**
 * Class Articles
 */
class Articles extends Widget{
    var $pages_navigation;
    var $items_per_page = 1;
    var $category;
    var $uploaddir = '../file/articles/'; # Папка для хранения картинок (default)
  
    function Articles(&$parent){
        parent::Widget($parent);
        $this->add_param('page');
        $this->add_param('category');
        if (!empty($this->settings->article_path_to_uploaded_images)){
            $this->uploaddir = '../' . $this->settings->article_path_to_uploaded_images;
        }
        $this->prepare();
    }

    function prepare(){
  	    if((isset($_POST['act']) && $_POST['act']=='delete' || isset($_GET['act']) && $_GET['act']=='delete') &&
          (isset($_POST['items']) || isset($_GET['item_id']) ))
        {
            $this->check_token();

            if(isset($_GET['item_id']) && !empty($_GET['item_id'])){
                $items = array($_GET['item_id']);
            }

            if(is_array($items)){
                $items_sql = implode("', '", $items);
            }
            else{
                $items_sql = $items;
            }

            $query = "DELETE FROM articles WHERE article_id IN ('$items_sql')";
  		    $this->db->query($query);
  		    $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        # Сделать статью видимой
        if(isset($_GET['set_enabled'])){
            $this->check_token();

            $id = $_GET['set_enabled'];
            $query = sql_placeholder('UPDATE articles SET enabled=1-enabled WHERE article_id=?',$id);
            $this->db->query($query );

            $get = $this->form_get(array());
      
            if(isset($_GET['from'])) {
              header("Location: " . $_GET['from']);
            }
            else{
              header("Location: index.php$get");
            }
        }
    }

    function fetch(){
        $this->title = 'Статьи';

        $query = "SELECT * FROM articlescategories ORDER BY order_num ASC";
        $this->db->query($query);
        $categories = $this->db->results();

        $category_id = intval($this->param('category'));

        if(empty($category_id)){
            $category_id = $categories[0]->id;
        }

        $query = sql_placeholder("SELECT * FROM articlescategories WHERE id = ? LIMIT 1", $category_id);
        $this->db->query($query);
        $this->category = $this->db->result();
        $this->title = $this->category->name;

        $query = sql_placeholder("SELECT *,
                              DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(created, '%H:%i') AS time_created,
                              DATE_FORMAT(modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(modified, '%H:%i') AS time_modified 
                              FROM articles 
                              WHERE category_id=? 
                              ORDER BY order_num ASC", $category_id);
        $this->db->query($query);
        $items = $this->db->results();

        foreach($items as $key=>$item){
            $items[$key]->edit_get = $this->form_get(array('section'=>'Article','item_id'=>$item->id, 'token'=>$this->token));
            $items[$key]->delete_get = $this->form_get(array('act'=>'delete','item_id'=>$item->id, 'token'=>$this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'category'=>$category_id, 'token'=>$this->token));
        }

        $this->smarty->assign('categories', $categories);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('current_category', $category_id);
        $this->body = $this->smarty->fetch('articles.tpl');
    }
}
