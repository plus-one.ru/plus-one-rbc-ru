<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class PostTags
 */
class PushMessages extends Widget
{
    private $tableName = 'push_notify';
    var $uploaddir = '../files/push/'; # Папка для хранения картинок (default)

    /**
     * @param $parent
     */
    function PushMessages(&$parent)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_show'])) {
            $this->check_token();

            $this->setShow($_GET['set_show'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }
    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Отправленные PUSH уведомления';

        $items = $this->getNotifys();

        foreach ($items as $key => $item) {
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
        }

        $this->smarty->assign('Items', $items);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

        $this->body = $this->smarty->fetch('pushNotify/push_notifys.tpl');
    }


    function getNotifys(){
        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS created FROM " . $this->tableName . " ORDER BY created DESC");
        $this->db->query($query);
        $items = $this->db->results();

        return $items;
    }
}
