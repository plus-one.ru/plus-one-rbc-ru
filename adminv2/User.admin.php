<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

############################################
# Class EditServiceSection - edit the static section
############################################
class User extends Widget
{
  var $user;
  function User(&$parent)
  {
    Widget::Widget($parent);
    $this->add_param('page');
    $this->add_param('group_id');
    $this->prepare();
  }

  function prepare()
  {
  	$user_id = $this->param('user_id');
  	if(isset($_POST['name']))
  	{
  	    $this->check_token();
  	    
  		$this->item->name			= $_POST['name'];
  		$this->item->email			= $_POST['email'];
		$this->item->company		= $_POST['company'];
		$this->item->town			= $_POST['town'];
		$this->item->phone			= $_POST['phone'];
		$this->item->sent_notice	= $_POST['sent_notice'];
  		$this->item->group_id = $_POST['group_id'];
  		$this->item->enabled = 0;
        if(isset($_POST['enabled']))
          $this->item->enabled = $_POST['enabled'];

  		$query = sql_placeholder('UPDATE users SET email=?, name=?, name_company=?, city_company=?, phone_company=?, group_id=?, enabled=? WHERE user_id=? LIMIT 1',
  			                          $this->item->email,
									  $this->item->name,
  			                          $this->item->company,
									  $this->item->town,
									  $this->item->phone,
  			                          $this->item->group_id,
  			                          $this->item->enabled,
  			                          $user_id);

  		$this->db->query($query);
		
		if ($this->item->sent_notice==0 && $this->item->group_id==3) // уведомление о том, что перенесли в группу партнеров не отправлялось
		{
			// отправляем уведомление о смене статуса учетной записи.
			$message = "";
			$message .= "Здравствуйте!<br/>";
			$message .= "Вы зарегистрировались на сайте <a href='www.elitech-tools.ru'>www.elitech-tools.ru</a> в качестве Партнера.<br/>";
			$message .= "Сообщаем Вам, что аккаунт успешно активирован.<br/><br/>";
			
			$message .= "Для доступа в раздел \"Партнерам\" Вам необходимо повторно <a href='http://www.elitech-tools.ru/relogin'>Авторизоваться</a> на сайте.<br/><br/>";
			
			$message .= "По возникающим вопросам просим обращаться на почту <a href='mailto:omr@litopt.ru'>omr@litopt.ru</a><br/><br/>";
			$message .= "Желаем приятной работы!";
			
    		$this->sent_email($this->item->email, 'Смена статуса учетной записи', $message);
			$query = sql_placeholder('UPDATE users SET sent_notice=1 WHERE user_id=? LIMIT 1', $user_id);
			$this->db->query($query);
		}
		
		
  		if($this->item->group_id == $this->param('group_id'))
    		$get = $this->form_get(array('section'=>'Users', 'group'=>$this->item->group_id, 'page'=>$this->param('page'), 'keyword'=>$this->param('keyword')));
        else
    		$get = $this->form_get(array('section'=>'Users', 'group'=>$this->item->group_id, 'keyword'=>$this->param('keyword')));
  		header("Location: index.php$get");

  	}

   $query = sql_placeholder('SELECT * FROM users WHERE user_id=? LIMIT 1', $user_id);
   $this->db->query($query);
   $this->user = $this->db->result();
  }

  function fetch()
  {
      $this->title = $this->lang->EDIT_USER;

      $this->db->query("SELECT * FROM groups ORDER BY discount");
      $groups = $this->db->results();

      $this->smarty->assign('Groups', $groups);
      $this->smarty->assign('User', $this->user);
 	  $this->smarty->assign('Error', $this->error_msg);
      $this->smarty->assign('Lang', $this->lang);
 	  $this->body = $this->smarty->fetch('user.tpl');
  }
  
    function sent_email($to, $subject, $message, $additional_headers='')
    {
    	$site_name = "=?utf-8?B?www.elitech-tools.ru?=";
    	
    	if(!empty($this->settings->notify_from_email))
    		$from = "www.elitech-tools.ru <noreply@elitech-tools.ru>";
    	else
    		$from = "www.elitech-tools.ru <noreply@elitech-tools.ru>";
    		
    	$headers = "MIME-Version: 1.0\n" ;
    	$headers .= "Content-type: text/html; charset=utf-8; \r\n"; 
    	$headers .= "From: $from \r\n";
    	
    	$subject = "=?utf-8?B?".base64_encode($subject)."?=";
    	@mail($to, $subject, $message, $headers);
    }
}
