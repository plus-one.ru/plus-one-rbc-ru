<?php
	session_start();
	chdir('..'); 
	require_once('Widget.admin.php');
    $widget = new Widget();

    require_once('BlogPosts.admin.php');
    $postsClass = new BlogPosts();

    session_write_close();

	if (isset($_GET['item_id'])){
        $where = " AND b.id!=" . $_GET['item_id'];
	}

    $posts['data'] = $postsClass->getPostsData($where);

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	echo json_encode($posts);
