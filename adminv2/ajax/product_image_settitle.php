<?php
chdir('../');

require_once('Widget.admin.php');
$widget = new Widget();

$galleryId = $_POST['galleryId'];
$photo_id   = $_POST['photo_id'];
//$title      = $_POST['title'];
//$author = $_POST['author'];

$type = $_POST['type'];
$value = $_POST['value'];

if ($galleryId!="") {
    // сохраняем подпись к картинке
    if ($type == 'image_name'){
        $query = sql_placeholder('UPDATE images SET image_name=?  WHERE gallery_id=? AND id=?', $value, $galleryId, intval($photo_id));
        $widget->db->query($query);
        $result = true;
    }


    if ($type == 'image_author'){
        $query = sql_placeholder('UPDATE images SET image_author=?  WHERE gallery_id=? AND id=?', $value, $galleryId, intval($photo_id));
        $widget->db->query($query);
        $result = true;
    }


    print json_encode($result);
}
