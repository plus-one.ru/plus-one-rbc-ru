<?php
chdir('../');

require_once('Widget.admin.php');
$widget = new Widget();

$galleryId = $_POST['galleryId'];
$photo_id   = $_POST['photo_id'];
$result = false;

if ($galleryId!="" && $photo_id!=0) {

    // выбираем удаляемое фото из БД
    $query = sql_placeholder('SELECT filename FROM images WHERE gallery_id=? AND id=? LIMIT 1', $galleryId, intval($photo_id));
    $widget->db->query($query);
    $image = $widget->db->result();
    if (!empty($image)){
        // удаляем запись из базы
        $query = sql_placeholder('DELETE FROM images WHERE gallery_id=? AND id=? LIMIT 1', $galleryId, intval($photo_id));
        $widget->db->query($query);

        // удаляем файл
        unlink("../files/products/".$image->filename);
        $result = true;
    }
    print $result;
}
