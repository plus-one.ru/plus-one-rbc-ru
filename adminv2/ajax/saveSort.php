<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
chdir('..');
require_once('BlogPosts.admin.php');
$blogPost = new BlogPosts();

$values = $_POST['values'];
$table = $_POST['table'];

$result = $blogPost->saveSort($values, $table);

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);

?>