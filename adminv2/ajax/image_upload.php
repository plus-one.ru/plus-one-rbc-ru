<?php
chdir('../../');

require_once('Widget.class.php');
$widget = new Widget();

$uploaddir = "/files/photogallerys/";

$targetPath = getcwd() . $uploaddir;

function isImage($filename) {
  return file_exists($filename) && @is_array(getimagesize($filename));
}

if (!empty($_FILES)) {

    $tempFile = $_FILES['file']['tmp_name'];

    $tmpFileName = explode(".", $_FILES['file']['name']);
    $fileExt = $tmpFileName[1];
    $galleryId = (int)$_GET['galleryId'];


  if ($fileExt === 'php' || !isImage($tempFile)) {
    // Ошибка. Файл не является изображением
    print 0;
    exit;
  }

    $newFullFileName = $galleryId . "_" . microtime(true) . "_image." . $fileExt;

    $targetFile = $targetPath . $newFullFileName;

    if (move_uploaded_file($tempFile, $targetFile)) {
        // картинок может быть несколько,
        // поэтому необходимо убедиться, что в к данному товару еще небыло загруженно ни одной картинки,
        // а если было - то к имени файла добавить хэш строку

        $query = sql_placeholder('SELECT COUNT(id) AS cnt FROM images WHERE filename=? AND gallery_id=?', $newFullFileName, $galleryId);
        $widget->db->query($query);
        $res = $widget->db->result();

        // запишем в базу картинку товара
        $query = sql_placeholder('INSERT IGNORE INTO images SET filename=?, gallery_id=?', $newFullFileName, $galleryId);
        if ($widget->db->query($query)) {
            $image_id = $widget->db->insert_id();

            $query = sql_placeholder('UPDATE images SET order_num=foto_id WHERE foto_id=?', $image_id);
            $widget->db->query($query);

            print $galleryId;
        } else {
            print 0;
        }
    } else {
        print 0;
    }
} else {
    print 0;
}
