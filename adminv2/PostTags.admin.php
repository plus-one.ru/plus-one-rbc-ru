<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class PostTags
 */
class PostTags extends Widget
{
    private $tableName = 'post_tags';
    private $section = 'posttags';

    /**
     * @param $parent
     */
    function PostTags(&$parent)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_show'])) {
            $this->check_token();

            $this->setShow($_GET['set_show'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if ($_POST['seo_title'] || $_POST['seo_description'] || $_POST['seo_keywords']){
            $this->setSeoContent($_POST);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Теги второго уровня';

        $items = $this->getTags();

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'PostTag', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
        }

        $seoContent = $this->getSeoContent();

        $this->smarty->assign('Items', $items);
        $this->smarty->assign('seoContent', $seoContent);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('posttags/posttags.tpl');
    }


    function getTags(){
        $query = sql_placeholder("SELECT t.*,
                                    DATE_FORMAT(t.created, '%d.%m.%Y') AS date_created,
                                    DATE_FORMAT(t.created, '%H:%i') AS time_created,
                                    DATE_FORMAT(t.modified, '%d.%m.%Y') AS date_modified,
                                    DATE_FORMAT(t.modified, '%H:%i') AS time_modified,
                                    bt.name AS parent_tag
                                  FROM " . $this->tableName . " AS t
                                   INNER JOIN blogtags AS bt ON bt.id=t.parent
                                   ORDER BY t.name ASC");
        $this->db->query($query);
        $items = $this->db->results();

        return $items;
    }

    /**
     * @param string $tag
     * @return bool
     */
    function saveTag($tag = ''){

        $query = sql_placeholder("INSERT IGNORE INTO " . $this->tableName . "
          SET url=?, name=?, enabled=1, created=NOW(), modified=NOW()", $this->translit($tag), $tag);

        if ($this->db->query($query)){
            return true;
        }
        else{
            return false;
        }
    }

    function getRelations($tags, $postId){

        $query = sql_placeholder('SELECT posttag_id FROM relations_postitem_tags WHERE post_id=?',$postId);
        $this->db->query($query);
        $selectedTags = $this->db->results();

        foreach ($selectedTags AS $selectedTag){
            $selectTags[$selectedTag->posttag_id] = $selectedTag->posttag_id;
        }

        foreach ($tags AS $key=>$tag){
            if (in_array($tag->id, (array)$selectTags)){
                $tags[$key]->check = 1;
            }
            else{
                unset ($tags[$key]);
            }
        }

        return $tags;
    }

    function setSeoContent($post)
    {
        $query = sql_placeholder("SELECT * FROM seo_content WHERE section = ?", $this->section);
        $this->db->query($query);
        $seoContent = $this->db->result();

        if (!empty($seoContent->id)){
            $query = sql_placeholder("UPDATE seo_content SET 
              section = ?,
              seo_title =?,
              seo_description =?,
              seo_keywords = ?
              WHERE id = ?
              ", $this->section, $post['seo_title'], $post['seo_description'], $post['seo_keywords'], $seoContent->id);
        }
        else{
            $query = sql_placeholder("INSERT IGNORE INTO seo_content SET 
              section = ?,
              seo_title =?,
              seo_description =?,
              seo_keywords = ?
              ", $this->section, $post['seo_title'], $post['seo_description'], $post['seo_keywords']);
        }

        $this->db->query($query);
    }


    function getSeoContent()
    {
        $query = sql_placeholder("SELECT * FROM seo_content WHERE section = ?", $this->section);
        $this->db->query($query);
        $seoContent = $this->db->result();

        return $seoContent;
    }

}
