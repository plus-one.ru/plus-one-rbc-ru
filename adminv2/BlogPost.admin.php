<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once ('../PresetFormat.class.php');
require_once('BlogWriters.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('UnitMeasures.admin.php');
require_once('Specprojects.admin.php');
require_once('Conferences.admin.php');
//require_once('RemoteTypograf.php');

/**
 * Class BlogPost
 */
class BlogPost extends Widget
{
    /** @var   */
    public $item;
    /** @var   */
    public $errors;
    /** @var string  */
    private $tableName = 'blogposts';
    /** @var string  */
    public $uploaddir = '../files/blogposts/';
    /** @var string  */
    public $large_image_width = "900";
    /** @var string  */
    public $large_image_height = "600";
    /** @var int  */
    public $thumbnailMaxSideLength = "80";

    public $sizeImage_1_1 = array('x' => 1141, 'y' => 505);
    public $sizeImage_1_2 = array('x' => 559, 'y' => 365);
    public $sizeImage_1_3 = array('x' => 363, 'y' => 293);
    public $sizeImage_1_4 = array('x' => 615, 'y' => 410);

    public $preview = false;
    public $articleUrl = "";

    function BlogPost(&$parent = null){
        Widget::Widget($parent);
        $this->prepare();
    }

    function setData($post, $itemId)
    {

        $this->item->id = $itemId;

        $this->item->name = $post['name'];
        $this->item->header = $post['header'];
        $this->item->lead = $post['lead'];
        $this->item->header_rss = $post['header_rss'];
        $this->item->url = $post['url'];

        $this->item->blocks = $post['blocks'];
        $this->item->blocks_blog = $post['blocks_blog'];
        $this->item->blocks_author = $post['blocks_author'];
        $this->item->body = $post['body'];
        $this->item->meta_title = $post['meta_title'];
        $this->item->meta_description = $post['meta_description'];
        $this->item->meta_keywords = $post['meta_keywords'];
        $this->item->type_material = $post['type_material'];
        $this->item->video_on_main = $post['video_on_main'];

        $this->item->seo_title = $post['seo_title'];
        $this->item->seo_description = $post['seo_description'];
        $this->item->seo_keywords = $post['seo_keywords'];


        $this->item->body_color = "";
        if (isset($_POST['body_color'])){
            $this->item->body_color = $_POST['body_color'];
        }

        $this->item->font_color = "";
        if (isset($_POST['font_color'])){
            $this->item->font_color = $_POST['font_color'];
        }

        $this->item->border_color = "";
        if (isset($_POST['border_color'])){
            $this->item->border_color = $_POST['border_color'];
        }

        $this->item->type_video = $post['type_video'];
        if(isset($_POST['state'])) {
            $this->item->state = $_POST['state'];
        }

        $this->item->dzen = 0;
        if (isset($_POST['dzen'])){
            $this->item->dzen = $_POST['dzen'];
        }

        $this->item->writers = 0;
        if (isset($_POST['writers'])){
            $this->item->writers = $_POST['writers'];
        }

        $this->item->tags = 0;
        if (isset($_POST['tags'])){
            $this->item->tags = $_POST['tags'];
        }

        $this->item->post_tag = 0;
        if (isset($_POST['post_tag_main'])){
            $this->item->post_tag = $_POST['post_tag_main'];
        }

        $this->item->year_to_ammount = 0;
        if (isset($_POST['year_to_ammount'])){
            $this->item->year_to_ammount = $_POST['year_to_ammount'];
        }

        $this->item->unit_measure = 0;
        if(isset($_POST['unit_measure'])) {
            $this->item->unit_measure = $_POST['unit_measure'];
        }

        $amountToAccount = str_replace(" ", "", $_POST['amount_to_account']);
        $amountToAccount = str_replace(",", ".", $amountToAccount);

        $this->item->amount_to_account = $amountToAccount;
        $this->item->amount_to_displaying = $_POST['amount_to_displaying'];
        $this->item->signature_to_sum = $_POST['signature_to_sum'];
        $this->item->text_body = $_POST['text_body'];

        $this->item->enabled = 0;
        if(isset($_POST['enabled']) && $_POST['enabled']==1) {
            $this->item->enabled = 1;
        }

        $this->item->useful_city = 0;
        if(isset($_POST['useful_city']) && $_POST['useful_city']==1) {
            $this->item->useful_city = 1;
        }

        $this->item->anounce_new_author = 0;
        if(isset($_POST['anounce_new_author']) && $_POST['anounce_new_author']==1) {
            $this->item->anounce_new_author = 1;
        }

//        $this->item->show_fund = 0;
//        $this->item->shop_article_id = 0;
//        $this->item->fund_resource_id = 0;
//        if(isset($_POST['show_fund_at_blogpost']) && $_POST['show_fund_at_blogpost']==1) {
//            $query = sql_placeholder('SELECT resource_id FROM fund WHERE shop_article_id=? LIMIT 1',$_POST['fund_select']);
//            $this->db->query($query);
//            $res = $this->db->result();
//            $resourceId = $res->resource_id;
//
//            $this->item->show_fund = 1;
//            $this->item->shop_article_id = $_POST['fund_select'];
//            $this->item->fund_resource_id = $resourceId;
//        }

        $this->item->partner = 0;
        $this->item->partner_url  = '';
        if(isset($_POST['partner']) && $_POST['partner']!='') {
            $this->item->partner = $_POST['partner'];
            $this->item->partner_url  = $_POST['partner_url'];
        }

        $this->item->is_partner_material = 0;
        if(!empty($_POST['is_partner_material'])) {
            $this->item->is_partner_material = $_POST['is_partner_material'];
        }
        
        $this->item->show_main_page = 0;
        if(isset($_POST['show_main_page']) && $_POST['show_main_page']==1) {
            $this->item->show_main_page = 1;
        }

        $this->item->picture_day = 0;
        if(isset($_POST['picture_day']) && $_POST['picture_day']==1) {
            $this->item->picture_day = 1;
        }

        $created = $_POST['created'];
        $createdToDb = date('Y-m-d H:i:s', strtotime($created));
        $this->item->created = $createdToDb;

        if (isset($_POST['type_post'])) {
            $this->item->type_post = (int)@$_POST['type_post'];
        }

        $this->item->header_on_page = "";
        if (isset($_POST['header_on_page'])){
            $this->item->header_on_page = $_POST['header_on_page'];
        }

        $this->item->spec_project = 0;
        if (isset($post['spec_project'])){
            $this->item->spec_project = $post['spec_project'];
        }

        $this->item->conference = 0;
        if (isset($post['conference'])){
            $this->item->conference = $post['conference'];
        }

        $this->item->citate_author_name = "";
        if (isset($post['citate_author_name'])){
            $this->item->citate_author_name = $post['citate_author_name'];
        }


        $this->item->format = "";
        if (isset($post['format'])){
            $this->item->format = $post['format'];
        }
        $this->item->font = "";
        if (isset($post['font'])){
            $this->item->font = $post['font'];
        }

        $this->item->type_announce_1_1 = "text";
        if (isset($post['type_announce_1_1'])){
            $this->item->type_announce_1_1 = $post['type_announce_1_1'];
        }

        $this->item->type_announce_1_2 = "text";
        if (isset($post['type_announce_1_2'])){
            $this->item->type_announce_1_2 = $post['type_announce_1_2'];
        }

        $this->item->type_announce_1_3 = "text";
        if (isset($post['type_announce_1_3'])){
            $this->item->type_announce_1_3 = $post['type_announce_1_3'];
        }
    }

    function checkRequiredFields(){
        $validations = array(
            'meta_title' => array(
                'maxLength' => array(
                    'length' => 100,
                    'error' => 'Превышено максимальное количество символов в поле \'Метатег Title\' (80 символов)')
            ),
            'meta_description' => array(
                'maxLength' => array(
                    'length' => 170,
                    'error' => 'Пост не сохранен. Превышено максимальное количество символов в поле \'Метатег Description\' (170 символов)')
            ),
            'meta_keywords' => array(
                'maxLength' => array(
                    'length' => 170,
                    'error' => 'Превышено максимальное количество символов в поле \'Метатег Keywords\' (170 символов)')
            ),
            'url' => array(
                'maxLength' => array(
                    'length' => 255,
                    'error' => 'Превышено максимальное количество символов в поле \'URL\' (255 символов)')
            ),
            'name' => array(
                'maxLength' => array(
                    'length' => 255,
                    'error' => 'Превышено максимальное количество символов в поле \'Заголовок\' (255 символов)')
            ),
            'header_rss' => array(
                'maxLength' => array(
                    'length' => 140,
                    'error' => 'Превышено максимальное количество символов в поле \'Заголовок для социальных сетей\' (140 символов)')
            )
        );
        $this->errors = new stdClass();
        switch (intval($this->item->type_post)) {
            case 1:
            case 2:
            case 3:
                // проверка на обычный пост
                if (empty($this->item->name) || empty($this->item->header_rss) || $this->item->created == '1970-01-01 00:00:00' || $this->item->writers == 0 || empty($this->item->url) || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 4:
                // проверка анонса нового блога
                if ($this->item->writers == 0 || $this->item->created == '1970-01-01 00:00:00' || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 5:
                // проверка на аналитику (данные)
                if ($this->item->writers == 0 || $this->item->created == '1970-01-01 00:00:00' || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 6:
                // проверка на фото дня
                if (empty($this->item->name) || $this->item->writers == 0 || empty($this->item->header_rss) || $this->item->created == '1970-01-01 00:00:00' || empty($this->item->url) || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 7:
                // проверка на факт дня
                if (empty($this->item->name) || $this->item->writers == 0 || $this->item->created == '1970-01-01 00:00:00' || empty($this->item->url) || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 8:
                // проверка на цитата дня
                if (empty($this->item->name) || $this->item->writers == 0 || empty($this->item->header_rss) || $this->item->created == '1970-01-01 00:00:00' || empty($this->item->url) || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 9:
                // проверка анонса спецпроекта
                if ($this->item->spec_project == 0 || $this->item->created == '1970-01-01 00:00:00' || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 10:
                // проверка на цитата дня
                if (empty($this->item->name) || empty($this->item->header_rss) || $this->item->created == '1970-01-01 00:00:00' || empty($this->item->url) || $this->item->tags == 0){
                    $this->errors->requiredFields = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
                break;
            case 12:
                // проверка на бегущая строка
                if (@iconv_strlen(strip_tags(html_entity_decode($this->item->name)), "UTF-8") > 25) {
                    $this->errors->name = 'Максимальная длина "Текста в бегущей строке" = 25 символов';
                }
                if (empty($this->item->name) ){
                    $this->errors->name .= "Заполните поле 'Текст в бегущей строке'";
                }
                if ( empty($this->item->url) ){
                    $this->errors->url = "Укажите URL";
                }
                if ($this->item->tags == 0){
                    $this->errors->tags = "Выберете анонс (тег первого уровня)";
                }
                break;
        }

        foreach ($validations as $field => $rules) {
            foreach ($rules as $rule => $options) {
                if ($rule === "maxLength") {
                    if (@iconv_strlen($this->item->{$field}, "UTF-8") > $options['length']) {
                        $this->errors->{$field} = $options['error'];
                    }
                }
            }
        }
    }

    function prepare(){
        $itemId = intval($this->param('item_id'));

        if ($_POST){
            $this->check_token();

            $action = $_POST['save'];

            // создаем объект из переданных данных
            $this->setData($_POST, $itemId);

            // проверка на обязательные поля
            $this->checkRequiredFields();

            if (count(get_object_vars($this->errors)) == 0) {

//                $typograf = new RemoteTypograf('UTF-8');
//                $typograf->typografFields($this->item, ['name', 'header', 'header_on_page'], ['meta_title', 'header_rss', 'meta_description']);

                if(empty($itemId)) {
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $itemId = $this->add_article();

                    if (is_null($itemId)){
                        $this->errors->error_create = 'Пост не сохранен. Ошибка при создании записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    if (is_null($this->update_article($itemId))){
                        $this->errors->error_update = 'Пост не сохранен. Ошибка при изменении записи.';
                    }
                }

                // партнерские ссылки-посты
                if (!empty($_POST['partnerlink'])){
                    $this->addRelatedPartnersPosts($itemId, $_POST['partnerlink']);
                }

                //
                if (!empty($_POST['post_tags'])){
                    $this->setRelationsPostItemTags($_POST['post_tags'], $itemId);
                }
                elseif(!isset($_POST['post_tags'])){
                    // никаких тегов не выбрано, убираем все старые теги от этой статьи
                    $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $itemId);
                    $this->db->query($query);
                }

                // добавляем данные для аналитики
                if ($_POST['value_to_yeats']){
                    // записываем данные аналитики в бд
                    $this->setWritersData($itemId, $this->item->writers, $_POST);
                }

                // загрузка картинки
                $this->add_fotos($itemId);

                // переданы видео (список видео)
                if (isset($_POST['videoId'])){
                    $postVideoIds = $_POST['videoId'];
                    foreach ($postVideoIds AS $key=>$videoId){
                        // загрузка картинки к видео и заголовка для каждой записи
                        $res = $this->addVideoImages($videoId, $_POST['header_interview'][$videoId], $_POST['video_url'][$videoId], $_POST['type_video'][$videoId]);

                        if (isset($_POST['description_time_division'][$videoId])){

                            foreach ($_POST['description_time_division'][$videoId] AS $fragmentId => $videoFragments){
                                $minute = $_POST['minute'][$videoId][$fragmentId];
                                $secunde = $_POST['secunde'][$videoId][$fragmentId];
                                $resSaveFragment = $this->saveVideoFragment($videoId, $fragmentId, $minute, $secunde, $videoFragments);
                            }
                        }
                    }
                }
                if (count(get_object_vars($this->errors)) == 0) {
                    if (isset($this->config->indexing) && $this->config->indexing) {
                        $postObject = new stdClass();
                        $postObject->id = $itemId;
                        $postObject->name = trim($this->item->name);
                        $postObject->header = trim($this->item->header);
                        $postObject->lead = trim($this->item->lead);
                        $postObject->body = trim($this->item->body);
                        $postObject->enabled = $this->item->enabled;
                        $postObject->modified = $this->item->modified;
                        $postObject->writers = $this->item->writers;
                        $this->addPostSearchIndexSphinx($postObject);
                    }

                    $this->unsetBlocked($itemId);

                    if ($action == 'save') {
                        header("Location: /adminv2/blogpost/");
                        exit();
                    } else if ($action == 'apply'){
                        if (!empty($itemId)) {
                            $redirectURL = "/adminv2/blogpost/edit/{$_REQUEST['type_visual_block']}/{$itemId}/{$_REQUEST['token']}";
                            header("Location: {$redirectURL}" );
                            exit();
                        }
                    } elseif ($action == 'preview'){
                        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created, UNIX_TIMESTAMP(blocked_at) as blocked_ts FROM " . $this->tableName . " WHERE id=?", $itemId);
                        $this->db->query($query);
                        $this->item = $this->db->result();

                        $query = sql_placeholder("SELECT url FROM blogtags WHERE id=?", $this->item->tags);
                        $this->db->query($query);
                        $tagUrl = $this->db->result();

                        $this->articleUrl = "/blog/" . $tagUrl->url . "/" . $this->item->url;

                        $this->preview = true;
                    } else{
                        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created, UNIX_TIMESTAMP(blocked_at) as blocked_ts FROM " . $this->tableName . " WHERE id=?", $itemId);
                        $this->db->query($query);
                        $this->item = $this->db->result();
                    }
                }
            }
            header("X-XSS-Protection: 0");
        }
        elseif (!empty($itemId)){
            $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created, UNIX_TIMESTAMP(blocked_at) as blocked_ts FROM " . $this->tableName . " WHERE id=?", $itemId);
            $this->db->query($query);
            $this->item = $this->db->result();

        }
//        header("Content-Type:text/html; charset=utf-8");
//        echo "<pre>";
//        print_r($this->item);
//        echo "</pre>";
//        die;

        if ($this->errors !== null && count(get_object_vars($this->errors)) !== 0) {
            $this->item->date_created = $_POST['created'];
        }
    }

    function fetch()
    {
        if (empty($this->item)) { $this->item = new stdClass();}
        if(empty($this->item->id)){
            $this->item->id = null;
            $this->title = 'Новая запись в блоге';
            $this->item->type_post = 1;
            $this->item->date_created = date('d.m.Y H') . ":00";
        }
        else{
            $nowDate = (new DateTime('now'));
            $this->title = 'Изменение записи: ' . $this->item->name;
            $this->item->is_blocked = $this->item->blocked_ts >= $nowDate->getTimestamp() ? 'Y' : 'N';
//            var_dump($this->item->blocked_ts);
//            var_dump($this->item->is_blocked);
//            die;

            if ($this->item->is_blocked === 'N') {
                $nowDate->modify('+35 seconds');
                $this->item->blocked_at = $nowDate->format('Y-m-d H:i:s');
                $this->item->blocked_ts = $nowDate->getTimestamp();
                $this->item->blocked_user_id = $this->user->id;
            }
        }

        // тип материала может приетет как гетом, так и постом
        $type_visual_block = $_REQUEST['type_visual_block'];

        $bw = new BlogWriters();
        $writers = $bw->getWriters();

        foreach ($writers as $key=>$writer) {
            if ($writer->useplatforma == 1){
                $writers[$key]->name = "Платформа: " . $writers[$key]->name;
            }
        }

        $bt = new BlogTags();
        $tags = $bt->getTags();

        $um = new UnitMeasures();
        $unitMeasures = $um->getItems();

        // сперпроекты
        $sp = new Specprojects();
        $specialProjects = $sp->getSpecProjects();

        $conf = new Conferences();
        $conferences = $conf->getСonferences();

        $blogTags = $tags;

        foreach ($blogTags AS $k=>$blogTag){

            $query = sql_placeholder("SELECT id, name FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);

            $blogTags[$k]->items = $this->db->results();
        }

        $query = sql_placeholder('SELECT id, name FROM partners');
        $this->db->query($query);
        $partners = $this->db->results();

        $query = sql_placeholder('SELECT id, name FROM typematerial');
        $this->db->query($query);
        $typeMaterials = $this->db->results();

        $query = sql_placeholder('SELECT shop_article_id, name FROM fund');
        $this->db->query($query);
        $funds = $this->db->results();

        // партнерские посты
        $query = sql_placeholder('SELECT * FROM partners_posts_links WHERE post_id=? LIMIT 3',$this->item->id);
        $this->db->query($query);
        $partnersPosts = $this->db->results();

        $query = sql_placeholder('SELECT posttag_id FROM relations_postitem_tags WHERE post_id=?',$this->item->id);
        $this->db->query($query);
        $selectedTags = $this->db->results();
        $selectTags = array();
        if (is_array($selectedTags)) {
            foreach ($selectedTags AS $selectedTag) {
                $selectTags[$selectedTag->posttag_id] = $selectedTag->posttag_id;
            }
        }

        foreach ($tags AS $key=>$tag){
            foreach ($tag->items AS $k=>$v){
                if (in_array($v->id, (array)$selectTags)){
                    $tags[$key]->items[$k]->check = 1;
                }
                else{
                    $tags[$key]->items[$k]->check = 0;
                }
            }

        }

        $query = sql_placeholder("SELECT years, value_to_yeats FROM blogwriters_data WHERE post_id=? AND writer_id=? ORDER BY years DESC LIMIT 7",
            $this->item->id,
            $this->item->writers);
        $this->db->query($query);
        $this->item->analitycData = $this->db->results();

        $postsClass = new BlogPosts();
        $linkedPosts = $postsClass->getLinkedPost($this->item->id);
        $this->smarty->assign('linkedPosts', $linkedPosts);

        $query = sql_placeholder("SELECT * FROM videos WHERE parent_video_post=? AND type_video = 'live' ORDER BY id DESC",
            $this->item->id);
        $this->db->query($query);
        $videos = $this->db->results();

        $query = sql_placeholder("SELECT * FROM videos WHERE parent_video_post=? AND type_video = 'record' ORDER BY id DESC",
            $this->item->id);
        $this->db->query($query);
        $videosRecord = $this->db->results();

        foreach ($videos AS $key=>$video){
            $query = sql_placeholder("SELECT * FROM division_time_videos WHERE parent_video=? ORDER BY id DESC",
                $video->id);
            $this->db->query($query);
            $divisionTime = $this->db->results();
            $videos[$key]->divisionTime = $divisionTime;
        }

        foreach ($videosRecord AS $key=>$videoRec){
            $query = sql_placeholder("SELECT * FROM division_time_videos WHERE parent_video=? ORDER BY id DESC",
                $videoRec->id);
            $this->db->query($query);
            $divisionTime = $this->db->results();
            $videosRecord[$key]->divisionTime = $divisionTime;
        }

        $this->item->videos = $videos;
        $this->item->videosRecord = $videosRecord;

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('writers', $writers);
        $this->smarty->assign('tags', $tags);
        $this->smarty->assign('blogTags', $blogTags);
        $this->smarty->assign('typeMaterials', $typeMaterials);
        $this->smarty->assign('funds', $funds);
        $this->smarty->assign('partners', $partners);
        $this->smarty->assign('partnersPosts', $partnersPosts);
        $this->smarty->assign('unitMeasures', $unitMeasures);
        $this->smarty->assign('specProjects', $specialProjects);
        $this->smarty->assign('conferences', $conferences);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('Errors', $this->errors);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('root_url', $this->root_url);

        $this->smarty->assign('preview', $this->preview);
        $this->smarty->assign('articleUrl', $this->articleUrl);
        $this->smarty->assign('presetFont',  PresetFormat::getPresetFont());

        if ($type_visual_block == 'post'){
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/blogpost.tpl');
        }
        elseif ($type_visual_block == 'data'){
            $this->body = $this->smarty->fetch('blogpost/blogdata.tpl');
        }
        elseif ($type_visual_block == 'newvideo'){
            $this->body = $this->smarty->fetch('blogpost/videopost.tpl');
        }
        elseif ($type_visual_block == 'newblog'){
            $this->body = $this->smarty->fetch('blogpost/newblogpost.tpl');
        }
        elseif ($type_visual_block == 'imageday'){
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/blogimageday.tpl');
        }
        elseif ($type_visual_block == 'factday'){
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/blogfactday.tpl');
        }
        elseif ($type_visual_block == 'citate'){
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/blogcitate.tpl');
        }
        elseif ($type_visual_block == 'specproject'){
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/newSpecProject.tpl');
        }
        elseif ($type_visual_block == 'diypost'){
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/diypost.tpl');
        }
        elseif ($type_visual_block == 'ticker'){
//            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/ticker.tpl');
        }
        else{
            $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats($type_visual_block));
            $this->body = $this->smarty->fetch('blogpost/blogpost.tpl');
        }
    }

    /**
     * @return int|null
     */
    function add_article(){

        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);

        if ($this->db->query($query)){
            $itemId = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $itemId);

            $this->db->query($query);

            return $itemId;
        }
        else{
            $this->errors->error_insert_trace = $this->db->error_msg;
            return null;
        }
    }

    /**
     * @param $itemId
     * @return null
     */
    function update_article($itemId){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $itemId);

//        header("Content-Type: html/text; charset=utf-8");
//        echo "<pre>";
//        var_dump($query);
//        echo "</pre>";
//        die;

        if ($this->db->query($query)){
            return $itemId;
        }
        else {
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * сохранение связей между постом и тегами-разделами
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
            foreach ($tags AS $tag){
                $query = sql_placeholder('INSERT INTO relations_post_tags SET tag_id=?, post_id=?', $tag, $itemId);
                $this->db->query($query);
            }
        }
        elseif (empty($tags) && $itemId!=0){
            // если к нам прилетел пустой массив с тегами первого уровня (рубриками)
            // это значит что пост должен быть удален отовсюду.
            // и отображаться только в блоге автора материала
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
        }
    }

    function setWritersData($postId = 0, $writerId = 0, $data = array()){
        if ($postId != 0 && $writerId != 0 && !empty($data)){
            if (isset($data['value_to_yeats'])){

                $query = sql_placeholder('DELETE FROM blogwriters_data WHERE post_id=? AND writer_id=?', $postId, $writerId);
                $this->db->query($query);

                foreach ($data['value_to_yeats'] AS $key=>$value){
                    if (!empty($value)){
                        $year = $data['years'][$key];

                        $value = str_replace(",", ".", $value);

                        $query = sql_placeholder('INSERT INTO blogwriters_data SET post_id=?, writer_id=?, years=?, value_to_yeats=?',
                            $postId, $writerId, $year, $value);
                        $this->db->query($query);
                    }
                }
            }
            return false;
        }
        return false;
    }

    /**
     * сохранение связей между постом и тегами самой статьи (облаком тегов)
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsPostItemTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $itemId);
            $this->db->query($query);

            if (is_array($tags)){
                foreach ($tags AS $posttagId){
                    $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $posttagId, $itemId);
                    $this->db->query($query);
                }
            }
            elseif($tags != 0){
                $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $tags, $itemId);
                $this->db->query($query);
            }
        }
    }

    /**
     * добавление партнерского поста
     *
     * @param int $postId
     * @param array $partnerPosts
     * @return null
     */
    function addRelatedPartnersPosts($postId = 0, $partnerPosts = array()){

        if ($postId != 0 && !empty($partnerPosts)){
            foreach ($partnerPosts AS $key=>$post){
                // проверим, нет ли такого партнерского материала, у этой статьи с такой же ссылкой
                $query = sql_placeholder('SELECT id FROM partners_posts_links WHERE post_id=? AND link=?',
                    $postId, $post['link']);
                $this->db->query($query);
                $availablePartnerPost = $this->db->result();

                if (!$availablePartnerPost) {
                    $post['post_id'] = $postId;
                    if(isset($_FILES)){
                        $post['image'] = '';
                    }

                    $query = sql_placeholder('INSERT INTO partners_posts_links SET ?%', $post);
                    $this->db->query($query);
                    $partnerPostId = $this->db->insert_id();

                    $this->addPartnerPostPhoto($partnerPostId, $key);
                }
                else{
                    $query = sql_placeholder('UPDATE partners_posts_links SET ?% WHERE id=?', $post, $availablePartnerPost->id);
                    $this->db->query($query);

                    $this->addPartnerPostPhoto($availablePartnerPost->id, $key);
                }

            }
        }
        else{
            return null;
        }
    }

    function resize_image($file, $widthFinal, $heightFinal) {
        $imgInfo = getimagesize($file);
        $widthOrigin = $imgInfo[0];
        $heightOrigin = $imgInfo[1];
        switch ($imgInfo['mime']) {
            case 'image/png':
                $src = imagecreatefrompng($file);
                break;
            case 'image/gif':
                $src = imagecreatefromgif($file);
                break;
            default:
                $src = imagecreatefromjpeg($file);
        }
        $dst = imagecreatetruecolor($widthFinal, $heightFinal);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $widthFinal, $heightFinal, $widthOrigin, $heightOrigin);
        return $dst;
    }
    function getThumbnail($sourceImgPath)
    {
        list($widthOrigin, $heightOrigin) = getimagesize($sourceImgPath);
        if ($widthOrigin >= $heightOrigin) {
            $k = $widthOrigin / $heightOrigin;
            $width = $this->thumbnailMaxSideLength;
            $height = $this->thumbnailMaxSideLength / $k;
        } else {
            $k = $heightOrigin / $widthOrigin;
            $height = $this->thumbnailMaxSideLength;
            $width = $this->thumbnailMaxSideLength / $k;
        }
        $imgSrc = $this->resize_image($sourceImgPath, $width, $height);
        return $imgSrc;
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        $image_1_1 = $itemId . '-1_1.jpg';
        $image_1_1_c = $itemId . '-1_1_c.jpg';
        $image_1_2 = $itemId . '-1_2.jpg';
        $image_1_3 = $itemId . '-1_3.jpg';
        $image_1_4 = $itemId . '-1_4.jpg';
        $image_rss = $itemId . '-rss.jpg';
        $image_citate_author = $itemId . '-cauthor.jpg';
        $thumbnail = $itemId . '-thumbnail.jpg';

        if(isset($_FILES['citate_author_photo']) && !empty($_FILES['citate_author_photo']['tmp_name'])){
            if (!move_uploaded_file($_FILES['citate_author_photo']['tmp_name'], $this->uploaddir.$image_citate_author)){
                echo $this->uploaddir.$image_citate_author;
                $this->errors->citate_author_photo = 'Ошибка при загрузке файла фото автора цитаты';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET citate_author_photo='$image_citate_author' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_1']) && !empty($_FILES['image_1_1']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->uploaddir.$image_1_1)){
                echo $this->uploaddir.$image_1_1;
                $this->errors->image_1_1 = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1='$image_1_1' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_1_c']) && !empty($_FILES['image_1_1_c']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1_c']['tmp_name'], $this->uploaddir.$image_1_1_c)){
                $this->errors->image_1_1_c = 'Ошибка при загрузке файла 1/1 цитата';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1_c='$image_1_1_c' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_2']) && !empty($_FILES['image_1_2']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->uploaddir.$image_1_2)){
                $this->errors->image_1_2 = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_2='$image_1_2' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_3']) && !empty($_FILES['image_1_3']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->uploaddir.$image_1_3)){
                $this->errors->image_1_3 = 'Ошибка при загрузке файла 1/3';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_3='$image_1_3' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_4']) && !empty($_FILES['image_1_4']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_4']['tmp_name'], $this->uploaddir.$image_1_4)){
                $this->errors->image_1_4 = 'Ошибка при загрузке файла 1/4';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_4='$image_1_4' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_rss']) && !empty($_FILES['image_rss']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_rss']['tmp_name'], $this->uploaddir.$image_rss)){
                $this->errors->image_rss = 'Ошибка при загрузке файла RSS';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_rss='$image_rss' WHERE id={$itemId}");
                $imageRssExists = true;
                $result = true;
            }
        }

        if (!empty($imageRssExists)) {
            $thumbnailSrc = $this->getThumbnail($this->uploaddir . $image_rss);
            if (!imagejpeg($thumbnailSrc, $this->uploaddir . $thumbnail)) {
                $this->error_msg = 'Ошибка при сохранении файла превью';
            } else {
                $this->db->query("UPDATE {$this->tableName} SET thumbnail='$thumbnail' WHERE id={$itemId}");
                return true;
            }
        }

        if (isset($this->item->type_post) && $this->item->type_post == 8) {
            $thumbnailSrc = $this->getThumbnail($this->uploaddir . $image_1_1);
            if (!imagejpeg($thumbnailSrc, $this->uploaddir . $thumbnail)) {
                $this->error_msg = 'Ошибка при сохранении файла превью';
            } else {
                $this->db->query("UPDATE {$this->tableName} SET thumbnail='$thumbnail' WHERE id={$itemId}");
                return true;
            }
        }

        return $result;
    }

    /**
     * добавление фото к партнерскому посту
     *
     * @param $partnerPostId
     * @param $key
     * @return bool
     */
    function addPartnerPostPhoto($partnerPostId, $key){
        $result = false;

        $imageName = 'pi_' . $partnerPostId . '-1_3.jpg';

        if ($key == 1){
            if(isset($_FILES['partnerimage1']) && !empty($_FILES['partnerimage1']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage1']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->errors->partnerimage1 = 'Ошибка при загрузке файла 1';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 2){
            if(isset($_FILES['partnerimage2']) && !empty($_FILES['partnerimage2']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage2']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->errors->partnerimage2 = 'Ошибка при загрузке файла 2';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 3){
            if(isset($_FILES['partnerimage3']) && !empty($_FILES['partnerimage3']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage3']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->errors->partnerimage3 = 'Ошибка при загрузке файла 3';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }
        return $result;
    }

    function addVideoImages($videoId, $headerText, $urlVideo, $typeVideo){
        $result = false;

        $imageName = 'videoImage_' . $videoId . '.jpg';

        if (isset($_FILES['imageVideo']) && !empty($_FILES['imageVideo']['tmp_name'][$videoId])) {


            if (!move_uploaded_file($_FILES['imageVideo']['tmp_name'][$videoId], $this->uploaddir . $imageName)) {
                $this->errors->imageVideo = 'Ошибка при загрузке файла 1';
            } else {
                if (!empty($urlVideo) && !empty($headerText)){
                    $query = "UPDATE videos SET image='{$imageName}', video_url='{$urlVideo}', name='{$headerText}', type_video='{$typeVideo}' WHERE id={$videoId}";
                    $this->db->query($query);
                    $result = true;
                }
            }
        }
        else{
            if (!empty($urlVideo) && !empty($headerText)){
                $query = "UPDATE videos SET image='{$imageName}', video_url='{$urlVideo}', name='{$headerText}', type_video='{$typeVideo}' WHERE id={$videoId}";
                $this->db->query($query);
                $result = true;
            }
        }

        return $result;
    }

    function saveVideoFragment($videoId, $fragmentId, $min, $sec, $name){

        $result = false;

        $query = "UPDATE division_time_videos SET parent_video='{$videoId}', minute='{$min}', secunde='{$sec}', name='{$name}' WHERE id={$fragmentId}";
        if ($this->db->query($query)){
            $result = true;
        }

        return $result;

    }

    public function addPostSearchIndexSphinx($content)
    {
        if (isset($this->config->indexing) && $this->config->indexing) {
            require_once './../Sphinx.class.php';
            $sphinxConfig = $this->config->sphinx;
            $indexDB = new Sphinx($sphinxConfig['name'], $sphinxConfig['host'], $sphinxConfig['user'], $sphinxConfig['password'], $sphinxConfig['port']);
            $tablePrefix = @$sphinxConfig['table_prefix'];
            $indexDB->connect();
            $now = new DateTime();
            $query = sql_placeholder("REPLACE INTO {$tablePrefix}postsRT (id, name, header, lead, body, writers, enabled, date_add, model) VALUES (?,?,?,?,?,?,?,?,1)",
                (int)$content->id, $content->name, $content->header, $content->lead, $content->body, (int)$content->writers, (int)$content->enabled, (int)$now->getTimestamp());

            $indexDB->query($query);
            $indexDB->disconnect();
        }

        return true;
    }

    public function setBlocked($postId = null, $ts = true) {
        $nowDate = new DateTime('now');
        $nowDate->modify('+35 seconds');
        $query = sql_placeholder("UPDATE {$this->tableName} SET blocked_at = ?, blocked_user_id = ? WHERE id = ?"
            , $nowDate->format('Y-m-d H:i:s')
            , $this->user->id
            , $postId
        );
        $this->db->query($query);
        $res = $this->db->result();

        return is_null($res) ? $nowDate->getTimestamp() : false ;
    }

    public function unsetBlocked($postId = null) {
        $query = sql_placeholder("UPDATE {$this->tableName} SET blocked_at = NULL, blocked_user_id = NULL WHERE id = ? AND blocked_user_id = ?", $postId, $this->user->id);
        $this->db->query($query);
        return true;
    }
}
