<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('PagesNavigation.admin.php');
require_once('BlogWriters.admin.php');



/**
 * Class BlogPosts
 */
class BlogPosts extends Widget{
    var $pages_navigation;
    var $items_per_page = 1;
    var $category;
    var $uploaddir = '../files/blogposts/'; # Папка для хранения картинок (default)
    private $tableName = 'blogposts';
  
    function BlogPosts(&$parent){
        parent::Widget($parent);
//        $this->add_param('page');
//        $this->add_param('category');
//        if (!empty($this->settings->article_path_to_uploaded_images)){
//            $this->uploaddir = '../' . $this->settings->article_path_to_uploaded_images;
//        }
        $this->prepare();
    }

    function prepare(){

        if (isset($_GET['act']) && $_GET['act'] == 'delete_post' && (isset($_POST['items']) || isset($_GET['item_id']))) {

            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);

            header("Location: /adminv2/blogpost/");
        }

        if (isset($_GET['enable_post'])) {
            $this->check_token();

            $this->setEnable($_GET['enable_post'], $this->tableName);

            header("Location: /adminv2/blogpost/");
        }
    }

    function fetch(){
        $this->title = 'Записи в блогах';

        $bw = new BlogWriters();
        $blogWriters = $bw->getWriters();

        $writerId = intval($this->param('writer_id'));

        if(!empty($writerId)){
            $where = ' AND writers=' . $writerId;
        }

        $query = sql_placeholder("SELECT *,
                              DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(created, '%H:%i') AS time_created,
                              DATE_FORMAT(modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(modified, '%H:%i') AS time_modified 
                              FROM " . $this->tableName . "
                              WHERE 1 $where
                              ORDER BY order_num ASC");
        $this->db->query($query);
        $items = $this->db->results();

        foreach($items as $key=>$item){

            switch ($item->type_post) {
                case 1:
                case 2:
                case 3:
                    $items[$key]->typePost = "post";
                    break;
                case 4:
                    $items[$key]->typePost = "newblog";
                    break;
                case 5:
                    $items[$key]->typePost = "data";
                    break;
                case 6:
                    $items[$key]->typePost = "newvideo";
                    break;
            }

            $items[$key]->edit_get = $this->form_get(array('page'=>'post', 'item_id'=>$item->id, 'token'=>$this->token, 'section'=>'BlogPost'));
            $items[$key]->delete_get = $this->form_get(array('act'=>'delete_post','item_id'=>$item->id, 'writer_id'=>$writerId, 'token'=>$this->token));
            $items[$key]->enable_get = $this->form_get(array('enable_post'=>$item->id, 'token'=>$this->token));

            $query = "SELECT t.name, t.url FROM blogtags AS t
                          WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $items[$key]->tag = $this->db->result();


            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $items[$key]->postTags = $this->db->results();

//            $query = "SELECT t.name, t.url FROM post_tags AS t
//                          WHERE t.id=" . $item->post_tag;
//            $this->db->query($query);
//            $items[$key]->postTags = $this->db->result();


            $query = "SELECT name FROM partners WHERE id=" . $item->partner . " LIMIT 1";
            $this->db->query($query);
            $items[$key]->partnerRecord = $this->db->result();

            $query = "SELECT name FROM blogwriters WHERE id=" . $item->writers . " LIMIT 1";
            $this->db->query($query);
            $items[$key]->writer = $this->db->result();

            $query = "SELECT id, name FROM typematerial WHERE id=" . $item->type_material . " LIMIT 1";
            $this->db->query($query);
            $items[$key]->typematerial = $this->db->result();

        }

        $this->smarty->assign('blogWriters', $blogWriters);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('current_writer', $writerId);
        $this->smarty->assign('token', $this->token);
        $this->body = $this->smarty->fetch('blogposts.tpl');
    }

    /**
     * @param null $writerId
     * @return array|bool|int
     */
    function getPostsShortList($writerId = null){
        if (!is_null($writerId)){
            $query = sql_placeholder("SELECT *
                              FROM " . $this->tableName . "
                              WHERE  writers=" . $writerId . "
                              ORDER BY order_num ASC");
            $this->db->query($query);
            $items = $this->db->results();
            return $items;
        }
        else{
            return false;
        }
    }

    /**
     * @param null $postId
     * @param null $parentId
     * @return bool
     */
    function setLinkedPost($postId = null, $parentId = null){
        if (!is_null($postId) && !is_null($parentId)){
            $query = sql_placeholder("INSERT IGNORE INTO related_posts SET post_id=?, parent_id=?",$postId, $parentId);
            $this->db->query($query);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * @param null $parentId
     * @return array|bool|int
     */
    function getLinkedPost($parentId = null){
        if (!is_null($parentId)){
            $query = sql_placeholder("SELECT p.id, p.name FROM blogposts AS p
              INNER JOIN related_posts AS rp ON rp.post_id=p.id
              WHERE rp.parent_id=?",$parentId
            );
            $this->db->query($query);
            return $this->db->results();
        }
        else{
            return false;
        }
    }

    /**
     * @param null $product_id
     * @param null $parent_id
     * @return bool
     */
    function setUnLinkedPost($postId = null, $parentId = null){
        if (!is_null($postId) && !is_null($parentId)){
            $query = sql_placeholder("DELETE FROM related_posts WHERE post_id=? AND parent_id=?",$postId, $parentId);
            $this->db->query($query);
            $result = true;
        }
        else{
            $result = false;
        }
        return $result;
    }


    /**
     * Резервирование записи в БД под новое видео.
     * необходимое чтобы знать ИД обрабатываемой записи
     * в последующем, к этому видео добавяться еще записи, динамически,
     * и вот там нужен будет этот Id
     *
     * @param integer $parentVideoId Id родительской записи (поста)
     * @return int Id новой записи в которую запишется добавляемое видео и дочерние элементы к нему
     */
    function getNewVideoId($parentVideoId){
        $newVideoId = 0;

        if (!empty($parentVideoId) && $parentVideoId != 0){
            $query = sql_placeholder('INSERT INTO videos SET parent_video_post = ?, created = now(), modified = now()',
                intval($parentVideoId));
            $this->db->query($query);
            $newVideoId = $this->db->insert_id();
        }

        return $newVideoId;
    }

    function enableVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('UPDATE videos SET enabled=1-enabled WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    function deleteVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('DELETE FROM videos WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Резервирование записи в БД под новое видео.
     * необходимое чтобы знать ИД обрабатываемой записи
     * в последующем, к этому видео добавяться еще записи, динамически,
     * и вот там нужен будет этот Id
     *
     * @param integer $parentVideoId Id родительской записи (поста)
     * @return int Id новой записи в которую запишется добавляемое видео и дочерние элементы к нему
     */
    function getNewDivisionVideoId($parentVideoId){
        $newVideoId = 0;

        if (!empty($parentVideoId) && $parentVideoId != 0){
            $query = sql_placeholder('INSERT INTO division_time_videos SET parent_video = ?, created = now(), modified = now()',
                intval($parentVideoId));
            $this->db->query($query);
            $newVideoId = $this->db->insert_id();
        }

        return $newVideoId;
    }

    /**
     * активация/дезактивация видеофрагмента
     *
     * @param $videoId
     * @return bool
     */
    function enablePartVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('UPDATE division_time_videos SET enabled=1-enabled WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    /**
     * удаление видеофрагмента
     *
     * @param $videoId
     * @return bool
     */
    function deletePartVideo($videoId, $parentVideoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('DELETE FROM division_time_videos WHERE id=?', intval($videoId));
            $this->db->query($query);

            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM division_time_videos WHERE parent_video=?", intval($parentVideoId));
            $this->db->query($query);
            $result = $this->db->result();
        }
        return $result;
    }

    /**
     * Сохранение сортировки видео
     *
     * @param array $values
     * @param string $table
     * @return bool
     */
    function saveSort($values = array(), $table = ''){

        if ($table != "" && !empty($values)){
            $order_num = 1;
            foreach ($values as $value_id){
                $value_id = str_replace("row_", "", $value_id);
                $query = sql_placeholder("UPDATE $table SET order_num=? WHERE id=?", $order_num, $value_id);
                if (!$this->db->query($query)){
                    return false;
                }
                ++$order_num;
            }
            return true;
        }
        else{
            return false;
        }

    }

}
