<?PHP
require_once('Widget.admin.php');
require_once('PagesNavigation.admin.php');
require_once('../placeholder.php');

class Users extends Widget{
  	var $pages_navigation;
	var $items_per_page = 20;

    function Users(&$parent){
        parent::Widget($parent);
        $this->add_param('page');
        $this->add_param('group');
        $this->add_param('keyword');

        $this->pages_navigation = new PagesNavigation($this);
        $this->prepare();
    }

    function prepare(){

        if ($_GET['xls']==1){
            $charset = 'cp1251';
            setlocale ( LC_ALL, 'ru_RU.'.$charset);
            $this->db->query('SET NAMES '.$charset);

            $users = $this->getUsers(1);

            $ctype="application/vnd.ms-excel";
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-Type: $ctype");
            header('Content-disposition: attachment; filename="price.xls"');
            header("Content-Transfer-Encoding: binary");
            $this->print_xls_header();

            print '<body link="blue" vlink="purple">'."\n";
            print '<table x:str border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;table-layout:fixed;">'."\n";

            /* заголовки столбцов */
            print "<tr>";
                print "<td  class='xl24'>".iconv("utf8", "windows-1251", "Зарегистрирован")."</td>";
                print "<td  class='xl24'>".iconv("utf8", "windows-1251", "Последний визит")."</td>";
                print "<td  class='xl24'>".iconv("utf8", "windows-1251", "Имя")."</td>";
                print "<td  class='xl24'>".iconv("utf8", "windows-1251", "Email")."</td>";
                print "<td  class='xl24'>".iconv("utf8", "windows-1251", "заказов")."</td>";
            print "</tr>"."\n";

            foreach ($users as $user)
            {
                print "<tr>";
                    print "<td  class='xl24'>{$user->date_created} {$user->time_created}</td>";
                    print "<td  class='xl24'>{$user->date_modified} {$user->time_modified}</td>";
                    print "<td  class='xl24'>{$user->name}</td>";
                    print "<td  class='xl24'>{$user->email}</td>";
                    print "<td  class='xl24'>{$user->orders_num}</td>";
                print "</tr>"."\n";
            }
            print '</table></body></html>';
            exit();
        }

        if(isset($_GET['delete_user'])){
            $user_id = intval($_GET['delete_user']);

            $query = sql_placeholder("SELECT count(*) as count FROM orders WHERE orders.user_id = ?  LIMIT 1", $user_id);
            $this->db->query($query);
            $user_orders_num = $this->db->result();

            if($user_orders_num->count){
                $this->error_msg = 'Нельзя удалить пользователя, имеющего заказы';
            }
            else{
                $query = sql_placeholder("DELETE FROM users WHERE user_id =?", $user_id);
                $this->db->query($query);
                $get = $this->form_get(array());
                header("Location: ".$this->root_admin_url."/users/");
            }
        }

        if(isset($_GET['set_enabled'])){
            $id = $_GET['set_enabled'];
            $query = sql_placeholder('UPDATE users SET enabled=1-enabled WHERE user_id=?',$id);
            $this->db->query($query );
            header("Location: ".$this->root_admin_url."/users/");
        }
    }

    function fetch(){
        $this->title = $this->lang->USERS;

        $keyword = mysql_real_escape_string($this->param('keyword'));
        $group_id = $this->param('group');
        if (empty($group_id)){
            $group_id = 1;
        }

        $filter = '1';
        if(!empty($keyword))
            $filter .= " AND (u.name LIKE '%$keyword%' OR u.email LIKE '%$keyword%') ";

        if(!empty($group_id))
            $filter .= " AND u.group_id = '$group_id' ";

        $users = $this->getUsers($filter);

        foreach($users as $key=>$user){
            $users[$key]->enable_get = $this->form_get(array('enable'=>$user->user_id, 'token'=>$this->token));
            $users[$key]->delete_get = $this->form_get(array('delete_user'=>$user->user_id,'token'=>$this->token));
        }

        $query = 'SELECT * FROM groups';
        $this->db->query($query);
        $groups = $this->db->results();


        $this->smarty->assign('Items', $users);
        $this->smarty->assign('Groups', $groups);
        $this->smarty->assign('Error', $this->error_msg);
        $this->body = $this->smarty->fetch('users.tpl');
    }

    function getUsers($filter){
        $query = "SELECT u.user_id,  u.enabled, u.email, u.firstname, u.lastname, u.subscribe_news, g.name as group_name,
                  COUNT(o.order_id) as orders_num,
                  DATE_FORMAT(registered, '%d.%m.%Y') AS date_created,
                  DATE_FORMAT(registered, '%H:%i') AS time_created,
                  DATE_FORMAT(lastlogin, '%d.%m.%Y') AS date_modified,
                  DATE_FORMAT(lastlogin, '%H:%i') AS time_modified
                  FROM users AS u
                    LEFT JOIN groups AS g ON g.group_id = u.group_id
                    LEFT JOIN orders AS o ON o.user_id = u.user_id
                  WHERE $filter
                    GROUP BY u.user_id
                    ORDER BY o.order_id DESC";
        if ($this->db->query($query)){
            $users = $this->db->results();
        }
        else{
            $users = null;
        }
        return $users;
    }

    function print_xls_header()
    {
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<meta http-equiv="Content-Language" content="ru" />
<meta name="ProgId" content="Excel.Sheet">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:LastAuthor>Simpla CMS</o:LastAuthor>
  <o:LastSaved>2005-01-02T07:46:23Z</o:LastSaved>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\ ";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	white-space:normal;}
	
.xl25
	{mso-style-parent:style0;
	white-space:normal;
	background: #C0C0C0;
	}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
	<x:Name>{/literal}{$Settings->site_name}{literal}</x:Name>
	<x:WorksheetOptions>
	 <x:Selected/>
	 <x:ProtectContents>False</x:ProtectContents>
	 <x:ProtectObjects>False</x:ProtectObjects>
	 <x:ProtectScenarios>False</x:ProtectScenarios>
	</x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>10005</x:WindowHeight>
  <x:WindowWidth>10005</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>135</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>
<?php    


  }
  
}

