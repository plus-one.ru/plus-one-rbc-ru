<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

$blocks = array(
    array('name' => '1/1', 'id' => '1_1'),
    array('name' => '1/2', 'id' => '1_2'),
    array('name' => '1/3', 'id' => '1_3'),
    array('name' => 'Баннер', 'id' => 'banner'),
    array('name' => 'Бегущая строка', 'id' => 'ticker')
);

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($blocks);
?>