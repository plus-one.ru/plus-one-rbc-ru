<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('BlogPosts.admin.php');
$postObject = new BlogPosts();
session_write_close();

if (!empty($HTTP_RAW_POST_DATA)){
    $sendData = json_decode($HTTP_RAW_POST_DATA);
    $result = $postObject->setMainPage($sendData);
}
else{
    $result = $postObject->getMainPage();
}





header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);
?>