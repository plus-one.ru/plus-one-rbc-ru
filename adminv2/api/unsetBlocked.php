<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('BlogPost.admin.php');
$postObject = new BlogPost($widget);
session_write_close();

$postId = $_REQUEST['postId'];
$result = $postObject->unsetBlocked($postId);

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
echo json_encode(array('ok' => true, 'ts' => $result));
