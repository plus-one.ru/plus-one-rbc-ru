<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('PagesNavigation.admin.php');
require_once('BlogWriters.admin.php');
require_once ('../PresetFormat.class.php');
require_once ('../Helper.class.php');


/**
 * Class BlogPosts
 */
class BlogPosts extends Widget{
    var $pages_navigation;
    var $items_per_page = 50;
    var $category;
    var $uploaddir = '../files/blogposts/'; # Папка для хранения картинок (default)
    private $tableName = 'blogposts';

    function BlogPosts(&$parent = null){
        parent::Widget($parent);
        $this->prepare();
    }

    function prepare(){

        if (isset($_GET['act']) && $_GET['act'] == 'delete_post' && (isset($_POST['items']) || isset($_GET['item_id']))) {

            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);

            header("Location: /adminv2/blogpost/");
        }

        if (isset($_GET['enable_post'])) {
            $this->check_token();

            $this->setEnable($_GET['enable_post'], $this->tableName);

            header("Location: /adminv2/blogpost/");
        }
    }

    function fetch(){
        $this->title = 'Записи в блогах';

        $link = "index.php?section=BlogPosts";

        $where = "";

        $writerId = intval($this->param('writer_id'));
        $postsIds = false;
        if ($search = $this->param('search')){

            if (isset($this->config->indexing) && $this->config->indexing) {
                require_once './../Sphinx.class.php';
                $indexDBConfig = $this->config->sphinx;
                $sphinxDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
                $sphinxDB->connect();
                $tablePrefix = @$indexDBConfig['table_prefix'];
                $sphinxDB->query(sql_pholder("SELECT id, WEIGHT() as weight FROM {$tablePrefix}posts WHERE MATCH(?) LIMIT 5000 OPTION field_weights=(name=100, header=60, lead=10, body=5) ", $search));
                $postsIds = $sphinxDB->results();
                $sphinxDB->disconnect();
                $postsIdsStr = join(',', array_merge([0],array_map(function ($item ){ return $item->id;}, $postsIds)));
                $where .= " AND (id IN ({$postsIdsStr})) ";
            } else {
                $where .= " AND (name LIKE '%$search%' OR header LIKE '%$search%' OR lead LIKE '%$search%' OR text_body LIKE '%$search%')";
            }
            $link .= "&search=" . $search;
        }
        if ($type_post = $this->param('type_post')) {
            $where .= " AND type_post = " . (int)$type_post;
            $link .= "&type_post=" . $type_post;
        }
        if ($filterWriters = $this->param('author')){
            $where .= " AND writers = " . (int)$filterWriters;
            $link .= "&author=" . $filterWriters;
        }


        if(!empty($writerId)){
            $where = ' AND writers=' . $writerId;
        }

        // общее кол-во записей
        $totalRecordCount = $this->getTotalRows($where);

        if ($this->param('page')){
            $page = intval($this->param('page'));
        }
        else {
            $page = 1;
        }

        $start = ($page * $this->items_per_page) - $this->items_per_page;

        $totalPages = ceil($totalRecordCount / $this->items_per_page);

        if ($page > $totalPages){
            $page = $totalPages;
        }

        $paginationStructure = new stdClass();

        $paginationStructure->startPageLink = null;
        $paginationStructure->prevPageLink = null;
        $paginationStructure->nextPageLink = null;
        $paginationStructure->lastPageLink = null;

        $paginationStructure->page3left = null;
        $paginationStructure->page2left = null;
        $paginationStructure->page1left = null;

        $paginationStructure->page3right = null;
        $paginationStructure->page2right = null;
        $paginationStructure->page1right = null;


        if ($page != 1) {
            $paginationStructure->startPageLink = $link . "&page=1";
        }
        // Проверяем нужны ли стрелки вперед
        if ($page != $totalPages) {
            $paginationStructure->lastPageLink = $link . "&page=" . $totalPages;
        }

        // Находим три ближайшие станицы с обоих краев, если они есть
        if($page - 3 > 0) {
            $paginationStructure->page3left = $link . "&page=" . ($page - 3);
        }

        if($page - 2 > 0) {
            $paginationStructure->page2left = $link . "&page=" . ($page - 2);
        }
        if($page - 1 > 0) {
            $paginationStructure->page1left = $link . "&page=" . ($page - 1);
        }

        if($page + 3 < $totalPages) {
            $paginationStructure->page3right = $link . "&page=" . ($page + 3);
        }
        if($page + 2 < $totalPages) {
            $paginationStructure->page2right = $link . "&page=" . ($page + 2);
        }
        if($page + 1 < $totalPages) {
            $paginationStructure->page1right = $link . "&page=" . ($page + 1);
        }


        $this->smarty->assign('paginationStructure', $paginationStructure);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('totalRecordCount', $totalRecordCount);

        $items = $this->getPostDataJson($where, $start, (bool)$postsIds);

        if ($postsIds) {
            $result = array();
            foreach ($postsIds as $p) {$result[$p->id] = false;}
            foreach ($items as $i) {
                $result[$i->id] = $i;
            }
            $items = array_filter($result, function ($r) {return $r;});
        }

        $this->db->query('SELECT * FROM blogwriters');
        $writers = $this->db->results();

        $this->smarty->assign('writers', $writers);
        $this->smarty->assign('search', $search);
        $this->smarty->assign('filterWriters', $filterWriters);
        $this->smarty->assign('type_post', $type_post);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('current_writer', $writerId);
        $this->smarty->assign('token', $this->token);
        $this->body = $this->smarty->fetch('blogposts.tpl');
    }

    function getTotalRows($where)
    {
        $query = sql_placeholder("SELECT COUNT(*) AS recordCount FROM {$this->tableName} WHERE 1 {$where}");
        $this->db->query($query);
        $count = $this->db->result();

        return $count->recordCount;
    }

    function getPostDataJson($where = '', $start, $noLimit = false){


        $LIMIT = $noLimit ? "" : "LIMIT {$start}, {$this->items_per_page}";
        $query = sql_placeholder("SELECT id, url, image_1_1, image_1_2, created, modified, show_main_page, name, header, meta_title,
                              type_post, tags, partner, writers, type_material, enabled, post_tag, spec_project, text_body, thumbnail,
                              DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(created, '%H:%i') AS time_created,
                              DATE_FORMAT(modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(modified, '%H:%i') AS time_modified
                              FROM " . $this->tableName . "
                              WHERE 1 $where
                              ORDER BY created DESC, order_num ASC
                              {$LIMIT}");
        $this->db->query($query);
        $items = $this->db->results();

        $mainPagePostIDs = $this->getMainPagePostIDs();

        foreach ($items as &$item_1) {
            if (in_array($item_1->id, $mainPagePostIDs)) {
                $item_1->show_main_page = 1;
            }
        }

        $items = $this->makeAddInfoToPostData($items);

        return $items;
    }


    function makeAddInfoToPostData($items){
        $nowDate = new DateTime();
        foreach($items as $key=>$item){
            try {
                $createdDate = new DateTime($item->created);
                $item->deferred = $createdDate > $nowDate;
            } catch (Exception $e) {
                $item->deferred = false;
            }
            $enabledLink = '<a href="blogpost/enable/' . $item->id . '/' . $this->token . '">';
            if ($item->enabled == 1){
                $enabledLink .= '<i class="fa fa-toggle-on fa-1x"></i>';
            }
            else{
                $enabledLink .= '<i class="fa fa-toggle-off fa-1x"></i>';
            }
            $enabledLink .= '</a>';
            $items[$key]->enabled = $enabledLink;

            $query = "SELECT t.name, t.url FROM blogtags AS t
                          WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $tag = $this->db->result();
            $items[$key]->tag = $tag;

            $items[$key]->linkToPost = '<a href="../blog/' . $tag->url . '/' . $item->url . '?mode=preview" target="_blank"><i class="fa fa-eye"></i></a>';

            $showMainPage = "<em>На главной: </em>";
            if ($item->show_main_page == 1){
                $showMainPage .= '<i class="glyphicon glyphicon-check text-success"></i>';
            }
            else{
                $showMainPage .= '<i class="glyphicon glyphicon-unchecked text-muted"></i>';
            }

            $items[$key]->tagStr = '<span class="text-bolder">'.$tag->name . "</span><br/>" . $showMainPage. '';


            // Тег для главной страницы
            $query = "SELECT name FROM post_tags WHERE id=" . $item->post_tag . " LIMIT 1";
            $this->db->query($query);
            $postTagMain = $this->db->result();

            // Теги для внутренней страницы
            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $postTags = $this->db->results();

            $mainTag = "";
            if (!empty($postTagMain->name)){
                $mainTag = "<span class='mainTag'>" . $postTagMain->name . "</span><br/> ";
            }

            $items[$key]->postTagsStr = $mainTag . $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $query = "SELECT name FROM partners WHERE id=" . $item->partner . " LIMIT 1";
            $this->db->query($query);
            $partner = $this->db->result();

            $items[$key]->partnerRecordStr = $this->getStrFromObject($partner);
            $items[$key]->partnerRecord = $partner;

            $query = "SELECT id, name, enabled FROM blogwriters WHERE id=" . $item->writers . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            $items[$key]->writerStr = $this->getStrFromObject($writer);
            $items[$key]->writerEnabled = $writer ? $writer->enabled : true;
            $items[$key]->partnerRecord = $writer;


            if ($item->spec_project != 0){
                $query = "SELECT name, image_1_1 FROM spec_projects WHERE id=" . $item->spec_project . " LIMIT 1";
                $this->db->query($query);
                $sp = $this->db->result();

//                $items[$key]->spStr = $this->getStrFromObject($sp);
                $items[$key]->specProject = $sp;
            }


            $query = "SELECT id, name FROM typematerial WHERE id=" . $item->type_material . " LIMIT 1";
            $this->db->query($query);

            $typeMaterial = $this->db->result();

            $items[$key]->typematerial = @$typeMaterial->name;

            $item->date_created .= "<br/>" . $item->time_created;
            $item->date_modified .= "<br/>" . $item->time_modified;

            $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';

            $items[$key]->deleteBtn = '<a href="blogpost/delete/' . $item->id . '/' . $this->token . '" class="btn btn-danger btn-xs" type="button" onclick="if(!confirm("Удалить запись?")) return false;">
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>';


            $name = $item->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);
            $name = "<br/>" . $name;

            $header = "";
            if (!empty($item->header)){
                $header = $item->header;
                $header = str_replace("<p>", "", $header);
                $header = str_replace("</p>", "", $header);
                $header = "<br/>" . $header;
            }

            $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
            switch ((int)$item->type_post) {
                case 1:
                case 2:
                case 3:

                    $linkEditStart = '<a href="blogpost/edit/post/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "post";
                    $items[$key]->name = $linkEditStart . "Пост " .  $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Пост " .  $header . $linkEditEnd;
                    }

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 4:

                    $linkEditStart = '<a href="blogpost/edit/newblog/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "newblog";
                    $items[$key]->name = $linkEditStart . "Анонс нового блога <br/>" .  $item->partnerRecord->name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Анонс нового блога" .  $header . $linkEditEnd;
                    }
                    break;
                case 5:

                    $linkEditStart = '<a href="blogpost/edit/data/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "data";
                    $items[$key]->name = $linkEditStart . "Данные <br/>" . $item->partnerRecord->name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Данные " .  $header . $linkEditEnd;
                    }
                    break;
                case 6:

                    $linkEditStart = '<a href="blogpost/edit/imageday/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "imageday";
                    $items[$key]->name = $linkEditStart . "Фото дня " . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Фото дня " .  $header . $linkEditEnd;
                    }

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 7:

                    $linkEditStart = '<a href="blogpost/edit/factday/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "factday";
                    $items[$key]->name = $linkEditStart . "Факт дня " . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Факт дня " .  $header . $linkEditEnd;
                    }
                    break;
                case 8:

                    $linkEditStart = '<a href="blogpost/edit/citate/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "citate";
                    $items[$key]->name = $linkEditStart . 'Цитата' . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . 'Цитата' .  $header . $linkEditEnd;
                    }

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 9:

                    $linkEditStart = '<a href="blogpost/edit/specproject/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "specproject";
                    $items[$key]->name = $linkEditStart . "Анонс нового спецпроекта <br/>" .  $item->specProject->name . $linkEditEnd;
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Анонс нового спецпроекта" .  $header . $linkEditEnd;
                    }


                    if ($item->specProject->image_1_1){
                        if (file_exists('../files/spec_project/' . $item->specProject->image_1_1)){
                            $items[$key]->image = '<img src="../files/spec_project/' . $item->specProject->image_1_1 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 10:

                    $linkEditStart = '<a href="blogpost/edit/diypost/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "diypost";
                    $items[$key]->name = $linkEditStart . "Карточка DIY" . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Карточка DIY" .  $header . $linkEditEnd;
                    }

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 11:

                    $linkEditStart = '<a href="blogpost/edit/newvideo/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "newvideo";
                    $items[$key]->name = $linkEditStart . "Видео " . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . "Видео " .  $header . $linkEditEnd;
                    }
                    break;
                case 12:

                    $linkEditStart = '<a href="blogpost/edit/ticker/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "ticker";
                    $items[$key]->name = $linkEditStart . "Бегущая строка " . $name . $linkEditEnd;
                    break;
            }
            if (trim($item->thumbnail) && file_exists($this->uploaddir . $item->thumbnail)){
                $items[$key]->image = '<img src="' . $this->uploaddir . $item->thumbnail . '?'.$item->modified.'" style="max-width: 80px;" />';
            }
        }

        return $items;
    }


    function getPostsData($where = ''){

        $query = sql_placeholder("SELECT b.id, b.image_1_1, b.image_1_2, b.created, b.modified, b.show_main_page, b.name, b.header, b.meta_title,
                              b.type_post, b.tags, b.partner, b.writers, b.type_material, b.enabled,
                              DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(b.created, '%H:%i') AS time_created,
                              DATE_FORMAT(b.modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(b.modified, '%H:%i') AS time_modified,
                              bw.enabled AS writersEnabled
                              FROM " . $this->tableName . " b
                              LEFT JOIN blogwriters as bw ON bw.id = b.writers 
                              WHERE 1 $where
                              ORDER BY order_num ASC");

        $this->db->query($query);
        $items = $this->db->results();

        foreach($items as $key=>$item){

            $items[$key]->edit_get = $this->form_get(array('page'=>'post', 'item_id'=>$item->id, 'token'=>$this->token, 'section'=>'BlogPost'));
            $items[$key]->delete_get = $this->form_get(array('act'=>'delete_post','item_id'=>$item->id, 'writer_id'=>(isset($writerId) ? $writerId: null ), 'token'=>$this->token));
            $items[$key]->enable_get = $this->form_get(array('enable_post'=>$item->id, 'token'=>$this->token));

            $query = "SELECT t.name, t.url FROM blogtags AS t
                          WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $items[$key]->tag = $this->db->result();

            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);

            $postTags = $this->db->results();

            $items[$key]->postTagsStr = $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $query = "SELECT name FROM partners WHERE id=" . $item->partner . " LIMIT 1";
            $this->db->query($query);
            $partner = $this->db->result();

            $items[$key]->partnerRecordStr = $this->getStrFromObject($partner);
            $items[$key]->partnerRecord = $partner;

            $query = "SELECT id, name, enabled FROM blogwriters WHERE id=" . $item->writers . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            $items[$key]->writerStr = $this->getStrFromObject($writer);
            $items[$key]->writerEnabled = $writer ? $writer->enabled : true;
            $items[$key]->partnerRecord = $writer;

            $items[$key]->writer = $this->db->result();

            $query = "SELECT id, name FROM typematerial WHERE id=" . $item->type_material . " LIMIT 1";
            $this->db->query($query);
            $items[$key]->typematerial = $this->db->result();

            $item->date_created .= "<br/>" . $item->time_created;
            $item->date_modified .= "<br/>" . $item->time_modified;

            $items[$key]->typePost = "post";

            if ($item->enabled == 1 && $item->writersEnabled == 1) {
                $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';
            } else {
                $text = "Статья скрыта";
                if ($item->enabled == 1 && $item->writersEnabled != 1) { $text = "Автор скрыт";}
                $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post btn--disabled disabled">
                    <i class="fa fa-link"></i> Связать</a><br><br><div class="alert-msg">' . $text . '</div>';
            }
            $name = $item->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);

            $name = "<br/>" . $name;

            switch ($item->type_post) {
                case 1:
                case 2:
                case 3:
                    $items[$key]->typePost = "post";
                    $items[$key]->name = "Пост " .  $name;

                    if (file_exists($this->uploaddir . $item->image_1_2)){
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                    }
                    else{
                        $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    }
                    break;
                case 4:
                    $items[$key]->typePost = "newblog";
                    $items[$key]->name = "Анонс нового блога <br/>" .  $items[$key]->writer->name;
                    $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    break;
                case 5:
                    $items[$key]->typePost = "data";
                    $items[$key]->name = "Данные <br/>" . $items[$key]->writer->name;
                    $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    break;
                case 6:
                    $items[$key]->typePost = "imageday";
                    $items[$key]->name = "Фото дня " . $name;

                    if (file_exists($this->uploaddir . $item->image_1_1)){
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                    }
                    else{
                        $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    }
                    break;
                case 7:
                    $items[$key]->typePost = "factday";
                    $items[$key]->name = "Факт дня " . $name;
                    $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    break;
                case 8:
                    $items[$key]->typePost = "citate";
                    $items[$key]->name = 'Цитата' . $name;

                    if (file_exists($this->uploaddir . $item->image_1_1)){
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                    }
                    else{
                        $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    }
                    break;
                case 11:
                    $items[$key]->typePost = "data";
                    $items[$key]->name = "Видео<br/>" . $items[$key]->name;
                    $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    break;
                case 12:
                    $items[$key]->typePost = "ticker";
                    $items[$key]->name = "Бегущая строка <br/>" . $items[$key]->name;
                    $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                    break;
            }
        }

        return $items;
    }


    /**
     * object to string, string, string
     *
     * @param $object
     * @return string
     */
    function getStrFromObject($object){
        $objectString = "";
        if (!empty($object)){

            if (is_array($object)){
                foreach ($object AS $obj){
                    $tmp[] = $obj->name;
                }
            }
            else{
                $tmp[] = $object->name;
            }
            $objectString = implode(", ", $tmp);
        }

        return $objectString;
    }



    /**
     * @param null $writerId
     * @return array|bool|int
     */
    function getPostsShortList($writerId = null){
        if (!is_null($writerId)){
            $query = sql_placeholder("SELECT *
                              FROM " . $this->tableName . "
                              WHERE  writers=" . $writerId . "
                              ORDER BY order_num ASC");
            $this->db->query($query);
            $items = $this->db->results();
            return $items;
        }
        else{
            return false;
        }
    }

    /**
     * @param null $postId
     * @param null $parentId
     * @return bool
     */
    function setLinkedPost($postId = null, $parentId = null){
        if (!is_null($postId) && !is_null($parentId)){
            $query = sql_placeholder("INSERT IGNORE INTO related_posts SET post_id=?, parent_id=?",$postId, $parentId);
            $this->db->query($query);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * @param null $parentId
     * @return array|bool|int
     */
    function getLinkedPost($parentId = null){
        if (!is_null($parentId)){
            $query = sql_placeholder("SELECT p.id, p.name FROM blogposts AS p
              INNER JOIN related_posts AS rp ON rp.post_id=p.id
              WHERE rp.parent_id=?",$parentId
            );
            $this->db->query($query);
            return $this->db->results();
        }
        else{
            return false;
        }
    }

    /**
     * @param null $product_id
     * @param null $parent_id
     * @return bool
     */
    function setUnLinkedPost($postId = null, $parentId = null){
        if (!is_null($postId) && !is_null($parentId)){
            $query = sql_placeholder("DELETE FROM related_posts WHERE post_id=? AND parent_id=?",$postId, $parentId);
            $this->db->query($query);
            $result = true;
        }
        else{
            $result = false;
        }
        return $result;
    }
    /**
     * Резервирование записи в БД под новое видео.
     * необходимое чтобы знать ИД обрабатываемой записи
     * в последующем, к этому видео добавяться еще записи, динамически,
     * и вот там нужен будет этот Id
     *
     * @param integer $parentVideoId Id родительской записи (поста)
     * @return int Id новой записи в которую запишется добавляемое видео и дочерние элементы к нему
     */
    function getNewVideoId($parentVideoId){
        $newVideoId = 0;

        if (!empty($parentVideoId) && $parentVideoId != 0){
            $query = sql_placeholder('INSERT INTO videos SET parent_video_post = ?, created = now(), modified = now()',
                intval($parentVideoId));
            $this->db->query($query);
            $newVideoId = $this->db->insert_id();
        }

        return $newVideoId;
    }

    function enableVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('UPDATE videos SET enabled=1-enabled WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    function deleteVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('DELETE FROM videos WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Резервирование записи в БД под новое видео.
     * необходимое чтобы знать ИД обрабатываемой записи
     * в последующем, к этому видео добавяться еще записи, динамически,
     * и вот там нужен будет этот Id
     *
     * @param integer $parentVideoId Id родительской записи (поста)
     * @return int Id новой записи в которую запишется добавляемое видео и дочерние элементы к нему
     */
    function getNewDivisionVideoId($parentVideoId){
        $newVideoId = 0;

        if (!empty($parentVideoId) && $parentVideoId != 0){
            $query = sql_placeholder('INSERT INTO division_time_videos SET parent_video = ?, created = now(), modified = now()',
                intval($parentVideoId));
            $this->db->query($query);
            $newVideoId = $this->db->insert_id();
        }

        return $newVideoId;
    }

    /**
     * активация/дезактивация видеофрагмента
     *
     * @param $videoId
     * @return bool
     */
    function enablePartVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('UPDATE division_time_videos SET enabled=1-enabled WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    /**
     * удаление видеофрагмента
     *
     * @param $videoId
     * @return bool
     */
    function deletePartVideo($videoId, $parentVideoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('DELETE FROM division_time_videos WHERE id=?', intval($videoId));
            $this->db->query($query);

            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM division_time_videos WHERE parent_video=?", intval($parentVideoId));
            $this->db->query($query);
            $result = $this->db->result();
        }
        return $result;
    }

    /**
     * Сохранение сортировки видео
     *
     * @param array $values
     * @param string $table
     * @return bool
     */
    function saveSort($values = array(), $table = ''){

        if ($table != "" && !empty($values)){
            $order_num = 1;
            foreach ($values as $value_id){
                $value_id = str_replace("row_", "", $value_id);
                $query = sql_placeholder("UPDATE $table SET order_num=? WHERE id=?", $order_num, $value_id);
                if (!$this->db->query($query)){
                    return false;
                }
                ++$order_num;
            }
            return true;
        }
        else{
            return false;
        }

    }


    function getCountTotalPost()
    {
        $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM " . $this->tableName);
        $this->db->query($query);
        $item = $this->db->result();

        return $item->cnt;
    }

    function getPostByPage($start = 0)
    {
        $query = sql_placeholder("SELECT id, header, body, lead
                              FROM " . $this->tableName . "
                              ORDER BY id DESC
                              LIMIT " . $start . ", 100");
        $this->db->query($query);
        $items = $this->db->results();

        return $items;
    }

    function setNewPostToHttps($id, $header, $body, $lead)
    {
        $query = sql_placeholder("UPDATE " . $this->tableName . " SET header=?, body=?, lead=? WHERE id=?", $header, $body, $lead, $id);
        return $this->db->query($query);
    }


    function getPostByTypeBlock($blockType)
    {
        $result = array();

        $andWhere = "";

        if ($blockType == 'ticker'){
            $blockType = str_ireplace('_', '/', $blockType);
            $andWhere = "AND b.blocks = '" . $blockType . "'";
        }
        else{
            $andWhere = "AND b.blocks != 'ticker'";
        }


        $query = sql_placeholder("SELECT b.id, b.url, b.name, b.header, b.lead, b.created, b.blocks, 
                              b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss, 
                              b.format, b.font, b.citate_author_name, b.citate_author_name,
                              b.amount_to_displaying, b.signature_to_sum, b.text_body,
                              b.type_post, bt.url AS tags, bt.name AS tag_name
                              ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                              bw.enabled as writersEnabled   
                              FROM " . $this->tableName . " AS b
                              INNER JOIN blogtags AS bt ON bt.id=b.tags
                               LEFT JOIN blogwriters AS bw ON bw.id = b.writers
                              WHERE 1 = 1 
                              $andWhere
                              AND b.format IS NOT NULL
                              AND b.type_post != 11
                              AND b.enabled = 1
                              ORDER BY b.created DESC");
        $this->db->query($query);
        $items = $this->db->results();

        foreach ($items as $item) {

            $image = $this->getImage($item, $blockType);

            switch ($this->getTypeVisualBlock($item->type_post)) {
                case 'post':
                    $name = $item->name;
                    $header = $item->header;
                    break;
                case 'factday':
                case 'diypost':
                case 'imageday':
                    $name = $item->name;
                    $header = "";
                    break;
                case 'citate':
                    $name = $item->name;
                    $header = $item->citate_author_name;
                    break;
                default:
                    $name = $item->name;
                    $header = $item->header;
            }

            /** 12 - ticker (бегущая строка) */
            if ($item->type_post == 12) {
                $item->writersEnabled = "1";
            }

            $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat($this->getTypeVisualBlock($item->type_post), $item->format);

            $imageExists = $image ? file_exists(__DIR__.'/..'.str_replace($this->getHost(), '', $image)) : false;

            if ($blockType == '1_1' && $item->type_announce_1_1 == "picture"){
                $item->format = $imageExists ? 'picture' : 'text';
            }
            if ($blockType == '1_2' && $item->type_announce_1_2 == "picture"){
                $item->format = $imageExists ? 'picture' : 'text';
            }
            if ($blockType == '1_3' && $item->type_announce_1_3 == "picture"){
                $item->format = $imageExists ? 'picture' : 'text';
            }

            $result[] = array(
                'id' => intval($item->id),
                'block' => $item->blocks,
                'url' => $this->getHost() . "/blog/" . $item->tags . "/" . $item->url,
                'name' => strip_tags($name),
                'header' => strip_tags($header),
                'date' => date('d.m.Y', strtotime($item->created)),
                'image' => $image,
                'iconCode' => $item->tags,
                'iconName' => $item->tag_name,
                'format' => $item->format,
                'postFormat' => $postFormat,
                'font' => $item->font,
                'writersEnabled' => $item->writersEnabled,
                'typePost' => $this->getTypeVisualBlock($item->type_post)
            );
        }

        return $result;
    }

    public function getMainPage()
    {
        $result = [];

        $query = sql_placeholder("SELECT * FROM main_page ORDER BY row_number ASC");
        $this->db->query($query);
        $mainPageItems = $this->db->results();

        foreach ($mainPageItems AS $mainPageItem){

            $query = sql_placeholder("SELECT bp.id, bp.header, bp.name, bp.lead, bp.url, bp.format, bp.font,
                    bp.image_1_1, bp.image_1_2, bp.image_1_3, bp.image_rss, bp.type_post, bp.partner_url,
                     bp.amount_to_displaying, bp.signature_to_sum, bp.text_body,
                    DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created,
                    bt.name AS tagName, bt.url AS tagUrl,
                    p.name AS partnerName
                    ,bp.type_announce_1_1, bp.type_announce_1_2, bp.type_announce_1_3,
                    bw.enabled as writersEnabled   
                  FROM blogposts AS bp
                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                  LEFT JOIN partners AS p ON p.id = bp.partner
                  LEFT JOIN blogwriters AS bw  ON bw.id = bp.writers
                  WHERE bp.id IN (?@)",
                json_decode($mainPageItem->row_content));

            $this->db->query($query);
            $posts = $this->db->results();

            $postList = [];
            foreach (json_decode($mainPageItem->row_content) AS $key=>$v){
                if ($v == 'banner'){
                    $post = new stdClass();
                    $post->row_type = 'banner';
                    $post->params = $v->params;
                    $post->typeRow = $mainPageItem->row_type;
                    $postList[$key] = $post;
                }
                else{
                    foreach ($posts AS $post){
                        if ($post->id == $v){
                            $postList[$key] = $post;
                        }
                    }
                }
            }


            $rowContent = [];

            foreach ($postList AS $post){

                $image = $this->getImage($post, $mainPageItem->row_type);

                switch ($this->getTypeVisualBlock($post->type_post)) {
                    case 'post':
                        $name = $post->name;
                        $header = $post->header;
                        break;
                    case 'factday':
                    case 'diypost':
                    case 'imageday':
                        $name = $post->name;
                        $header = "";
                        break;
                    case 'citate':
                        $name = $post->name;
                        $header = $post->citate_author_name;
                        break;
                    default:
                        $name = $post->name;
                        $header = $post->header;
                }

                /** 12 - ticker (бегущая строка) */
                if ($post->type_post == 12) {
                    $post->writersEnabled = "1";
                }

                $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat($this->getTypeVisualBlock(
                    $post->type_post),
                    $post->format
                );

                $imageExists = $image ? file_exists(__DIR__.'/..'.str_replace($this->getHost(), '', $image)) : false;

                if ($mainPageItem->row_type == '1_1' && $post->type_announce_1_1 == "picture"){
                    $post->format = $imageExists ? 'picture' : 'text';
                }
                if ($mainPageItem->row_type == '1_2' && $post->type_announce_1_2 == "picture"){
                    $post->format = $imageExists ? 'picture' : 'text';
                }
                if ($mainPageItem->row_type == '1_3' && $post->type_announce_1_3 == "picture"){
                    $post->format = $imageExists ? 'picture' : 'text';
                }


                $rowContent[] = [
                    'id' => intval($post->id),
                    'block' => $mainPageItem->row_type,
                    'url' => $this->getHost() . "/blog/" . $post->tags . "/" . $post->url,
                    'name' => strip_tags($name),
                    'header' => strip_tags($header),
                    'date' => Helper::dateTimeWord($post->date_created),
                    'image' => $image,
                    'iconCode' => $post->tagUrl,
                    'iconName' => $post->tagName,
                    'format' => $post->format,
                    'postFormat' => $postFormat,
                    'font' => $post->font,
                    'writersEnabled' => $post->writersEnabled,
                    'typePost' => $this->getTypeVisualBlock($post->type_post)

                ];
            }

            $result[] = [
                'rowNumber' => $mainPageItem->row_number,
                'rowType' => $mainPageItem->row_type,
                'rowContent' => $rowContent
            ];
        }

        return $result;
    }

    private function getTypeVisualBlock($typePost)
    {
        $typeVisualBlock = array(
            1 => 'post',
            2 => 'post',
            3 => 'post',
            4 => 'post',
            5 => 'factday',
            6 => 'imageday',
            7 => 'factday',
            8 => 'citate',
            9 => 'post',
            10 => 'diypost'
        );

        return $typeVisualBlock[$typePost];

    }

    private function getHost()
    {
        $port = '';
        if ($this->config->port){
            $port = ':' . $this->config->port;
        }

        $host = $this->config->protocol . $this->config->host . $port;

        return $host;
    }

    private function getImage($item, $blockType)
    {
        $image = null;

        $typeFile = $item->type_post == 9 ? 'spec_project' :'blogposts';

        if ($blockType == '1_1' && $item->image_1_1){
            $image = $this->getHost() . "/files/{$typeFile}/" . $item->image_1_1;
        }

        if ($blockType == '1_2' && $item->image_1_2){
            $image = $this->getHost() . "/files/{$typeFile}/" . $item->image_1_2;
        }

        if ($blockType == '1_3' && $item->image_1_3){
            $image = $this->getHost() . "/files/{$typeFile}/" . $item->image_1_3;
        }

        return $image;
    }

    public function setMainPage($data)
    {
        $result = array();

        if (!empty($data)){

            try {
                $this->db->beginTransaction();
                $this->db->query(sql_placeholder("LOCK TABLES main_page WRITE"));
                $this->db->query(sql_placeholder("TRUNCATE TABLE main_page"));
                foreach ($data AS $post) {
                    $query = sql_placeholder("INSERT IGNORE INTO main_page 
                                          (`row_number`, `row_type`, `row_content`) 
                                          VALUES 
                                          (?, ?, ?)",
                        $post->rowNumber,
                        $post->rowType,
                        json_encode($post->rowContent)
                    );
                    $this->db->query($query);
                }
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollbackTransaction();
            }

            $result = array(
                'result' => 'success',
                'message' => 'Данные успешно сохранены.'
            );
        }
        else{
            $result = array(
                'result' => 'error',
                'message' => 'Не переданны данные для сохранения.'
            );
        }
        return $result;
    }

    public function getMainPagePostIDs()
    {
        $query = sql_placeholder("SELECT * FROM main_page");
        $mainPageItems = $this->db->query($query);

        $mainPageItemIDs = array();
        foreach ($mainPageItems as $item) {
            $row = json_decode($item['row_content']);
            if (is_array($row)) {
                $mainPageItemIDs = array_merge($mainPageItemIDs, $row);
            }
        }
        return $mainPageItemIDs;
    }
}
