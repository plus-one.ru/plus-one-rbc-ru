$(document).on('click', 'a.set_in_product', function(e)
{
    var category_id	= $(this).attr('catid') * 1;
    var feature_id	= $(this).attr('feature_id') * 1;

    if (catid!=0 || catid!='')
    {
        $.ajax({
            url: "ajax/set_params_features.php",
            data: {category_id:category_id, feature_id:feature_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {

                console.log(data);
                //$("#group_params_list").html(data);
                //$("#loading_names_group_params").hide();
            }/*,
            beforeSend: function(){
                $("#loading_names_group_params").show();
                $("#group_params_list").html('');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#loading_names_group_params").hide();
                $("#group_params_list").html('');
                alert(xhr.status);
                alert(thrownError);
            }*/
        });
    }
    return false;
});

$(document).on('click', '.select_category', function(e)
{
	var catid	= $(this).attr('catid') * 1;
	var parent	= $(this).attr('parent') * 1;
	
	$(".select_category").removeClass('selected');

	$(this).addClass('selected');

	// подчищаем результаты предыдущих выборок
	$("#values_list").html('');
	$("#products_list").html('');

	if (catid!=0 || catid!='')
	{
		$.ajax({
			url: "ajax/select_params_group.php",
			data: {catid:catid},
			type: "POST",
			dataType: 'json',
			success: function(data)
			{
				$("#group_params_list").html(data);
				$("#loading_names_group_params").hide();
			},
			beforeSend: function(){
				$("#loading_names_group_params").show();
				$("#group_params_list").html('');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				$("#loading_names_group_params").hide();
				$("#group_params_list").html('');
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	return false;
});

$(document).on('click', '.edit_param', function(e)
{
	var paramid	= $(this).attr('param-id') * 1;
	
	$("#textbox_param_"+paramid).show();

	$(this).removeClass('edit_param');
	$(this).addClass('save_param');

	return false;
});

$(document).on('click', '.save_param', function(e)
{
	var paramid		= $(this).attr('param-id') * 1;
	var new_name	= $("#textbox_param_"+paramid).val();

	$("#textbox_param_"+paramid).hide();

	$(this).removeClass('save_param');
	$(this).addClass('edit_param');

	if (paramid!=0 || catid!=0)
	{
		$.ajax({
			url: "ajax/save_param_name.php",
			data: {paramid:paramid, new_name:new_name},
			type: "POST",
			dataType: 'json',
			success: function(data)
			{
				console.log(data);
				if(data.result=="true"){
					$("#name_group_"+paramid).html(new_name);
					$("#textbox_param_"+paramid).val(new_name);
				}
			}
		});
	}
	return false;
});

 
$(document).on('click', '.select_param_group_name', function(e)
{
	var catid	= $(this).attr('cat-id') * 1;
	var paramid	= $(this).attr('param-id') * 1;

	$(".select_param_group_name").removeClass('selected');

	$(this).addClass('selected');

	// подчищаем результаты предыдущих выборок
	$("#products_list").html('');

	if (catid!=0 && paramid!=0)
	{
		$.ajax({
			url: "ajax/select_params_values.php",
			data: {catid:catid, paramid:paramid},
			type: "POST",
			dataType: 'json',
			success: function(data)
			{
				$("#values_list").html(data);
				$("#loading_params_values").hide();
			},
			beforeSend: function(){
				$("#loading_params_values").show();
				$("#values_list").html('');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				$("#loading_params_values").hide();
				$("#values_list").html('');
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	return false;
});

$(document).on('click', '.delete_value', function(e)
{
	var valueid	= $(this).attr('value-id') * 1;
	var catid	= $(this).attr('cat-id') * 1;
	var paramid	= $(this).attr('param-id') * 1;

	$("#products_list").html('');

	if (valueid!=0)
	{
		$.ajax({
			url: "ajax/delete_params_values.php",
			data: {valueid:valueid, catid:catid, paramid:paramid},
			type: "POST",
			dataType: 'json',
			success: function(data)
			{
				$("#values_list").html(data);
				$("#loading_params_values").hide();
			},
			beforeSend: function(){
				$("#loading_params_values").show();
				$("#values_list").html('');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				$("#loading_params_values").hide();
				$("#values_list").html('');
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	return false;
});




$(document).on('click', '.select_param_value', function(e)
{
	var valueid = $(this).attr('value-id') * 1;

	$(".select_param_value").removeClass('selected');

	$(this).addClass('selected');

	if (valueid!=0)
	{
		$.ajax({
			url: "ajax/select_linked_params_to_products.php",
			data: {valueid:valueid},
			type: "POST",
			dataType: 'json',
			success: function(data)
			{
				$("#loading_3").hide();
				$("#products_list").html(data);
			},
			beforeSend: function(){
				$("#loading_3").show();
				$("#products_list").html('');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				$("#loading_3").hide();
				$("#products_list").html('');
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	return false;
});



$(document).on('submit', '.frm_send_notice', function(e)
{
	console.log('fff');
	/*var ud			= $('input[name=ud]').val();
	var city		= $('input[name=city]').val();
	var street		= $('input[name=street]').val();
	var house		= $('input[name=house]').val();
	var kvartira	= $('input[name=kvartira]').val();
	var floor		= $('input[name=floor]').val();
	var pod			= $('input[name=pod]').val();
	
	$.ajax({
		url: "/ajax_data/edit_user_address_data.php",
		data: {ud:ud,city:city,street:street,house:house,kvartira:kvartira,floor:floor,pod:pod},
		dataType: 'json',
		success: function(data)
		{
			$('#address').html(data);
		}
	});*/
	
	
	$("#edit_address").hide();
	$("#view_address").show();

	return false;
});


$(document).on('click', '#save_user_address', function(e)
{
	
	var ud			= $('input[name=ud]').val();
	var city		= $('input[name=city]').val();
	var street		= $('input[name=street]').val();
	var house		= $('input[name=house]').val();
	var kvartira	= $('input[name=kvartira]').val();
	var floor		= $('input[name=floor]').val();
	var pod			= $('input[name=pod]').val();
	
	$.ajax({
		url: "/ajax_data/edit_user_address_data.php",
		data: {ud:ud,city:city,street:street,house:house,kvartira:kvartira,floor:floor,pod:pod},
		dataType: 'json',
		success: function(data)
		{
			$('#address').html(data);
		}
	});
	
	
	$("#edit_address").hide();
	$("#view_address").show();

	return false;
});

$(document).on('click', '#save_subscribe', function(e)
{
	var ud				= $('input[name=ud]').val();
	var r_spec_e		= $("#r_spec_e").prop("checked");
	var r_news_e		= $("#r_news_e").prop("checked");
	var r_spec_s		= $("#r_spec_s").prop("checked");
	var r_news_s		= $("#r_news_s").prop("checked");
	
	$.ajax({
		url: "/ajax_data/edit_user_subscribe_data.php",
		data: {ud:ud,r_spec_e:r_spec_e,r_news_e:r_news_e,r_spec_s:r_spec_s,r_news_s:r_news_s},
		dataType: 'json',
		success: function(data)
		{
			//$('#address').html(data);
		}
	});
	
});

$(document).on('click', '#edit_user_data', function(e)
{
	
	if ($(this).prop("checked")===true)
	{
		$("#view_names").hide();
		$("#edit_names").show();
		$("#save_user_data").show();
	}
	else if ($(this).prop("checked")===false)
	{
		$("#edit_names").hide();
		$("#view_names").show();
		
		$("#save_user_data").hide();
	}

});

$(document).on('click', '#save_user_data', function(e)
{
	
	var name			= $('input[name=name]').val();
	var lastname		= $('input[name=lastname]').val();
	var phone			= $('input[name=phone]').val();
	
	$.ajax({
		url: "/ajax_data/edit_user_personal_data.php",
		data: {name:name,lastname:lastname,phone:phone},
		dataType: 'json',
		success: function(data)
		{
			if (data.act===true)
			{
				$("#edit_names").hide();
				$("#view_names").show();
					
				$("#edit_address").hide();
				$("#view_address").show();
					
				$("#save_user_data").hide();
				$("#edit_user_data").prop({"checked":false});
				
				$('input[name=name]').val(data.name);
				$('input[name=lastname]').val(data.lastname);
				$('input[name=phone]').val(data.phone);
				
				$('input[name=name_dis]').val(data.name);
				$('input[name=lastname_dis]').val(data.lastname);
				$('input[name=phone_dis]').val(data.phone);
			}
		}
	});
	
	return false;
});



$(document).on('click', '#im_partner', function(e)
{
	$("#parner_form").show();
	return false;
});

$(document).on('click', '#save_partner_info', function(e)
{
	var name_contact		= $('input[name=name_contact]').val();
	var nameshop			= $('input[name=nameshop]').val();
	var prav_forma			= $('select[name=prav_forma]').val(); 
	var ur_adress			= $('input[name=ur_adress]').val();
	var fakt_adress			= $('input[name=fakt_adress]').val();
	var specializaciya		= $('input[name=specializaciya]').val();
	var phone				= $('input[name=phone_contact]').val();
	var email_contact		= $('input[name=email_contact]').val();
	var email_user			= $('input[name=email]').val();
	var message				= $('textarea[name=message]').val();
	
	$.ajax({
		url: "/ajax_data/post_message_shop_request.php",
		data: {name_contact:name_contact,nameshop:nameshop,prav_forma:prav_forma,ur_adress:ur_adress,fakt_adress:fakt_adress,specializaciya:specializaciya,email_contact:email_contact,email_user:email_user,phone:phone,message:message},
		dataType: 'json',
		success: function(data)
		{
			//console.log(data);
			if (data.error!="")
			{
				console.log(data.error)
			}
			if (data.sucess!="")
			{
				console.log(data.success)
			}
			//$('#form_data').html(data);
			//$('#success_message').show();
			//$('#data').show();
		}/*,
		// прячем слой с иконкой процесса.
		complete: function()
		{
			$('#data').show();
			$('#loader').hide();
		},
		// показываем слой с иконкой процесса.
		beforeSend: function()
		{
			$('#data').hide();
			$('#loader').show();
		}*/
	});
	
	return false;
});

$(document).on('submit', '#form_asqquestion', function(e)
{
	var name			= $(this).find('input[name=name]').val();
	var email			= $(this).find('input[name=email]').val();
	var phone			= $(this).find('input[name=phone]').val();
	var comment			= $(this).find('textarea[name=comment]').val();
	var captcha_code	= $(this).find('input[name=captcha_code]').val();
	
	
	$.ajax({
		url: "/ajax_data/post_message_question.php",
		data: {name: name, email: email, phone: phone, comment: comment, captcha_code: captcha_code},
		dataType: 'json',
		success: function(data)
		{
			$('#form_data').html(data);
		}
	});	
	return false;
});

$(document).on('submit', '#forshop_request', function(e)
{
	var nameshop			= $(this).find('input[name=nameshop]').val();
	var prav_forma			= $(this).find('select[name=prav_forma]').val(); 
	var ur_adress			= $(this).find('input[name=ur_adress]').val();
	var fakt_adress			= $(this).find('input[name=fakt_adress]').val();
	var specializaciya		= $(this).find('input[name=specializaciya]').val();
	var name				= $(this).find('input[name=name]').val();
	var phone				= $(this).find('input[name=phone]').val();
	var email				= $(this).find('input[name=email]').val();
	var message				= $(this).find('textarea[name=message]').val();
	var captcha_code		= $(this).find('input[name=captcha_code]').val();
	
	$.ajax({
		url: "/ajax_data/post_message_shop_request.php",
		data: {nameshop:nameshop,prav_forma:prav_forma,ur_adress:ur_adress,fakt_adress:fakt_adress,specializaciya:specializaciya,name: name,email:email,phone:phone,message:message,captcha_code:captcha_code},
		dataType: 'json',
		success: function(data)
		{
			$('#form_data_partner').html(data);

			//$('#forshop_request').hide();
			//$('#message_partner').show();
		}
	});
	return false;
});




$(document).on('click', '#apply_promocode', function(e)
{
	var promocode		= $('input[name=promo_code]').val();
	var delivery_price	= $('#delivery_price').html();
	delivery_price		= delivery_price*1;
	if (promocode!='')
	{
		$.ajax({
			url: "/ajax_data/check_promocode.php",
			data: {promocode:promocode},
			dataType: 'json',
			success: function(data)
			{
				if (data.error!="")
				{
					console.log(data.error)
				}
				if (data.sucess!="")
				{
					console.log(data.success)
					$("#discount_price").html(data.discount_summ);
					
					data.total_price = data.total_price*1;
					
					$("#subtotal_price_full").html(data.total_price+delivery_price);
					
					$("#total_bonuses").html(data.total_bonuses);
					$("#total_bonuses_inp").html(data.total_bonuses);					
				}
			}
		});
	}
	return false;
});




$(document).on('click', '#apply_bonuses', function(e)
{
	var total_use_bonuses	= $('input[name=total_use_bonuses]').val() * 1;
	var tb					= $(this).attr('tb') * 1;
	var orderid				= $(this).attr('orderid');
	console.log(total_use_bonuses+' '+tb);
	
	if (total_use_bonuses!='' && total_use_bonuses<=tb)
	{
		$.ajax({
			url: "/ajax_data/apply_bonuses.php",
			data: {total_use_bonuses:total_use_bonuses, tb:tb, orderid:orderid},
			dataType: 'json',
			success: function(data)
			{
				if (data.error!="")
				{
					console.log(data.error)
				}
				if (data.sucess!="")
				{
					console.log(data)
					//$("#discount_price").html(data.discount_summ);
					
					data.total_price = data.total_price*1;
					
					$("#subtotal_price_full").html(data.total_price);
					
					
					
					$('#payment_bonuses_total').html(data.used_bonuses);
					$('#payment_bonuses').show();
					
					$('#use_bonuses_layer').hide();
					$('#total_use_bonuses').val(data.available_bonuses);
					
					
					$('#available_bonuses').html(data.available_bonuses);
					
					$('#paysumm').val(data.total_price);
					
					//$("#total_bonuses").html(data.total_bonuses);
					//$("#total_bonuses_inp").html(data.total_bonuses);
				}
			}
		});
	}
	else
	{
		console.log('укажите верное значение бонусов');		
	}
	return false;
});




