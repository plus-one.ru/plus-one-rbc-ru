<?PHP
// Как и все классы, наследуется от класса widget
require_once('Widget.class.php');
require_once('Blog.class.php');

class Site extends Widget
{
    /** @var Blog main */
    var $main; // Центральный блок (класс). В данном случае он может быть разных классов, в зависимости от раздела
    var $categories; // Категории товаров
    var $articles_count = 3; // Количество свежих статей
    var $news_count = 3; // Количество свежих новостей
    var $section; // текущий раздел
    var $current_url = "";

    /**
     *
     * Конструктор
     *
     */
    function Site(&$parent = null)
    {
        //var_dump('dasfsdfsd');
        Widget::Widget($parent);

        // Пока не знаем какого класса центральный блок, он будет базового класса
        $this->main = new Widget($this);
//        echo "<pre>";
//        print_r($this->main);
//        echo "</pre>";
//        die();
//
//        // рубрики
//        $this->db->query("SELECT * FROM blogtags WHERE enabled=1 AND isshow=1 ORDER BY name");
//        $blogtags = $this->db->results();
//        $this->smarty->assign('blogtags', $blogtags);
//
//        $blog = new Blog();
//        $postData = $blog->fetch_list();
//
//        //$this->smarty->assign('postsMatrix', $postData['matrix']);
//       // $this->smarty->assign('totalPages', $postData['totalPages']);
//       // $this->smarty->assign('currentPage', $postData['currentPage']);
//
//
//        $this->db->query("SELECT * FROM sections WHERE enabled=1 AND menu_id=2 ORDER BY order_num");
//        $sections = $this->db->results();
//        $this->smarty->assign('sections', $sections);
//
//        // дата для шапки и день недели
//        $date = date('d.m.Y',time());
//        $day = date('w',time());
//        switch ($day){
//            case 0: $d='воскресенье'; break;
//            case 1: $d='понедельник'; break;
//            case 2: $d='вторник'; break;
//            case 3: $d='среда'; break;
//            case 4: $d='четверг'; break;
//            case 5: $d='пятница'; break;
//            case 6: $d='суббота'; break;
//        }
//
//        $ndate = explode(".", $date);
//        switch ($ndate[1]){
//            case 1: $m='января'; break;
//            case 2: $m='февраля'; break;
//            case 3: $m='марта'; break;
//            case 4: $m='апреля'; break;
//            case 5: $m='мая'; break;
//            case 6: $m='июня'; break;
//            case 7: $m='июля'; break;
//            case 8: $m='августа'; break;
//            case 9: $m='сентября'; break;
//            case 10: $m='октября'; break;
//            case 11: $m='ноября'; break;
//            case 12: $m='декабря'; break;
//        }
//
//
//        $dateToMainPage = $ndate[0] . " " . $m . ", " . $d;
//        $this->smarty->assign('dateToMainPage', $dateToMainPage);

        // Необходимо определить что выводить в центральном блоке.
        // Это может быть указано в качестве конкретного раздела сайта (section)
        // или в качестве указания модуля, который следует вывести
        // В первом случае мы узнаем из базы какого модуля нужный раздел
        // и создаем соотвествующий класс (например статическая страница или лента новостей)
        // а во втором - и узнавать ничего не надо, модуль по сути и есть класс
        // (ну почти, нужно только глянуть в таблице modules название класса для данного модуля, обычно оно совпадает)

        // итак, url текущего раздела (может быть не задан)
        $section_url = $this->url_filtered_param('section');

        // модуль (тоже может быть не задан)
        $module = $this->url_filtered_param('module');

        // если ничего не задано, текущим разделом будет раздел, заданный в настройках как главная страница
        if (empty($section_url) && empty($module)) {
            $section_url = $this->settings->main_section;

        }

        // если url раздела задан,
        if (!empty($section_url)) {
            // выбираем из базы этот раздел
            $query = sql_placeholder("SELECT sections.section_id, modules.module_id, modules.class, sections.url, sections.body, sections.header, sections.module_id, sections.meta_title, sections.meta_description, sections.meta_keywords  FROM sections LEFT JOIN modules ON sections.module_id=modules.module_id WHERE sections.url=? limit 1", $section_url);
            $this->db->query($query);
            $this->section = $this->db->result();
            $this->smarty->assign('section', $this->section);
        }

        // Если раздел с таким url действительно существует,
        if (!empty($this->section)) {
            $this->current_url = $this->section->url;
            // создадим класс этого раздела
            $class = $this->section->class;

            include_once("$class.class.php");

            if (class_exists($class)) {
                $_GET['section'] = $this->section->url;

                $this->main = new $class($this);
            } // возвращение false приводит к отображению 404 ошибки. Кстати это работает в любом модуле
            else {
                return false;
            }
        } // Если задан модуль, аналогично создаем нужный класс
        elseif (!empty($module)) {

            $query = sql_placeholder("SELECT class,url FROM modules WHERE modules.class=? LIMIT 1", $module);
            $this->db->query($query);
            $module = $this->db->result();
            $this->current_url = $module->url;

            if (!empty($module->class)) {
                $class = $module->class;
                include_once("$class.class.php");
                if (class_exists($class)) {
                    $this->main = new $class($this);
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     *
     * Отображение
     *
     */
    function fetch()
    {

        // Создаем основной блок страницы
        if (!$this->main->fetch()) {
            return false;
        }

//        var_dump($this->main->fetch());die();
        // Если у main установлен флаг single, значит страница состоит только из main, не обромляем ее ничем больше
        if (isset($this->main->single) && $this->main->single) {
            $this->body = $this->main->body;
            return $this->body;
        } else {
            $content = $this->main->body;

            //  Устанавливаем мета-теги, указанные в главном блоке
            /*$this->title = isset($this->section->meta_title)?$this->section->meta_title:$this->main->title;
            $this->keywords = isset($this->section->meta_keywords)?$this->section->meta_keywords:$this->main->keywords;
            $this->description = isset($this->section->meta_description)?$this->section->meta_description:$this->main->description;*/

            $this->title = isset($this->section->meta_title) ? $this->section->meta_title : $this->main->title;
            $this->keywords = isset($this->section->meta_keywords) ? $this->section->meta_keywords : $this->main->keywords;
            $this->description = isset($this->section->meta_description) ? $this->section->meta_description : $this->main->description;

//            if ($this->title == "")
//                $this->title = $this->settings->site_name;
//            else
//                $this->title = $this->title;
        }


        if (!$this->description){
            $this->description = "Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить.";
        }

        $this->smarty->assign('site_name', $this->settings->site_name);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('keywords', $this->keywords);
        $this->smarty->assign('description', $this->description);
        $this->smarty->assign('descriptionDefault', $this->descriptionDefault);
        $this->smarty->assign('content', $content);
        $this->smarty->assign('ogImage', $this->ogImage);

        $this->smarty->assign('ogImageDefault', $this->ogImageDefault);
        $this->smarty->assign('ogImage_1_1', $this->ogImage_1_1);
        $this->smarty->assign('ogImage_1_1_c', $this->ogImage_1_1_c);
        $this->smarty->assign('ogImage_1_2', $this->ogImage_1_2);
        $this->smarty->assign('ogImage_1_3', $this->ogImage_1_3);
        $this->smarty->assign('ogImage_galeries', $this->ogImage_galeries);

        $this->smarty->assign('ogSiteName', $this->ogSiteName);
        $this->smarty->assign('ogTitle', $this->ogTitle);

        if (!$this->withUrlParams) {
            $this->ogUrl = preg_replace('/^\//', '', @explode('?', $_SERVER['REQUEST_URI'], 2)[0]);
        }
        $this->smarty->assign('ogUrl', $this->ogUrl);

        $this->smarty->assign('rubrikaUrl', $this->rubrikaUrl);
        $this->smarty->assign('typeUrl', $this->typeUrl);
        $this->smarty->assign('tagUrl', $this->tagUrl);


//        var_dump($this->body);die();

        $this->smarty->assign('current_url', $this->current_url);
        $this->smarty->assign('page', '');

        // Создаем текущую страницу сайта
        if ($_GET['section'] == "mainpage") {
            $this->body = $this->smarty->fetch('index.tpl');
        }
        elseif ($_GET['module'] === 'Blog' && $_GET['mode'] === 'rkn') {
            $this->smarty->assign('title', 'Упс...');
            $this->smarty->assign('blockedRkn', true);
            $this->smarty->assign('content', $this->smarty->fetch('217471-8.tpl'));
            $this->body = $this->smarty->fetch('index_blog.tpl');
        }
        elseif($_GET['section'] == '404'){
            $this->body = $this->smarty->fetch('status_404.tpl');
        }
        elseif ($_GET['module'] == "Blog" && !empty($_GET['article_url'])) {

            $this->body = $this->smarty->fetch('index_blog.tpl');
        }
        elseif ($_GET['module'] == "Blog" && !empty($_GET['specprojects'])) {
            $this->body = $this->smarty->fetch('index_other.tpl');
        }
        elseif ($_GET['module'] == "Blog" && $_GET['mode'] == "search") {
            $this->body = $this->smarty->fetch('index_search.tpl');
        }
        elseif ($_GET['module'] == "Blog" && !empty($_GET['rubrika_url']) && $_GET['mode'] == 'getleaders') {
            $this->body = $this->smarty->fetch('index_other.tpl');
        }
        elseif ($_GET['module'] == "Blog" && !empty($_GET['rubrika_url']) && $_GET['mode'] == 'posttags') {
            $this->body = $this->smarty->fetch('index_other.tpl');
        }
        elseif ($_GET['module'] == "Blog" && empty($_GET['writer_id']) && $_GET['mode'] == 'autorslist') {
            $this->body = $this->smarty->fetch('index_other.tpl');
        }
        elseif ($_GET['module'] == "Blog" && $_GET['mode'] == 'getaboutpage') {
            $this->body = $this->smarty->fetch('index_nomenu.tpl');
        }

        elseif ($_GET['module'] == "Blog" && !empty($_GET['writer_id']) && $_GET['mode'] == 'autorslist') {
            $this->body = $this->smarty->fetch('index_blogs.tpl');
        }
        elseif ($_GET['module'] == "Blog" && !empty($_GET['rubrika_url']) && $_GET['mode'] == 'getsimpleblogs') {
            $this->body = $this->smarty->fetch('index_simpleblogs.tpl');
        }

        elseif ($_GET['module'] == "Blog" && !empty($_GET['rubrika_url']) && $_GET['mode'] == 'getpostsfromtag') {
            $this->body = $this->smarty->fetch('index_blogs.tpl');
        }
//        elseif ($_GET['module'] == "Blog" && !empty($_GET['tag']) && !empty($_GET['rubrika_url'])) {
//            $this->body = $this->smarty->fetch('index_blogtags.tpl');
//        }
        elseif ($_GET['module'] == "Blog" && !empty($_GET['rubrika_url']) && empty($_GET['tag'])) {
            $this->body = $this->smarty->fetch('index_blogs.tpl');
        }
//        elseif ($_GET['module'] == "Blog" && !empty($_GET['writer_id'])) {
//            $query = sql_placeholder("SELECT * FROM blogwriters WHERE enabled=1 AND id = ?", $_GET['writer_id']);
//            $this->db->query($query);
//            $writer = $this->db->result();
//
//            $this->smarty->assign('writer', $writer);
//
//            $this->body = $this->smarty->fetch('index_blogs_writer.tpl');
//        }
        else {
            $this->body = $this->smarty->fetch('index.tpl');
        }



        return $this->body;
    }
}
