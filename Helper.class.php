<?PHP
class Helper
{
    /**
     * убирает открывающий <p> и закрывающий </p> тег в строке.
     *
     * @param $text
     * @return mixed
     */
    public static function removeParagraphTag($text)
    {
        $text = str_replace("<p>", "", $text);
        $text = str_replace("</p>", "", $text);
        return $text;
    }


    public static function makeCitateHeader($name, $header)
    {
        $citateHeader = "";

        if ($name && $header){
            $citateHeader = $name . " <span>" . $header . "</span>";
        }

        return $citateHeader;
    }


    /**
     * преобразование даты поста в вид сегодня, 2 дня назад, 5 месяцев назад etc
     *
     * @param string $dateStr
     * @return string
     */
    public static function dateTimeWord($dateStr = '')
    {
        $dateToStr = "";

        if (!empty($dateStr)){
            $currentDateTimeStamp = time();

            $dateStr = strtotime($dateStr);

            $datetime1 = date_create(date('d.m.Y H:i:s', $currentDateTimeStamp));
            $datetime2 = date_create(date('d.m.Y H:i:s', $dateStr));

            $interval = date_diff($datetime1, $datetime2);

            // дни
            if ($interval->y == 0 && $interval->m == 0 && ($interval->d >= 0 && $interval->d <= 5)) {
                switch ($interval->d) {
                    case 0:
                        $dateToStr = "Сегодня";
                        break;
                    case 1:
                        $dateToStr = "Вчера";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $dateToStr = $interval->d . " дня назад";
                        break;
                    case 5:
                        $dateToStr = $interval->d . " дней назад";
                        break;
                }
            }

            // недели
            if ($interval->y == 0 && $interval->m == 0 && ($interval->d > 5 && $interval->d <= 30)) {

                $countWeek = round($interval->d / 7);

                switch ($countWeek) {
                    case 1:
                        $dateToStr = "Неделю назад";
                        break;
                    case 2:
                    case 3:
                        $dateToStr = $countWeek . " недели назад";
                        break;
                    case 4:
                        $dateToStr = "Месяц назад";
                        break;
                }
            }

            // месяцы
            if ($interval->y == 0 && $interval->m >= 1) {

                $countMonth = $interval->m % 12;

                switch ($countMonth) {
                    case 1:
                        $dateToStr = "Месяц назад";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $dateToStr = $countMonth . " месяца назад";
                        break;

                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        $dateToStr = $countMonth . " месяцев назад";
                        break;
                }
            }

            // годы
            if ($interval->y > 0) {
                $countYears = $interval->y;
                switch ($countYears) {
                    case 1:
                        $dateToStr = "Год назад";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $dateToStr = $countYears . " года назад";
                        break;
                    default:
                        $dateToStr = $countYears . " лет назад";
                }
            }
        }
        return $dateToStr;
    }
}