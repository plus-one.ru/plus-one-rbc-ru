'use strict';
yii2AngApp_site.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/blog/community', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .when('/blog/ecology', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .when('/blog/economy', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .otherwise({
            redirectTo: '/'
        });
}])
    .controller('index', ['$scope', '$http', function($scope,$http) {
        // Сообщение для отображения представлением
        $scope.message = 'Вы читаете главную страницу приложения. ';
    }])
    .controller('about', ['$scope', '$http', function($scope,$http) {
        // Сообщение для отображения представлением
        $scope.message = 'Это страница с информацией о приложении.';
    }])
    .controller('contact', ['$scope', '$http', function($scope,$http) {
        // Сообщение для отображения представлением
        $scope.message = 'Пишите письма. Мы будем рады!.';
    }]);