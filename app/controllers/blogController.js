'use strict';

var endPosts = false;
var pictureDayEmpty = true;
var film = '';
var $siteDomain = "http://plus-one.rbc.ru";

yii2AngApp_film.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/blog/', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .when('/', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .when('/blog/:rubrika', {
            templateUrl: 'app/views/index.html',
            controller: 'getRubrika',
        })
        //.when('/blog/:rubrika/:post', {
        //    templateUrl: 'app/views/post.html',
        //    controller: 'getPost',
        //})
        .when('/author/', {
            templateUrl: 'app/views/allauthors.html',
            controller: 'getAllAuthors',
        })
        .when('/author/:rubrika', {
            templateUrl: 'app/views/index.html',
            controller: 'getAuthor',
        })
        .when('/leaders/:rubrika', {
            templateUrl: 'app/views/allauthors.html',
            controller: 'getLeaders',
        })
        .when('/posttags/:rubrika', {
            templateUrl: 'app/views/posttags.html',
            controller: 'getPostTags',
        })
        .when('/posttags/:rubrika/:tag', {
            templateUrl: 'app/views/posttag.html',
            controller: 'getPostTag',
        })
        .when('/case-1.html', {
            //templateUrl: 'case-1.html',
            redirectTo: '/case-1.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    // use the HTML5 History API
    //$locationProvider.html5Mode(true);
}]);

yii2AngApp_film.controller('index', ['$scope', '$http', 'services', '$sce', 'Reddit', '$rootScope', '$timeout',
    function($scope,$http,services, $sce, Reddit, $rootScope, $timeout) {

        endPosts = false;
        $scope.reddit = new Reddit('main', '', 0, 'blogs');

        $http.get(serviceBase + 'services/?page=main').success(function(data) {
            $rootScope.meta = data;
            $rootScope.currentLink = '';
        });

        $scope.loadVideo = function (url) {
            $scope.videoUrl = $sce.trustAsResourceUrl('//www.youtube.com/embed/' + url);
        }

        $scope.loadVideoFragment = function (url, minute, secunde) {

            console.log(url);
            console.log(minute);
            console.log(secunde);
            // кол-во секунд в минутах
            var minToSec = minute * 60;
            var start = (minToSec * 1) + (secunde * 1);

            $scope.videoUrl = $sce.trustAsResourceUrl('http://www.youtube.com/embed/' + url + '?start=' + start + '&autoplay=1');
        }

        $timeout(function(){
            $rootScope.$broadcast('rebuild:me');
        }, 200);


        // безопасный вывод html
        $scope.textDangerousSnippet = function($text) {
            return $sce.trustAsHtml($text);
        };

        $scope.showVideoFragment = function($videoId) {
            $http.get(serviceBase + 'get-video-fragments/' + $videoId).success(function(data) {
                $scope.videoFragmentsMainVideo = data.video;
                $scope.videoFragments = data.videoFragments;
                $scope.videoMain = data.video;
            });

            $timeout(function(){
                $rootScope.$broadcast('rebuild:meto');
            }, 200);
        }

        $scope.goBack = function() {
            $scope.videoFragments = "";
        }

        pictureDayEmpty = false;

        $rootScope.pictureDay = null;
        $rootScope.pictureDay = $scope.reddit.pictureDayData;
    }])

    .controller('getRubrika', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            endPosts = false;
            $scope.reddit = new Reddit($routeParams.rubrika, '', 0, 'blogs');

            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika).success(function(data) {
                $rootScope.meta = data;
                $rootScope.currentRubrika = $routeParams.rubrika;
                $rootScope.currentLink = 'posts';
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])

    .controller('getPost', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope', "$timeout",
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope, $timeout) {


            $http.get(serviceBase + 'getpost/?post_url=' + $routeParams.post)
                .success(function(data) {
                    $scope.post = data;
                    $rootScope.meta = data.meta;
            });

            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika).success(function(data) {
                $rootScope.currentRubrika = $routeParams.rubrika;
                $rootScope.currentLink = 'posts';
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };

            $scope.siteDomain = $siteDomain;

            $timeout(function(){
                initGallery();
                var $fotorama = angular.element(document.querySelector(".fotorama"));
                $fotorama.fotorama();
                var $fotoData = $fotorama.data('fotorama');

                //var $textCaption =
                $fotorama.on('fotorama:showend ', function(e, fotorama) {
                    angular.element(document.querySelector(".fotorama-holder .numbering")).html((fotorama.activeIndex + 1) + '/' + fotorama.size);
                    var _cotent = angular.element(document.querySelector(".fotorama__active .fotorama__html div p")).html();
                    angular.element(document.querySelector(".fotorama-holder .info p")).html(_cotent);
                });

                angular.element(document.querySelector(".fotorama-holder .next")).click(function(event) {
                    $fotoData.show('>');
                    return false;
                });

                angular.element(document.querySelector(".fotorama-holder .prev")).click(function(event) {
                    $fotoData.show('<');
                    return false;
                });

                //console.log($fotoData.data);

            }, 1000);



        }])

    .controller('getAuthor', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            endPosts = false;
            $scope.reddit = new Reddit($routeParams.rubrika, '', 0, 'authors');

            var $postsTypeData;

            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=author').success(function(data) {
                $rootScope.meta = data;
            });

            $http.get(serviceBase + 'poststypedata/' + $routeParams.rubrika).success(function(data) {

                $postsTypeData = data;

                $scope.showButtonAnalitic = 0;

                if ($postsTypeData != ""){
                    $scope.showButtonAnalitic = 1;
                }
            });

            $scope.getAnalitics = function (evt) {

                //angular.element(document.querySelector(".i-graph")).hide('fast');

                if (angular.element(evt.target).hasClass('active')){
                    angular.element(evt.target).removeClass('active');
                    angular.element(document.querySelector(".data")).hide();
                    angular.element(document.querySelector(".posts")).show();
                }
                else{
                    $scope.postsTypeData = $postsTypeData;
                    angular.element(evt.target).addClass('active');
                    angular.element(document.querySelector(".posts")).hide();
                    angular.element(document.querySelector(".data")).show();
                }
                //angular.element(document.querySelector(".i-graph")).show('fast');
            };
            $rootScope.currentLink = '';
            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])

    .controller('getLeaders', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            //endPosts = false;
            //$scope.reddit = new Reddit('', '', 0, 'allauthors');

            $http.get(serviceBase + 'getleaders/' + $routeParams.rubrika)
                .success(function(data) {
                    $scope.autorsList = data;
                    //$rootScope.meta = data.meta;
                });


            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=authors').success(function(data) {
                $rootScope.meta = data;
                $rootScope.currentRubrika = $routeParams.rubrika;
                $rootScope.currentLink = 'leaders';
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])


    .controller('getPostTags', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            //endPosts = false;
            //$scope.reddit = new Reddit('', '', 0, 'allauthors');

            $http.get(serviceBase + 'getposttags/' + $routeParams.rubrika)
                .success(function(data) {
                    $scope.tagsList = data;
                    //$rootScope.meta = data.meta;
                });


            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=authors').success(function(data) {
                $rootScope.meta = data;
                $rootScope.currentRubrika = $routeParams.rubrika;
                $rootScope.currentLink = 'postTags';
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])


    .controller('getPostTag', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            $http.get(serviceBase + 'countposttag/' + $routeParams.rubrika + '/' + $routeParams.tag)
                .success(function(data) {
                    $rootScope.postTagsHeader = data;
                    //$rootScope.meta = data.meta;
                });

            endPosts = false;
            $scope.reddit = new Reddit($routeParams.rubrika, '', 0, 'postsByTags', $routeParams.tag);




            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=authors').success(function(data) {
                $rootScope.meta = data;
                $rootScope.currentRubrika = $routeParams.rubrika;
                $rootScope.currentLink = 'postTags';
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])


    .controller('getAllAuthors', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            //endPosts = false;
            //$scope.reddit = new Reddit('', '', 0, 'allauthors');

            $http.get(serviceBase + 'getallauthors')
                .success(function(data) {
                    $scope.autorsList = data;
                    //$rootScope.meta = data.meta;
                });


            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=authors').success(function(data) {
                $rootScope.meta = data;
                $rootScope.currentLink = '';
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }]);


yii2AngApp_film.factory('Reddit', function($http) {
    var Reddit = function($rubrikaUrl, $usedIdsPosts, $useElasticBlock, $mode, $tagUrl) {
        this.items = [];
        this.busy = false;
        this.usedIdsPosts = $usedIdsPosts;
        this.useElasticBlock = $useElasticBlock;
        this.page = 0;
        this.end = false;
        this.rubrikaUrl = $rubrikaUrl;
        this.tagUrl = $tagUrl;
        this.metaTitle = '';
        this.mode = $mode;
        this.pictureDayData = [];
    };

    Reddit.prototype.nextPage = function($page, $usedIdsPosts, $useElasticBlock) {
        if (this.busy) return;
        this.busy = true;
        this.page = $page + 1;

        if (endPosts == false){

            if (this.mode == 'blogs'){
                var url = serviceBase + "getblogs/" + this.rubrikaUrl + "/" + this.page + '?used_block=' + $usedIdsPosts + '&use_elastic_block=' + $useElasticBlock;
            }
            else if(this.mode == 'authors'){
                var url = serviceBase + "getauthorsblogs/" + this.rubrikaUrl + "/" + this.page + '?used_block=' + $usedIdsPosts + '&use_elastic_block=' + $useElasticBlock;
            }
            else if(this.mode == 'leaders'){
                var url = serviceBase + "getleaders/page" + this.page;
            }
            else if(this.mode == 'allauthors'){
                var url = serviceBase + "getallauthors/page" + this.page;
            }
            else if (this.mode == 'postsByTags'){

                var url = serviceBase + "getposttag/" + this.rubrikaUrl + "/" + this.tagUrl + "/" + this.page + '?used_block=' + $usedIdsPosts + '&use_elastic_block=' + $useElasticBlock;
            }


            $http.get(url).success(function(data) {

                if (pictureDayEmpty == false){
                    this.pictureDayData.push(data.pictureDay);
                    pictureDayEmpty = true;
                }

                if (angular.equals({}, data.posts) == false){
                    var postsData = data.posts;
                    this.usedIdsPosts = data.usedIdsPosts;
                    this.page = data.currentPage;

                    if (this.mode == 'allauthors'){
                        this.items.push(postsData);

                    }
                    else{
                        for (var key in postsData) {
                            for (var block in postsData[key]) {
                                for (var i = 0; i < postsData[key][block].length; i++) {
                                    this.items.push(postsData[key][block][i]);
                                }
                            }
                        }
                    }
                    this.busy = false;
                }
                else{
                    endPosts = true;
                }
                this.metaTitle = data.metaTitle;
                this.useElasticBlock = data.useElasticBlock

                this.busy = false;
            }.bind(this));

        }
        else{
            this.busy = false;
        }
    };

    return Reddit;
});

function initGallery() {
    var lastActiveIndex;
    $('.fotorama').on('fotorama:showend ', function(e, fotorama) {
        $('.fotorama-holder .numbering').html((fotorama.activeIndex + 1) + '/' + fotorama.size);
        var _cotent = $('.fotorama__active .fotorama__html div p').html();
        $('.fotorama-holder .info p').html(_cotent);
    });

    var fotorama = angular.element(document.querySelector(".fotorama"));

    //var fotorama = $('.fotorama').data('fotorama');
    console.log(fotorama);
    $('.fotorama-holder .next').click(function(event) {
        event.preventDefault();
        fotorama.show('>');
    });
    $('.fotorama-holder .prev').click(function(event) {
        event.preventDefault();
        fotorama.show('<');
    });
    //$('.fotorama-holder .numbering').html((fotorama.index + 1) + '/' + fotorama.size);
    var _cotent = $('.fotorama__active .fotorama__html div p').html();
    $('.fotorama-holder .info p').html(_cotent);
}