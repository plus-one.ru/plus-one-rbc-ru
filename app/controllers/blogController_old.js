'use strict';

var endPosts = false;
var film = '';

yii2AngApp_film.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/blog/', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .when('/', {
            templateUrl: 'app/views/index.html',
            controller: 'index'
        })
        .when('/blog/:rubrika', {
            templateUrl: 'app/views/index.html',
            controller: 'getRubrika',
        })
        .when('/blog/:rubrika/:post', {
            templateUrl: 'app/views/post.html',
            controller: 'getPost',
        })
        .when('/author/', {
            templateUrl: 'app/views/allauthors.html',
            controller: 'getAllAuthors',
        })
        .when('/author/:rubrika', {
            templateUrl: 'app/views/index.html',
            controller: 'getAuthor',
        })
        .otherwise({
            redirectTo: '/'
        });

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
}]);





yii2AngApp_film.controller('index', ['$scope', '$http', 'services', '$sce', 'Reddit', '$rootScope',
    function($scope,$http,services, $sce, Reddit, $rootScope) {

        endPosts = false;
        $scope.reddit = new Reddit('main', '', 0, 'blogs');

        $http.get(serviceBase + 'services/?page=main').success(function(data) {
            $rootScope.meta = data;
        });

        // безопасный вывод html
        $scope.textDangerousSnippet = function($text) {
            return $sce.trustAsHtml($text);
        };
    }])
    .controller('getRubrika', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            endPosts = false;
            $scope.reddit = new Reddit($routeParams.rubrika, '', 0, 'blogs');

            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika).success(function(data) {
                $rootScope.meta = data;
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])

    .controller('getPost', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {


            $http.get(serviceBase + 'getpost/?post_url=' + $routeParams.post)
                .success(function(data) {
                    $scope.post = data;
                    $rootScope.meta = data.meta;
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }])

    .controller('getAuthor', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
    function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

        endPosts = false;
        $scope.reddit = new Reddit($routeParams.rubrika, '', 0, 'authors');

        $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=authors').success(function(data) {
            $rootScope.meta = data;
        });

        // безопасный вывод html
        $scope.textDangerousSnippet = function($text) {
            return $sce.trustAsHtml($text);
        };
    }])

    .controller('getAllAuthors', ['$scope', '$http', 'services', '$sce', 'Reddit', '$routeParams', '$rootScope',
        function($scope,$http,services, $sce, Reddit, $routeParams, $rootScope) {

            //endPosts = false;
            //$scope.reddit = new Reddit('', '', 0, 'allauthors');

            $http.get(serviceBase + 'getallauthors')
                .success(function(data) {
                    $scope.autorsList = data;
                    $rootScope.meta = data.meta;
                });


            $http.get(serviceBase + 'services/?page=' + $routeParams.rubrika + '&type=authors').success(function(data) {
                $rootScope.meta = data;
            });

            // безопасный вывод html
            $scope.textDangerousSnippet = function($text) {
                return $sce.trustAsHtml($text);
            };
        }]);


yii2AngApp_film.factory('Reddit', function($http) {
    var Reddit = function($rubrikaUrl, $usedIdsPosts, $useElasticBlock, $mode) {
        this.items = [];
        this.busy = false;
        this.usedIdsPosts = $usedIdsPosts;
        this.useElasticBlock = $useElasticBlock;
        this.page = 0;
        this.end = false;
        this.rubrikaUrl = $rubrikaUrl;
        this.metaTitle = '';
        this.mode = $mode
    };

    Reddit.prototype.nextPage = function($page, $usedIdsPosts, $useElasticBlock) {
        if (this.busy) return;
        this.busy = true;
        this.page = $page + 1;

        if (endPosts == false){

            if (this.mode == 'blogs'){
                var url = serviceBase + "getblogs/" + this.rubrikaUrl + "/" + this.page + '?used_block=' + $usedIdsPosts + '&use_elastic_block=' + $useElasticBlock;
            }
            else if(this.mode == 'authors'){
                var url = serviceBase + "getauthorsblogs/" + this.rubrikaUrl + "/" + this.page + '?used_block=' + $usedIdsPosts + '&use_elastic_block=' + $useElasticBlock;
            }
            else if(this.mode == 'allauthors'){
                var url = serviceBase + "getallauthors/page" + this.page;
            }


            $http.get(url).success(function(data) {
                if (angular.equals({}, data.posts) == false){
                    var postsData = data.posts;
                    this.usedIdsPosts = data.usedIdsPosts;
                    this.page = data.currentPage;

                    if (this.mode == 'allauthors'){
                        this.items.push(postsData);

                    }
                    else{
                        for (var key in postsData) {
                            for (var block in postsData[key]) {
                                for (var i = 0; i < postsData[key][block].length; i++) {
                                    this.items.push(postsData[key][block][i]);
                                }
                            }
                        }
                    }



                    this.busy = false;
                }
                else{
                    endPosts = true;
                }
                this.metaTitle = data.metaTitle;
                this.useElasticBlock = data.useElasticBlock

                this.busy = false;
            }.bind(this));

        }
        else{
            this.busy = false;
        }
    };

    return Reddit;
});