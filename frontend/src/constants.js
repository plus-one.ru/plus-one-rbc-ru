// import banner from './img/banner.png';
// import content from './img/content_1-2.PNG';
// import content1 from './img/content_1-3.png';
// import content2 from './img/content_2-3.png';
// import content3 from './img/content_3-3.png';

// export const DOMAIN = 'https://dev.plus-one.ru';
let currentHostName = window.location.hostname;
if (currentHostName === 'localhost') currentHostName = 'rbc-dev.plus-one.ru'
export const DOMAIN = `//${currentHostName}`;
export const API_V1 = 'adminv2/api/v1'

// export const CONTENT_ROW_TYPES = [
// 	{ name: '1/1', id: 'one' },
// 	{ name: '1/2', id: 'two' },
// 	{ name: '1/3', id: 'three' },
// 	{ name: 'Баннер', id: 'banner' },
// 	{ name: 'Тег', id: 'tag' },
// ]

// export const CONTENT_TYPES = [
// 	{ id: 1, name: 'Обычный пост' },
// 	{ id: 2, name: 'Цитата недели' },
// 	{ id: 3, name: 'Мем недели' },
// 	{ id: 4, name: 'DIY' },
// 	{ id: 5, name: 'Факт недели' },
// 	{ id: 6, name: 'Растяжка' },
// ]

// export const PRESETS_MOCK = [
// 	{ id: 1, name: 'Gray', background: 'lightgray', color: '#000', fontFamily: 'default' },
// 	{ id: 2, name: 'Yellow', background: 'orange', color: '#000', fontFamily: 'default' },
// 	{ id: 3, name: 'Blue', background: 'blue', color: 'red', fontFamily: 'default' },
// 	{ id: 4, name: 'Pink', background: 'pink', color: '#000', fontFamily: 'default' },
// 	{ id: 5, name: 'Green', background: 'green', color: 'blue', fontFamily: 'default' },
// ];


export const CONTENT_MOCK = [
	{
		rowNumber: 1,
		rowType: '1_2',
		rowContent: [
			{
				block: "1/2",
				date: "15.06.2018",
				font: "Aeroport",
				format: "blue",
				header: "Лид",
				iconCode: "community",
				iconName: "Общество",
				id: 2696,
				image: "https://dev.plus-one.ru/files/blogposts/2696-1_1.jpg",
				name: "Текст цитаты",
				postFormat: { backgroundColor: "#eaeaea", fontColor: "#003fcc" },
				backgroundColor: "#eaeaea",
				fontColor: "#003fcc",
				typePost: "citate",
				url: "https://dev.plus-one.ru/blog/community/tekst-citaty",
			}
		]
	},
	{
		rowNumber: 2,
		rowType: "1_3",
		rowContent: [
			{
				date: "15.02.2019",
				font: "Geometric Sans Serif",
				header: "Как справится с предновогодним стрессом на работе?",
				iconCode: "community",
				iconName: "Общество",
				id: 2711,
				image: "https://dev.plus-one.ru/files/blogposts/2711-1_3.jpg",
				name: "Как справится с предновогодним стрессом на работе?",
				postFormat: { backgroundColor: null, fontColor: "#ffffff" },
				typePost: "diypost",
				url: "https://dev.plus-one.ru/blog/community/kak-spravitsya-s-prednovogodnim-stressom-na-rabote",
			},
			{
				date: "19.02.2019",
				font: "Aeroport",
				header: "факт недели",
				iconCode: "economy",
				iconName: "Экономика",
				id: 2713,
				imageUrl: null,
				name: "факт недели",
				postFormat: { backgroundColor: "#ffbc00", fontColor: "#0037cb" },
				typePost: "factday",
				url: "https://dev.plus-one.ru/blog/economy/fakt-nedeli",
			}
		],
	},
	{
		rowNumber: 3,
		rowType: "1_1",
		rowContent: [
			{
				date: "15.02.2019",
				font: "Geometric Sans Serif",
				header: "Магловские прототипы магических животных &mdash; в подборке +1",
				iconCode: "ecology",
				iconName: "Экология",
				id: 2706,
				imageUrl: null,
				name: "Где обитают реальные твари",
				postFormat: { backgroundColor: "#ffbc00", fontColor: "#000000" },
				typePost: "post",
				url: "https://dev.plus-one.ru/blog/ecology/gde-obitayut-realnye-tvari",
			},
		]
	},
	{
		rowNumber: 4,
		rowType: "ticker",
		value: 'sadfasdfsadfasdf',
	},
	{
		rowNumber: 5,
		rowType: "banner",
		rowContent: [],
	}
]

