import React from 'react';

export const communityIcon = (iconFontColor, iconBackgroundColor) => (
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style={{ width: '30px', height: '30px', marginRight: '10px' }}>
		<title>Asset 15</title>
		<g id="Layer_2" data-name="Layer 2">
			<g id="background">
				<path style={{ fill: iconBackgroundColor }} d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z" />
				<path style={{ fill: iconFontColor }} d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z" />
			</g>
		</g>
	</svg>
)

export const economyIcon = (iconFontColor, iconBackgroundColor) => (
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" style={{ width: '30px', height: '30px', marginRight: '10px' }}>
		<title>Asset 3</title>
		<g id="Layer_2" data-name="Layer 2">
			<g id="background">
				<circle style={{ fill: iconBackgroundColor }} cx="15" cy="15" r="15" />
				<polygon style={{ fill: iconFontColor }} points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29" />
			</g>
		</g>
	</svg>
)

export const ecologyIcon = (iconFontColor, iconBackgroundColor) => (
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style={{ width: '30px', height: '30px', marginRight: '10px' }}>
		<title>Asset 14</title>
		<g id="Layer_2" data-name="Layer 2">
			<g id="background">
				<path style={{ fill: iconBackgroundColor }} d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z" />
				<path style={{ fill: iconFontColor }} d="M26.91,13.82c-2.38,0-5.29,2.58-6.88,4.17-1.58-1.59-4.49-4.17-6.89-4.17a6.17,6.17,0,0,0,0,12.34c2.4,0,5.31-2.57,6.89-4.17,1.59,1.6,4.5,4.17,6.88,4.17a6.17,6.17,0,1,0,0-12.34ZM13.14,23.41a3.42,3.42,0,0,1,0-6.83c2,0,3.4,1.8,5,3.42C16.54,21.61,15.09,23.41,13.14,23.41Zm13.77,0c-1.89,0-3.38-1.8-5-3.41,1.61-1.62,3.33-3.42,5-3.42a3.42,3.42,0,1,1,0,6.83Z" />
			</g>
		</g>
	</svg>
)
