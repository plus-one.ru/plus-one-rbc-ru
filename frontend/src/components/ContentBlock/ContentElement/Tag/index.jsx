import React, { Component } from 'react';
import { communityIcon, economyIcon, ecologyIcon } from './icons';

class Tag extends Component {

	setIcon = (type, backgroundColor, fontColor) => {
		let iconBackgroundColor = fontColor;
		let iconFontColor = backgroundColor;

		switch (type) {
			case 'community': return communityIcon(iconFontColor, iconBackgroundColor);
			case 'economy': return economyIcon(iconFontColor, iconBackgroundColor);
			case 'ecology': return ecologyIcon(iconFontColor, iconBackgroundColor);
			default: return null
		}
	}

	render() {
		const { iconType, color, background, text = '', classes, styles } = this.props;
		return (
			<div className={classes} style={{ display: 'flex', alignItems: 'center', minHeight: "30px", fontFamily: 'MullerMedium', fontSize: '13px', ...styles }}>
				{this.setIcon(iconType, background, color)}
				<div>{text ? text.toUpperCase() : ''}</div>
			</div>
		);
	}
}

export default Tag;
