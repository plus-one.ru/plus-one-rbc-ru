import React, { Component } from 'react';

import Tag from '../Tag';

class DefaultPostPic extends Component {

	render() {
		const { rowType, contentPic, importedPresetStyle, backgroundImage, clearText } = this.props;
		const stylesPostPic = { ...importedPresetStyle, color: 'white', background: 'black' };

		const backGround_3 = contentPic && contentPic.postFormat && contentPic.postFormat.backgroundColor ? contentPic.postFormat.backgroundColor : '';
		const fontColor_3 = contentPic && contentPic.postFormat && contentPic.postFormat.fontColor ? contentPic.postFormat.fontColor : '';

		switch (rowType) {
			case '1_1': return (
				<div style={{ ...stylesPostPic, ...backgroundImage, backgroundPosition: '50% 50%', height: "480px", fontFamily: 'Aeroport-Bold' }} className="newsBl">
					<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...stylesPostPic} />
					<div className="newsMain" style={{ overflow: "hidden" }}>
						{contentPic.name && <p className="newsMain_title" style={{ fontSize: "66px", lineHeight: '66px' }}>{clearText(contentPic.name)}</p>}
						{contentPic.header && <p className="newsMain_newsMain_subtitle" style={{ fontSize: "24px", fontFamily: 'MullerRegular' }}>{clearText(contentPic.header)}</p>}
					</div>
					{contentPic.date ? <p className="articleBottom_today">{clearText(contentPic.date)}</p> : <div />}
				</div>
			);

			case '1_2': return (
				<div style={{
					...stylesPostPic, ...backgroundImage, paddingBottom: '20px', backgroundSize: 'cover', backgroundPosition: '50% 50%', maxHeight: '365px', overflow: 'hidden', height: '100%', display: 'flex',
					flexDirection: 'column', justifyContent: 'space-between', padding: '20px 30px 25px 30px',
				}}>
					<div>
						<div className="blTypeStringTop" style={{ padding: "0px", minHeight: "30px" }}>
							<div className="blTypeStringH">
								<div className="blTypeStringH_logo">
									<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...stylesPostPic} />
								</div>
							</div>
						</div>
						<div >
							{contentPic.name && <p style={{ maxHeight: "105px", overflow: "hidden", lineHeight: "34px", fontFamily: 'MullerBold' }} className="blHalfDescr_title" >{clearText(contentPic.name)}</p>}
							{contentPic.header && <p style={{ lineHeight: "24px" }} className="blHalfDescr_subtitle">{clearText(contentPic.header)}</p>}
						</div>
					</div>
					<div className="blTypeStringDays" style={{ marginLeft: "0px" }}>{clearText(contentPic.date)}</div>
				</div>
			)

			case '1_3': return (
				<div className="article" style={{ ...stylesPostPic }}>
					<div className="articleTop" style={{ backgroundImage: `url(${contentPic.imageUrl})`, backgroundSize: 'cover', backgroundPosition: '50% 50%', margin: 0 }}>
						<div className="articleTop_descr">
							<div className="articleTop_logo"></div>
							<Tag classes="articleTop_header" text={contentPic.iconName} iconType={contentPic.iconCode} {...stylesPostPic} />
						</div>
					</div>
					<div className="articleBottom" style={{ background: backGround_3 || '#eaeaea', color: fontColor_3 || 'black' }}>
						<p className="articleMem_title" style={{ overflow: "hidden", fontSize: "22px", lineHeight: "30px", marginTop: "0px", marginBottom: "20px", fontFamily: 'MullerBold' }}>{clearText(contentPic.name)}</p>
						{contentPic.header && <p className="articleBottom_subtitle" style={{ maxHeight: "63px", minHeight: "63px", lineHeight: "30px", overflow: "hidden" }}>{clearText(contentPic.header)}</p>}
						{contentPic.date && <p className="articleBottom_today">{clearText(contentPic.date)}</p>}
					</div>
				</div>
			)

			default: return null;
		}


	}
}

export default DefaultPostPic;
