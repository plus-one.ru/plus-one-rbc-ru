import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Tag from '../Tag';

class FactDay extends Component {

	// уменьшение шрифта, если текст не вписывается в блок
	myScale = (parent, text, rowType) => {
		const step = 0.5;
		const minFontSize = 10;

		const parentHeight = parent.offsetHeight;
		const parentWidth = parent.offsetWidth;

		const textHeight = text.offsetHeight;
		const textWidth = text.offsetWidth;

		const oldFontSize = parseInt(text.style.fontSize, 10);
		const newFontSize = `${oldFontSize - step}px`;

		// если ширина или высота блока с текстом больше родительского
		switch (rowType){
			case '1_1': if ((textHeight > parentHeight - 90 || textWidth > parentWidth - 60) && oldFontSize > minFontSize){
				text.style.fontSize = newFontSize;
				text.style.lineHeight = newFontSize;
				// this.myScale(parent, text)
				setTimeout( () => { this.myScale(parent, text, rowType); }, 0)
			} break;
			case '1_2': if ((textHeight > parentHeight - 70 || textWidth > parentWidth - 60) && oldFontSize > minFontSize){
				text.style.fontSize = newFontSize;
				text.style.lineHeight = newFontSize;
				// this.myScale(parent, text)
				setTimeout( () => { this.myScale(parent, text, rowType); }, 0)
			}; break;

			case '1_3': if ((textHeight > parentHeight - 125 || textWidth > parentWidth - 58) && oldFontSize > minFontSize){
				text.style.fontSize = newFontSize;
				text.style.lineHeight = newFontSize;
				// this.myScale(parent, text)
				setTimeout( () => { this.myScale(parent, text, rowType); }, 0)
			} break;
			default: return;
		}
	}

	componentDidMount() {
		this.afterRender()
	}
	componentDidUpdate() {
		this.afterRender()
	}

	afterRender = () => {
		document.fonts.ready.then(() => {
			const { rowType } = this.props;
			const parent = ReactDOM.findDOMNode(this.parentRef_3);
			const text = ReactDOM.findDOMNode(this.textRef_3);

			if (parent && text) this.myScale(parent, text, rowType);
		})
	}

	render() {
		const { rowType, contentPic, importedPresetStyle, clearText } = this.props;

		const stylesName_1 = {
			display: 'flex', 
			justifyContent: 'center', 
			alignItems: 'center', 
			minHeight: '100px', 
			fontSize: '42px', 
			lineHeight: '43px', 
			padding: '0 85px', 
			fontFamily: 'GeometricSansSerifv1', 
		}

		switch (rowType) {
			case '1_1': return (
				<div ref={(e) => this.parentRef_3 = e} className="quoteBlWrap" style={{ ...importedPresetStyle, height: "210px", maxHeight: "210px" }}>
					<div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
						<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
						{contentPic.date && <p className="articleBottom_today">{clearText(contentPic.date)}</p>}
					</div>
					{contentPic.name && <p ref={(e) => this.textRef_3 = e} className="mainQuote" style={stylesName_1}>{clearText(contentPic.name)}</p>}
				</div >
			);

			case '1_2': return (
				<div ref={(e) => this.parentRef_3 = e} className="quoteBlWrap" style={{
					...importedPresetStyle, height: '365px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between'
				}}>
					<div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', minHeight: '42px' }}>
						<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
						{contentPic.date && <p className="articleBottom_today">{clearText(contentPic.date)}</p>}
					</div>
					{contentPic.name && <p ref={(e) => this.textRef_3 = e} style={{ fontSize: '42px', lineHeight: "43px", fontFamily: "GeometricSansSerifv1" }} className="mainQuote">{clearText(contentPic.name)}</p>}
					<div></div>
				</div>
			)

			case '1_3': return (
				<div ref={(e) => this.parentRef_3 = e} className="quoteBlWrap" style={{
					...importedPresetStyle, height: '510px', margin: '0', textAlign: 'center', display: 'flex', flexDirection: 'column', alignItems: 'center',
					justifyContent: 'space-between', paddingLeft: '29px', paddingRight: '29px',
				}}>
					<Tag iconType={contentPic.iconCode} text={contentPic.iconName} styles={{marginBottom: '20px'}} {...importedPresetStyle} />
					<div style={{ maxHeight: '397px' }}>
						{contentPic.name && <p ref={(e) => this.textRef_3 = e} style={{ fontSize: '38px', lineHeight: "37px", fontFamily: 'GeometricSansSerifv1' }} className="mainQuote">{clearText(contentPic.name)}</p>}
					</div>
					{contentPic.date && <p className="articleBottom_today">{clearText(contentPic.date)}</p>}
				</div>
			)

			default: return null;
		}


	}
}

export default FactDay;
