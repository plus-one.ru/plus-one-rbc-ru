import React, { Component } from 'react';

import Tag from '../Tag';

class DefaultPost extends Component {

	render() {
		const { rowType, contentPic, importedPresetStyle, clearText } = this.props;
		const needUppercase = importedPresetStyle.fontFamily === 'Geometric';

		switch (rowType) {
			case '1_1': return (
				<div style={{ ...importedPresetStyle, height: "480px" }} className="newsBl">
					<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
					<div className="newsMain" style={{ overflow: "hidden" }}>
						{contentPic.name && <p style={{ fontSize: "66px", lineHeight: "66px", marginBottom: "20px", textTransform: needUppercase ? 'uppercase' : 'none' }} className="newsMain_title">{clearText(contentPic.name)}</p>}
						{contentPic.header && <p style={{ fontSize: "24px", lineHeight: "32px", fontFamily: 'MullerRegular' }} className="newsMain_newsMain_subtitle">{clearText(contentPic.header)}</p>}
					</div>
					{contentPic.date ? <p className="articleBottom_today">{clearText(contentPic.date)}</p> : <div />}
				</div>
			);

			case '1_2': return (
				<div style={{
					...importedPresetStyle, padding: "18px 30px 30px 30px", display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '365px'
				}}>
					<div>
						<div className="blTypeStringTop" style={{ padding: "0" }}>
							<div className="blTypeStringH">
								<div className="blTypeStringH_logo">
									<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
								</div>
							</div>
						</div>
						<div className="blHalfDescr" style={{ padding: "0" }}>
							{contentPic.name && <p style={{ lineHeight: "32px", fontSize: '30px', textTransform: needUppercase ? 'uppercase' : 'none' }} className="blHalfDescr_title">{clearText(contentPic.name)}</p>}
							{contentPic.header && <p className="blHalfDescr_subtitle">{clearText(contentPic.header)}</p>}
						</div>
					</div>
					<div className="blTypeStringDays" style={{ marginLeft: "0" }}>{clearText(contentPic.date)}</div>
				</div >
			)

			case '1_3': return (
				<div className="article center" style={{ ...importedPresetStyle }}>
					<div className="articleDescrW noPic">
						<div>
							<div style={{ marginBottom: '30px' }}>
								<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
							</div>
							<div>
								{contentPic.name && <p style={{ textTransform: needUppercase ? 'uppercase' : 'none' }} className="articleDefaultNoPic">{clearText(contentPic.name)}</p>}
								{contentPic.header && <p className="articleBottom_subtitle">{clearText(contentPic.header)}</p>}
							</div>
						</div>
						<div>
							{contentPic.date && <p className="articleBottom_today">{clearText(contentPic.date)}</p>}
						</div>
					</div>
				</div>
			)

			default: return null;
		}


	}
}

export default DefaultPost;
