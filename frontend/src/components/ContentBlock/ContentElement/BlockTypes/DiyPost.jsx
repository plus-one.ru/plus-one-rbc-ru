import React, { Component } from 'react';

import Tag from '../Tag';

class DiyPost extends Component {

	render() {
		const { rowType, contentPic, backgroundImage, importedPresetStyle, clearText } = this.props;

		switch (rowType) {
			case '1_1': return (
				<div>
					<div className="article" style={{ maxWidth: 'inherit', color: '#fff' }}>
						<div className="articleTop fullHeight" style={{ ...backgroundImage, padding: "20px 30px 30px 20px", backgroundPosition: '50% 50%' }}>
							<div className="articleTop_descr">
								<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
							</div>
							{contentPic.name && <p className="articleMem_title" style={{ fontFamily: 'MullerBold' }}>{clearText(contentPic.name)}</p>}
						</div>
					</div>
				</div>
			);

			case '1_2': return (
				<div>
					<div className="article" style={{ maxWidth: 'inherit', color: '#fff', height: '365px', }}>
						<div className="articleTop fullHeight" style={{ ...backgroundImage, padding: "20px 30px 17px 20px", backgroundPosition: '50% 50%' }}>
							<div className="articleTop_descr">
								<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
							</div>
							{contentPic.name && <p className="articleMem_title" style={{ fontFamily: 'MullerBold' }}>{clearText(contentPic.name)}</p>}
						</div>
					</div>
				</div>
			)

			case '1_3': return (
				<div className="article" style={{ color: '#fff' }}>
					<div className="articleTop fullHeight" style={{ ...backgroundImage, padding: "17px 30px 17px 20px", backgroundPosition: '50% 50%' }}>
						<div className="articleTop_descr">
							<Tag classes="blTypeStringH" iconType={contentPic.iconCode} text={contentPic.iconName} {...importedPresetStyle} />
						</div>
						{contentPic.name && <p className="articleMem_title" style={{ fontFamily: 'MullerBold' }}>{clearText(contentPic.name)}</p>}
					</div>
				</div>
			)

			default: return null;
		}


	}
}

export default DiyPost;
