import React, { Component } from "react";

import Tag from "../Tag";

class Citate extends Component {
  render() {
    const { rowType, contentPic, importedPresetStyle, clearText } = this.props;

    switch (rowType) {
      case "1_1":
        return (
          <div className="quoteBlWrap" style={{ ...importedPresetStyle, display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }} >
              <Tag
                text={contentPic.iconName}
                iconType={contentPic.iconCode}
                {...importedPresetStyle}
              />
              {contentPic.date && (
                <p className="articleBottom_today">
                  {clearText(contentPic.date)}
                </p>
              )}
            </div>
            <div>
              {contentPic.name && (
                <p className="mainQuote" style={{ lineHeight: '39px' }}>{clearText(contentPic.name)}</p>
              )}
              {contentPic.header && (
                <p className="qouteDescr">{clearText(contentPic.header)}</p>
              )}
            </div>
          </div>
        );

      case "1_2":
        return (
          <div className="quoteBlWrap"
            style={{ ...importedPresetStyle, height: "365px", width: "560px", display: "flex", flexDirection: "column", justifyContent: "space-between" }}
          >
            <div
              style={{ display: "flex", justifyContent: "space-between", alignItems: "center", marginBottom: "40px", minHeight: "30px" }}
            >
              <Tag
                text={contentPic.iconName}
                iconType={contentPic.iconCode}
                {...importedPresetStyle}
              />
              {contentPic.date && (
                <p className="articleBottom_today">
                  {clearText(contentPic.date)}
                </p>
              )}
            </div>
            {contentPic.name && (
              <p style={{ fontSize: "34px" }} className="mainQuote">
                {clearText(contentPic.name)}
              </p>
            )}
            <p className="qouteDescr">{clearText(contentPic.header)}</p>
          </div>
        );

      case "1_3":
        return (
          <div
            className="quoteBlWrap"
            style={{
              ...importedPresetStyle,
              height: "510px",
              margin: "0",
              textAlign: "center",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between"
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
              }}
            >
              <Tag
                iconType={contentPic.iconCode}
                text={contentPic.iconName}
                {...importedPresetStyle}
              />
              {contentPic.name && (
                <p
                  style={{ fontSize: "26px", lineHeight: "27px", marginTop: "25px", marginBottom: '15px' }}
                  className="mainQuote"
                >
                  {clearText(contentPic.name)}
                </p>
              )}
              {contentPic.header && (
                <p style={{ fontSize: "19px", lineHeight: "24px" }} className="qouteDescr">
                  {clearText(contentPic.header)}
                </p>
              )}
            </div>
            {contentPic.date && (
              <p className="articleBottom_today">
                {clearText(contentPic.date)}
              </p>
            )}
          </div>
        );

      default:
        return null;
    }
  }
}

export default Citate;
