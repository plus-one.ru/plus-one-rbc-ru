import React, { Component } from 'react';

import Button from '../Button';
import DropDown from '../DropDown';
import ContentRow from './ContentRow';

class ContentBlock extends Component {
	setType = (blockId) => (picNum, typeId) => this.props.setType(blockId, picNum, typeId)
	setPreset = (blockId) => (picNum, presetId) => this.props.setPreset(blockId, picNum, presetId)
	setRowType = (blockId) => (contentRowType) => this.props.setRowType(blockId, contentRowType)
	resetPicture = (blockId) => (numOfContent) => this.props.resetPicture(blockId, numOfContent)
	chooseContentModal = (blockId) => (numOfContent) => this.props.chooseContentModal(blockId, numOfContent)
	setStretchRow = (blockId) => (value) => this.props.setStretchRow(blockId, value)

	moveUp = (index) => () => this.props.moveUp(index)
	moveDown = (index) => () => this.props.moveDown(index)
	moveLeft = (index) => (numOfContent) => this.props.moveLeft(index, numOfContent)
	moveRight = (index) => (numOfContent) => this.props.moveRight(index, numOfContent)

	render() {
		const {
			confirmRemove,
			listIndex,
			contentTypes,
			contentRowTypes,
			content,
			presets,
		} = this.props;
		const { id, picture, rowTypeId } = content;

		return (
			<div className="blockWrap">
				<div className="blockNavW">
					<div className="blockNavPos">
						<Button classes="adminHBtn--arrUp" onClickHandler={this.moveUp(listIndex)} />
						<Button classes="adminHBtn--arrDown" onClickHandler={this.moveDown(listIndex)} />
						<DropDown id={rowTypeId} setElement={this.setRowType(id)} list={contentRowTypes} classes="downRow" />
					</div>
					<Button classes="del adminHBtn--white" onClickHandler={() => { confirmRemove(id) }}>Удалить</Button>
				</div>

				<ContentRow
					rowTypeId={rowTypeId}
					picture={picture}
					content={content}
					presets={presets}
					contentTypes={contentTypes}
					setPreset={this.setPreset(id)}
					setType={this.setType(id)}
					resetPicture={this.resetPicture(id)}
					chooseContentModal={this.chooseContentModal(id)}
					moveLeft={this.moveLeft(listIndex)}
					moveRight={this.moveRight(listIndex)}
					setStretchRow={this.setStretchRow(id)}
				/>
			</div>
		);
	}
}

export default ContentBlock;
