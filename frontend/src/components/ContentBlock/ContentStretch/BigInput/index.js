import React, { Component } from 'react';

class BigInput extends Component {

	render() {
		const { value } = this.props;

		return (
			<div className="bigInputContainer">
				<div className="parallaxTxt bigInput">{value || ''}</div>
			</div>
		);
	}
}

export default BigInput;
