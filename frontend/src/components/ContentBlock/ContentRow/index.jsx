import React, { Component } from 'react';

import ContentSelector from '../ContentSelector';
import ContentElement from '../ContentElement';
import ContentStretch from '../ContentStretch';

import defaultBlock from '../../../img/defaulBlock.png';

class ContentRow extends Component {
	banner = null;
	bannerId = `adfox_${Math.random()}`;

	componentDidMount() {
		this.updateBanner()
	}

	componentDidUpdate() {
		this.updateBanner()
	}

	updateBanner = () => {
		if (this.banner && window.Ya && !this.banner.firstChild) {
			window.Ya.adfoxCode.create({
				ownerId: 260854,
				containerId: this.bannerId,
				params: {
					pp: 'g',
					ps: 'cock',
					p2: 'ghgl'
				}
			});
		}
	}

	defaultOne = () => <img style={{ width: '100%', maxHeight: '300px' }} src={defaultBlock} alt="баннер" />;
	defaultTwo = () => <img src={defaultBlock} alt="баннер" />;
	defaultBanner = () => <img style={{ width: '100%', maxHeight: '300px' }} src={defaultBlock} alt="баннер" />

	chooseContentModal = (numOfContent) => () => this.props.chooseContentModal(numOfContent);
	resetPicture = (numOfContent) => () => this.props.resetPicture(numOfContent);
	moveLeft = (numOfContent) => () => this.props.moveLeft(numOfContent);
	moveRight = (numOfContent) => () => this.props.moveRight(numOfContent);

	render() {
		const { rowTypeId, content, setStretchRow } = this.props;
		switch (rowTypeId) {
			case '1_1':
				return (
					<ContentSelector
						classes="blockWrap"
						settedPreset={content.picture ? content.picture["picOne"].preset : null}
						settedContentType={content.picture ? content.picture["picOne"].type : null}
						chooseContentModal={this.chooseContentModal('picOne')}
						resetPicture={this.resetPicture('picOne')}
					>
						<ContentElement
							contentPic={content.picture ? content.picture["picOne"] : {}}
							rowTypeId={rowTypeId}
						/>
					</ContentSelector>
				);

			case '1_2':
				return (
					<div className="blockWrap">
						<div className="blTypeString">
							<div className="blTypeStringHalfWrap">
								<ContentSelector
									classes="blTypeStringHalf"
									settedPreset={content.picture ? content.picture["picOne"].preset : null}
									settedContentType={content.picture ? content.picture["picOne"].type : null}
									chooseContentModal={this.chooseContentModal('picOne')}
									resetPicture={this.resetPicture('picOne')}
									moveRight={this.moveRight('picOne')}
								>
									<ContentElement contentPic={content.picture["picOne"]} rowTypeId={rowTypeId} />
								</ContentSelector>

								<ContentSelector
									classes="blTypeStringHalf"
									settedPreset={content.picture ? content.picture["picTwo"].preset : null}
									settedContentType={content.picture ? content.picture["picTwo"].type : null}
									chooseContentModal={this.chooseContentModal('picTwo')}
									resetPicture={this.resetPicture('picTwo')}
									moveLeft={this.moveLeft('picTwo')}
								>
									<ContentElement contentPic={content.picture["picTwo"]} rowTypeId={rowTypeId} />
								</ContentSelector>
							</div>
						</div>
					</div>
				);

			case '1_3':
				return (
					<div className="blockWrap">
						<div className="blTypeString">
							<div className="blTypeStringThirdWrap blTypeStringHalfWrap">
								<ContentSelector
									classes="blTypeStringThird"
									settedPreset={content.picture ? content.picture["picOne"].preset : null}
									settedContentType={content.picture ? content.picture["picOne"].type : null}
									chooseContentModal={this.chooseContentModal('picOne')}
									resetPicture={this.resetPicture('picOne')}
									moveRight={this.moveRight('picOne')}
								>
									<ContentElement contentPic={content.picture ? content.picture["picOne"] : {}} rowTypeId={rowTypeId} />
								</ContentSelector>

								<ContentSelector
									classes="blTypeStringThird"
									settedPreset={content.picture ? content.picture["picTwo"].preset : null}
									settedContentType={content.picture ? content.picture["picTwo"].type : null}
									chooseContentModal={this.chooseContentModal('picTwo')}
									resetPicture={this.resetPicture('picTwo')}
									moveRight={this.moveRight('picTwo')}
									moveLeft={this.moveLeft('picTwo')}
								>
									<ContentElement contentPic={content.picture ? content.picture["picTwo"] : {}} rowTypeId={rowTypeId} />
								</ContentSelector>

								<ContentSelector
									classes="blTypeStringThird"
									settedPreset={content.picture ? content.picture["picThree"].preset : null}
									settedContentType={content.picture ? content.picture["picThree"].type : null}
									chooseContentModal={this.chooseContentModal('picThree')}
									resetPicture={this.resetPicture('picThree')}
									moveLeft={this.moveLeft('picThree')}
								>
									<ContentElement contentPic={content.picture ? content.picture["picThree"] : {}} rowTypeId={rowTypeId} />
								</ContentSelector>
							</div>
						</div>
					</div>
				);

			case 'banner':
				return (
					<div className="blockWrap" key={this.bannerId}>
						<div ref={(c) => { this.banner = c }} id={this.bannerId}></div>
					</div>
				);


			case 'ticker':
				return (
					<ContentSelector
						classes="blockWrap"
						settedPreset={content.picture ? content.picture["picOne"].preset : null}
						settedContentType={content.picture ? content.picture["picOne"].type : null}
						chooseContentModal={this.chooseContentModal('ticker')}
						resetPicture={this.resetPicture('picOne')}
					>
						<ContentStretch
							onChange={setStretchRow}
							value={content && content.picture ? content.picture["picOne"].stretchValue : ''}
						/>
					</ContentSelector>
				);

			default: return null;
		}
	}
}

export default ContentRow;
