import React, { Component } from 'react';

import Button from '../../Button';

class ContentSelector extends Component {
	state = {
		hovered: true,
	}

	render() {
		const {
			children,
			classes,
			resetPicture,
			chooseContentModal,
			moveLeft,
			moveRight,
		} = this.props;
		const { hovered } = this.state;

		return (
			<div
				className={classes}
				onMouseEnter={() => { this.setState({ hovered: true }) }}
				// onMouseLeave={() => { this.setState({ hovered: false }) }}
				style={{ position: 'relative' }}
			>
				{hovered && (
					<div style={{
						position: 'absolute',
						display: 'flex',
						justifyContent: 'space-between',
						width: '100%',
						padding: '10px',
						zIndex: '100',
					}}>
						<div style={{ display: 'flex' }}>
							{moveLeft && <Button classes="adminHBtn--arrLeft blTypeStringBtn" onClickHandler={moveLeft} />}
							{moveRight && <Button classes="adminHBtn--arrRight blTypeStringBtn" onClickHandler={moveRight} />}
						</div>
						<div style={{ display: 'flex' }}>
							<Button classes="adminHBtn--white blTypeStringBtn" onClickHandler={resetPicture}>Сбросить</Button>
							<Button classes="adminHBtn--white blTypeStringBtn" onClickHandler={chooseContentModal}>Выбрать</Button>
						</div>
					</div>
				)}
				{children}
			</div>
		);
	}
}

export default ContentSelector;
