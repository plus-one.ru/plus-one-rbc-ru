import React, { Component } from 'react';
import { DOMAIN } from '../../constants';

import Button from '../Button';

class Header extends Component {
	render() {
		const { addRow, resetAll, publishContent } = this.props;

		return (
			<header className="adminH">
				<div className="container adminH_container">
					<div className="adminHLeft">
						<p className="adminHLeft_title">главная страница</p>
						<Button classes="adminHBtn--red" onClickHandler={() => { resetAll('all') }}>Сбросить</Button>
						<Button classes="adminHBtn--yellow" style={{ marginRight: '5px' }} onClickHandler={publishContent}>Опубликовать</Button>
						<Button classes="adminHBtn--red" onClickHandler={() => { window.location.href = `${DOMAIN}/adminv2/`; }}>В админку</Button>
					</div>
					<div className="adminHBtn adminHBtn--white" onClick={addRow}>+Добавить строку</div>
				</div>
			</header>
		);
	}
}

export default Header;
