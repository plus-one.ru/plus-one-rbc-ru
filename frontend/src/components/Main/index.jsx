import React, { Component } from 'react';
import uuid4 from 'uuid4';
import cloneDeep from 'clone-deep';

import api from '../../api';
import Header from '../Header';
import Button from '../Button';
import ContentBlock from '../ContentBlock';
import ConfirmModal from '../Modal/ConfirmModal';
import ContentModal from '../Modal/ContentModal';
import { arrayMove, setFieldsOnPic } from '../../utils';

class Main extends Component {
	newRow = {
		picture: {
			picOne: {},
			picTwo: {},
			picThree: {},
		},
		id: uuid4(),
		rowTypeId: null,
		name: '',
		header: '',
	};

	state = {
		publishContentButtonDisabled: false,
		isShowConfirmModal: null,
		isShowContentModal: null,
		contentList: null,
		contentRowTypes: [],
		list: [{ ...this.newRow }],
	};

	componentDidMount() {
		api.getContentRowTypes().then(contentRowTypes => this.setState({ contentRowTypes }));

		// подгрузка ранее сохранённой страницы
		api.uploadPage().then(data => {
			const newList = data.map(row => {
				return {
					id: uuid4(),
					rowTypeId: row.rowType,
					picture: {
						picOne: row.rowContent ? setFieldsOnPic(row.rowContent[0]) : {},
						picTwo: row.rowContent ? setFieldsOnPic(row.rowContent[1]) : {},
						picThree: row.rowContent ? setFieldsOnPic(row.rowContent[2]) : {},
					}
				}
			})
			this.setState({ list: newList });
		})
	}

	// работа со строками контентов
	resetAll = () => this.setState({ list: [], isShowConfirmModal: null });
	addRow = (addToEnd = false) => {
		const list = cloneDeep(this.state.list);

		addToEnd !== 'toEnd' ? list.unshift({ ...this.newRow, id: uuid4() }) : list.push({ ...this.newRow, id: uuid4() });
		this.setState({ list });
	}
	removeRow = () => {
		const list = this.state.list.filter(el => el.id !== this.state.isShowConfirmModal);
		this.setState({ list, isShowConfirmModal: null });
	}
	moveContentUp = (indexList) => {
		if (indexList > 0) {
			const list = cloneDeep(this.state.list);
			arrayMove(list, indexList, indexList - 1);
			this.setState({ list });
		}
	}
	moveContentDown = (indexList) => {
		if (indexList + 1 < this.state.list.length) {
			const list = cloneDeep(this.state.list);
			arrayMove(list, indexList, indexList + 1);
			this.setState({ list });
		}
	}
	moveContentLeft = (indexList, numOfContent) => {
		const { list } = this.state;
		const newList = cloneDeep(this.state.list);

		switch (numOfContent) {
			case 'picTwo': {
				newList[indexList].picture.picOne = list[indexList].picture.picTwo;
				newList[indexList].picture.picTwo = list[indexList].picture.picOne;
				break;
			}
			case 'picThree': {
				newList[indexList].picture.picTwo = list[indexList].picture.picThree;
				newList[indexList].picture.picThree = list[indexList].picture.picTwo;
				break;
			}
			default: return null
		}
		this.setState({ list: newList })
	}
	moveContentRight = (indexList, numOfContent) => {
		const { list } = this.state;
		const newList = cloneDeep(this.state.list);

		switch (numOfContent) {
			case 'picOne': {
				newList[indexList].picture.picTwo = list[indexList].picture.picOne;
				newList[indexList].picture.picOne = list[indexList].picture.picTwo;
				break;
			}
			case 'picTwo': {
				newList[indexList].picture.picThree = list[indexList].picture.picTwo;
				newList[indexList].picture.picTwo = list[indexList].picture.picThree;
				break;
			}
			default: return null
		}
		this.setState({ list: newList })
	}

	// работа с модалкой удаления строки контента
	confirmRemove = (blockId) => {
		if (blockId === 'all') return this.setState({ isShowConfirmModal: 'all' });
		this.setState({ isShowConfirmModal: blockId });
	}
	rejectRemove = () => this.setState({ isShowConfirmModal: null });

	// работа с модалкой выбора контента
	chooseContentModal = (blockId, numOfContent) => {
		const { list } = this.state;
		this.setState({ isShowContentModal: { id: blockId, numOfContent } });
		const blockType = list.find(e => e.id === blockId).rowTypeId;
		api.getContents(blockType).then(contentList => this.setState({ contentList }))
	}
	hideContentModal = () => this.setState({ isShowContentModal: null });
	selectContentFromModal = ({ header, image, name, url, date, postFormat, typePost, font, iconName, iconCode, id, block, format }) => {
		const { isShowContentModal } = this.state;
		let list = null;

		if (block === 'ticker') {
			list = this.state.list.map(el =>
				el.id === isShowContentModal.id
					? {
						...el,
						picture: {
							...el.picture,
							picOne: {
								// ...el.picture[isShowContentModal.numOfContent],
								typePost: typePost || 'ticker',
								stretchValue: name,
								id,
							},
						}
					}
					: el
			);
		} else {
			list = this.state.list.map(el =>
				el.id === isShowContentModal.id
					? {
						...el,
						picture: {
							...el.picture,
							[isShowContentModal.numOfContent]: {
								...el.picture[isShowContentModal.numOfContent],
								name,
								header,
								url,
								date,
								imageUrl: image,
								postFormat,
								typePost: typePost || 'post',
								font,
								iconName,
								iconCode,
								format,
								id,
							},
						}
					}
					: el
			);
		}

		this.setState({ list, isShowContentModal: null });
	}

	setStretchRow = (blockId, value) => {
		let list = cloneDeep(this.state.list);
		list = list.map(el => el.id === blockId ? { ...el, picture: { ...el.picture, picOne: { ...el.picture.picOne, stretchValue: value } } } : el);
		this.setState({ list });
	}

	setContentRowType = (blockId, contentRowTypeId) => {
		const list = this.state.list.map(el => el.id === blockId ? { ...el, rowTypeId: contentRowTypeId } : el);
		this.setState({ list });
	}

	resetPicture = (blockId, numOfContent) => {
		const list = this.state.list.map(el => el.id === blockId ? { ...el, picture: { ...el.picture, [numOfContent]: {} } } : el);
		this.setState({ list })
	}

	publishContent = () => {
		if (this.state.publishContentButtonDisabled === true) { return;}
		this.setState({publishContentButtonDisabled: true})
		const sendArray = this.state.list.map((el, i) => {
			const picsOnRow = Object.keys(el.picture);
			const contents = [];
			for (let i = 0; i < picsOnRow.length; i++) {
				const picId = el.picture[picsOnRow[i]].id;
				if (picId) contents.push(picId);
			}
			if (el.rowTypeId === 'banner') return {
				rowNumber: i + 1,
				rowType: el.rowTypeId,
				rowContent: {
					type: 'banner',
					params: {
						pp: 'g',
						ps: 'cock',
						p2: 'ghgm',
					}
				}
			}

			if (el.rowTypeId === '1_1') contents.length = 1;
			if (el.rowTypeId === '1_2') contents.length = 2;

			return {
				rowNumber: i + 1,
				rowType: el.rowTypeId,
				rowContent: contents,
			}
		})

		api.postContentList(sendArray).then(res => {
			if (res && res.status === 200) {
				// this.resetAll();
				console.log('saved');
				this.setState({publishContentButtonDisabled: false})
			}
		})
	}

	render() {
		const { list, isShowConfirmModal, isShowContentModal, contentList, contentRowTypes } = this.state;

		return (
			<React.Fragment>
				<Header addRow={this.addRow} resetAll={this.confirmRemove} publishContent={this.publishContent}/>

				<main className="mainWrapperAdmin">
					<div className="container">
						<nav className="navW">
							<div className="navHambW"><span className="navHamb"></span></div>
							<div className="navLogo"></div>
							<div className="navSearch"></div>
						</nav>

						<section className="contentW">
							{list.map((el, i) =>
								<React.Fragment key={el.id}>
									<ContentBlock
										content={el}
										listIndex={i}
										lastElement={(i + 1) === list.length}
										contentRowTypes={contentRowTypes}
										confirmRemove={this.confirmRemove}
										resetPicture={this.resetPicture}
										setRowType={this.setContentRowType}
										moveUp={this.moveContentUp}
										moveDown={this.moveContentDown}
										moveLeft={this.moveContentLeft}
										moveRight={this.moveContentRight}
										chooseContentModal={this.chooseContentModal}
										setStretchRow={this.setStretchRow}
									/>
								</React.Fragment>
							)}
						</section>

						<div style={{ display: 'flex', justifyContent: 'center', position: 'relative' }}>
							<Button classes="adminHBtn--white" onClickHandler={() => { this.addRow('toEnd') }}>Добавить в конец</Button>
							<div style={{
								width: '100%',
								position: 'absolute',
								borderTop: '1px solid #fff',
								top: '16px',
								zIndex: '-1',
							}} />
						</div>
					</div>
				</main>

				{isShowConfirmModal &&
					<ConfirmModal
						removeRow={isShowConfirmModal === 'all' ? this.resetAll : this.removeRow}
						rejectRemove={this.rejectRemove}
						title={isShowConfirmModal === 'all' ? 'Сбросить все строки?' : 'Убрать строку?'}
					/>
				}
				{isShowContentModal &&
					<ContentModal
						hideContentModal={this.hideContentModal}
						selectContentFromModal={this.selectContentFromModal}
						contentList={contentList}
					/>
				}

			</React.Fragment>
		);
	}
}

export default Main;
