import React, { Component } from 'react';

import Button from '../../Button';

class ContentModal extends Component {
	state = {
		findValue: '',
	}

	componentWillUnmount() {
		if (this.span) this.span.remove();
	}

	span = document.createElement('span');

	convertHTMLEntity = (text) => {
		return text
			.replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
				this.span.innerHTML = entity;
				return this.span.innerText;
			});
	}

	clearText = (text) => text ? this.convertHTMLEntity(text.replace(/<\/?.+?>/g, '')) : '';

	listRow = (element) => {
		let choosen = element.writersEnabled === "1"
			? (<Button classes="adminHBtn--white chooseContentBtn" onClickHandler={() => {this.props.selectContentFromModal(element)}}>Выбрать</Button>)
			: (<div className="adminHBtn--disabled">Автор неактивен</div>);
		let searchClass = 'searchElemW';
		searchClass += element.writersEnabled === "1" ? '' : ' author--disabled';
		return (
			<li className={searchClass} key={element.id}>
				<p className="searchDescr">{this.clearText(element.name)}</p>
				<p className="searchData">{element.date}</p>
				{choosen}
			</li>
		)
	};

	render() {
		const { hideContentModal, contentList } = this.props;
		const { findValue } = this.state;

		// фильтрация списка по введённому значению
		const newContentList = contentList ? contentList.filter(content => content.name.toLowerCase().includes(findValue.toLowerCase())) : [];

		return (

			<div className="modalAddContentW show">
				<div className="modalAddContenBg" onClick={hideContentModal} />
				<div className="searchWrapper">
					<div className="modalAddContent">
						<div className="searchInputW">
							<input
								type="text"
								className="searchInput"
								placeholder="Введите значене"
								value={findValue}
								onChange={(e) => { this.setState({ findValue: e.target.value }) }}
							/>
						</div>
						<ul className="resultSerch">
							{newContentList.map(el => this.listRow(el))}
						</ul>
					</div>
					<p className="modalContentCloseBtn" onClick={hideContentModal} />
				</div>
			</div>
		);
	}
}

export default ContentModal;
