import React, { Component } from 'react';
import enhanceWithClickOutside from 'react-click-outside'

class DropDown extends Component {
	state = {
		showList: false,
	}

	toggleList = () => this.setState({ showList: !this.state.showList });
	handleClickOutside = () => { if (this.state.showList) this.toggleList() };
	findType = (id) => {
		const type = this.props.list.find(el => el.id === id);
		return type ? type.name : null;
	}

	render() {
		const { showList } = this.state;
		const { id, setElement, classes, defaultValue, list = [] } = this.props;

		return (
			<div className={`blockNavType ${classes}`} onClick={this.toggleList}>
				{defaultValue && !id ? <p>{defaultValue}</p> : <p>{id && list.length ? this.findType(id) : 'Не выбрано'}</p>}
				<div className={`dropDownTypeW ${showList ? 'show' : ''}`}>
					<ul className="dropDownType">
						{list.map(type =>
							<li
								key={type.id}
								className="dropDownType_elem"
								onClick={() => setElement(type.id)}
							>
								{type.name}
							</li>
						)}
					</ul>
				</div>
			</div>
		);
	}
}

export default enhanceWithClickOutside(DropDown);
