import { DOMAIN, API_V1 } from '../constants';
import { checkAuth } from './utils';

const contentsTemp = {};

export const getContents = async (type) => {
	checkAuth()

	if (!contentsTemp[type]) {
		const response = await fetch(`${DOMAIN}/${API_V1}/post?block-type=${type}`);
		contentsTemp[type] = await response.json();
	}

	return contentsTemp[type];
}

export const getContentRowTypes = async () => {
	checkAuth()
	const response = await fetch(`${DOMAIN}/${API_V1}/block`);
	const getContentRowTypes = await response.json();

	return getContentRowTypes;
}

export const postContentList = async (list) => {
	checkAuth()
	const rawResponse = await fetch(`${DOMAIN}/${API_V1}/main-page`, {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(list)
	});

	return rawResponse;
}

export const uploadPage = async () => {
	checkAuth()
	const response = await fetch(`${DOMAIN}/${API_V1}/main-page`);
	const uploadedData = await response.json();

	return uploadedData;
}