import { getContents, getContentRowTypes, postContentList, uploadPage } from './content';
import { checkAuth } from './utils';

export default {
	getContents,
	getContentRowTypes,
	postContentList,
	uploadPage,
	checkAuth,
};