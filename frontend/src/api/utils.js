export const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));

const getCookie = name => {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

export const checkAuth = () => {
  const cookie = getCookie('mainpageconstruct');
  const authorized = cookie === 'true';

  if (!authorized) {
    if (window.location.hostname === 'plus-one.ru') window.location.href = 'http://plus-one.ru/adminv2/';
    if (window.location.hostname === 'rbc-dev.plus-one.ru') window.location.href = 'http://rbc-dev.plus-one.ru/adminv2/';
    console.error('NO AUTH!');
    return false;
  }
  return true;
}