import React, { Component } from 'react';
import './App.scss';
import api from './api';
import Main from './components/Main';

class App extends Component {
  state = {
    authorized: false,
  }

  componentWillMount() {
    this.setState({authorized: api.checkAuth()});
  }

  render() {
    if (!this.state.authorized) return null;
    return (
      <Main />
    );
  }
}

export default App;
