<?PHP

/**
 * Simpla CMS
 * StaticPage class: Отображение статической страницы
 *
 * @copyright 	2009 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон static_page.tpl,
 * для вывода статической страницы
 *
 */
 
//require_once('Widget.class.php');

class PresetFormat
{

    private static $presets = array(
        'post' => array(
            'colors' => array(
                'white' => array(
                    'backgroundColor' => '#ffffff',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcWhite'
                ),
                'orange' => array(
                    'backgroundColor' => '#ffbc00',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#0037cb',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#ff4e98',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00b946',
                    'fontColor' => '#0037cb',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'specproject' => array(
            'colors' => array(
                'white' => array(
                    'backgroundColor' => '#ffffff',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcWhite'
                ),
                'orange' => array(
                    'backgroundColor' => '#ffbc00',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#0037cb',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#ff4e98',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00b946',
                    'fontColor' => '#0037cb',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'citate' => array(
            'colors' => array(
                'white' => array(
                    'backgroundColor' => '#ffffff',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcWhite'
                ),
                'orange' => array(
                    'backgroundColor' => '#ffbc00',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#0037cb',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#ff4e98',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00b946',
                    'fontColor' => '#0037cb',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'diypost' => array(
            'colors' => array(
                'white' => array(
                    'backgroundColor' => '#ffffff',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcWhite'
                ),
                'orange' => array(
                    'backgroundColor' => '#ffbc00',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#0037cb',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#ff4e98',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00b946',
                    'fontColor' => '#0037cb',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'imageday' => array(
            'colors' => array(
                'white' => array(
                    'backgroundColor' => '#ffffff',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcWhite'
                ),
                'orange' => array(
                    'backgroundColor' => '#ffbc00',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#0037cb',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#ff4e98',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00b946',
                    'fontColor' => '#0037cb',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'factday' => array(
            'colors' => array(
                'white' => array(
                    'backgroundColor' => '#ffffff',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcWhite'
                ),
                'orange' => array(
                    'backgroundColor' => '#ffbc00',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#0037cb',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#ff4e98',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00b946',
                    'fontColor' => '#0037cb',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#ffbc00',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
    );

    private static $fonts = array(
        'Krok' => [
            'frontClass' => 'Krok'
        ],
        'CSTM' => [
            'frontClass' => 'cstm'
        ],
        'Geometric' => [
            'frontClass' => 'Geometric'
        ],
        'MullerBold' => [
            'frontClass' => 'MullerBold'
        ],
    );


	/**
	 *
	 * Конструктор
	 *
	 */	
//	function PresetFormat(&$parent)
//	{
//		Widget::Widget($parent);
//	}


	public static function getPresetFormats($typeVisualBlock = 'post')
    {
        return self::$presets[$typeVisualBlock]['colors'];
    }

    public static function getPresetFormatByVisualBlockAndFormat($typeVisualBlock = '', $format = '')
    {
        return self::$presets[$typeVisualBlock]['colors'][$format];
    }

    public static function getPresetFontByKey($key)
    {
        return self::$fonts[$key];
    }

    public static function getPresetFont()
    {
        return self::$fonts;
    }


}
