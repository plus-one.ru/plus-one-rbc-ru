<?php

##########################################################
# File Name: database.php
# Author: Denis Pikusov
# Date Created: 29/01/2005
# Description: Module for working with database
##########################################################

class Database
{
    var $db_name;
    var $host;
    var $user;
    var $pass;
    var $link;
    var $res_id;
    var $error_msg;
    var $errors = array();
    var $queries = array();

    # Constructor
    function Database($database_name, $host_name = "localhost", $user_name = "", $password = "")
    {
        $this->db_name = $database_name;
        $this->host = $host_name;
        $this->user = $user_name;
        $this->pass = $password;
        $this->link = 0;
        $this->res_id = 0;
        $this->error_msg = "";
    }

    # Connecting to the database
    function connect()
    {
        if (!$this->link = mysqli_connect($this->host, $this->user, $this->pass)) {
            $this->error_msg = "Could not connect to the database on $this->host";
            return 0;
        }
        if (!mysqli_select_db($this->link, $this->db_name)) {
            $this->error_msg = "Could not select the $this->db_name database";
            return 0;
        }
        return $this->link;
    }

    # Close the database connection
    function disconnect()
    {
        if (!mysqli_close($this->link)) {
            $this->error_msg = "Could not close the $this->db_name database";
            return 0;
        }
        return 1;
    }

    # Execute the query or queries array
    function query($q)
    {
        $query = new stdClass();
        if ($this->link) {
            $start = microtime(true);
            mysqli_query($this->link, 'SET NAMES utf8mb4;');
            $this->res_id = mysqli_query( $this->link, $q);
            $end = microtime(true);
            $query->sql = $q;
            $query->exec_time = $end - $start;
            // нафиг логирование запросов
            //$this->queries[] = $query;

        } else {
            $this->error_msg = "Could not execute query to $this->db_name database, wrong database link";
            return 0;
        }
        if (!$this->res_id) {
            $this->error_msg = "Could not execute query to $this->db_name database, wrong result id" ;
            $this->errors = array_map(function ($err){ return $err['errno'] .' '. $err['error'];}, mysqli_error_list($this->link));
            return 0;
        }

        return $this->res_id;
    }

    # Returns results array of the query in array of objects
    function results()
    {
        $result = array();
        if (!$this->res_id) {
            $this->error_msg = "Could not execute query to $this->db_name database, wrong result id";
            return 0;
        }
        while ($row = mysqli_fetch_object($this->res_id)) {
            array_push($result, $row);
        }
        return $result;
    }

    # Returns result of the query in array of objects
    function result()
    {
        $result = array();
        if (!$this->res_id) {
            $this->error_msg = "Could not execute query to $this->db_name database, wrong result id";
            return 0;
        }
        $row = mysqli_fetch_object($this->res_id);
        return $row;
    }

    # Returns last inserted id
    function insert_id()
    {
        return mysqli_insert_id($this->link);
    }

    # Returns last inserted id
    function num_rows()
    {
        return mysqli_num_rows($this->res_id);
    }

    # Returns affected rows
    function affected_rows()
    {
        return mysqli_affected_rows($this->link);
    }

    function beginTransaction() {
        return mysqli_begin_transaction($this->link, MYSQLI_TRANS_START_READ_WRITE);
    }

    function commitTransaction() {
        return mysqli_commit($this->link, MYSQLI_TRANS_START_READ_WRITE);
    }
    function rollbackTransaction() {
        return mysqli_rollback($this->link, MYSQLI_TRANS_START_READ_WRITE);
    }
}

?>