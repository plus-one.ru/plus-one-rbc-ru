<?PHP
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

//header('Content-Type: text/html; charset=utf-8');
// Засекаем время
$time_start = microtime(true);

session_start();
require_once('Site.class.php');

// Создаем экземпляр класса Site
// В параметре передается родитель класса, но не в смысле наследования,
// а в смысле вложенности блоков страницы. Класс Site не имеет родителя,
// поэтому передаем null
$site = new Site($a = null);

// Если все хорошо
if($site->fetch() !== false)
{
	// Выводим результат
	print $site->body;
}
else
{
	// Иначе страница об ошибке
	header("http/1.0 404 not found");
	// Подменим переменную, чтобы вывести страницу 404
	$_GET['section'] = '404';
	$site = new Site($a = 0);
	$site->fetch();
	print $site->body;    
}
?>
