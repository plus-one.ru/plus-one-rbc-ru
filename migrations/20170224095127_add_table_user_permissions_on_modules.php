<?php

use Phinx\Migration\AbstractMigration;

class AddTableUserPermissionsOnModules extends AbstractMigration
{
    private $tablename = 'users_admin_permissions';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('user_id', 'integer', ['null' => false])
            ->addColumn('module_id', 'integer', ['null' => false])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
