<?php


use Phinx\Migration\AbstractMigration;

class NewFieldBlogposts extends AbstractMigration
{
    private $tablename = 'blogposts';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('blocked_at', 'datetime', ['default' => null, 'null' => true])
            ->addColumn('blocked_user_id', 'integer', ['default' => null, 'null' => true])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table
            ->removeColumn('blocked_at')
            ->removeColumn('blocked_user_id')
            ->save();
    }
}
