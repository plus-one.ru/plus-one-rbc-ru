<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnState extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
                ->addColumn('state', 'integer', ['default' => null, 'null' => true])
                ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
                ->removeColumn('state')
                ->save();
    }
}
