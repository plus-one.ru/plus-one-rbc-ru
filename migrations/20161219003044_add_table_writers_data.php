<?php

use Phinx\Migration\AbstractMigration;

class AddTableWritersData extends AbstractMigration
{
    private $tablename = 'blogwriters_data';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('post_id', 'integer', ['null' => false])
            ->addColumn('writer_id', 'integer', ['null' => false])
            ->addColumn('years', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('value_to_yeats', 'string', ['limit' => 20, 'null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
