<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnPostTag extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('post_tag', 'integer', ['null' => true, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('post_tag')
            ->save();
    }
}
