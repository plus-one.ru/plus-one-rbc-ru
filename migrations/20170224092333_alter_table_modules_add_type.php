<?php

use Phinx\Migration\AbstractMigration;

class AlterTableModulesAddType extends AbstractMigration
{
    private $tablename = 'modules';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('type_module', 'string', ['limit' => 255, 'null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('type_module')
            ->save();
    }
}
