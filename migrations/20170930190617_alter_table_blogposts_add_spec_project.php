<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddSpecProject extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('spec_project', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('spec_project')
            ->save();
    }
}
