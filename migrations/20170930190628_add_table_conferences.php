<?php

use Phinx\Migration\AbstractMigration;

class AddTableConferences extends AbstractMigration
{
    private $tablename = 'conferences';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('tag', 'integer', ['null' => false])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('external_url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('header', 'text', ['null' => false])
            ->addColumn('image_1_1', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('color', 'string', ['limit' => 6, 'null' => true])
            ->addColumn('enabled', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
