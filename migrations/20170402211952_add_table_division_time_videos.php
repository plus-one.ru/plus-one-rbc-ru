<?php

use Phinx\Migration\AbstractMigration;

class AddTableDivisionTimeVideos extends AbstractMigration
{
    private $tablename = 'division_time_videos';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('parent_video', 'integer', ['null' => false])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('minute', 'string', ['limit'=> 2, 'null' => false, 'default' => '0'])
            ->addColumn('secunde', 'string', ['limit'=> 2, 'null' => false, 'default' => '0'])
            ->addColumn('enabled', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('order_num', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
