<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddColumnImageForBlocks extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('image_1_1', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->addColumn('image_1_1_c', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->addColumn('image_1_2', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->addColumn('image_1_3', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->addColumn('image_1_4', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('image_1_1')
            ->removeColumn('image_1_1_c')
            ->removeColumn('image_1_2')
            ->removeColumn('image_1_3')
            ->removeColumn('image_1_4')
            ->save();
    }
}
