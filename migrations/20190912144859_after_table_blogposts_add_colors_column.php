<?php


use Phinx\Migration\AbstractMigration;

class AfterTableBlogpostsAddColorsColumn extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->changeColumn('block_color', 'string', ['limit' => 18, 'null' => true])
            ->changeColumn('body_color', 'string', ['limit' => 18, 'null' => true])
            ->addColumn('font_color', 'string', ['limit' => 18, 'null' => true])
            ->addColumn('border_color', 'string', ['limit' => 18, 'null' => true])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('font_color')
            ->removeColumn('border_color')
            ->save();
    }
}
