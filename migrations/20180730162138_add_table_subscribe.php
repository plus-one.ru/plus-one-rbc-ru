<?php

use Phinx\Migration\AbstractMigration;

class AddTableSubscribe extends AbstractMigration
{
    private $tablename = 'subscribe';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('email', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('check_hash', 'string', ['limit'=>36, 'null' => false])
            ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
