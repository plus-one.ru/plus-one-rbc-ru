<?php

use Phinx\Migration\AbstractMigration;

class AddTableVideo extends AbstractMigration
{
    private $tablename = 'videos';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('video_url', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('parent_video_post', 'integer', ['null' => false])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('image', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('enabled', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('order_num', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
