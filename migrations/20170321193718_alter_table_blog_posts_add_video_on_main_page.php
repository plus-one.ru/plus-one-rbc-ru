<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddVideoOnMainPage extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('video_on_main', 'string', ['limit' => 255, 'null' => true])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('video_on_main')
            ->save();
    }
}
