<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnThumbnail extends AbstractMigration
{
    private $tablename = 'blogposts';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('thumbnail', 'string', ['limit'=>50, 'null' => true, 'default' => null])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('thumbnail')->save();
    }
}
