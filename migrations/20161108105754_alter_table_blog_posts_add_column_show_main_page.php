<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnShowMainPage extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('show_main_page', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('show_main_page')
            ->save();
    }
}
