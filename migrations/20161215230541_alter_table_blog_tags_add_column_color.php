<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogTagsAddColumnColor extends AbstractMigration
{
    private $tablename = 'blogtags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('color', 'string', ['limit' => 6, 'null' => true])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('color')
            ->save();
    }
}
