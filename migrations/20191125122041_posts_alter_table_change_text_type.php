<?php


use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;


class PostsAlterTableChangeTextType extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {

        $this->table('blogposts')
            ->changeColumn('body', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('lead', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('header', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'null' => false])
            ->changeColumn('name', 'string', [
                'length' => 511,
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'null' => false])
            ->save();
    }

    public function down()
    {
    }

}
