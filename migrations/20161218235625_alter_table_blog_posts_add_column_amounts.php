<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnAmounts extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('amount_to_account', 'string', ['limit' => 20, 'null' => true, 'default' => null])
            ->addColumn('amount_to_displaying', 'string', ['limit' => 20, 'null' => true, 'default' => null])
            ->addColumn('signature_to_sum', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->addColumn('text_body', 'text', ['null' => true, 'default' => null])


            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('amount_to_account')
            ->removeColumn('amount_to_displaying')
            ->removeColumn('signature_to_sum')
            ->removeColumn('text_body')
            ->save();
    }
}
