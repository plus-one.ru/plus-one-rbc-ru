<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAlterColumnAmountToDisplaying extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->changeColumn('amount_to_displaying', 'string', ['limit' => 255])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->changeColumn('amount_to_displaying', 'string', ['limit' => 20])
            ->save();
    }
}
