<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnsAuthorPhotoAndAuthorName extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('citate_author_photo', 'string', ['null' => true])
            ->addColumn('citate_author_name', 'string', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('citate_author_photo')
            ->removeColumn('citate_author_name')
            ->save();
    }
}
