<?php

use Phinx\Migration\AbstractMigration;

class AlterTableDividionVideosChangeColumnMinuteAndSecundeToInteger extends AbstractMigration
{
    private $tablename = 'division_time_videos';

    public function up()
    {
        $this->table($this->tablename)
            ->changeColumn('minute', 'integer', ['limit'=> 2, 'null' => false, 'default' => '0'])
            ->changeColumn('secunde', 'integer', ['limit'=> 2, 'null' => false, 'default' => '0'])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->changeColumn('minute', 'string', ['limit'=> 2, 'null' => false, 'default' => '0'])
            ->changeColumn('secunde', 'string', ['limit'=> 2, 'null' => false, 'default' => '0'])
            ->save();
    }
}
