<?php

use Phinx\Seed\AbstractSeed;

class SettingsSeeder extends AbstractSeed
{
    private $tablename = 'settings';

    public function run()
    {
        $data = array(
            array('name'=>'1cexchange_path_to_import_files', 'name_cyr'=>'Папка для загрузки файлов обмена (импорт)', 'description'=>'', 'value'=>'1CExchange/', 'type_value'=>'urltext', 'group_id'=>'5', 'order_num'=>'0', ),
            array('name'=>'company_name', 'name_cyr'=>'Название компании', 'description'=>'', 'value'=>'Все для стройки', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'1', ),
            array('name'=>'article_path_to_uploaded_images', 'name_cyr'=>'Путь для хранения картинок', 'description'=>'', 'value'=>'files/blog/', 'type_value'=>'urltext', 'group_id'=>'2', 'order_num'=>'1', ),
            array('name'=>'product_path_to_uploaded_images', 'name_cyr'=>'Путь для хранения картинок товара', 'description'=>'', 'value'=>'files/products/', 'type_value'=>'urltext', 'group_id'=>'4', 'order_num'=>'1', ),
            array('name'=>'category_path_to_uploaded_images', 'name_cyr'=>'Путь для хранения картинок раздела каталога', 'description'=>'', 'value'=>'files/categories/', 'type_value'=>'urltext', 'group_id'=>'4', 'order_num'=>'1', ),
            array('name'=>'main_section', 'name_cyr'=>'Главная страница сайта', 'description'=>'', 'value'=>'mainpage', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'2', ),
            array('name'=>'category_image_width', 'name_cyr'=>'Ширина большой картинки раздела каталога', 'description'=>'', 'value'=>'800', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'2', ),
            array('name'=>'article_large_image_width', 'name_cyr'=>'Ширина большой картинки', 'description'=>'', 'value'=>'800', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'2', ),
            array('name'=>'site_name', 'name_cyr'=>'Название сайта', 'description'=>'', 'value'=>'Интернет-магазин все для стройки', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'3', ),
            array('name'=>'category_image_height', 'name_cyr'=>'Высота большой картинки раздела каталога', 'description'=>'', 'value'=>'600', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'3', ),
            array('name'=>'article_large_image_height', 'name_cyr'=>'Высота большой картинки', 'description'=>'', 'value'=>'333', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'3', ),
            array('name'=>'admin_email', 'name_cyr'=>'Email администратора сайта', 'description'=>'', 'value'=>'order@yousun.ru', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'4', ),
            array('name'=>'category_thumbnail_width', 'name_cyr'=>'Ширина маленькой картинки раздела каталога', 'description'=>'', 'value'=>'275', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'4', ),
            array('name'=>'article_small_image_width', 'name_cyr'=>'Ширина маленькой картинки', 'description'=>'', 'value'=>'150', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'4', ),
            array('name'=>'category_thumbnail_height', 'name_cyr'=>'Высота маленькой картинки раздела каталога', 'description'=>'', 'value'=>'275', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'5', ),
            array('name'=>'manager_email', 'name_cyr'=>'Email менеджера магазина', 'description'=>'', 'value'=>'xoma99@mail.ru', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'5', ),
            array('name'=>'article_small_image_height', 'name_cyr'=>'Высота маленькой картинки', 'description'=>'', 'value'=>'75', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'5', ),
            array('name'=>'callback_email', 'name_cyr'=>'Email для получения заказов обратного звонка', 'description'=>'', 'value'=>'info@yousun.ru', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'6', ),
            array('name'=>'products_num', 'name_cyr'=>'Количество товаров на странице', 'description'=>'', 'value'=>'30', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'6', ),
            array('name'=>'brand_path_to_uploaded_images', 'name_cyr'=>'Путь для хранения логотипов брендов', 'description'=>'', 'value'=>'files/brands/', 'type_value'=>'urltext', 'group_id'=>'4', 'order_num'=>'7', ),
            array('name'=>'notify_from_email', 'name_cyr'=>'Email для уведомлений', 'description'=>'', 'value'=>'order@stroyshop.ru', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'7', ),
            array('name'=>'site_url', 'name_cyr'=>'URL адрес сайта', 'description'=>'', 'value'=>'stroyshop.ru', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'8', ),
            array('name'=>'brand_large_image_width', 'name_cyr'=>'Ширина большого логотипа бренда', 'description'=>'', 'value'=>'180', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'8', ),
            array('name'=>'theme', 'name_cyr'=>'Тема (оформление) фронтенда сайта', 'description'=>'', 'value'=>'plusone', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'9', ),
            array('name'=>'brand_large_image_height', 'name_cyr'=>'Высота большого логотипа бренда', 'description'=>'', 'value'=>'180', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'9', ),
            array('name'=>'meta_autofill', 'name_cyr'=>'Автоматическое заполнение метатегов', 'description'=>'', 'value'=>'1', 'type_value'=>'checkbox', 'group_id'=>'1', 'order_num'=>'10', ),
            array('name'=>'brand_small_image_width', 'name_cyr'=>'Ширина маленького логотипа бренда', 'description'=>'', 'value'=>'90', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'10', ),
            array('name'=>'brand_small_image_height', 'name_cyr'=>'Высота маленького логотипа бренда', 'description'=>'', 'value'=>'90', 'type_value'=>'text', 'group_id'=>'4', 'order_num'=>'11', ),
            array('name'=>'banner_uploaddir', 'name_cyr'=>'Путь для хранения изображений банеров', 'description'=>'', 'value'=>'files/bims/', 'type_value'=>'urltext', 'group_id'=>'6', 'order_num'=>'44', ),
            array('name'=>'banner_width', 'name_cyr'=>'Ширина изображения баннера', 'description'=>'', 'value'=>'920', 'type_value'=>'text', 'group_id'=>'6', 'order_num'=>'45', ),
            array('name'=>'banner_height', 'name_cyr'=>'Высота изображения баннера', 'description'=>'', 'value'=>'380', 'type_value'=>'text', 'group_id'=>'6', 'order_num'=>'46', ),
            array('name'=>'last_1c_orders_export_date', 'name_cyr'=>'Дата последней выгрузки заказов', 'description'=>'', 'value'=>'2016-01-11 11:33:32', 'type_value'=>'disabledtext', 'group_id'=>'5', 'order_num'=>'48', ),
            array('name'=>'news_path_to_uploaded_images', 'name_cyr'=>'Путь для хранения картинок', 'description'=>'', 'value'=>'files/news/', 'type_value'=>'urltext', 'group_id'=>'3', 'order_num'=>'59', ),
            array('name'=>'news_large_image_width', 'name_cyr'=>'Ширина большой картинки', 'description'=>'', 'value'=>'800', 'type_value'=>'text', 'group_id'=>'3', 'order_num'=>'60', ),
            array('name'=>'news_large_image_height', 'name_cyr'=>'Высота большой картинки', 'description'=>'', 'value'=>'333', 'type_value'=>'text', 'group_id'=>'3', 'order_num'=>'61', ),
            array('name'=>'news_small_image_width', 'name_cyr'=>'Ширина маленькой картинки', 'description'=>'', 'value'=>'150', 'type_value'=>'text', 'group_id'=>'3', 'order_num'=>'62', ),
            array('name'=>'news_small_image_height', 'name_cyr'=>'Высота маленькой картинки', 'description'=>'', 'value'=>'75', 'type_value'=>'text', 'group_id'=>'3', 'order_num'=>'63', ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
