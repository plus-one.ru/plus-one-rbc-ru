<?php

use Phinx\Seed\AbstractSeed;

class UsersAdminSeeder extends AbstractSeed
{
    private $tablename = 'users_admin';

    public function run()
    {
        $data = array(
            array(
                'login' => 'admin',
                'password' => '1c78228d037863611a46cc92460127a3', // XomA101278
                'enabled' => 1
            ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
