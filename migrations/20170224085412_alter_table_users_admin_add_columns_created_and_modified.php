<?php

use Phinx\Migration\AbstractMigration;

class AlterTableUsersAdminAddColumnsCreatedAndModified extends AbstractMigration
{
    private $tablename = 'users_admin';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('created')
            ->removeColumn('modified')
            ->save();
    }
}
