<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();


$block = str_replace("-", "/", $_GET['block']);
$start = $_GET['start'];
$limit = explode("/", $block);

$page = $_GET['pagenum'];
$rubrika = $_GET['rubrika'];
$type = $_GET['type'];
$usedBlock = unserialize($_GET['used_block']);
$useElasticBlock = $_GET['use_elastic_block'];

$result = $blogClass->getPosts($rubrika, $page, $usedBlock, $useElasticBlock);
//die();
header("Content-type: application/jsonp; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

print json_encode($result);
?>