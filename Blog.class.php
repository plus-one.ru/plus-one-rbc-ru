<?PHP

require_once('Widget.class.php');
require_once ('PresetFormat.class.php');
require_once ('Helper.class.php');

class Blog extends Widget
{
    private $postMatrix = array();

    private $postsMatrix;

    private $numItemsOnPage;

    private $tags = array();

    private $isSubscribe = false;

    public $itemPerPage = 12;

    public $typeVisualBlock = array(
        1 => 'post',
        2 => 'post',
        3 => 'post',
        4 => 'post',
        5 => 'factday',
        6 => 'imageday',
        7 => 'factday',
        8 => 'citate',
        9 => 'post',
        10 => 'diypost'
    );

    /**
     *
     * Конструктор
     *
     */
    function Blog(&$parent)
    {
        Widget::Widget($parent);
        $this->postMatrix = array(
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
        );
        $this->postsMatrix = new stdClass();
        $this->numItemsOnPage = 21;
        $this->numAuthorsOnPage = 12;

        $this->tags = array(
            'ecology' => "Экология",
            'economy' => "Экономика",
            'community' => "Общество"
        );
//        $this->fetch();
    }

    /**
     *
     * Отображение
     *
     */
    function fetch()
    {

        // чтобы работала пагинация в бесконечном скролле
        // нам необходимы ID записей, которые уже вывелись раньше
        // за неимением "гоничной" (не может jScroll работать с POST запросами
        // информацию об уже выведенных ID храним в сессии
        // и если пользователь обновил страницу - иы сбрасываем сохраненные в сессии значения
        if (intval($_GET['page']) == 0 || is_null($_GET['page'])){
            unset($_SESSION['usedIdsPosts']);
            unset($_SESSION['useElasticBlock']);
        }

        // Оформлял ли пользователь подписку
        $this->isSubscribe = $_COOKIE['issubscribe'];

        // Какую статью нужно вывести?
        $article_url = $this->url_filtered_param('article_url');
        $rubrika_url = $this->url_filtered_param('rubrika_url');
        $tagUrl = $this->url_filtered_param('tag_url');
        $writerId = $this->url_filtered_param('writer_id');
        $isRobot = $this->url_filtered_param('robot');
        $specProjects = $this->url_filtered_param('specprojects');
        $isInfiniteScroll = $this->url_filtered_param('infinitescroll');
        $page = $this->url_filtered_param('page');
        $mode = $this->url_filtered_param('mode');

        $this->rubrikaUrl = $rubrika_url;


        if (!empty($article_url) && empty($isRobot)) {
            // Если передан id статьи, выводим ее
            return $this->fetch_item($article_url, $rubrika_url);
        }
        elseif (!empty($article_url) && !empty($isRobot)) {
            return $this->fetchItemForRobot($article_url, $rubrika_url);
        }
        elseif (!empty($specProjects)){
            return $this->fetchListSpecProjects();
        }
        elseif ($mode == 'getaboutpage'){
            return $this->getAboutPage();
        }
        elseif ($mode == 'autorslist' && empty($writerId)){
            return $this->getAllAuthors();
        }
        elseif ($mode == 'autorslist' && !empty($writerId)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            return $this->getAuthorsPosts($writerId, $page, $usedids, $useelastic, false);
        }
        elseif ($mode == 'getleaders' && !empty($rubrika_url)){
            $this->typeUrl = "leaders";
            return $this->getLeaders($rubrika_url);
        }
        elseif(!empty($rubrika_url) && empty($mode)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            $this->typeUrl = "";

            return $this->getPosts($rubrika_url, $page, $usedids, $useelastic, false);
        }
        elseif(!empty($rubrika_url) && $mode == 'posttags' && empty($tagUrl)){
            $this->typeUrl = "posttags";
            return $this->getPostTags($rubrika_url);
        }
        elseif(!empty($rubrika_url) && $mode == 'getsimpleblogs' && empty($tagUrl)){
            // Если нет, выводим список всех новостей
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }

            $this->typeUrl = "getsimpleblogs";

            return $this->getPosts('main', $page, $usedids, $useelastic, false, 1);
        }
        elseif(!empty($rubrika_url) && $mode == 'main' && empty($tagUrl)){
            // Если нет, выводим список всех новостей
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }

            $this->typeUrl = "getsimpleblogs";

            return $this->getPosts($rubrika_url, $page, $usedids, $useelastic, false);
        }
        elseif(!empty($rubrika_url) && $mode == 'getpostsfromtag' && !empty($tagUrl)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            return $this->fetchListByTag($rubrika_url, $tagUrl, $page, $usedids, $useelastic, false);
        }
        elseif($mode == "search"){
            return $this->getSearch($_GET['query'], $_GET['page']);
        }
        elseif ($mode == "confirmSubscribe"){
            $hash = $_GET['hash'];
            $confirmSubscribe = $this->confirmSubscribe($hash);

            // Если нет, выводим список всех новостей
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }

            return $this->getPosts('main', $page, $usedids, $useelastic, false, 0, $confirmSubscribe);
        }
        elseif ($mode == 'getPostInfinityScroll' && $rubrika_url != 'news'){
            return $this->getPostListToInfinityScroll($rubrika_url);
        }
        elseif ($mode == 'getPostInfinityScroll' && $rubrika_url == 'news'){
            return $this->getNewsListToInfinityScroll($rubrika_url);
        }
        elseif($mode == "sitemap"){
            $this->getSitemap();
//            return true;
        }
        else {
            // Если нет, выводим список всех постов
            return $this->getMainPage();
        }
    }

    public function formatPostList($postList, $rowType = '')
    {
        foreach ($postList AS $key=>$post){
            $post->typeRow = $rowType;

            if ($rowType == 'ticker'){
                $post->postUrl = $post->url;
            }
            elseif (!empty($post->url)){
                $post->postUrl = "" . $post->tagUrl . "/" . $post->url;
            }
            else{
                $post->postUrl = null;
            }
            $post->tagCode = $post->tagUrl;
            $post->tagUrl = "" . $post->tagUrl;

            $post->dateStr = Helper::dateTimeWord($post->date_created);

            $post->typePost = $this->typeVisualBlock[$post->type_post];

            $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat($this->typeVisualBlock[$post->type_post], $post->format);
            $postFont = PresetFormat::getPresetFontByKey($post->font);

            $post->frontClass = $postFormat['frontClass'] ? $postFormat['frontClass'] : "bgGray";
            $post->fontClass = $postFont['frontClass'];

            $post->image = null;
            $post->imageMobile = null;
            $post->imageFolder = "/files/blogposts/";
            $post->specProject = false;
            $post->showPartner = true;
            $post->linkTypePost = 'self';
            $post->linkTypeTag = 'self';

            /* spec_projects */
            if ($post->type_post == 9){
                $post->name = $post->sp_name;
                $post->header = $post->sp_header;
                $post->image_1_1 = $post->sp_image_1_1;
                $post->image_1_2 = $post->sp_image_1_2;
                $post->image_1_3 = $post->sp_image_1_3;
                $post->type_announce_1_1 = $post->sp_type_announce_1_1;
                $post->type_announce_1_2 = $post->sp_type_announce_1_2;
                $post->type_announce_1_3 = $post->sp_type_announce_1_3;
                $post->postUrl = $post->sp_url;
                $post->imageFolder = "/files/spec_project/";
                $post->tagUrl = $post->sp_url;
                $post->tagName = "СПЕЦПРОЕКТ";
                $post->specProject = true;
                if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST)){
                    $post->linkTypePost = 'parent';
                    $post->linkTypeTag = 'parent';
                }
            }
            elseif (($post->type_post == 1 || $post->type_post == 6) && $post->spec_project != 0){
                $post->tagName = "СПЕЦПРОЕКТ " . $post->sp_name;
                $post->showPartner = false;
                $post->tagUrl = $post->sp_url;
                $post->specProject = true;

                if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST)){
                    $post->linkTypePost = 'self';
                    $post->linkTypeTag = 'parent';
                }
            }

            if ($post->typeRow == '1_1' && $post->image_1_1){
                $post->image = $post->imageFolder . $post->image_1_1;
            }
            if ($post->typeRow == '1_2' && $post->image_1_2){
                $post->image = $post->imageFolder . $post->image_1_2;
            }
            if ($post->typeRow == '1_3' && $post->image_1_3){
                $post->image = $post->imageFolder . $post->image_1_3;
            }

            $imageExists = $post->image ? file_exists(__DIR__.$post->image) : false;

            if ($post->typeRow == '1_1' && $post->type_announce_1_1 == "picture"){
                $post->format = $imageExists ? 'picture' : 'text';
            }

            if ($post->typeRow == '1_2' && $post->type_announce_1_2 == "picture"){
                $post->format = $imageExists ? 'picture' : 'text';
            }

            if ($post->typeRow == '1_3' && $post->type_announce_1_3 == "picture"){
                $post->format = $imageExists ? 'picture' : 'text';
            }

            $post->imageMobile = $post->imageFolder . $post->image_1_3;

            switch ($post->typePost) {
                case 'post':
                    $name = $post->name;
                    $header = $post->header;
                    break;
                case 'imageday':
                    $name = $post->name;
                    $header = "";
                    break;
                case 'factday':
                    $name = $post->name;
                    $header = "";
                    break;
                case 'citate':
                    $name = $post->name;
                    $header = $post->citate_author_name;
                    break;
                case 'diypost':
                    $name = $post->name;
                    $header = "";
                    break;
                default:
                    $name = $post->name;
                    $header = $post->header;
            }

            $post->name = $name;
            $post->header = $header;


            $postList[$key] = $post;
        }
//        echo "<pre>";
//        print_r($postList);
//        echo "</pre>";
//        die();
        return $postList;
    }

    public function getMainPage()
    {
        $query = sql_placeholder("SELECT * FROM main_page ORDER BY row_number ASC");
        $this->db->query($query);
        $mainPageRows = $this->db->results();

        foreach ($mainPageRows AS $k=>$row){

            $query = sql_placeholder("SELECT bp.id, bp.header, bp.name, bp.lead, bp.url, bp.format, bp.font,
                    bp.image_1_1, bp.image_1_2, bp.image_1_3, bp.image_rss, bp.type_post, bp.partner_url, bp.is_partner_material,
                    DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created, bp.citate_author_name,
                    bp.amount_to_displaying, bp.signature_to_sum, bp.text_body, bp.spec_project,
                    bt.name AS tagName, bt.url AS tagUrl,
                    p.name AS partnerName
                    ,bp.type_announce_1_1, bp.type_announce_1_2, bp.type_announce_1_3
                    ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url 
                    ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                    ,sp.type_announce_1_1 AS sp_type_announce_1_1
                    ,sp.type_announce_1_2 AS sp_type_announce_1_2
                    ,sp.type_announce_1_3 AS sp_type_announce_1_3
                  FROM blogposts AS bp
                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                  LEFT JOIN partners AS p ON p.id = bp.partner
                  LEFT JOIN spec_projects AS sp ON sp.id = bp.spec_project
                  LEFT JOIN blogwriters bw ON bw.id = bp.writers
                  WHERE bp.id IN (?@) AND (bw.enabled = 1 OR bp.spec_project)",
                json_decode($row->row_content));

            $this->db->query($query);
            $posts = $this->db->results();

            $postList = [];
            foreach (json_decode($row->row_content) AS $key=>$v){
                if ($v == 'banner'){
                    $post = new stdClass();
                    $post->row_type = 'banner';
                    $post->params = $v->params;
                    $post->typeRow = $row->row_type;
                    $postList[$key] = $post;
                }
                else{
                    foreach ($posts AS $post){
                        if ($post->id == $v){
                            $postList[$key] = $post;
                        }
                    }
                }
            }
            $mainPageRows[$k] = $this->formatPostList($postList, $row->row_type);
        }

        $this->smarty->assign('posts', $mainPageRows);

        // video
        // state 1 = record
        // state 2 = live
        $query = sql_placeholder("SELECT id, image_1_1_c AS imageDesktop, image_1_4 AS imageMobile, state
                      FROM blogposts 
                      WHERE type_post = 11
                      AND enabled = 1
                      AND (state = 2 OR state = 1) 
                      ORDER BY id DESC LIMIT 1");
        $this->db->query($query);
        $mainVideo = $this->db->result();

        switch ($mainVideo->state) {
            case 1:
                $typeVideo = 'record';
                break;
            case 2:
                $typeVideo = 'live';
                break;
        }

        $query = sql_placeholder("SELECT * FROM videos WHERE parent_video_post = ? AND type_video = ?", $mainVideo->id, $typeVideo);
        $this->db->query($query);
        $videoList = $this->db->results();

        $this->smarty->assign('firstVideo', $videoList[0]->video_url);
        $this->smarty->assign('videoImageDesktop', $mainVideo->imageDesktop);
        $this->smarty->assign('videoImageMobile', $mainVideo->imageMobile);
        $this->smarty->assign('videoList', $videoList);
        $this->smarty->assign('typeVideo', $typeVideo);

        $this->body = $this->smarty->fetch('mainPage.tpl');
        return $this->body;
    }

    function getSitemap()
    {
        $query = sql_placeholder("SELECT b.url, DATE_FORMAT(b.modified, '%Y-%m-%d') AS post_modified, t.url AS tag_url 
                                FROM blogposts AS b
                          INNER JOIN blogtags AS t on t.id=b.tags
                           LEFT JOIN blogwriters bw ON bw.id = b.writers
                               WHERE b.enabled = 1
                                 AND (bw.enabled = 1 OR b.spec_project)
                                 AND b.url != ''
                            ORDER BY b.created DESC");
        $this->db->query($query);
        $result = $this->db->results();


        $strHead = <<<END
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
END;
        $strItem = "";

        $strFoot = <<<END
</urlset>
END;

        $dateNow = (new DateTime('now'))->format('Y-m-d');
        $sileLink = array(
            array('link' =>  $this->root_url .'/about', 'modDate' => $dateNow ),
            array('link' =>  $this->root_url .'/ecology', 'modDate' => $dateNow ),
            array('link' =>  $this->root_url .'/economy', 'modDate' => $dateNow ),
            array('link' =>  $this->root_url .'/society', 'modDate' => $dateNow ),
        );

        foreach ($sileLink as $link) {
            $strItem .= <<<END
   <url>
      <loc>{$link['link']}</loc>
      <lastmod>{$link['modDate']}</lastmod>
   </url>

END;
        }

        foreach($result AS $post){
            $url = $this->root_url . "/" . $post->tag_url . "/" . $post->url;
            $modDate = $post->post_modified;


            $strItem .= <<<END
   <url>
      <loc>$url</loc>
      <lastmod>$modDate</lastmod>
   </url>

END;

        }




        $sitemap = $strHead . $strItem . $strFoot;

        header("Content-type: text/xml; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        echo $sitemap;
        exit(0);
    }

    function confirmSubscribe($hash)
    {
        $query = sql_placeholder("SELECT * FROM subscribe WHERE enabled = 0 AND check_hash = ? ", $hash);
        $this->db->query($query);

        if ($this->db->num_rows() == 0) {
            return false;
        }
        $item = $this->db->result();

        if ($item){
            $query = sql_placeholder("UPDATE subscribe SET enabled = 1 WHERE id = ? ", $item->id);
            $this->db->query($query);
        }

        $this->sendEmailToMailChimp($item->email);

        setcookie("issubscribe", 1, 7842552499, "/");

        return true;
    }

    function sendEmailToMailChimp($emailAddress)
    {
        $mailChimpDc = "us15";

        $mailChimpApiKey = "d8837a0da0847891e0b0181f3fec534e-" . $mailChimpDc;

        $listId = "cf6f0a7d24";

        $url = "https://{$mailChimpDc}.api.mailchimp.com/3.0/lists/{$listId}/members";

        $ch = curl_init($url);

        $postFields = array(
            "email_address" => $emailAddress,
            "status" => "subscribed",
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_USERPWD, "user:" . $mailChimpApiKey);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postFields));

        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    function fetchListSpecProjects()
    {
        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i:%s') AS date_created FROM spec_projects WHERE enabled = 1 ORDER BY created DESC");
        $this->db->query($query);

        // Если не существует такая статья - ошибка 404
        if ($this->db->num_rows() == 0) {
            return false;
        }
        $items = $this->db->results();


        foreach ($items AS $k => $v) {

            $relatedPostName = $v->name;
            $relatedPostName = str_replace("<p>", "", $relatedPostName);
            $relatedPostName = str_replace("</p>", "", $relatedPostName);
            $items[$k]->name = $relatedPostName;

            $relatedPostHeader = $v->header;
            $relatedPostHeader = str_replace("<p>", "", $relatedPostHeader);
            $relatedPostHeader = str_replace("</p>", "", $relatedPostHeader);
            $items[$k]->header = $relatedPostHeader;

            $items[$k]->dateStr = Helper::dateTimeWord($v->date_created);

        }


        $this->title = "Спецпроекты +1";
        // Передаем в шаблон
        $this->smarty->assign('specialProjects', $items);

        $this->body = $this->smarty->fetch('specialprojects.tpl');
        return $this->body;
    }

    function getSearch($queryString, $page = 1, $limit = 15, $ajax = false)
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; }

        $limit = intval($limit);
        $page = intval($page);
        $page = max(1, $page);
        $offset = ($page-1) * $limit;
        $results = array();
        $countAll = new stdClass();
        $countAll->count_all = 0;
        if (isset($this->config->indexing) && $this->config->indexing) {
            require_once './Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $indexDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $indexDB->connect();
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $indexDB->query(sql_placeholder("SELECT id FROM {$tablePrefix}posts WHERE MATCH(?) AND enabled=1
                                         ORDER BY WEIGHT() DESC LIMIT {$offset}, {$limit}
                                         OPTION field_weights=(name=100, header=60, lead=10, body=5)", $queryString));
            $postsIds = $indexDB->results();
            if (!$ajax) {
                $indexDB->query(sql_pholder("SELECT COUNT(*) as count_all FROM {$tablePrefix}posts WHERE MATCH(?) AND enabled=1", $queryString));
                $countAll = $indexDB->result();
            }

            $indexDB->disconnect();

            foreach ($postsIds as $p) {
                $results[$p->id] = false;
            }

            $postsIdsStr = join(',', array_map(function ($item) {
                return $item->id;
            }, $postsIds));

            if (!trim($postsIdsStr)) {
                $postsIdsStr = "0";
            }

            $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                            sp.font_color AS specProjectFontColor,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,   
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName, b.is_partner_material
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url 
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3     
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE b.enabled = 1 
                          AND (bw.enabled = 1 OR b.spec_project) 
                          AND (b.id IN ({$postsIdsStr})) 
                          AND b.type_post <> 12
                 ");


            $this->db->query($query);

            $result = $this->db->results();

            $result = $this->formatPostList($result, '1_3');

            foreach ($result as $r) {
                $results[$r->id] = $r;
            }

            $this->title = "Результаты поиска / РБК +1";

            if (!$ajax) {
                $this->smarty->assign('countAll', $countAll);
            }
            // Передаем в шаблон
            $this->smarty->assign('items', array_filter($results, function ($item) {
                return $item;
            }));
            $this->smarty->assign('queryString', htmlspecialchars($queryString));

            $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
        }

        return $this->body;
    }

    function getSearchAutocomplette($searchQuery)
    {
        $results = array();
        if (isset($this->config->indexing) && $this->config->indexing) {
            require_once './Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $indexDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $indexDB->connect();
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $indexDB->query(sql_pholder("SELECT id  FROM {$tablePrefix}posts WHERE MATCH(?) AND enabled=1 ORDER BY WEIGHT() DESC LIMIT 15 
                                         OPTION field_weights=(name=100, header=60, lead=10, body=5)", $searchQuery));
            $postsIds = $indexDB->results();
            $indexDB->disconnect();
            foreach ($postsIds as $sItem) {$results[$sItem->id] = false;}
            $postsIdsStr = join(',', array_map(function ($item) {return $item->id;}, $postsIds));

            if (!trim($postsIdsStr)) {$postsIdsStr = "0";}

            $query = sql_placeholder("SELECT  'blog' as type, b.id, b.blocks, b.tags, b.name, b.header, CONCAT ('/', bt.url) AS blogTagUrl, b.url, b.type_post,
                            b.type_post, b.partner_url, b.post_tag, bw.name AS writer, bw.id AS writer_id,
                            bw.image AS writerImage, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            b.citate_author_name, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,    
                            DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created, 
                            bt.name AS tagName, b.format, b.font, p.name AS partnerName 
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          WHERE b.enabled=1 AND b.blocks != 'ticker' AND (b.id IN ({$postsIdsStr})) AND bw.enabled = 1 AND b.type_post <> 12
            ");

        } else {
            $queryString = '%' . $searchQuery . '%';
            $query = sql_placeholder("SELECT 'blog' as type, b.name, b.url, CONCAT ('blog/', bt.url) AS blogTagUrl
                                  FROM blogposts AS b
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE b.enabled=1 AND (b.name like ? OR b.header LIKE ? OR b.lead LIKE ? OR b.body LIKE ?) AND b.type_post <> 12
                                  LIMIT 15",
                $queryString, $queryString, $queryString, $queryString, $queryString, $queryString, $queryString, $queryString);
        }
        $this->db->query($query);
        $searchItems = $this->db->results();

        if (isset($this->config->indexing) && $this->config->indexing) {
            foreach ($searchItems as $r) {
                $results[$r->id] = $r;
            }
            $searchItems = $results;
        }


        $return['counterRecords'] = count($results);

        foreach ($searchItems AS $item){
            $itemUrl = $item->blogTagUrl . '/' . $item->url;
            if ($item->type == 'blog') {
                if (preg_match('#^(http|https)://#is', $item->url) === 1) {
                    $itemUrl = $item->url;
                }
            }

            $return[] = array(
                'label' => htmlspecialchars_decode(html_entity_decode(strip_tags($item->name))),
                'url' => $itemUrl
            );
        }
        return $return;
    }

    /**
     * Отображение отдельной статьи
     * @param $url
     * @param $rubrika_url
     * @return bool|false|mixed|string|void|null
     * @throws Exception
     */
    function fetch_item($url, $rubrika_url)
    {

        $nowDate = new DateTime();
        if ($_GET['mode'] == 'preview'){
            $andEnabled = "";
        } else {
            $andEnabled = " AND b.enabled = 1 AND b.created <= '{$nowDate->format('Y-m-d H:i:s')}' ";
        }

        // Выбираем статью из базы
        $query = sql_placeholder("SELECT b.id, b.body, b.tags, b.partner, b.partner_url, b.name, b.lead, b.header, b.header_rss, b.url, b.block_color,
                                   b.type_post, b.post_tag,
                                   b.font_color, b.border_color,
                                   DATE_FORMAT(b.created, '%d.%m.%Y') AS post_created,
                                   b.meta_title, b.meta_description, b.meta_keywords, p.name AS partner_name,  b.is_partner_material,
                                   bt.url as bt_url,        
                                   p.color AS partner_color, b.signature_to_sum, b.text_body, b.amount_to_displaying,
                                   b.header_on_page, b.image_rss, b.image_1_1, b.image_1_1_c, b.image_1_2, b.image_1_3,
                                   b.spec_project, b.conference, b.body_color,
                                   sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                                   conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor, b.citate_author_name,
                                   b.seo_title, b.seo_description, b.seo_keywords, b.show_fund, b.shop_article_id, b.fund_resource_id, p.name AS partnerName
                                  FROM blogposts AS b
                                  LEFT JOIN blogtags bt ON bt.id = b.tags
                                  LEFT JOIN blogwriters bw ON bw.id = b.writers
                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  WHERE b.url LIKE ? 
                                    AND (bw.enabled = 1 OR b.spec_project)
                                    {$andEnabled}
                                  LIMIT 1", $url);
        $this->db->query($query);

        // Если не существует такая статья - ошибка 404
        if ($this->db->num_rows() == 0) {
            return false;
        }
        $item = $this->db->result();

        // редиректим если статья под запретом РКН
        // т.к. статей попадающих под запрет РКН мало, проверяем по id без добавления флага РКН в базу
        if ($item->id === '287' || $item->id === '1235' || $item->id === '1250' || $item->id === '1327') {
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: /217471-8');
        }

        $_SESSION['vievedPostIds'] = [];
        $_SESSION['vievedPostIds'][$rubrika_url] = [];
        $_SESSION['vievedPostIds'][$rubrika_url][$item->id] = $item->id;
        session_write_close();

        if ($item->bt_url !== $rubrika_url || @$_GET['article_url'] !== $item->url) {
            header( "Location: /$item->bt_url/{$item->url}", true, 301 );
            exit(0);
        }


        // замена подписи автора в старых постах
        $pattern = '#<span class="author">(.+?)</span>#is';
        preg_match($pattern, $item->body, $arr, PREG_OFFSET_CAPTURE);
        $newAuthorNameStr = str_ireplace("Автор:", "", $arr[0][0]);
        $newAuthorNameStr = strip_tags($newAuthorNameStr);
        $newHtmlAuthorSign = '<div class="articleAuthor"><p class="articleAuthor_h">Автор</p><p class="articleAuthor_name">' . $newAuthorNameStr . '</p></div>';
        $item->body =  preg_replace($pattern, $newHtmlAuthorSign, $item->body);

        $this->bodyColor = $item->body_color;

        $item->body = str_replace("<p>&nbsp;</p>", "", $item->body);

        $query = "SELECT name, url, color FROM blogtags WHERE id=" . $item->tags;
        $this->db->query($query);
        $item->tags = $this->db->result();

        $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
        $this->db->query($query);
        $item->postTags = $this->db->results();

        $postTagMainPage = $this->getPostTagMainPage($item->post_tag);
        $item->postTagMainPage = $postTagMainPage;

        $item->postDateStr = $item->post_created; //$this->dateTimeWord($item->post_created);

        $query = "SELECT * FROM partners WHERE id=" . $item->partner;
        $this->db->query($query);
        $item->partner = $this->db->result();

        // связанные материалы
        $query = sql_pholder("SELECT b.id, b.name,b.header, b.url, b.tags, b.block_color, b.type_post, b.partner_url,
                        pr.name AS partner_name, pr.color AS partner_color,
                        b.signature_to_sum, b.amount_to_displaying, b.text_body,
                        b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss, b.format, b.font,
                        DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                        bt.name AS tagName, bt.url AS tagUrl, pr.name AS partnerName, b.is_partner_material
                        ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3   
                               FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id = b.writers 
                          LEFT JOIN partners AS pr ON pr.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          INNER JOIN related_posts AS rp ON rp.post_id=b.id
                          WHERE rp.parent_id=" . $item->id . " AND b.enabled=1 
                            AND b.created <= ?
                            AND (bw.enabled = 1 OR b.spec_project)
                          ORDER BY b.created 
                          LIMIT 3", $nowDate->format('Y-m-d H:i:s') );
        $this->db->query($query);
        $relatedPosts = $this->db->results();


        $relatedPosts = $this->formatPostList($relatedPosts, '1_3');

        $item->relatedPosts = $relatedPosts;

        // партнерские посты
        $query = sql_placeholder('SELECT p.post_id AS id, p.header AS name, p.subheader AS header, p.link AS postUrl,
                                      p.image, t.name AS tagName, t.url AS tagUrl, pr.name AS partner_name, pr.color AS partner_color 
                                      
                                      FROM partners_posts_links AS p
                                    JOIN blogtags AS t ON t.id=p.rubrika_id
                                    LEFT JOIN partners AS pr ON pr.id=p.partner_id
                                    WHERE p.post_id=?
                                    
                                    ORDER BY p.id ASC
                                    LIMIT 3',
                                $item->id);
        $this->db->query($query);
        $partnersPosts = $this->db->results();

        foreach ($partnersPosts AS $key => $post) {

            $post->tagUrl = "" . $post->tagUrl;

            $post->dateStr = '';

            $post->typePost = 'post';

            $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat('post', 'gray');
            $postFont = PresetFormat::getPresetFontByKey('MullerBold');

            $post->frontClass = $postFormat['frontClass'];
            $post->fontClass = $postFont['frontClass'];

            if ($post->image) {
                $post->image = "/files/blogposts/" . $post->image;
                $post->format = 'picture';
            }

            $partnersPosts[$key] = $post;
        }


        $item->partnersPosts = $partnersPosts;

        if ($item->type_post == 7){
            $analitycData = $this->getAnalitycData($item->id, $item->writer_id);
            $item->analitycData = $analitycData;

            $item->postTags = $this->getPostTagTwoLevel($item->id);
        }

        $name = $item->name;
        $name = str_replace("<p>", "", $name);
        $name = str_replace("</p>", "", $name);
        $item->name = $name;

        $citateName = str_replace("<p>", "", $item->name);
        $citateName = str_replace("</p>", "", $citateName);

        $citateHeader = str_replace("<p>", "", $item->header);
        $citateHeader = str_replace("</p>", "", $citateHeader);

        $item->citateText = $citateName . " <span>" . $citateHeader . "</span>";


        $item->typePost = $this->typeVisualBlock[$item->type_post];



        // Устанавливаем метатеги для страницы с этой записью
        $this->title = "";

        if ($item->typePost == 'factday'){
            $this->title = htmlspecialchars(strip_tags(trim($item->name))) . " / +1";
        }
        else{
            if ($item->seo_title !=""){
                $this->title = htmlspecialchars(strip_tags(trim($item->seo_title))) . " / +1";
            }
            else{
                $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";
            }
        }



        $this->description = "";
        if ($item->seo_description != ""){
            $this->description = strip_tags($item->seo_description);
        }
        else{
            if (!empty($item->lead)){
                $this->description = strip_tags($item->lead);
            }
            elseif(!empty($item->header)){
                $this->description = strip_tags($item->header);
            }
        }

        $this->keywords = $item->seo_keywords;

        $item->meta->title = htmlspecialchars(strip_tags(trim($item->meta_title)));
        $item->meta->description = htmlspecialchars(strip_tags(trim($item->meta_description)));
        $item->meta->keywords = htmlspecialchars(strip_tags(trim($item->meta_keywords)));

        $item->meta->headerMenu = $this->getHeaderMenu();

        $imageUrlBlogposts = 'files/blogposts/';


        $this->config->og_version = isset( $this->config->og_version) ?   "?" .$this->config->og_version : '';

        if (!$item->image_rss) {
            $item->ogImage = 'design/rbk_v3/img/og-rbc.png' .          $this->config->og_version;
        } else {
            $this->ogImage = $imageUrlBlogposts . $item->image_rss .     $this->config->og_version;
        }
        $this->ogImage_1_1 = $imageUrlBlogposts . $item->image_1_1 .     $this->config->og_version;
        $this->ogImage_1_1_c = $imageUrlBlogposts . $item->image_1_1_c . $this->config->og_version;
        $this->ogImage_1_2 = $imageUrlBlogposts . $item->image_1_2 .     $this->config->og_version;
        $this->ogImage_1_3 = $imageUrlBlogposts . $item->image_1_3 .     $this->config->og_version;
        $this->ogImage_galeries = "";

        $queryArray = array();
        parse_str(@$_SERVER["QUERY_STRING"], $queryArray);
        unset($queryArray['module']);
        unset($queryArray['rubrika_url']);
        unset($queryArray['article_url']);

        $queryString = count($queryArray)>0 ? '?' .  http_build_query($queryArray) : "";
        $this->withUrlParams = true;

        $this->ogTitle = "" . $item->header_rss;
        $this->ogUrl = "" . $rubrika_url . "/" . $url . $queryString;
        $this->ogSiteName = $this->settings->site_name;

        // вычисляем примерное время на прочтение поста
        $item->typePost = $this->typeVisualBlock[$item->type_post];
        if ($this->typeVisualBlock[$item->type_post] == 'imageday' || $this->typeVisualBlock[$item->type_post] == 'diypost'){
            $totalReadTime = 3; // #PLUS1-186 для этого типа поста время на чтение всегда 3 минуты
        }
        else{
            $lenghtLead = iconv_strlen(strip_tags($item->lead));
            $lenghtBody = iconv_strlen(strip_tags($item->body));
            $totalLenght = intval($lenghtBody + $lenghtLead);
            $totalReadTime = round(($totalLenght / 1000) / 2);

            if ($totalReadTime == 0){
                $totalReadTime = 1; // #PLUS1-257 Выводить 1 минуту там где получается 0 минут на чтение
            }
        }

        $totalReadTimeCaption = $this->getHeaderMenuSigns('минут', 'минута', 'минуты', $totalReadTime);

        $item->dateStr = $item->post_created;
        $item->fullUrl = $rubrika_url . "/" . $item->url;
//        echo "<pre>";
//        print_r($item);
//        echo "</pre>";
//        die();


        $customColors = false;

        if ($item->font_color != "" || $item->border_color != ""){
            $customColors = true;
        }

        $previewMode = @$queryArray['mode'] === 'preview' ? '?mode=preview' : '';

        // Передаем в шаблон
        $this->smarty->assign('previewMode', $previewMode);
        $this->smarty->assign('blog', $item);

        $this->smarty->assign('titleToFavorite', $this->title);
        $this->smarty->assign('urlToFavorite', $this->root_url . "/" . $this->ogUrl);
        $this->smarty->assign('totalReadTime', $totalReadTime);
        $this->smarty->assign('totalReadTimeCaption', $totalReadTimeCaption);


        $this->smarty->assign('issubscribe', $this->isSubscribe);

        $this->smarty->assign('bodyColor', $this->bodyColor);
        $this->smarty->assign('customColors', $customColors);



        $this->body = $this->smarty->fetch('blog.tpl');
        return $this->body;
    }

    /**
     * Метод возвращает все записи по выбранному тегу первого уровня
     *
     * @param string $rubrika_url - Урл тега первого уровня
     * @param string $tagUrl - Урл тега второго уровня
     * @param int $page - страница с которой начать отображение блоков (по умолчанию 0 - с самого начала)
     * @param array $usedIdsPosts - Id постов уже выведенных на странице (нужно чтобы не выводить несколько раз одну и ту же статью)
     * @param int $useElasticBlock - какие блоки использованы в 1/4 (блоки 1/4 могут менять свой вид в зависимости от их кол-ва в строке)
     * @return array|bool - массив с постами, или false если ничего не найдено
     */
    function fetchListByTag($rubrika_url, $tagUrl = '', $page = 1, $usedIdsPosts = array(), $useElasticBlock = 0, $ajax = true)
    {

        $query = sql_placeholder("SELECT pt.id FROM post_tags AS pt
                                    INNER JOIN blogtags AS bt ON bt.id = pt.parent
                                  WHERE bt.url = ? AND pt.url = ?", $rubrika_url, $tagUrl);
        $this->db->query($query);
        $tagId = $this->db->result();

        $nowDate = new \DateTime();
        $query = sql_placeholder("SELECT COUNT(bp.id) AS cnt FROM blogposts AS bp
                                    LEFT JOIN blogwriters as bw ON bw.id = bp.writers
                                    INNER JOIN relations_postitem_tags AS rpt ON rpt.post_id = bp.id
                                  WHERE bp.enabled = 1 AND rpt.posttag_id = ?
                                    AND bp.created <= ?
            AND (bw.enabled = 1 OR bp.spec_project)"
            , $tagId->id
            , $nowDate->format('Y-m-d H:i:s')
        );
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->cnt / $this->numItemsOnPage);

        if ($page > $totalPages) {
            return false;
        }

        if ($tagUrl != "") {

            $query = sql_placeholder("SELECT id, name, seo_title, seo_description, seo_keywords FROM post_tags WHERE enabled=1 AND url=?", $tagUrl);
            $this->db->query($query);
            $selectedTag = $this->db->result();

            $start = ($page - 1) * $this->itemPerPage;

            $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                            sp.font_color AS specProjectFontColor,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,   
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3                             
                            FROM blogposts AS b
                                    INNER JOIN relations_postitem_tags AS rel_pt ON rel_pt.post_id=b.id
                                    INNER JOIN post_tags AS pt ON pt.id=rel_pt.posttag_id
                                    INNER JOIN blogwriters AS bw ON bw.id=b.writers
                                    LEFT JOIN partners AS p ON p.id=b.partner
                                    LEFT JOIN blogtags AS bt ON bt.id = b.tags
                                    LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                    LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  WHERE
                                  b.enabled=1
                                  AND pt.url=?
                                  AND b.blocks != 'ticker' 
                                  AND b.created <= ? 
                                  AND (bw.enabled = 1 OR b.spec_project)
                                  ORDER BY b.created DESC
                                  LIMIT ?, ?",
                $tagUrl, $nowDate->format('Y-m-d H:i:s'), $start, $this->itemPerPage);

            $this->db->query($query);
            $result = $this->db->results();


            $result = $this->formatPostList($result, '1_3');

            $page = intval($page + 1);

            if ($ajax){
                $return = array(
                    'posts' => $result,
                    'page' => $page,
                );
                return $return;
            }
            else{
                $title = $selectedTag->seo_title ?  "+1 — "  . $selectedTag->seo_title : "+1 — " . $this->tags[$rubrika_url] . ", " . $selectedTag->name;
                // метатеги
                $this->title = $title;
                $this->description = $selectedTag->seo_description;
                $this->keywords = $selectedTag->seo_keywords;

                $this->smarty->assign('items', $result);
                $this->smarty->assign('rubrika', $rubrika_url);
                $this->smarty->assign('tagUrl', $tagUrl);
                $this->smarty->assign('page', $page);

                $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
                return $this->body;
            }
        }
        return false;
    }

    function getPostsByType($writerId, $typePost){

        $query = sql_placeholder("SELECT  b.id, b.blocks_author AS blocks, b.tags, b.name, b.header, b.url, b.block_color, b.type_post, b.post_tag, b.signature_to_sum, b.text_body, b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS post_created 
                                FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          WHERE
                          b.enabled=1
                          AND b.type_post = ?
                          AND b.writers=?
                          AND (bw.enabled = 1 OR b.spec_project)
                          ORDER BY b.created DESC",
            $typePost,
            $writerId
        );
        $this->db->query($query);
        $result = $this->db->results();

        foreach ($result AS $k => $v) {

            $name = $v->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);
            $v->name = $name;

            if ($v->type_post == 2) {

                $v->name = $name . ":";

                $body = $v->body;
                $body = str_replace("<p>", "", $body);
                $body = str_replace("</p>", "", $body);
                $v->body = $body;

                $header = $v->header;
                $header = str_replace("<p>", "", $header);
                $header = str_replace("</p>", "", $header);
                $v->header = $header;
            }

            $citateName = str_replace("<p>", "", $v->name);
            $citateName = str_replace("</p>", "", $citateName);

            $citateHeader = str_replace("<p>", "", $v->header);
            $citateHeader = str_replace("</p>", "", $citateHeader);


            $result[$k]->citateText = $citateName . " <span>" . $citateHeader . "</span>";

            if ($v->type_post == 5) {
                $analitycData = $this->getAnalitycData($v->id, $writerId);
                $result[$k]->analitycData = $analitycData;
            }


            $query = "SELECT name, url FROM blogtags WHERE id=" . $v->tags;
            $this->db->query($query);
            $result[$k]->tags = $this->db->result();

            $result[$k]->postTags = $this->getPostTagTwoLevel($v->id);

            $result[$k]->postDateStr = Helper::dateTimeWord($v->post_created);
        }

        $block = '1/3';
        $key = 0;
        $this->postsMatrix = $result;

        $return = array(
            'posts' => $result,
        );
        return $result;


    }

    function countMaxRecords($block, $whereBy, $where)
    {

        if ($whereBy == "rubrika") {
            $queryCnt = sql_placeholder("SELECT count(b.id) as cnt FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN relations_post_tags AS rel ON rel.post_id=b.id
                          WHERE
                          b.enabled=1
                          AND b.blocks = ?
                          AND rel.tag_id = ?
                          AND (bw.enabled = 1 OR b.spec_project)
                          ORDER BY b.created DESC",
                $block, $where);
            $this->db->query($queryCnt);
            $resultCnt = $this->db->result();
        }

        if ($whereBy == "writer") {
            $queryCnt = sql_placeholder("SELECT count(b.id) as cnt FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          WHERE
                          b.enabled=1
                          AND b.blocks = ?
                          AND b.writers=?
                          AND (bw.enabled = 1 OR b.spec_project)
                          ORDER BY b.created DESC",
                $block, $where);
            $this->db->query($queryCnt);
            $resultCnt = $this->db->result();
        }


        return $resultCnt->cnt;
    }

    /**
     *
     * Отображение списка статей
     *
     */
    function fetch_list($rubrika_url = 'main', $page = 0, $usedBlocks = array())
    {
        $query = sql_placeholder("SELECT COUNT(b.id) AS count 
                        FROM blogposts AS b
                   LEFT JOIN blogwriters as bw ON bw.id = b.writers 
                       WHERE b.enabled=1
                         AND (bw.enabled = 1 OR b.spec_project)
                       ");
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);

        if ($page > $totalPages) {
            return false;
        }

        if ($rubrika_url != "") {
            $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled=1 AND url=?", $rubrika_url);
            $this->db->query($query);

            if ($this->db->num_rows() == 0) {
                return false;
            }
            $rubrika = $this->db->result();

            $usedBlocks['1/1']['maxItems'] = $this->countMaxRecords('1/1', 'rubrika', $rubrika->id);
            $usedBlocks['1/2']['maxItems'] = $this->countMaxRecords('1/2', 'rubrika', $rubrika->id);
            $usedBlocks['1/3']['maxItems'] = $this->countMaxRecords('1/3', 'rubrika', $rubrika->id);
            $usedBlocks['1/4']['maxItems'] = $this->countMaxRecords('1/4', 'rubrika', $rubrika->id);

            foreach ($this->postMatrix AS $key => $matrix) {
                foreach ($matrix AS $block => $configBlock) {

                    if ($usedBlocks[$block]['limit']) {
                        $start = $usedBlocks[$block]['limit'];
                    } else {
                        $start = 0;
                    }

                    if ($start <= $usedBlocks[$block]['maxItems']) {
                        if ($rubrika_url == 'main') {
                            $query = sql_placeholder("SELECT b.id, b.name, b.header, b.url, b.block_color, b.type_post, bw.name AS writer, bw.id AS writer_id, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS post_created FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN relations_post_tags AS rel ON rel.post_id=b.id
                          WHERE
                          b.enabled=1
                          AND b.blocks = ?
                          AND rel.tag_id=?
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                                $block,
                                $rubrika->id,
                                $start,
                                $configBlock['settings']['limit']);
                        } else {
                            $query = sql_placeholder("SELECT b.id, b.name, b.blocks_blog AS blocks, b.header, b.url, b.block_color, b.type_post, bw.name AS writer, bw.id AS writer_id, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS post_created FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN relations_post_tags AS rel ON rel.post_id=b.id
                          WHERE
                          b.enabled=1
                          AND b.blocks_blog = ?
                          AND rel.tag_id=?
                          AND (bw.enabled = 1 OR b.spec_project)
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                                $block,
                                $rubrika->id,
                                $start,
                                $configBlock['settings']['limit']);
                        }
                        $this->db->query($query);
                        $result = $this->db->results();

                        foreach ($result AS $k => $v) {

                            if ($v->type_post == 2) {
                                $body = $v->body;
                                $body = str_replace("<p>", "", $body);
                                $body = str_replace("</p>", "", $body);
                                $v->body = $body;
                            }


                            $query = "SELECT t.name, t.url FROM relations_post_tags AS rel
                              INNER JOIN blogtags AS t ON t.id=rel.tag_id
                              WHERE rel.post_id=" . $v->id . " AND t.url <> 'main' ORDER BY t.name";
                            $this->db->query($query);
                            $result[$k]->tags = $this->db->results();

                            $result[$k]->postTags = $this->getPostTagTwoLevel($v->id);

                            $result[$k]->postDateStr = Helper::dateTimeWord($v->post_created);
                        }

                        $this->postsMatrix->$key->$block = $result;
                        // наращиваем стартовую позицию в следующем запросе постов в такой же блок
                        $usedBlocks[$block]['limit'] = $usedBlocks[$block]['limit'] + $configBlock['settings']['limit'];
                    }
                }
            }
            $this->smarty->assign('postsMatrix', $this->postsMatrix);
            $this->smarty->assign('totalPages', $totalPages);
            $this->smarty->assign('currentPage', $page + 1);

            $this->smarty->assign('usedBlocks', serialize($usedBlocks));
            $this->body = $this->smarty->fetch('blogs.tpl');

            return $this->body;
        }
        return false;
    }

    /**
     * @return int|object|stdClass
     */
    function getConferenceLink()
    {
        $query = "SELECT external_url FROM conferences WHERE enabled = 1 LIMIT 1";
        $this->db->query($query);
        return $this->db->result();
    }


    /**
     * метатеги и метатайтл для страницы
     *
     * Главная: +1 — Главная
     * Рубрики — по тому же шаблону. Вместо "Главная" название рубрики
     * Внутренние страницы: в соответствии с мета этих страниц (это кстати не работает сейчас)
     * Блоги авторов: Блог %автор блога% — +1
     * @param string $page
     * @param string $type
     * @return array
     */
    function getMetaData($page = 'main', $type = '')
    {
        if ($type == 'author'){
            $query = "SELECT w.*, t.name_lead, bt.url, w.image AS writerImage FROM blogwriters AS w
                                  LEFT JOIN post_tags AS t ON t.id=w.leader
                                  LEFT JOIN blogtags AS bt ON bt.id=t.parent
                                  WHERE w.id=" . $page . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            if ($writer->private == 1){
                $writerName = "Колонка " . $writer->name_genitive;
            }
            elseif ($writer->bloger == 1){
                $writerName = "Блог " . $writer->name_genitive;
            }
            else{
                $writerName = $writer->name;
            }

            $title = $writer->seo_title ? "+1 — " . $writer->seo_title : "+1 — " . $writerName;
            $description = $writer->seo_description ? $writer->seo_description : "";
            $keywords = $writer->seo_keywords ? $writer->seo_keywords : "";


            $result = array(
                'metaTitle' => $title,
                'metaDescription' => $description,
                'metaKeyword' => $keywords,
                'writer' => $writer
            );
        }
        elseif($type == 'posttags'){
            $query = sql_placeholder("SELECT * FROM seo_content WHERE section = 'posttags'");
            $this->db->query($query);
            $seoContent = $this->db->result();

            $title = $seoContent->seo_title ? "+1 — " . $seoContent->seo_title : "+1 — Теги направления " . $page;
            $description = $seoContent->seo_description ? $seoContent->seo_description : "";
            $keywords = $seoContent->seo_keywords ? $seoContent->seo_keywords : "";

            $result = array(
                'metaTitle' => $title,
                'metaDescription' => $description,
                'metaKeyword' => $keywords,
                'writer' => ""
            );
        }
        elseif ($type == 'authors'){

            $query = sql_placeholder("SELECT * FROM seo_content WHERE section = 'writers'");
            $this->db->query($query);
            $seoContent = $this->db->result();

            $title = $seoContent->seo_title ? "+1 — " . $seoContent->seo_title : "+1 — Все участники блогов";
            $description = $seoContent->seo_description ? $seoContent->seo_description : "";
            $keywords = $seoContent->seo_keywords ? $seoContent->seo_keywords : "";


            $result = array(
                'metaTitle' => $title,
                'metaDescription' => $description,
                'metaKeyword' => $keywords,
                'writer' => ""
            );
        }
        else{
            switch ($page) {
                case 'main':
                case 'economy':
                case 'ecology':
                case 'society':
                    $query = sql_placeholder("SELECT * FROM blogtags WHERE url=?", $page);
                    $this->db->query($query);
                    $rubrika = $this->db->result();

                    $title = $rubrika->seo_title ? "+1 — " . $rubrika->seo_title : "+1 — " . $rubrika->name;
                    $description = $rubrika->seo_description ? $rubrika->seo_description : "";
                    $keywords = $rubrika->seo_keywords ? $rubrika->seo_keywords : "";

                    $result = array(
                        'metaTitle' => $title,
                        'metaDescription' => $description,
                        'metaKeyword' => $keywords,
                        'writer' => ""
                    );
                    break;
                case "about":
                    $result = array(
                        'metaTitle' => "+1 — О проекте",
                        'metaDescription' => "",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
                case "events":
                case "event":
                    $result = array(
                        'metaTitle' => "+1 — Мероприятия",
                        'metaDescription' => "",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
                default:
                    $result = array(
                        'metaTitle' => "+1 — Главная",
                        'metaDescription' => "",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
            }
        }
        return $result;
    }

    function getHeaderMenu($currentRubrika = '', $currentType = ''){

        $query = sql_placeholder("SELECT bt.id, bt.url, bt.`name` FROM blogtags as bt WHERE bt.isshow = 1 AND bt.enabled = 1");
        $this->db->query($query);
        $tags = $this->db->results();

        $nowDate = new DateTime();

        $return = array();
        foreach ($tags AS $key => $tag){
            if (!isset($return[$tag->id])) {
                $return[$tag->id] = new stdClass();
            }

            $return[$tag->id]->url = $tag->url;

            $query = sql_placeholder("
                    SELECT COUNT( bp.id) AS cnt 
                      FROM blogposts as bp 
                INNER JOIN blogwriters as bw ON bw.id = bp.writers 
                     WHERE bw.enabled = 1 AND bp.enabled = 1 AND bp.tags = ? AND bp.created <= ?
                       AND (bw.enabled = 1 OR b.spec_project)
                ", $tag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $return[$tag->id]->posts = $this->db->result();
            $return[$tag->id]->posts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $return[$tag->id]->posts->cnt);


            $query = sql_placeholder("
          SELECT COUNT(pt.id) AS cnt 
            FROM post_tags as pt
           WHERE pt.enabled = 1 AND pt.parent=? AND EXISTS(
                    SELECT null 
                      FROM blogposts as bp 
                INNER JOIN relations_postitem_tags as rpt ON rpt.post_id = bp.id
                INNER JOIN blogwriters as bw ON bw.id = bp.writers 
                     WHERE bw.enabled = 1 AND bp.enabled = 1 AND  bp.tags = pt.parent  AND rpt.posttag_id = pt.id
                       AND bp.created <= ?
                 )", $tag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $return[$tag->id]->postTags = $this->db->result();
            $return[$tag->id]->postTags->text = $this->getHeaderMenuSigns('тегов', 'тег', 'тега', $return[$tag->id]->postTags->cnt);

//            лидеры
            $query = sql_placeholder("SELECT pt.name_lead, bw.name, bw.id FROM post_tags AS pt
                                    INNER JOIN blogwriters AS bw ON bw.leader = pt.id
                                  WHERE pt.enabled = 1 AND bw.enabled = 1 AND pt.enabled = 1 AND pt.parent = ? AND EXISTS (
                                            SELECT null 
                                              FROM blogposts as bp 
                                             WHERE bp.enabled = 1 AND bw.id = bp.writers  AND bp.created <= ?
                                             )
                ", $tag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $authors = $this->db->results();

            $return[$tag->id]->leaders = new stdClass();
            $return[$tag->id]->leaders->cnt = count($authors);
            $return[$tag->id]->leaders->text = $this->getHeaderMenuSigns('лидеров', 'лидер', 'лидера', $return[$tag->id]->leaders->cnt);

        }


        // пдатформа

        $return['platforma']->url = 'platforma';

        $query = sql_placeholder("SELECT COUNT(bp.id) AS cnt FROM blogposts AS bp INNER JOIN blogwriters AS w ON w.id=bp.writers WHERE w.useplatforma = 1");
        $this->db->query($query);
        $postCount = $this->db->result();

        $return['platforma']->posts->cnt = $postCount->cnt;
        $return['platforma']->posts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $postCount->cnt);

        $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM blogwriters WHERE useplatforma = 1");
        $this->db->query($query);
        $writerCount = $this->db->result();

        $return['platforma']->leaders->cnt = $writerCount->cnt;
        $return['platforma']->leaders->text = $this->getHeaderMenuSigns('участников', 'участник', 'участника', $writerCount->cnt);

        $result = array(
            'menu' => $return,
            'currentRubrika' => $currentRubrika,
            'currentType' => $currentType,
        );
        return $result;
    }

    function getHeaderMenuSigns($ptext = '', $ptext1 = '', $ptext2 = '', $num = 0){
        $p1 = $num%10;
        $p2 = $num%100;

        if ($p1==1 && !($p2>=11 && $p2<=19)){
            $ptext = $ptext1;
        }

        if ($p1>=2 && $p1<=4 && !($p2>=11 && $p2<=19)){
            $ptext = $ptext2;
        }

        return $ptext;
    }


    /**
     * Вывод лидеров по тегу первог оуровня
     *
     * @param string $rubrikaUrl
     * @return array|int
     * @throws Exception
     */
    function getLeaders($rubrikaUrl = '')
    {
        $nowDate = new \DateTime();
        if ($rubrikaUrl == 'platform'){
            $query = sql_placeholder("SELECT pt.name_lead, bw.name, bw.id, count(bp.id) AS total
              FROM post_tags AS pt 
              RIGHT JOIN blogwriters AS bw ON bw.leader = pt.id 
              JOIN blogposts AS bp ON bp.writers = bw.id
              WHERE bw.useplatforma = 1 AND bw.private = 0 AND bw.bloger = 0
              GROUP BY bw.id
              ORDER BY total DESC");
            $this->db->query($query);
            $authors = $this->db->results();
        }
        else{
            $query = sql_placeholder("SELECT id, color, url FROM blogtags WHERE url = ?", $rubrikaUrl);
            $this->db->query($query);
            $blogTag = $this->db->result();

            $query = sql_placeholder("SELECT pt.name_lead, bw.name, bw.id, count(bp.id) AS total FROM post_tags AS pt
              INNER JOIN blogwriters AS bw ON bw.leader = pt.id
              JOIN blogposts AS bp ON bp.writers = bw.id
              WHERE pt.parent = ? AND bw.private = 0 AND bw.bloger = 0 AND bp.created <= ?
                AND (bw.enabled = 1 OR b.spec_project)
              GROUP BY bw.id
              ORDER BY total DESC", $blogTag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $authors = $this->db->results();
        }

        foreach ($authors AS $k=>$author){
            $authors[$k]->lenAuthor = strlen($author->name);
            $authors[$k]->tagurl = $blogTag->url;
            $authors[$k]->blogTagColor = $blogTag->color;
            $authors[$k]->countPost = $author->total;
            $authors[$k]->countPostText = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $author->total);
        }

        $this->title = "Лидеры направления " . $this->tags[$rubrikaUrl] . " / +1";
        $this->description = "";
        $this->keywords = "";

        if ($rubrikaUrl == 'platform'){
            $header = "Участники";
        }
        else{
            $header = "Лидеры";
        }

        $this->smarty->assign('authors', $authors);
        $this->smarty->assign('rubrikaUrl', $rubrikaUrl);
        $this->smarty->assign('header', $header);


        $this->body = $this->smarty->fetch('allauthors.tpl');
        return $this->body;
    }

    function getPostTags($tag){
        $query = sql_placeholder("SELECT pt.id, pt.url, pt.name, pt.name_lead, COUNT(rel.id) AS total FROM post_tags AS pt
            INNER JOIN blogtags AS bt ON bt.id = pt.parent
            JOIN relations_postitem_tags AS rel ON rel.posttag_id = pt.id
            WHERE pt.enabled = 1 AND bt.url = ?
            GROUP BY pt.id ORDER BY total DESC", $tag);
        $this->db->query($query);
        $postTags = $this->db->results();

        $postTags->tag = $tag;

        foreach ($postTags AS $k => $res){
            $postTags[$k]->countPosts->count = $res->total;
            $postTags[$k]->countPosts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $res->total);
        }

        $metaData = $this->getMetaData($this->tags[$tag], 'posttags');

        // метатеги
        $this->title = $metaData['metaTitle'];
        $this->description = $metaData['metaDescription'];
        $this->keywords = $metaData['metaKeyword'];

        $this->smarty->assign('tags', $postTags);
        $this->smarty->assign('tagUrl', $tag);

        $this->body = $this->smarty->fetch('posttags.tpl');
        return $this->body;

    }

    function countPostsByTag($tag){

        $query = sql_placeholder("SELECT COUNT(bp.id) AS cnt, pt.name FROM blogposts AS bp
                                    LEFT JOIN blogwriters as bw ON bw.id = bp.writers
                                    INNER JOIN relations_postitem_tags AS rel_pt ON rel_pt.post_id=bp.id
                                    INNER JOIN post_tags AS pt ON pt.id=rel_pt.posttag_id
                                  WHERE pt.url=? AND (bw.enabled = 1 OR bp.spec_project)", $tag);
        $this->db->query($query);
        $postTagsHeader = $this->db->result();

        $postTagsHeader->text = $this->getHeaderMenuSigns('статей', 'статья', 'статьи', $postTagsHeader->cnt);

        $return = array(
            'tagHeader' => $postTagsHeader,
            'url' => $tag
        );
        return $return;
    }

    function getAllAuthors($page = 0, $usedIdsPosts = array(), $useElasticBlock = 0){

        $query = sql_placeholder("SELECT w.*, t.name_lead AS lead, bt.color AS blog_tags_color, bt.url AS tagurl FROM blogwriters AS w
                                    LEFT JOIN post_tags AS t ON t.id = w.leader
                                    LEFT JOIN blogtags AS bt ON bt.id=t.parent
                                    WHERE w.enabled = 1
                                    ORDER BY w.name ASC");
        $this->db->query($query);
        $authors = $this->db->results();

        foreach ($authors AS $k=>$author){
            $authors[$k]->lenAuthor = strlen($author->name);
        }

        $this->smarty->assign('authors', $authors);

        $metaData = $this->getMetaData('main', 'authors');

        $this->title = $metaData['metaTitle'];
        $this->description = $metaData['metaDescription'];
        $this->keywords = $metaData['metaKeyword'];


        $this->body = $this->smarty->fetch('autorslist.tpl');
        return $this->body;
    }

    function getPictureDay($rubrikaUrl = 'main')
    {
        $pictureDay = array();

        if ($rubrikaUrl == 'main'){
            $query = sql_placeholder("SELECT name, header, url, tags FROM blogposts AS b
                          WHERE
                          b.enabled=1
                          AND b.picture_day = 1
                          AND b.show_main_page=1
                          ORDER BY b.created DESC
                          LIMIT 9");
            $this->db->query($query);
            $pictureDay = $this->db->results();

            foreach ($pictureDay AS $k => $pd){
                $query = "SELECT name, url FROM blogtags WHERE id=" . $pd->tags;
                $this->db->query($query);
                $pictureDay[$k]->tags = $this->db->result();
            }
        }

        return $pictureDay;
    }

    /**
     * получение постов по рубрикам или для главной страницы
     *
     * @param string $rubrika_url
     * @param int $page
     * @param array $usedIdsPosts
     * @return array|bool
     */
    function getPosts($rubrika_url = '', $page = 1, $usedIdsPosts = array(), $ajax = true, $getsimpleblogs = 0, $confirmSubscribe = 0)
    {

        $andWhereUsefulCity = "";
        if ($_GET['mode'] == 'getsimpleblogs'){
            $andWhereUsefulCity = " AND useful_city = 1";
        }

        $query = sql_placeholder("SELECT COUNT(b.id) AS count FROM blogposts AS b WHERE b.enabled=1" . $andWhereUsefulCity);
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);

        if ($page >= $totalPages) {
            return null;
        }

        if ($rubrika_url != 'platform'){
            $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled=1 AND url=?", $rubrika_url);
            $this->db->query($query);

            if ($this->db->num_rows() == 0) {
                return false;
            }
            $rubrika = $this->db->result();
        }

        if ($rubrika_url != "") {
            $start = ($page - 1) * $this->itemPerPage;

            $result = $this->getListPosts($rubrika_url, $rubrika->id, $start);

            $result = $this->formatPostList($result, '1_3');

            $page = intval($page + 1);

            if ($ajax){

                $return = array(
                    'items' => $result,
                    'page' => $page,
                );
                return $return;
            }
            else{

                if ($rubrika_url == 'platform'){
                    $header = "Платформа";
                }
                else{
                    $header = $rubrika->name;
                }

                // метатеги
                $meta = $this->getMetaData($rubrika_url);
                if ($getsimpleblogs){
                    $this->title = "+1 — Полезный город";
                }
                else{
                    $this->title = $meta['metaTitle'];
                }
                $this->description = $meta['metaDescription'];
                $this->keywords = $meta['metaKeyword'];
                $this->smarty->assign('items', $result);
                $this->smarty->assign('header', $header);
                $this->smarty->assign('rubrika', $rubrika_url);
                $this->smarty->assign('page', $page);
                $this->smarty->assign('subscribe', $confirmSubscribe);
                $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
                return $this->body;
            }
        }
        return false;
    }




    /**
     * метод возвращает теги второго уровня для статьи,
     *
     * Помимо самого тега второг оуровня - метод так же вернет
     * и название тега перовго уровня, к которому он привязан, и его урл.
     *
     * @param int $postId - Id поста - к которому надо вернуть список тегов второг оуровня
     * @return array|int|null
     */
    function getPostTagTwoLevel($postId){
        $postTagTwoLevel = null;
        if ($postId){
            $query = "SELECT t.name, t.url, bt.name AS blogtag_name, bt.url AS blogtag_url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              INNER JOIN blogtags AS bt ON bt.id=t.parent
                              WHERE rel.post_id=" .$postId . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $postTagTwoLevel = $this->db->results();
        }

        return $postTagTwoLevel;
    }

    function getPostTagMainPage($tagId){
        $postTagMainPage = null;
        if ($tagId){
//            $query = "SELECT name, url FROM post_tags WHERE id=" . $tagId;

            $query = "SELECT t.name, t.url, bt.name AS blogtag_name, bt.url AS blogtag_url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              INNER JOIN blogtags AS bt ON bt.id=t.parent
                              WHERE t.id=" .$tagId ;

            $this->db->query($query);
            $postTagMainPage = $this->db->result();
        }

        return $postTagMainPage;
    }

    /**
     * метод формирует данные для отображения в карточкас с типом "данные"
     *
     * @param int $postId Id поста с данными, для которого необходимо вернуть эти самые данные
     * @param int $writerId Id писателя, "автора" поста, по которому формируются данные
     * @return array|int
     */
    function getAnalitycData($postId, $writerId){

        $query = sql_placeholder("SELECT years, value_to_yeats FROM blogwriters_data WHERE post_id=? AND writer_id=? ORDER BY years ASC LIMIT 7",
            $postId,
            $writerId);
        $this->db->query($query);
        $analitycData = $this->db->results();

        if (!empty($analitycData)){
            $query = sql_placeholder("SELECT year_to_ammount AS years, amount_to_account AS value_to_yeats FROM blogposts WHERE id=? AND writers=? LIMIT 1",
                $postId,
                $writerId);
            $this->db->query($query);
            $analitycDataCurrent = $this->db->result();

            // нам нужны еще и данные по текущей записи (текущеиму году)
            if (intval($analitycDataCurrent->years) > 0){
                array_push($analitycData, $analitycDataCurrent);
            }

            $query = sql_placeholder("SELECT bp.amount_to_account, MAX(wd.value_to_yeats) AS max_value FROM blogwriters_data AS wd
                                    INNER JOIN blogposts AS bp ON bp.id=wd.post_id
                                    WHERE wd.post_id=? AND wd.writer_id=?",
                $postId,
                $writerId);

            $this->db->query($query);
            $maxValue = $this->db->result();

            if ($maxValue->max_value > $maxValue->amount_to_account){
                $maxValue = $maxValue->max_value;
            }
            elseif($maxValue->max_value < $maxValue->amount_to_account){
                $maxValue = $maxValue->amount_to_account;
            }
            else{
                $maxValue = $maxValue->amount_to_account;
            }

            foreach ($analitycData AS $keyAnalitic=>$valueAnalitic){
                if ($maxValue == $valueAnalitic->value_to_yeats){
                    $analitycData[$keyAnalitic]->percent = 100;
                }
                else{
                    $p = $valueAnalitic->value_to_yeats * 100 / $maxValue;
                    $analitycData[$keyAnalitic]->percent = round($p);
                }
            }
        }



        return $analitycData;
    }

    /**
     * возвращает набор данных для RSS канала
     *
     * @return array|int
     */
    function getRssFeed(){

        $query = sql_placeholder("SELECT b.id, b.header_rss, b.header, b.url, b.image_rss, t.name AS tag, t.url AS tag_url, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i') AS post_created 
                                FROM blogposts AS b
                          INNER JOIN blogtags AS t on t.id=b.tags
                           LEFT JOIN blogwriters AS bw on bw.id=b.writers
                          WHERE b.enabled = 1
                          AND b.url != ''
                          AND b.header_rss != ''
                          AND (bw.enabled = 1 OR b.spec_project)
                          ORDER BY b.created DESC");
        $this->db->query($query);
        $result = $this->db->results();

        return $result;
    }

    function getAboutPage(){
        $query = sql_placeholder("SELECT * FROM about LIMIT 1");
        $this->db->query($query);
        $about = $this->db->result();


        $this->title = "+1 — О проекте";
        $this->smarty->assign('about', $about);

        $this->body = $this->smarty->fetch('about.tpl');
        return $this->body;
    }

    function getEvents(){
        $query = sql_placeholder("SELECT * FROM events WHERE enabled = 1 ORDER BY created DESC");
        $this->db->query($query);
        $result = $this->db->results();

        foreach ($result AS $k => $event){
            $query = sql_placeholder("SELECT * FROM blogtags WHERE id = ?", $event->tag);
            $this->db->query($query);
            $tag = $this->db->result();

            if ($tag->url == 'economy'){
                $tag->blockColor = 'blue';
            }

            if ($tag->url == 'ecology'){
                $tag->blockColor = 'green';
            }

            if ($tag->url == 'community'){
                $tag->blockColor = 'yellow';
            }

            $result[$k]->tags = $tag;
        }

        return $result;
    }

    function getEvent($url){
        $query = sql_placeholder("SELECT * FROM events WHERE url = ?", $url);
        $this->db->query($query);
        $result = $this->db->result();

        $query = sql_placeholder("SELECT * FROM blogtags WHERE id = ?", $result->tag);
        $this->db->query($query);
        $tag = $this->db->result();

        if ($tag->url == 'economy') {
            $tag->blockColor = 'blue';
        }

        if ($tag->url == 'ecology') {
            $tag->blockColor = 'green';
        }

        if ($tag->url == 'community') {
            $tag->blockColor = 'yellow';
        }

        $result->tags = $tag;


        return $result;
    }

    function translitIt($str)
    {
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ё" => "E", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "-", "." => "", "/" => "-", "»" => "", "«" => "",
            " - " => "-", " — " => "-", "&quot;" => "", "!" => "",
            "”" => "", "“" => "", "," => "", "(" => "", ")" => "",
            "°" => "", "\"" => "", "№" => "", "*" => "-"
        );
        return strtr($str, $tr);
    }

    function getVideoFragments($videoId = 0){

        $result = new stdClass();

        if ($videoId != 0){
            $query = sql_placeholder("SELECT * FROM division_time_videos WHERE
                parent_video=?
                AND enabled=1
                AND name != ''
              ORDER BY minute, secunde ASC", $videoId);
            $this->db->query($query);
            //echo $query;die();
            $videoFragments = $this->db->results();

            foreach ($videoFragments AS $key => $fragment){

                $min = $fragment->minute;
                $sec = $fragment->secunde;

                if (strlen($min) < 2){
                    $min = '0' . $min;
                }

                if (strlen($sec) < 2){
                    $sec = '0' . $sec;
                }

                $videoFragments[$key]->name = $min . ":" . $sec . ": " . $fragment->name;
            }

            $query = sql_placeholder("SELECT * FROM videos WHERE id=?", $videoId);
            $this->db->query($query);
            $video = $this->db->result();

            $result->video = $video;
            $result->videoFragments = $videoFragments;
        }
        return $result;
    }

    function test(){
        $query = sql_placeholder("SELECT b.id, b.body FROM blogposts AS b
                          WHERE b.body != ''
                          ORDER BY b.created DESC");
        $this->db->query($query);
        $result = $this->db->results();

        return $result;
    }

    function saveTr($body, $id){
        if ($body && $id){
            $query = sql_placeholder('UPDATE blogposts SET body=? WHERE id=?', $body, $id);
            $this->db->query($query);
        }
    }


    function getListPosts($rubrika_url, $rubrikaId, $start){

        $andWhereUsefulCity = "";
        if ($_GET['mode'] == 'getsimpleblogs'){
            $andWhereUsefulCity = " AND useful_city = 1";
        }
        $nowDate = new \DateTime();

        if ($rubrika_url == 'main') {
            $query = sql_placeholder("SELECT b.id, b.blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                            sp.font_color AS specProjectFontColor,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,   
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created, 
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url 
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3        
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE 
                          b.enabled=1
                          AND (bw.enabled = 1 OR b.spec_project) 
                          AND b.blocks != 'ticker' 
                          AND b.type_post != 11
                          AND b.created <= ? 
                          " . $andWhereUsefulCity . "
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                $nowDate->format('Y-m-d H:i:s'),
                $start,
                $this->itemPerPage);
        }
        else {
            $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                            sp.font_color AS specProjectFontColor,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,   
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName 
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url 
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3     
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE 
                          b.enabled=1
                          AND b.blocks != 'ticker' 
                          AND b.type_post != 11
                          AND (bw.enabled = 1 OR b.spec_project) 
                          AND tags=?
                          AND b.created <= ? 
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                $rubrikaId,
                $nowDate->format('Y-m-d H:i:s'),
                $start,
                $this->itemPerPage);
        }


        if (!empty($query)){
            $this->db->query($query);
            $result = $this->db->results();
        }
        else{
            $result = null;
        }
        return $result;
    }

    function htmlReplacer($str)
    {

        $str = trim ($str);
        $tr = array(
            "<p>" => "", "</p>" => "",
            "&laquo;" => "","&raquo;" => "","&mdash;" => "","&ndash;" => "", "&amp;" => "", "&pound;" => "", "&trade;" => "",
            "&cent;" => "", "&yen;" => "", "&uml;" => "", "&deg;" => "", "&plusmn;" => "", "&sup1;" => "", "&sup2;" => "",
            "&sup3;" => "", "&acute;" => "", "&micro;" => "", "&para;" => "", "&middot;" => "", "&ordm;" => "",
            "&frac12;" => "", "&frac34;" => "", "&times;" => "", "&ordf;" => "", "&sect;" => "", "&curren;" => "",
            "&rsaquo;" => " ",
            "&lsaquo;" => " ",
            "&tilde;" => " ",
            "&bull;" => " ",
            "&rdquo;" => " ",
            "&ldquo;" => " ",
            "&rsquo;" => " ",
            "&lsquo;" => " ",
            "&permil;" => " ",
            "&circ;" => " ",
            "&hellip;" => " ",
            "&bdquo;" => " ",
            "&sbquo;" => " ",
            "&euro;" => " ",
            "&fnof;" => " ",
            "&macr;" => " ",
            "&cedil;" => " ",
            "&reg;" => "-",
            "&copy;" => "-",
            "&quot;" => "-",
            "&lt;" => "-",
            "&gt;" => "-",
            "&nbsp;" => "-",

        );
        return strtr($str,$tr);

    }


    function getPostListToInfinityScroll($rubrikaUrl)
    {
        $andNotInIds = "";
        if (isset($_SESSION['vievedPostIds'][$rubrikaUrl])){
            $notInIds = implode(',', $_SESSION['vievedPostIds'][$rubrikaUrl]);
            $andNotInIds = "AND b.id NOT IN ({$notInIds})";
        }

        if ($_GET['mode'] == 'preview'){
            $andEnabled = "";
        }
        else{
            $andEnabled = " AND b.enabled = 1 ";
        }

        $nowDate = new DateTime();


        // Выбираем статью из базы
        $query = sql_placeholder("SELECT b.id, b.body, b.tags, b.partner, b.partner_url, b.name, b.lead, b.header, b.url, b.block_color, b.is_partner_material,
                                   b.type_post, bw.name AS writer, bw.id AS writer_id, b.post_tag,
                                   DATE_FORMAT(b.created, '%d.%m.%Y') AS post_created,
                                   b.meta_title, b.meta_description, b.meta_keywords, p.name AS partner_name,  b.is_partner_material,
                                   p.color AS partner_color, b.signature_to_sum, b.text_body, b.amount_to_displaying,
                                   b.header_on_page, b.image_rss, b.image_1_1, b.image_1_1_c, b.image_1_2, b.image_1_3,
                                   b.spec_project, b.conference, b.body_color, b.font_color, b.border_color,
                                   bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                                   bw.image AS writerImage, bw.useplatforma AS platformPost,
                                   sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                                   conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor, b.citate_author_name,
                                   b.seo_title, b.seo_description, b.seo_keywords, b.show_fund, b.shop_article_id, b.fund_resource_id, p.name AS partnerName,  
                                   bw.bloger, bw.name as writerName, bw.id as writerId, bw.tag_url as writerTag
                                  FROM blogposts AS b
                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE 
                                    b.created <= ? 
                                    AND bt.url LIKE ? 
                                    AND b.spec_project = 0
                                    AND b.type_post NOT IN (5, 12, 11)
                                    {$andEnabled} 
                                    {$andNotInIds} 
                                  ORDER BY b.created DESC
                                  LIMIT 1", $nowDate->format('Y-m-d H:i:s'), $rubrikaUrl);
        $this->db->query($query);
        $item = $this->db->result();

        if ($item->id) {
            if (!isset($_SESSION['vievedPostIds'][$rubrikaUrl])) {
                $_SESSION['vievedPostIds'][$rubrikaUrl] = [];
            }
            $_SESSION['vievedPostIds'][$rubrikaUrl][$item->id] = $item->id;
            session_write_close();
        }

        // замена подписи автора в старых постах
        $pattern = '#<span class="author">(.+?)</span>#is';
        preg_match($pattern, $item->body, $arr, PREG_OFFSET_CAPTURE);
        $newAuthorNameStr = str_ireplace("Автор:", "", $arr[0][0]);
        $newAuthorNameStr = strip_tags($newAuthorNameStr);
        $newHtmlAuthorSign = '<div class="articleAuthor"><p class="articleAuthor_h">Автор</p><p class="articleAuthor_name">' . $newAuthorNameStr . '</p></div>';
        $item->body =  preg_replace($pattern, $newHtmlAuthorSign, $item->body);

        $this->bodyColor = $item->body_color;

        $this->customColors = false;

        if ($item->font_color != "" && $item->border_color != ""){
            $this->customColors = true;
        }


        $item->body = str_replace("<p>&nbsp;</p>", "", $item->body);

        $query = "SELECT name, url, color FROM blogtags WHERE id=" . $item->tags;
        $this->db->query($query);
        $item->tags = $this->db->result();

        $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
        $this->db->query($query);
        $item->postTags = $this->db->results();

        $postTagMainPage = $this->getPostTagMainPage($item->post_tag);
        $item->postTagMainPage = $postTagMainPage;

        $item->postDateStr = $item->post_created; //$this->dateTimeWord($item->post_created);

        if ($item->type_post == 7){
            $analitycData = $this->getAnalitycData($item->id, $item->writer_id);
            $item->analitycData = $analitycData;

            $item->postTags = $this->getPostTagTwoLevel($item->id);
        }

        $name = $item->name;
        $name = str_replace("<p>", "", $name);
        $name = str_replace("</p>", "", $name);
        $item->name = $name;

        $citateName = str_replace("<p>", "", $item->name);
        $citateName = str_replace("</p>", "", $citateName);

        $citateHeader = str_replace("<p>", "", $item->header);
        $citateHeader = str_replace("</p>", "", $citateHeader);

        $item->citateText = $citateName . " <span>" . $citateHeader . "</span>";


        $item->typePost = $this->typeVisualBlock[$item->type_post];



        // Устанавливаем метатеги для страницы с этой записью
        $this->title = "";

        if ($item->typePost == 'factday'){
            $this->title = htmlspecialchars(strip_tags(trim($item->name))) . " / +1";
        }
        else{
            if ($item->seo_title !=""){
                $this->title = htmlspecialchars(strip_tags(trim($item->seo_title))) . " / +1";
            }
            else{
                $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";
            }
        }

        $this->description = "";
        if ($item->seo_description != ""){
            $this->description = strip_tags($item->seo_description);
        }
        else{
            if (!empty($item->lead)){
                $this->description = strip_tags($item->lead);
            }
            elseif(!empty($item->header)){
                $this->description = strip_tags($item->header);
            }
        }


        // вычисляем примерное время на прочтение поста
        $item->typePost = $this->typeVisualBlock[$item->type_post];
        if ($this->typeVisualBlock[$item->type_post] == 'imageday' || $this->typeVisualBlock[$item->type_post] == 'diypost'){
            $totalReadTime = 3; // #PLUS1-186 для этого типа поста время на чтение всегда 3 минуты
        }
        else{
            $lenghtLead = iconv_strlen(strip_tags($item->lead));
            $lenghtBody = iconv_strlen(strip_tags($item->body));
            $totalLenght = intval($lenghtBody + $lenghtLead);
            $totalReadTime = round(($totalLenght / 1000) / 2);

            if ($totalReadTime == 0){
                $totalReadTime = 1; // #PLUS1-257 Выводить 1 минуту там где получается 0 минут на чтение
            }
        }

        $totalReadTimeCaption = $this->getHeaderMenuSigns('минут', 'минута', 'минуты', $totalReadTime);

        $item->dateStr = $item->post_created;

        $item->fullUrl = $rubrikaUrl . "/" . $item->url;

        // Передаем в шаблон
        $this->smarty->assign('blog', $item);

        $this->smarty->assign('titleToFavorite', $this->title);
        $this->smarty->assign('urlToFavorite', $this->root_url . "/" . $this->ogUrl);
        $this->smarty->assign('totalReadTime', $totalReadTime);
        $this->smarty->assign('totalReadTimeCaption', $totalReadTimeCaption);


        $this->smarty->assign('issubscribe', $this->isSubscribe);

        $this->smarty->assign('bodyColor', $this->bodyColor);
        $this->smarty->assign('customColors', $this->customColors);

        $this->smarty->assign('hasCookieSbscr', @$_COOKIE['sbscr']);


        $this->body = $this->smarty->fetch('blog.tpl');

        $result = array(
            'id' => $item->id,
            'url' => $rubrikaUrl . "/" . $item->url,
            'body' => $this->body,
            'session' => $_SESSION['vievedPostIds']
        );

        header("Content-type: application/json; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        echo json_encode($result);
        exit();
    }
}
